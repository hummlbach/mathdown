---
title: "Lösungen zum Blatt Exponentialgleichungen"
author: [Klasse 10]
date: 22.02.2022
keywords: [Logarithmen]
lang: de
---

# I Starters
1. 
   \Begin{multicols}{4}
   a) $x = 4$
   b) $x = 2$
   c) $x = 0,5$
   d) $x = \log_5 8$
   e) $x = 0,8$
   f) $x = \frac{2}{3}$
   g) $x = \frac{1}{2}$
   h) $x = \log_2{10} -2$

   \End{multicols}

2. 
   \Begin{multicols}{4}
   a) $x = 4$
   b) $x = 10$
   c) $x = \frac{4}{3}$

   \End{multicols}

# II Hauptgang
1. 
   \Begin{multicols}{3}
   a) $x = 3$
   f) $x = 3$
   d) $x = 0$

   \End{multicols}
   d) $$ \begin{array}{lll}
      10^{x} &= \sqrt[3]{0,001} \quad &| \quad \log_{10}{(\cdot)} \\
          x  &= \log_{10}{\left(10^{-3}\right)^\frac{1}{3}} = \log_{10} 10^{-1} = -1 &
      \end{array} $$
   e) $$ \begin{array}{lll}
      2^{x} &= 4\sqrt{2} \quad &| \quad \log_{2}{(\cdot)} \\
          x  &= \log_{2}{4\sqrt{2}} = \log_{2}{\sqrt{2}^5} = \log_{2}{2^\frac{5}{2}} = 2,5&
      \end{array} $$
   f) $$ \begin{array}{lll}
      7^{x-2} -\sqrt{7} &= 0 \quad &| \quad +\sqrt{7} \\
      7^{x-2} &= \sqrt{7}    \quad &| \quad \log_{7}{(\cdot)} \\
           x -2 &= \log_{7}{\sqrt{7}} = \frac{1}{2} \quad &| \quad +2 \\
           x &= 2,5 & \\
      \end{array} $$


2. 
   a)  $$ \begin{array}{lll}
      2^{x+2} &= 5 \quad &| \quad \log_{2}{(\cdot)} \\
          x+2 &= \log_{2}{5} \quad &| \quad -2 \\
          x   &= \log_{2}{5}-2 \approx  0,32 &
      \end{array} $$
   b)  $$ \begin{array}{lll}
      3^{4x} &= 5 \quad &| \quad \log_{3}{(\cdot)} \\
          4x &= \log_{3}{5} \quad &| \quad :4 \\
          x   &= \frac{\log_{3}{5}}{4} \approx 0,37 &
      \end{array} $$
   c)  $$ \begin{array}{lll}
      4^{2x+1} &= 5 \quad &| \quad \log_{4}{(\cdot)} \\
          2x+1 &= \log_{4}{5} \quad &| \quad -1 \\
          2x &= \log_{4}{5}-1 \quad &| \quad :2 \\
          x   &= \frac{\log_{4}{5}-1}{2} \approx 0,08 &
      \end{array} $$

3. Die Formel zur Zinseszinsberechnung lautet $K_n = K_0\cdot \left(1+\frac{p}{100}\right)^n$. Hier ist $p=3,5$.
   a) Wenn sich das Guthaben verdoppeln soll, dann ist $K_n=10000$. Wir setzen alles in die Zinseszinsformel ein und nur $n$ bleibt "unbekannt":
      $$ \begin{array}{lll}
      10000 &= 5000 \cdot \left(1+\frac{3,5}{100}\right)^n \quad &| \quad :5000 \\
      2 &= \left(1+\frac{3,5}{100}\right)^n = 1,035^n \quad &| \quad  \log_{1,035}(\cdot) \\
      n &= \log_{1,035}2 \approx 20,15 &
      \end{array} $$
   b) Verdreifachen bedeutet $K_n=15000$:
      $$ \begin{array}{lll}
      15000 &= 5000 \cdot \left(1+\frac{3,5}{100}\right)^n \quad &| \quad :5000 \\
      3 &= \left(1+\frac{3,5}{100}\right)^n = 1,035^n \quad &| \quad  \log_{1,035}(\cdot) \\
      n &= \log_{1,035}3 \approx  31,94&
      \end{array} $$
   c) Analog... $n \approx 40,3$

4. Wenn sich die Fläche pro Stunde um $9\%$ verkleinert, können wir die Anfangsfläche mit $0,09$ multiplizieren, um die bedeckte Fläche nach der ersten Stunde zu erhalten. Diese Fläche müssen wir wiederum mit $0,09$ multiplizieren, um die Fläche, die nach $2$ Stunden noch von Bakterien bedeckt ist, zu erhalten; usw. Die Größe der Bakterienkultur nach $n$ Stunden erhalten wir also durch:
   $$ A_n = 32\cdot 0,09^n$$
   a) $A_n = 16$ (halbierte Fläche):
      $$ \begin{array}{lll}
      16 &= 32 \cdot 0,09^n \quad &| \quad :32 \\
      \frac{1}{2} &= 0,09^n \quad &| \quad  \log_{0,09}(\cdot) \\
      n &= \log_{0,09}\frac{1}{2} \approx  0,29&
      \end{array} $$
   b) Analog... $n \approx 1,91$

