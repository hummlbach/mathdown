---
title: "2. Klassenarbeit zu Potenzen und co"
author: "Name:"
date: 24.2.2022
keywords: [Potenz, Basis, Exponent, Logarithmus, Zinseszins]
lang: de
footer-center: 20 Punkte
fontsize: 16pt
---

# I Teil mit Taschenrechner

1. Lösen Sie die Exponentialgleichungen: \hfill (**5P**)
   a) $10^x = 50$
   b) $2\cdot 4^{x-1} = 10$
   c) $7\cdot 2^{-x-2} - 13 = 15$

2. Das Haus von Robert hat heute einen Marktwert von $300000$ Euro. Über die letzten $10$ Jahre hat sich im Schnitt eine Wertsteigerung um $3\%$ pro Jahr ergeben. Angenommen die Wertsteigerung setzt sich so fort... \hfill (**5P**)
   a) Wieviel ist Roberts Haus in $5$ Jahren wert?
   b) Wieviele Jahre dauert es bist Roberts Haus $400000$ Euro wert ist?

\pagebreak

# II Taschenrechnerfreier Teil

1. Wenden Sie Potenzgesetze (sichtbar mit einem Zwischenschritt) an: \hfill (**5P**)
   a) $27^\frac{1}{3} =$
   b) $4^{-3} =$
   c) $5^7 \cdot 5^3 : 5^8 =$
   d) $\left(2^{0,5}\right)^8=$
   e) $\sqrt{9\cdot 2}\cdot \sqrt{25\cdot 2}=$
   f) $\frac{\left(2^{23}\cdot 2^{25}\right)^\frac{1}{4}}{2^{12}} =$

2. Berechnen Sie die Logarithmen: \hfill (**2P**)
   a) $\log_5{125} =$
   b) $\log_3{\frac{1}{9}} =$
   c) $\log_{16}{4} =$
   d) $\log_{27}{\frac{1}{9}} =$

3. Berechnen Sie mit Hilfe der Logarithmengesetze: \hfill (**3P**)
   a) $\log_3{2} + \log_3{13,5} =$
   b) $2\cdot\log_{10}5+\log_{10}4=$
   c) $\log_6{2}+\log_6{12}+\log_6{9}=$

