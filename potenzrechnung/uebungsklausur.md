---
title: "Übungsklausur"
author: "Name:"
date: 16.12.2021
keywords: [Quadratische Gleichungen, Mitternachtsformel, Lineare Gleichungen mit zwei Unbekannten]
lang: de
---

1. Wenden Sie die Potenzgesetze an:
   \Begin{multicols}{3}
   a) $\left(\sqrt{2}\right)^8 =$
   b) $\left(\left(\sqrt[6]{13}\right)^\frac{1}{3}\right)^{-18} =$
   c) $\left(\sqrt[3]{2}\cdot\sqrt[9]{2}\right)^{54} =$
   d) $\frac{\sqrt{9\cdot 16 \cdot 25}}{125^\frac{1}{3}} =$
   e) $\frac{\sqrt[16]{8}}{\left(\sqrt[16]{8}\right)^{-47}} =$

   \End{multicols}

2. Max leiht sich von seinem guten Freund Moritz für $24$ Monate $400$ Euro. Wieviel Geld muß Max an Moritz zurückzahlen, wenn Moritz

   a) monatlich $1\%$ Zinsen mit Zinseszins
   b) jährlich $10\%$ Zinsen mit Zinseszins
   c) monatlich $2\%$ Zinsen ohne Zinseszins

   verlangt?

3. Moritz möchte $100$ Euro des von Max geliehenen Betrages anlegen, sodass es nach $3$ Jahren $200$ Euro sind. Welcher jährliche Zinssatz wäre dazu nötig?

4. Berechnen Sie die Logarithmen:
   \Begin{multicols}{3}
   a) $\log_5{625} =$
   b) $\log_2{\frac{1}{128}} =$
   c) $\log_5{0,04} =$
   d) $\log_{12345}{1} =$
   e) $\log_7{\sqrt[3]{49}}=$
   f) $\log_4{2^{12}} =$

   \End{multicols}

5. Wenden Sie die Logarithmengesetze an:
   \Begin{multicols}{3}
   a) $\log_2{4^{1000}} =$
   b) $3\cdot\log_{10}5 + \log_{10}8 =$
   c) $\log_{15}5-\log_{15}{75} =$

   \End{multicols}

6. Extraaufgabe zur Halbwertszeit...

