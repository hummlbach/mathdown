---
title: "Wiederholung"
author: [Klasse 10]
date: 10.01.2022
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
geometry:
  - landscape
  - left=2cm
  - right=2cm
fontsize: 18pt
---

1. Nicht positive Zahlen im Exponenten
   \Begin{multicols}{3}
   a) $4^{-2} =$
   b) $5^{-3} =$
   c) $9^{-1} =$
   d) $123456789^0 =$
   d) $\left(\frac{1}{2}\right)^{-5} =$
   e) $\left(\frac{1}{3}\right)^{-4} =$
   f) $\left(\frac{4}{3}\right)^{-2} =$
   g) ${-234}^0 =$
   h) $0^{1234} =$

   \End{multicols}

1. Multiplikation von Potenzen mit gleicher Basis
   \Begin{multicols}{2}
   a) $11^{15}\cdot 11^{-13} =$
   b) $2^{32}\cdot 2^{-26} =$
   c) $3^{55}\cdot 3^{46}\cdot 3^{-97} =$
   d) $10^{3}\cdot 10^{2}\cdot 10^{4} =$

   \End{multicols}


2. Potenzieren von Potenzen
   \Begin{multicols}{3}
   a) $\left(2^{30}\right)^{0.1} =$
   b) $\left(5^{12}\right)^\frac{1}{4} =$
   c) $\left(\left(3^{0.2}\right)^{30}\right)^{\frac{1}{3}} =$

   \End{multicols}

3. Gebrochene Hochzahlen
   \Begin{multicols}{3}
   a) $9^\frac{1}{2} =$
   b) $16^\frac{1}{2} =$
   c) $27^\frac{1}{3} =$
   d) $16^\frac{1}{4} =$
   e) $27^\frac{2}{3} =$
   f) $81^\frac{3}{4} =$

   \End{multicols}

   \pagebreak

4. Dividieren von Potenzen mit gleicher Basis
   \Begin{multicols}{2}
   a) $\frac{7^{20}}{7^{18}} =$
   b) $\frac{2^{100}}{2^{95}} =$ 
   c) $\frac{\left(\frac{10^{13}}{10^{9}}\right)}{10^4} =$
   c) $\left(29999^{384}:29999^{380}\right) : 29999^4 =$
   d) $\frac{3^{20}}{3^{22}} =$
   e) $\frac{5^{-10}}{5^{-13}} =$

   \End{multicols}

5. Multiplizieren von Potenzen mit gleichen Exponenten
   \Begin{multicols}{2}
   a) $100^{150}\cdot {0,01}^{150} =$
   b) $4^{30}\cdot \frac{1}{4}^{30} =$
   c) $2^{8}\cdot 5^{8} =$
   d) $6^{8}\cdot \frac{1}{3}^{8} =$

   \End{multicols}

