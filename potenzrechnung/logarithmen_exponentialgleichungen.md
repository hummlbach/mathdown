---
title: "Exponentialgleichungen"
author: [Klasse 10]
date: 14.02.2022
keywords: [Logarithmen]
lang: de
header-center: "Potenzrechnung" 
---

# I Starters
1. Berechne $x$:
   \Begin{multicols}{2}
   a) $2^x = 16$
   b) $3^{2x} = 81$
   c) $6^{4x} = 36$
   d) $5^x = 8$
   e) $4^{5x-1} = 64$
   f) $5^{3x+1} = 125$
   g) $2^{-2x+5} = 16$
   h) $2^{x+2} = 10$

   \End{multicols}

2. Finde die Lösung folgender Gleichungen:
   \Begin{multicols}{3}
   a) $2\cdot 3^x = 162$
   b) $4+4\cdot 2^{x} = 4100$
   c) $7\cdot 8^{-x-2} = 28$

   \End{multicols}

# II Hauptgang
1. Löse die Exponentialgleichung (ohne Taschenrechner):
   \Begin{multicols}{3}
   a) $10^{x}-25 = 975$
   f) $\frac{4}{3}\cdot6^{x}+62 = 350$
   d) $\left(6^{2x+1}\right)^2 = 36$
   g) $10^{x} = \sqrt[3]{0,001}$
   h) $2^{x} = 4\sqrt{2}$
   i) $7^{x-2}-\sqrt{7} = 0$

   \End{multicols}

2. Löse die Exponentialgleichung (mit Taschenrechner):
   \Begin{multicols}{3}
   a) $2^{x+2} = 5$
   b) $3^{4x} = 5$
   c) $4^{2x+1} = 5$

   \End{multicols}

2. Ein Sparguthaben mit $5000$ Euro wird jährlich mit $3,5$% verzinst. Nach wie vielen Jahren hat es sich...
   a) verdoppelt?
   b) verdreifacht?
   c) vervierfacht?

3. Eine Bakterienkultur bedeckt eine Fläche von $32 \text{cm}^2$. In einem Experiment wird sie ultravioletter Höhenstrahlung ausgesetzt. Dadurch sterben Organismen ab, so dass sich die Fläche pro Stunde um $9$% verkleinert.
   a) Nach welcher Zeit ist die von Bakterien bedeckte Fläche auf die Hälfte geschrumpft?
   b) Wann ist nur noch ein Hundertstel der ursprünglichen Fläche belebt?

\pagebreak

# III Dessert

1. Berechnen die Basis $b$:
   \Begin{multicols}{3}
   a) $\log_b 25 = 2$
   b) $\log_b \frac{1}{49} = -2$
   c) $\log_b 16 = -4$

   \End{multicols}

2. Berechnen die Zahl $a$:
   \Begin{multicols}{3}
   a) $\log_3 a = 4$
   b) $\log_4 a = 3$
   c) $\log_9 a = 1,5$

   \End{multicols}

3. Berechne $x$:
   \Begin{multicols}{2}
   a) $\log_7 (x+4)+\log_7 (x-2) = 1$
   b) $\log_5 (6x+5)-\log_5 (2x-3) = \log_5 1$
   c) $\log_2 x + \log_2 3 = \log_2 5$
   d) $\log_5 (x+2) + 1 = log_5 12$
   
   \End{multicols}


