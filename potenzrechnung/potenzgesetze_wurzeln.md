---
title: "Wurzeln"
author: [Klasse 10]
date: 10.12.2021
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
---

# I Starters

Berechnen Sie:

\Begin{multicols}{2}

a) $2139^4\cdot 2139^{200} \cdot 2139^{3987} \cdot 2139^{-4191} =$
b) $\left(\left(\left(\left(\frac{13}{7}\right)^3\right)^{360}\right)^\frac{1}{3}\right)^\frac{2}{360} =$
c) $13^\frac{4}{13}\cdot 13^\frac{7}{13}\cdot 13^\frac{1}{26} \cdot 13^\frac{29}{26} =$
d) $\left(\frac{25\cdot 6}{26}\right)^\frac{1}{2}\cdot\left(13\cdot 12\right)^\frac{1}{2} =$ 
e) $100^9\cdot 100^{380} \cdot 100^{391147} \cdot 100^{-391536} \cdot 100 =$
f) $\left(\left(3^8\right)^4 : 3^{-29}\right)\cdot 3^{-61} =$
g) $\left(29999^{384}:29999^{380}\right) : 29999^4 =$
i) $49\cdot 49\cdot 49 \cdot 49 \cdot 49 \cdot 49 \cdot 1283947^0 \cdot 49^{-5} =$
j) $362^\frac{1}{7}:\left(362^\frac{30}{7}:362^\frac{29}{7}\right) =$

\End{multicols}

# II Hauptgang

Berechnen Sie:
\Begin{multicols}{2}

a) $\left(\sqrt{2}\right)^8 =$
b) $\left(\left(\sqrt[6]{13}\right)^\frac{1}{3}\right)^{-18} =$
c) $\left(\sqrt[3]{2}\cdot\sqrt[9]{2}\right)^{54} =$
f) $\sqrt{9\cdot 16 \cdot 25} =$
d) $\left(\left(\sqrt[8]{64}\cdot\sqrt[9]{64}\cdot\sqrt[10]{64}\right)^\frac{1}{3}\right)^\frac{1080}{121} =$
e) $\frac{\sqrt[16]{8}}{\left(\sqrt[16]{8}\right)^{-47}} =$

\End{multicols}

