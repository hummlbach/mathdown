---
title: "Logarithmen Wiederholung"
author: [Klasse 10]
date: 03.02.2022
keywords: [Logarithmen]
lang: de
header-center: "Potenzgesetze" 
---

# Starters
1. Vervollständigen Sie die Logarithmengesetze:
    a) $\log_\text{\textvisiblespace}(x)+\log_a(y) = \log_a(x\cdot \text{\textvisiblespace})$ 
    b) $\log_\text{\textvisiblespace}(x^n) = \text{\textvisiblespace}\log_a(x)$ 
    c) $\log_a(x)-\log_a(\text{\textvisiblespace}) = \log_\text{\textvisiblespace}(\frac{x}{y})$ 


2. Berechnen Sie mit Hilfe der Logarithmengesetze:
   \Begin{multicols}{3}
   a) $\log_{10}5+\log_{10}2$
   a) $\log_{6}4+\log_{6}9$
   a) $\log_{15}3+\log_{15}75$
   a) $3\log_{10}5+\log_{10}8$
   a) $2\log_{6}12+\log_{6}1,5$
   a) $2\log_{16}3-\log_{16}72$

   \End{multicols}


# Hauptgang
1. Berechnen Sie mit Hilfe der Logarithmengesetze:
   \Begin{multicols}{2}
   a) $\log_{10}8+\log_{10}5+log_{10}25$
   a) $\log_{2}10+\log_{2}\frac{3}{5}+log_{2}\frac{1}{3}$
   a) $\log_{2}7+\log_{2}12-log_{2}\frac{21}{4}$
   a) $\log_{3}\frac{4}{5}-\log_{3}\frac{4}{7}+log_{3}\frac{5}{7}$

   \End{multicols}

2. Zwischen welchen ganzen Zahlen liegen die folgenden Logarithmen? Argumentieren Sie und überprüfen sie **danach** mit dem Taschenrechner:
   \Begin{multicols}{4}
   a) $\log_{2}3$
   a) $\log_{2}5$
   a) $\log_{2}\frac{1}{3}$
   a) $\log_{3}2$
   a) $\log_{4}13$
   a) $\log_{5}36$
   a) $\log_{6}99$

   \End{multicols}

3. Es ist $\log_2 3 \approx 1,6$. Bestimmen Sie ohne Taschenrechner die Logarithmen zur Basis $2$ von:
   \Begin{multicols}{7}
   a) $9$
   b) $27$
   c) $36$
   d) $12$
   e) $\sqrt{3}$
   f) $\frac{1}{3}$
   g) $\frac{2}{3}$

   \End{multicols}
