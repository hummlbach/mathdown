---
title: "Abschlussübung"
author: [Klasse 10]
date: 6.12.2021
keywords: [Folgen]
lang: de
header-center: "Folgen" 
---
# I Starters
1. Berechnen Sie die ersten $5$ Folgenglieder:
   \Begin{multicols}{3}

   a) $a_n=4n-1$
   b) $b_n=n^2-3n$
   c) $c_n=\frac{1}{n+1}$
   d) $d_n=3^{n-1}$
   e) $e_n=3^n -1$
   f) $f_n=\frac{2^{n+1}}{2^{n-1}}$

   \End{multicols}
   Welche der Folgen ist eine arithmetische, welche eine geometrische?

2. Geben Sie das Bildungsgesetz der arithmetischen Folge an:
   \Begin{multicols}{2}

   a) $6, 13, 20, 27, 34, \dots$
   b) $13, 5, -3, -11, -19 \dots$
   c) $a_1 = 7$ und $a_2=3$
   d) $a_{10} = 12$ und $a_{20}=18$

   \End{multicols}

3. Geben Sie das Bildungsgesetz der geometrischen Folge an:
   \Begin{multicols}{2}

   a) $27, -18, 12, -8, \frac{16}{3}, \dots$
   b) $0.9, 0.81, 0.729, \dots$
   c) $a_1=6$ und $a_5=96$
   d) $a_4=16$ und $a_8=256$

   \End{multicols}

# II Hauptgang
1. Auf welchen Betrag wachsen $16000$€ an, wenn das Guthaben $5$ Jahre mit
   \Begin{multicols}{3}
   a) $1\%$
   b) $5\%$
   c) $10\%$

   \End{multicols}
   verzinst wird?

2. Ein Vater möchte, dass seinem Sohn am 31.12.2020 ein Betrag von $30000$€ ausgezahlt wird.
Welche Summe musste er am 01.01.2006 anlegen, wenn er mit einer Verzinsung von $10\%$ rechnet?

3. Zu welchem Zinssatz war ein Kapital von $5000$€ ausgeliehen, wenn es in $5$ Jahren auf $6535$€ angewachsen ist? 

5. Bestimmen Sie die Summe der ersten $n$ Folgenglieder der arithmetischen Folge:
   $1, 4, 7, 10, 13, \dots$

6. In der Legende zur Erfindung des Schachspiels wurde aufs erste Feld des Schachbretts ein Reiskorn gelegt, aufs zweite $2$, aufs dritten Feld $4$ usw. Die Anzahl also jeweils verdoppelt.
   a) Wieviele Körner liegen insgesamt auf dem Schachbrett?
   b) Wieiviele Körner liegen in der ersten Reihe, wenn mit 5 Körnern begonnen und die Anzahl jeweils vervierfacht wird?

\pagebreak

# III Dessert

Nähern sich die Folgenglieder der folgenden Folgen immer mehr einer Zahl an? Wenn ja, welcher?

\Begin{multicols}{3}

a) $a_n = \frac{1}{n^2}$
b) $b_n = \frac{1}{2^n}$
c) $d_n = \frac{n^2+1}{n}$
d) $c_n = \frac{(-1)^n}{n}$
e) $e_n = \frac{3n+1}{n}$
f) $f_n = \sum_{i=1}^n{b_n}$

\End{multicols}

