---
title: "Lösung zur Übungsklausur"
author: "Klasse 10"
date: 16.12.2021
keywords: [Potenzen, Exponent, Logarithmus, Zinseszins]
lang: de
---

1. Wir wenden die Potenzgesetze an.
   a) Wurzeln können in gebrochene Exponenten umgewandelt werden, und die Exponenten von potenzierten Potenzen werden multipliziert:
      $$\left(\sqrt{2}\right)^8 = {(2^\frac{1}{2})}^8 = 2^{\frac{1}{2}\cdot 8} = 2^{\frac{8}{2}}= 2^4 = 16$$
   b) Die Exponenten werden wieder miteinander multipliziert. Ein negativer Exponent wird positiv, indem man die "Basis durch ihren Kehrwert ersetzt":
      $$\left(\left(\sqrt[6]{13}\right)^\frac{1}{3}\right)^{-18} = \left(\left(13^\frac{1}{6}\right)^\frac{1}{3}\right)^{-18} =
        13^{\frac{1}{6}\cdot\frac{1}{3}\cdot({-18})} = 13^{-1} = \frac{1}{13}$$
   c) Wir schreiben die Wurzeln wieder als "hoch $1$ durch $n$". Potenzen mit gleicher Basis werden multipliziert, indem man die Basis belässt und die Exponenten addiert:
      $$\left(\sqrt[3]{2}\cdot\sqrt[9]{2}\right)^{54} = \left(2^\frac{1}{3}\cdot 2^\frac{1}{9}\right)^{54} = \left(2^{\frac{1}{3} + \frac{1}{9}}\right)^{54} = \left(2^{\frac{3}{9} + \frac{1}{9}}\right)^{54} = \left(2^\frac{4}{9} \right)^{54} = 2^{\frac{4}{9}\cdot {54}} = 2^{4\cdot 6} = 2^{24}$$
   d) Das Produkt mehrerer Potenzen mit verschiedenen Basen aber gleichen Exponenten berechnet man, indem man die Basen multipliziert und zusammen mit dem gemeinsamen Exponenten potenziert:
     $$\frac{\sqrt{9\cdot 16 \cdot 25}}{125^\frac{1}{3}} = \frac{(9\cdot 16 \cdot 25)^\frac{1}{2}}{\sqrt[3]{125}} =\frac{9^\frac{1}{2}\cdot 16^\frac{1}{2} \cdot 25^\frac{1}{2}}{5} = \frac{3\cdot 4 \cdot 5}{5} = 12$$
   e) Der Quotient von Potenzen mit gleichen Basen wird berechnet, indem die Exponenten subtrahiert werden:
      $$\frac{\sqrt[16]{8}}{\left(\sqrt[16]{8}\right)^{-47}} = \frac{\left(\sqrt[16]{8}\right)^1}{\left(\sqrt[16]{8}\right)^{-47}} = \left(\sqrt[16]{8}\right)^{1-(-47)} = \left(\sqrt[16]{8}\right)^{48} = \left(8^\frac{1}{16}\right)^{48} = 8^{\frac{1}{16}\cdot 48} = 8^\frac{48}{16} = 8^3$$

2. Die Formel für den Zinseszins lautet $K_n = K_0 \cdot \left(1+\frac{p}{100}\right)^n$ und findet bei a) und b) Anwendung. $K_0=400$ ist das Startkapital für alle Teilaufgaben.

   a) Die Laufzeit ist $n=24$ und der Zinssatz $p$ beträgt $1\%$. Wir setzen in die Formel ein:
      $$K_{24} = 400 \cdot \left(1+\frac{1}{100}\right)^{24} = 400 \cdot 1,01^{24} \approx 400 \cdot 1,27 = 507,9$$
   b) Bei jährlicher Verzinsung ist $n=2$, da $24$ Monate $2$ Jahren entsprechen. Weiter ist $p=10$.
      $$K_{2} = 400 \cdot \left(1+\frac{10}{100}\right)^{2} = 400 \cdot 1,1^{2} = 400 \cdot 1,21 = 484$$
   c) Im Fall von Verzinsung ohne Zinseszins, kommen Jahr für Jahr $p\%$ vom Startkapital, also $K_0\cdot \frac{p}{100}$, hinzu:
      $$K_n = K_0 + n\cdot K_0 \cdot \frac{p}{100}$$ 
      Nun ist $p=2\%$ und $n$ ist wieder $24$ und wir setzen wieder ein:
      $$K_{24} = 400 + 24\cdot 400\cdot \frac{2}{100} = 400 +48 \cdot 4 = 592$$ 

3. Wir kennen diesmal das Startkapital $K_0 = 100$, die Laufzeit $n=3$ und das Endkapital nach $3$ Jahren $K_3 = 200$. Wir verwenden wieder die Zinseszinsformel und stellen nach dem unbekannten Zinssatz $p$ um.
   $$200 = 100 \cdot \left(1+\frac{p}{100}\right)^3$$
   Wir teilen zunächst durch $100$
   $$2 = \left(1+\frac{p}{100}\right)^3$$
   ziehen dann die dritte Wurzel
   $$\sqrt[3]{2} = 1+\frac{p}{100}$$
   subtrahieren $1$ und multilpizieren mit $100$:
   $$p = 100\cdot\left(\sqrt[3]{2} - 1\right) \approx 100 \cdot 0,25 = 25$$
   Es bräuchte also einen Zinssatz von $25\%$.

4. Die Logarithmen sind wie folgt:
   a) $\log_5{625} = 4$ denn $5^4 =625$
   b) $\log_2{\frac{1}{128}} = -7$ denn $2^{-7} = \left(2^{-1}\right)^7 = \left(\frac{1}{2}\right)^7 = \frac{1}{128}$
   c) $\log_5{0,04} = -2$ denn $5^{-2} = \left(\frac{1}{5}\right)^2 = 0,2^2= 0,04$
   d) $\log_{12345}{1} = 0$ denn $12345^0 = 1$
   e) $\log_7{\sqrt[3]{49}}= \log_7{\sqrt[3]{7^2}}= \log_7{7^\frac{2}{3}} = \frac{2}{3}$
   f) $\log_4{2^{12}} =\log_4{\left(2^{2}\right)^6} = \log_4{4^6} = 6$

5. Nun noch Logarithmengesetze:
   a) $\log_2{4^{1000}} = 1000 \cdot \log_2{4} = 2000$
   b) $3\cdot\log_{10}5 + \log_{10}8 = \log_{10}{5^3} + \log_{10}8 = \log_{10}{125\cdot 8} = \log_10{1000} = 3$
   c) $\log_{15}5-\log_{15}{75} = \log_{15}\frac{5}{75} = \log{15}\frac{1}{15} = -1$

