---
title: "Warmup"
author: [Klasse 10]
date: 13.12.2021
keywords: [Logaritmus, Potenz, Exponential]
lang: de
header-center: "Logarithmen" 
geometry:
  - landscape
  - left=3cm
  - right=3cm
---

# I Anlagedauer

Ein Anfangskapital von $100000$ Euro wird zu einem Zinssatz von $p\%$ angelegt. Wie lange dauert es, bis sich das Kapital verdoppelt hat wenn

\Begin{multicols}{3}
a) $p=1$
b) $p=2$
c) $p=5$

\End{multicols}

ist?

# II Halbwertszeit

Radioaktive Stoffe zerfallen mit der Zeit (weil sie radioaktiv sind). Die **Halbwertszeit** eines radioaktiven Stoffes gibt an, wie lange es dauert, bis die Hälfte einer (beliebigen) Menge des Stoffes zerfallen ist.

1. Im Labor wird das Verhalten von Caesium untersucht und festgestellt, dass die Menge jedes Jahr um $2,284\%$ abnimmt. Berechnen Sie beruhend darauf die Halbwertszeit von Caesium?

2. Die Halbwertszeit von Radium beträgt $1580$ Jahre. Wie lange dauert es, bis von $3$g Radium nur noch $3$mg übrig sind?

\pagebreak

## Hilfstabelle

$n$           $2$       $3$       $4$      $5$      $6$      $7$     $8$     $9$     $10$     $15$    $20$    $25$     $30$    $40$    $50$    $60$    $70$
---           ------    ------    ------   ------   ------   ------  ------  -----   -----    -----   -----   ----     ------  -----   -----   -----   ----
$1,01^n$      $1,02$    $1,03$    $1,04$   $1,05$   $1,06$   $1,07$  $1,08$  $1,09$  $1,10$   $1,16$  $1,22$  $1,28$   $1,34$  $1,48$  $1,64$  $1,8$   $1,99$
$1,02^n$      $1,04$    $1,06$    $1,08$   $1,1$    $1,12$   $1,14$  $1,17$  $1,19$  $1,21$   $1,35$  $1,47$  $1,64$   $1,78$  $2,15$  $2,69$  $3,28$  $3,99$
$1,05^n$      $1,1$     $1,15$    $1,22$   $1,27$   $1,34$   $1,4$   $1,48$  $1,55$  $1,62$   $2,07$  $2,65$  $3,39$   $4,32$  $7,04$  $11,47$ $18,68$ $30,43$
$0,97716^n$   $0,95$    $0,93$    $0,91$   $0,89$   $0,87$   $0,85$  $0,83$  $0,81$  $0,79$   $0,71$  $0,63$  $0,56$   $0,5$   $0,4$   $0,31$  $0,25$  $0,2$  
$0,5^n$       $0,25$    $0,125$   $0,062$  $0,031$  $0,016$  $0,007$ $0,004$ $0,002$ $0,001$  $0$     $0$     $0$      $0$     $0$     $0$     $0$     $0$
