---
title: "Warmup"
author: [Klasse 10]
date: 7.12.2021
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
---

# I Starters
Ermitteln Sie die Grenzwerte, falls existent.

\Begin{multicols}{3}

a) $a_n=\frac{5n+1}{2n^2}$
b) $b_n=\frac{n^3}{-n^2-8n-1}$
c) $c_n=\frac{4n^2+8n-3}{2n^2}$
d) $d_n=\frac{42n^100}{2^n}$
e) $e_n=\frac{1}{3}\cdot\left(\frac{1}{2}\right)^{n-1}$
f) $f_n=\sum_{i=1}^n e_n$

\End{multicols}

# II Hauptgang

1. Berechnen bzw. vereinfachen Sie:

   \Begin{multicols}{3}

   a) $\frac{{(2^3)}^4}{2^{12}} =$
   b) $\frac{{(3^6)}^2}{3^{11}} =$
   c) ${30000000^0}^{400000} =$
   d) $a^5\cdot a^3 \cdot a^2 =$
   e) $2^6\cdot 2^5 \cdot 2^9 =$
   f) $5^9\cdot 5^8 \cdot 5^0 =$

   \End{multicols}

2. Berechnen oder vereinfachen Sie wieder:

   \Begin{multicols}{3}

   a) $\frac{11^5}{11^3} =$
   b) $\frac{3^7\cdot 5^9\cdot 20^9 \cdot 7^7}{21^7\cdot 100^9} =$
   c) $\frac{4^{1299}}{4^{1296}} =$
   d) $3^5\cdot 3^7 \cdot 3^{-8} =$
   e) $\frac{a^5}{a^7} =$
   f) $a^{5}\cdot a^{-7} =$

   \End{multicols}

# III Dessert

1. Vereinfache:
   $$\frac{a^{n-1}\cdot {(a^2)}^{n-1}\cdot b^{3n-3}}{(ab)^{3n-4}}=$$
2. Warum ist $a^n\cdot b^n = (a\cdot b)^n$ und } $\left(\frac{a}{b}\right)^n = \frac{a^n}{b^n}$?
3. Gelten die folgenden Gleichungen?
   $$a^{(n^m)} = {(a^n)}^m
   \qquad \text{ und } \qquad
   a^{n^m} = a^{m^n}$$
