---
title: "Ganzzahlige Exponenten"
author: [Klasse 10]
date: 8.12.2021
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
---

# I Starters

Wenden Sie die Potenzgesetze und Definitionen an:
\Begin{multicols}{3}

a) $389427891298432^0 =$
b) $4^0\cdot 4^1\cdot 4^2 =$
c) $4^3 \cdot 2^{-4} =$
d) $a^4 \cdot a^6 =$
e) $2^n\cdot a^2 \cdot \frac{2^{-n}}{a^2} =$
f) $\frac{a^n\cdot b^n}{a\cdot b} \cdot a^{1-n} \cdot b \cdot b^{-n} =$

\End{multicols}

# II Hauptgang

1. Potenzen von Potenzen:

   \Begin{multicols}{2}

   a) ${(5^{-2})}^8\cdot 5^{17} =$
   b) $33981^0\cdot 0^{33981} =$
   c) ${({(3^5)}^5)}^5 \cdot 3^{-124} =$
   d) $2^4\cdot 2^{20}\cdot 2^{-49} \cdot {(2^5)}^5 =$
   e) $\left(\frac{95871}{31957}\right)^{10}\cdot (31957^5)\cdot {(95871^{-5})}^2 =$
   f) ${(3^1)}^3 =$ und $3^{(1^3)} =$

   \End{multicols}

2. Potenzen mit gleichen Exponenten:

   \Begin{multicols}{2}

   a) $2^3\cdot 3^3 \cdot 6^{-3} =$
   b) $27^3\cdot \left(\frac{1}{9}\right)^3 =$
   c) $\frac{30^{100000}}{3^{100000}}\cdot 10^{200000}\cdot 10^{-300000} =$
   d) $\frac{3^7\cdot 5^9\cdot 20^9 \cdot 7^7}{21^7\cdot 100^9} =$

   \End{multicols}

# III Dessert von Gestern

1. Vereinfachen Sie:
   $$\frac{a^{n-1}\cdot {(a^2)}^{n-1}\cdot b^{3n-3}}{(ab)^{3n-4}}=$$
2. Warum ist $a^n\cdot b^n = (a\cdot b)^n$ und } $\left(\frac{a}{b}\right)^n = \frac{a^n}{b^n}$?
3. Gelten die folgenden Gleichungen?
   $$a^{(n^m)} = {(a^n)}^m
   \qquad \text{ und } \qquad
   a^{n^m} = a^{m^n}$$
4. Vereinfachen Sie:
   $$\frac{{\left(a^{-2}\cdot(a^{-3}+a^{-6})\right)}^2}{\left((1+a^{-3})\cdot (a^5)^{-1}\right)^2} =$$

