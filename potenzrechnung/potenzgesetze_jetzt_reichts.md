---
title: "Jetzt reichts - kein Bock mehr"
author: [Klasse 10]
date: 20.01.2022
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
---

# Starters
Vervollständigen Sie die Potenzgesetze:

\Begin{multicols}{3}
a) $a^n\cdot a^m =$ 
b) $\left(a^n\right)^\text{\textvisiblespace} = a^{n\cdot m}$
c) $\frac{a^n}{\text{\textvisiblespace}^m} = a^{n-m}$
d) $\left(\frac{a}{b}\right)^{-n} =$
e) $a^\text{\textvisiblespace}\cdot b^n = (a\cdot \text{\textvisiblespace})^n$
f) $a^\text{\textvisiblespace} = 1$

\End{multicols}

# Hauptgang
1. Berechnen oder vereinfachen Sie:
   \Begin{multicols}{3}
   a) $12^3 \cdot 12^{-5} \cdot 12^2$
   b) $\frac{10^{n+2}}{10^n}$
   c) $\left(\frac{5}{6}\right)^3\cdot\left(\frac{18}{25}\right)^3\cdot\left(\frac{5}{3}\right)^3$
   d) $0,5^{20}\cdot 10^{20}\cdot 0,2^{20}$
   e) $\left(\frac{3}{4}\right)^{-2}$
   f) $\left(\left(3^6\right)^2\right)^\frac{1}{4}$

   \End{multicols}

2. Lösen Sie die Wurzeln oder gebrochenen Exponenten auf:
   \Begin{multicols}{3}
   a) $\frac{\sqrt{27}}{\sqrt{3}}$
   b) $\left(16^\frac{5}{8}\right)^{0,4}$
   c) $\sqrt{2a}\cdot \sqrt{4,5a}$
   d) $\sqrt[3]{\sqrt{x^6}}$
   e) $\sqrt{3}^4$
   f) $\sqrt[4]{2}^{12}$

   \End{multicols}

3. Berechnen Sie:
   \Begin{multicols}{3}
   a) $\left(4^{10}\right)^\frac{1}{20}$
   b) $1^{-5}+5^3-9^\frac{6}{2}$
   e) $\frac{8^\frac{1}{4}\cdot 8^\frac{3}{4}}{8^{0,5}}$
   b) $\frac{\left(15^{10}\cdot \left(\frac{1}{5}\right)^{10}\right)^\frac{1}{2}}{3^4}$
   a) $\left(\frac{4^{20}\cdot 4^{-10}}{4^2}\right)^\frac{1}{4}$
   c) $\left(5^\frac{1}{6}\cdot 5^\frac{2}{6}\right)^2$
   d) $\sqrt{8}^\frac{2}{3}$
   f) $\left(12,5^5\cdot\frac{4^{30}}{4^{25}}\cdot\frac{1}{2^5}\right)^\frac{1}{10}$

   \End{multicols}

# Dessert
1. Lösen Sie die Exponentialgleichungen:
   \Begin{multicols}{3}
   a) $a^x = \frac{a^5}{a^2}$
   b) $y^{x+1} = \frac{y^{12}}{y^8}$
   c) $a^{2x+1} = \frac{a^{15}}{a^{10}}$
   d) $z^{x+5} = \frac{z^{3x+5}}{z^{3x-15}}$
   e) $a^{2x+n} = \frac{a^{3x+2n}}{a^{3n}}$
   f) $a^{x+1}  = \frac{a^{m+1}}{a^{m-2}}$

   \End{multicols}

2. Vereinfachen Sie:
   \Begin{multicols}{3}
   a) $\left(a^5\cdot b^3\right)^3$
   b) $\frac{z^n\cdot z^{m-n}}{z^m}$
   c) $\frac{1-x^5}{x^{-2}}+\frac{1}{x^{-7}}$
   d) $\frac{\left(a^{-3}\cdot x^5\right)^{-2}}{\left(a^2\cdot x^{-3}\right)^4}$
   e) $(s^6-s^5)\cdot s^{n-4}$
   f) $\frac{\left((2x)^3 \cdot y\right)^2\cdot z}{4\cdot(xy)^3\cdot (4y\sqrt{z})^2}$

   \End{multicols}
