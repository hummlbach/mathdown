---
title: "Arithmetische und geometrische"
author: [Klasse 10]
date: 30.11.2021
keywords: [Folgen]
lang: de
header-center: "Folgen" 
---
# I Starters

ToDo: Bildungsgesetze aus einzelnen Folgengliedern erschließen.

# II Hauptgang

1. In $25$m Tiefe ist die Temperatur gleich er mittleren Jahrestemperatur des betreffenden Ortes und nimmt dann von da aus je $32$m Tiefe um $1^\circ$ Celcius zu.
   a) Welche Temperatur ergäbe sich (ohne Frischluftzufuhr) in einer Kohlenzeche auf $960$m Tiefe, wenn die mittlere Jahrestemperatur$11^\circ\text{C}$ beträgt?
   b) In welcher Tiefe herscht (ohne Kühlung) die für die Bergleute maximal als "normal" erlaubte Temperatur von $28^\circ\text{C}$?

2. ToDo: C14-Methode!?!

3. Der Luftdruck beträgt auf Meereshöhe im Durchschnitt $1016$ mbar (Millibar). Je $80$m Höhenzunahme sinkt der Luftdruck um $1\%$. Technisch gesehen ist ein Luftdruck von $300$mbar oder weniger schon ein sogenanntes Feinvakuum.
   a) Ab $4800$m Höhe werden, bei mangelhafter Akklimatisation, die meisten Menschen ($50-85\%$) höhenkrank. Wie hoch ist der Luftdruck in dieser Höhe?
   b) Wie hoch ist der Luftdruck noch auf dem Mount Everest in gut $8800$m Höhe?

# III Desert

Josef von Nazaret legt im Jahre 0 bei der Sparkasse Jerusalem den Betrag von $1$ Euro-Cent auf einem Sparbuch an.
Die Verzinsung beträgt jährlich $5\%$. Die Zinsgewinne werden wieder angelegt. Im Jahre 2000 finden seine Nachkommen in einer verbeulten Blechbüchse sein Sparbuch. Bei einer nun unternommenen Reise nach Israel entdecken sie, dass es die Sparkasse Jerusalem heute immer noch gibt. Sie gehen hinein und bitten die Angestellte, doch die Zinsen für die vergangenen 2000 Jahre nachzutragen.

a) Wie müssen Sie rechnen, um auf das aktuelle Guthaben zu kommen?
b) Wie wäre das aktuelle Guthaben ohne Zinseszins?
