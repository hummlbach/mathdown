---
title: "Zinseszins"
author: [Klasse 10]
date: 02.12.2021
keywords: [Folgen]
lang: de
header-center: "Potenzrechnung" 
---
# I Starters

\Begin{multicols}{2}

Berechnen Sie das Endkapital $K_n$ bei einer Anlagedauer von $n$ Jahren und einem Zinsfuß von $p\%$ ausgehend vom Startkapital $K_1$ in Euro:

$K_1$       $p$       $n$       $K_n$
-----       -----     ------    -----
$2000$      $5$       $3$       
$5000$      $1$       $4$           
$1000$      $2$       $6$           

\End{multicols}

# II Hauptgang

1. Auf welchen Betrag wächst ein Kapital von $5000$€ bei einem Zinssatz von $4\%$ in $5$ Jahren an?

2. Anja möchte das Auto und den Führerschein ihrer Tochter finanzieren. Wieviel Geld muss sie anlegen, damit sie nach $20$ Jahren bei $2\%$ Zinsen, den nötigen Betrag von $25000$€ zusammen hat? ($1,02^{20} \approx 1,5$)

3. Jemand möchte durch das Verleihen von $20000$ Euro in $4$ Jahren $1000$ Euro verdienen. Wieviel Prozent Zinsen müssen dazu verlangt werden? ($\sqrt[4]{\frac{21}{20}}\approx 1,01$)

4. Füllen Sie die Tabelle aus:

   $K_0$     $1200$€                  $2500$€
   -----     --------    ---------    ----------
   $K_n$                 $595$€       $5000$€
   $p$       $5\%$       $6\%$
   $n$       $4$ Jahre   $3$ Jahre    $7$ Jahre

   ($\sqrt[7]{2} \approx 1,1$)

# III Dessert

1. Harry möchte sich für $120000$ Euro eine Wohnung kaufen und legt dazu $100000$ Euro, verzinst mit $5\%$, an. Wie lange muss er warten bis er sich die Wohnung leisten kann?

2. Berechnen Sie die Summe über die ersten $20$ ungeraden Zahlen. Tipp: Was ist die Summe aus der ersten und der letzten Zahl? Was ergeben die zweite und die vorletzte zusammen?

3. Wieviel Reiskörner liegen in der Legende von der Erfindung des Schachbretts auf den ersten $8$ Feldern?

