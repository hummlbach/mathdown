---
title: "Klausur Gruppe A"
author: "Name:"
date: 17.12.2021
keywords: [Potenz, Basis, Exponent, Logarithmus, Zinseszins]
lang: de
header-center: "Potenzrechnung und Co"
footer-center: 17 Punkte + 5 Extrapunkte
---

1. Wenden Sie die Potenzgesetze (sichtbar mit einem Zwischenschritt) an: \hfill (**6P**)
   \Begin{multicols}{3}
   a) $3^{14}\cdot 3^{-11} =$
   b) $\left(6^8\right)^\frac{1}{4} =$
   c) $\left(121\cdot 81\right)^\frac{1}{2} =$
   d) $64^\frac{2}{3} =$
   e) $\frac{7^{30}}{7^{29}} =$
   f) $\frac{\left(2^3\cdot 2^5\right)^\frac{1}{4}}{2^4} =$

   \End{multicols}

2. Ein Kapital von $10000$€ wird über $5$ Jahre zu einem Zinssatz von $3\%$ angelegt. Wie groß ist das Endkapital? \hfill (**2P**)

3. Wie hoch muss der Zinssatz sein, um mit einem Startkapital von $10000$€ innerhalb von $2$ Jahren mit Zins und Zinseszins $2100$€ zu verdienen? \hfill (**3P**)

4. Berechnen Sie die Logarithmen: \hfill (**3P**)
   \Begin{multicols}{3}
   a) $\log_4{64} =$
   b) $\log_{10}{\frac{1}{1000}} =$
   c) $\log_\frac{1}{11}{121} =$
   d) $\log_{12}{\sqrt[3]{144}} =$
   e) $\log_9{3^6} =$
   f) $\log_{\sqrt{3}}{9} =$

   \End{multicols}

5. Wenden Sie die Logarithmengesetze (Zwischenschritt aufschreiben) an: \hfill (**3P**)
   \Begin{multicols}{3}
   a) $\log_8{4}+\log_8{16} =$
   b) $4\cdot\log_{9}3 =$
   c) $\log_{25}125-\log_{25}{5} =$

   \End{multicols}

6. Eine Tasse mit heißem Kaffee ($80^\circ C$) kühlt pro Minute um ca. $6\%$ ab. \hfill (**5EP**)
   a) Auf welche Temperatur ist er nach $5$ Minuten abgekühlt?
   b) Nach welcher Zeit (in Minuten) ist der Kaffee kühl genug, um getrunken zu werden (ca. $45^\circ C$?


## Hilfstabelle

$n$          $2$     $3$     $4$     $5$     $6$    $7$     $8$     $9$   $10$   $11$
----        ----    ----    ----    ----    ----   ----    ----    ----   ----   ---- 
$1,03^n$    $1,06$  $1,09$  $1,13$  $1,16$  $1,19$ $1,23$  $1,27$  $1,30$ $1,34$ $1,38$
$1,04^n$    $1,08$  $1,12$  $1,17$  $1,22$  $1,27$ $1,32$  $1,37$  $1,42$ $1,48$ $1,54$
$0,94^n$    $0,88$  $0,83$  $0,78$  $0,73$  $0,69$ $0,64$  $0,61$  $0,57$ $0,54$ $0,50$ 
