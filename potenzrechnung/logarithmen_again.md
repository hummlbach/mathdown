---
title: "Logarithmen Wiederholung"
author: [Klasse 10]
date: 27.01.2022
keywords: [Logarithmen]
lang: de
header-center: "Potenzgesetze" 
---

## Berechnen Sie die folgenden Logarithmen ...

1. ... mit positiver ganzzahliger Basis:
   
   \Begin{multicols}{2}
   a) $\log_3 9$
   a) $\log_2 256$
   d) $\log_3 1$
   e) $\log_4 4$
   c) $\log_2 2^13$
   f) $\log_5 625$

   \End{multicols}

2. ... mit negativem Ergebnis:

   \Begin{multicols}{2}
   a) $\log_2 \frac{1}{16}$
   b) $\log_\frac{1}{3} 81$
   c) $\log_7 \frac{1}{49}$
   d) $\log_\frac{1}{10} 1000$

   \End{multicols}

3. ... mit wurzeliger Basis:

   \Begin{multicols}{2}
   a) $\log_{\sqrt{2}} 4$
   b) $\log_{\sqrt{4}} 2$
   c) $\log_{\sqrt{3}} 27$
   d) $\log_{\sqrt[3]{8}} 4$

   \End{multicols}

4. ... mit gebrochenem Ergebnis:

   \Begin{multicols}{2}
   a) $\log_{10} 100$
   b) $\log_{36} 6$
   c) $\log_{125} 5$
   c) $\log_\frac{1}{16} \frac{1}{4}$
   b) $\log_{4} 8$
   d) $\log_{0,125} 0,5$

   \End{multicols}

5. ... mit noch mehr Wurzeln...

   \Begin{multicols}{2}
   a) $\log_5 \sqrt{5}$
   b) $\log_2 \sqrt[3]{2}$
   c) $\log_9 \sqrt{3}$
   d) $\log_3 {\sqrt{3^3}}$

   \End{multicols}


6. ... mit noch mehr Brüchen und Kommazahlen...

   \Begin{multicols}{2}
   a) $\log_\frac{3}{2} \frac{4}{9}$
   b) $\log_{0,25} 16$
   c) $\log_\frac{2}{5} 6,25$
   d) $\log_{0,4} \frac{25}{4}$

   \End{multicols}



