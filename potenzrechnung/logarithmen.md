---
title: "Logarithmen"
author: [Klasse 10]
date: 15.12.2021
keywords: [Logarithmen]
lang: de
header-center: "Potenzgesetze" 
---

# I Starters
Bestimme folgende Logarithmen und begründe wie folgt: $\log_{2}{64} = 6$ denn $2^6=64$

\Begin{multicols}{3}

a) $\log_2{512}$
b) $\log_4{1024}$
c) $\log_3{729}$
d) $\log_6{36}$
e) $\log_8{64}$
f) $\log_12{12}$
g) $\log_2{\frac{1}{256}}$
h) $\log_2{0,125}$
i) $\log_3{\frac{1}{243}}$
j) $\log_7{\frac{1}{49}}$
k) $\log_8{\frac{1}{512}}$
l) $\log_6{6^5}$

\End{multicols}

# II Hauptgang
1. Bestimme die Logarithmen zur Basis $2$:
   \Begin{multicols}{3}
   a) $\log_2{1024}$
   b) $\log_2{1}$
   c) $\log_2{\frac{1}{8}}$
   d) $\log_2{\frac{1}{128}}$
   e) $\log_2{\sqrt{2}}$
   f) $\log_2{2^{13}}$

   \End{multicols}

2. Bestimme die Logarithmen zur Basis $3$:
   \Begin{multicols}{3}
   a) $\log_3{9}$
   b) $\log_3{1}$
   c) $\log_3{243}$
   d) $\log_3{\frac{1}{81}}$
   e) $\log_3{3^7}$
   f) $\log_3{\sqrt{3^3}}$

   \End{multicols}

3. Bestimme die Logarithmen zur Basis $4$:
   \Begin{multicols}{3}
   a) $\log_4{4}$
   b) $\log_4{16}$
   c) $\log_4{256}$
   d) $\log_4{4^5}$
   e) $\log_4{0,25}$
   f) $\log_4{2}$

   \End{multicols}

4. Bestimme die Logarithmen zur Basis $5$:
   \Begin{multicols}{3}
   a) $\log_5{625}$
   b) $\log_5{25}$
   c) $\log_5{1}$
   d) $\log_5{0,2}$
   e) $\log_5{\frac{1}{625}}$
   f) $\log_5{\sqrt[3]{5}}$

   \End{multicols}

# III Dessert
Bestimme folgende Logarithmen:
\Begin{multicols}{2}

a) $\log_2{8}$, $\log_2{16}$ und $\log_2{8 \cdot 16}$
b) $\log_2{64}$, $\log_2{\frac{1}{16}}$ und $\log_2{64 \cdot \frac{1}{16}}$
c) $\log_2{8}$ und $\log_2{8^4}$
d) $\log_{10}{1000}$, $\log_{10}{100}$ und $\log_{10}{1000 \cdot 100}$
e) $\log_{10}{100}$ und $\log_{10}{100^5}$
f) $\log_{10}{100000}$, $\log_{10}{100}$ und $\log_{10}{\frac{100000}{100}}$
g) $\log_5{625}$, $\log_5{125}$ und $\log_5{\frac{625}{125}}$
h) $\log_{10}{5} + \log_{10}{2}$ und $\log_6{4}+\log_6{9}$

\End{multicols}
