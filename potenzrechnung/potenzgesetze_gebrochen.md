---
title: "Gebrochene Exponenten"
author: [Klasse 10]
date: 9.12.2021
keywords: [Potenzgesetze]
lang: de
header-center: "Potenzgesetze" 
---

# I Starters

Berechnen oder vereinfachen Sie:

\Begin{multicols}{3}

a) $\frac{1000^6}{125^6} \cdot 2^{-18} =$
b) $\frac{2^{16}}{2^4} =$
d) $\left(\frac{3}{4}\right)^{-1} =$
c) $4^3 \cdot 4^{-3} =$ und $4^{-3} =$
e) $\left({\left(\frac{3}{2}\right)^{-1}}\right)^3 =$
f) $a^{-2}\cdot a^{-1} \cdot a^0 \cdot a^1 \cdot a^2 =$
g) ${\left(2^3\right)}^{(-2^3)}\cdot {2^2}^2=$
h) $\left(a^{2}\right)^3\cdot a^{-5} \cdot b^{-8}\cdot\left(({b^2}\right)^4 =$
i) $36^{8}\cdot\left(\frac{1}{18}\right)^{8}\cdot \left(2^2\right)^3\cdot 2^{-5} =$

\End{multicols}

# II Hauptgang

1. Berechnen Sie, indem Sie das passende Potenzgesetz anwenden:
   \Begin{multicols}{3}

   a) $\left({9^\frac{1}{2}}\right)^2 =$
   b) $4^\frac{1}{2}\cdot 4^\frac{1}{2} =$
   c) $\left({2^3}\right)^\frac{1}{3} =$
   d) $\left({27^\frac{1}{3}}\right)^3 =$
   e) $\left({3^2}\right)^\frac{1}{2} =$
   f) $8^\frac{1}{3}\cdot 8^\frac{1}{3}\cdot 8^\frac{1}{3} =$

   \End{multicols}

2. Schreiben Sie ohne Exponent:
   \Begin{multicols}{3}

   a) $9^\frac{1}{2} =$
   b) $4^\frac{1}{2} =$
   c) $27^\frac{1}{3} =$
   d) $8^\frac{1}{3} =$
   e) $3^\frac{1}{2} =$
   f) $2^\frac{1}{3} =$

   \End{multicols}

3. Berechnen oder vereinfachen Sie wieder:

   \Begin{multicols}{3}

   a) $4^\frac{1}{3}\cdot 4^\frac{8}{3} =$
   d) $\frac{8^\frac{1}{3}}{8^{-\frac{2}{3}}} =$
   c) $\left(\left(7^\frac{1}{4}\right)^\frac{1}{3}\right)^{12} =$
   b) $7^\frac{4}{9}\cdot 7^\frac{3}{10} \cdot 7^{-\frac{34}{9}} =$
   e) $\left(3^\frac{13987}{420}\right)^\frac{420}{13987} =$
   f) $6^\frac{1}{2}\cdot 24^\frac{1}{2} =$

   \End{multicols}

# III Dessert

Was könnte $0^0$ sein?

a) Was ist der Grenzwert der Folge $a_n = 0^{\frac{1}{n}}$?
b) Wogegen konvergiert $b_n = \left(\frac{1}{n}\right)^0$?
c) Welcher Wert für $0^0$ wäre in der Summenformel für die geometrische Reihe $\sum_{i=1}^n{q^{i-1}}=\frac{q^n-1}{q-1}$ geschickt? (Wenn $q=0$ ist.)
d) Welcher Wert für $0^0$ bietet sich an, wenn Sie an den binomischen Lehrsatz $(x+y)^n=\sum_{k=0}^n{\binom{n}{k}x^{n-k}y^k}$ denken? (Zum Beispiel für $x=0$ und $n=1$.)

