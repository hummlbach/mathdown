---
title: "Fläche zwischen Graphen"
author: [Klasse 12]
date: 22.02.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

\Begin{multicols}{2}
1. Gegeben sei das folgende Schaubild:

   ![](dreieck_im_1_quadranten.png)

   a) Stellen Sie die Funktionsgleichungen von $f$ und $g$ auf.
   b) Berechnen Sie den Inhalt der Fläche zwischen der $x$-Achse und $G_f$ über dem Intervall $[0,3]$.
   c) Wie groß ist die Fläche zwischen der $x$-Achse und $G_g$ über dem Intervall $[0,3]$.
   d) Berechnen Sie nun den Flächeninhalt, der von den beiden Graphen sowie der $y$-Achse eingeschlossen wird.
   e) Integrieren Sie über die Funktion $h(x)=f(x)-g(x)$ von $x=0$ bis $x=3$.

2. Stellen Sie die Funktionsgleichungen zu den Graden im Schaubild unten auf und berechnen Sie...
\newline

   ![](zwei_graden_zum_integrieren.png)

   a) die Fläche, die von den Graphen $G_f$, $G_g$ sowie der $y$-Achse eingeschlossen wird.
   b) die Fläche zwischen $G_f$ und $G_g$ über dem Intervall $[0;5]$.
   c) die Integrale $\int_0^3 h(x)dx$ und $\int_3^5 h(x) dx$ mit $h(x)=g(x)-f(x)$.

\End{multicols}

3. Zeichnen Sie die Graphen der Funktionen $g(x)=-2x+8$ und $f(x)=-\frac{1}{2}x+\frac{7}{2}$ in ein (kartesisches) Koordinatensystem ein und berechnen Sie (mittels Integralrechnung)...

   a) den Flächeninhalt zwischen den Graphen im ersten Quadranten.
   b) den Flächeninhalt zwischen den beiden Graphen $G_g$ und $G_f$ über dem Interval $[3,7]$.
   c) den Inhalt der Fläche zwischen $G_g$ und $G_f$ über dem Interval $[0,3]$.

\pagebreak

\Begin{multicols}{2}

4. Berechnen Sie die Inhalte der Flächen, die von der Parabel im Schaubild rechts, sowie den Graphen der folgenden Funktionen eingeschlossen werden:

   a) $g_1(x)=5$
   b) $g_2(x)=x+2$
   c) $g_3(x)=x+6$
   d) $g_4(x)=2x+4$
   e) $g_5(x)=-2x+11$

   ![](parabel_scheitel_1_9.png)

\End{multicols}

\Begin{multicols}{2}

![](parabel_scheitel_-2_5.png)

5. Berechnen Sie die Inhalte der Flächen, die von der Parabel im Schaubild links und den Graphen der folgenden Funktionen eingeschlossen werden:

   a) $p_1(x)=x^2+1$
   b) $p_2(x)=-\frac{1}{4}x^2-x+1$
   c) $p_3(x)=\frac{1}{2}x^2+2x+1$

\End{multicols}

6. Gegeben sei die Funktion $h(x)=\frac{1}{4}x^3-x^2+4$.

   a) Zeichnen Sie den Graph $G_h$ von $h$ in ein rechtwinkliges Koordinatensystem ein. (Wo hat $G_h$ Hoch-, Tief- und Wendepunkte?)
   b) Berechnen Sie die Fläche, die von $G_h$ und der Gerade $y=x$ eingeschlossen wird.
