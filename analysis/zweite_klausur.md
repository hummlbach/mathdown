---
title: "2. Klausur"
author: "Name:"
date: 22.10.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte]
lang: de
header-center: "Analysis" 
footer-right: "Bearbeitungszeit 90 Minuten"
footer-center: 33 Punkte + 3 Extrapunkte
---

1. Diskutieren Sie die Kurve $G_h$ der Funktion

   $$h(x) = (x+1)(x^2+2x-2)$$

   a) Geben Sie die Schnittpunkte mit der $x$- sowie den Schnittpunkt mit der $y$-Achse an. \hfill (__3P__)
   b) Zeigen Sie, dass $G_h$ zwei Extrema hat und geben Sie die Art der Extrema und deren Koordinaten an. \hfill (__6P__)
   c) Untersuchen Sie den Graph von $h$ auf Wendepunkte und ermitteln Sie gegebenenfalls die Koordinaten. \hfill (__3P__)
   d) Berechnen Sie $h(1)$ sowie $h(-3)$ und zeichnen Sie $G_h$ unter Verwendung der bisherigen Erkenntnisse in ein Koordinatensystem ein. \hfill (__5P__)

2. Gegeben sei die Funktion

   $$f(x) = \frac{3}{8}x^5-\frac{5}{4}x^3+\frac{15}{8}x$$

   und ihre erste Ableitung

   $$f'(x) = \frac{15}{8}(x^2-1)^2$$

   a) Weist der Graph $G_f$ von $f$ Extrema oder Terrassenpunkte auf? Geben Sie die Koordinaten und die Art dieser markanten Stellen an. \hfill (__7P__)
   b) Zeigen Sie, dass es einen (weiteren) Wendepunkt gibt und geben Sie dessen Koordinaten an. \hfill (__5P__)
   c) Berechnen Sie $f(2)$ sowie $f(-2)$ und zeichnen Sie $G_f$ in ein Koordinatensystem ein. \hfill (__4P__)

3. Welcher der folgenden Funktionsterme gehört zum Schaubild? Belegen Sie Ihre Wahl mit einer Grenzwertuntersuchung. \hfill (__3P\*__)

   \Begin{multicols}{2}

   $$p(x)=\frac{-1}{x^2-1}$$
   $$q(x)=\frac{x}{x^2-1}$$
   $$r(x)=\frac{x^2}{x^2-1}$$
   $$s(x)=\frac{x}{x^2+1}$$

   ![](asymptoten.png){height=200px}

   \End{multicols}

