---
title: "CO2 Budget"
author: [Klasse 13]
date: 25.10.2022
keywords: [Integral]
lang: de
header-center: "Analysis" 
geometry:
 - top=3cm
 - bottom=3.5cm
 - left=2.5cm
 - right=2.5cm
---

Um das Pariser Klima(optimal)ziel, die Erderwärmung auf unter $1,5^\circ$ (über dem vorindustriellen Niveau) zu beschränken, einzuhalten, darf die Menschheit schätzungsweise nicht mehr mehr als $450$ Gigatonnen $\text{CO}_2$ ausstoßen. Das deutsche $\text{CO}_2$ Restbudget beträgt damit anteilig etwa $5$ Gigatonnen. <!-- Das heißt um das $2$-Grad-Ziel einzuhalten sollte Deutschland über dieses Budget hinaus kein weiteres Kohlendioxid aus fossilen Quellen in die Atmosphäre entlassen.
Man spricht vom sogenannten $\text{CO}_2$-Budget. Will man das $2$-Grad-Ziel einhalten darf über dieses Budget hinaus kein weiteres Kohlendioxid aus fossilen Quellen in die Atmosphäre gelangen. --> \newline
Die Graphen im folgenden Schaubild zeigen mögliche Kohlendioxidausstöße für Deutschland über die kommenden Jahre. Die $t$-Achse zeigt dabei die Jahre beginnend mit dem Jahr $2020$ bei $x=0$. Die Werte auf der $y$-Achse geben den $\text{CO}_2$-Ausstoß in Hunderten von Megatonnen an.

![](co2_budget.png){height=320px}

Weiter sind die Funktionsterme $h_c(x)=-\frac{2}{c}(x-c)(x+3,5)$ sowie $e(x)=\frac{8}{e^{0,1x}}-1$ gegeben.

a) Entnehmen Sie den Schaubildern den deutschen $\text{CO}_2$-Ausstoß im Jahr $2020$.
b) Der Graph $K_f$ zeigt eine lineare Reduktion des Kohlendioxidausstoßes ab dem Jahr $2020$.
   (i) Bestimmen Sie einen Funktionsterm für $f$. Ab welchem Jahr stößt die Menschheit in diesem Szenario kein Kohlendioxid mehr aus.
   (ii) Ermitteln Sie den (kummulierten) $\text{CO}_2$-Ausstoß für Deutschland ab $2020$ gemäß $K_f$.
c) Begründen Sie, dass mit der Reduktion die $K_g$ zeigt, das $1,5^\circ$-Ziel kaum einzuhalten sein wird.
d) Sei zunächst $c=9$ im Funktionsterm von $h$. Berechnen Sie gemäß $h_9$:
   (i)  Wieviel $\text{CO}_2$ im Jahr $2025$ ausgestoßen wird.
   (ii) Den maximalen Kohlendioxidausstoß.
   (iii) Den Zeitpunkt zu dem kein Kohlendioxid mehr ausgestoßen wird.

   Und zeichnen Sie den Graph von $h_9$ in Schaubild ein.
e) Zeigen Sie das $e$ einen Reduktionspfad beschreibt mit dem sich das $1,5^\circ$-Ziel einhalten ließe.
f) Untersuchen Sie wie groß der Parameter $c$ höchstens sein darf, um das Pariser Abkommen, im Szenario das durch $h_c$ beschrieben wird, gerade noch einzuhalten.
g) Beschreiben Sie wie sich die drei nicht-linearen Reduktionsszenarien vom linearen unterscheiden.

