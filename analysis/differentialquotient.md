---
title: "Steigung/Differenzialquotient"
author: [Klasse 12]
date: 15.09.2021
keywords: [Funktionen, Steigung, Differenzenquotient, Differentialquotient]
lang: de
header-center: "Analysis" 
geometry:
   - bottom=3cm
   - top=3cm
   - left=2.5cm
   - right=2.5cm
---

# I Starters

Berechnen Sie die Steigung der folgenden Funktionen an den Stellen $-3$, $-2$, $-1$, $0$, $1$, $2$, $3$ und zeichnen Sie die Steigungen (an den jeweiligen Stellen) in ein Koordinatensystem.

\Begin{multicols}{4}
a) $f(x)=x^2$
b) $g(x)=2x^2$
c) $h(x)=x^2-3x$
d) $p(x)=x^3$

\End{multicols}

# II Hauptgang

1. Berechnen Sie die Steigung der folgenden Funktionen an einer gewissen Stelle $x_1$.

   \Begin{multicols}{4}
   a) $f(x)=x^2$
   b) $g(x)=2x^2$
   c) $h(x)=x^2-3x$
   d) $p(x)=x^3$

   \End{multicols}

2. Stellen Sie sich die Funktionsgraphen als Flugbahn, in Seitenansicht, vor (die von links nach rechts abgeflogen wird).

   \Begin{multicols}{2}
   ![](symmetrie_1.png){height=200px}
   ![](symmetrie_2.png){height=200px}
   \End{multicols}

   \Begin{multicols}{2}
   ![](symmetrie_3.png){height=200px}
   ![](symmetrie_4.png){height=200px}
   \End{multicols}

   a) Markieren Sie die Teile der Graphen grün, die einen Steigflug darstellen. (Positive Steigung)
   b) Markieren Sie alle Teile blau, die in diesem Bild einen Sinkflug bedeuten. (Steigung ist negativ)
   c) Färben Sie alle Stellen rot, an denen das Flugzeug weder sinkt noch steigt. (Steigung ist $0$)
   d) Schreiben Sie die Koordinaten der höchsten und der tiefsten Punkte der Graphen auf.

# III Dessert

Zeigen Sie:

$$\lim_{h\rightarrow\infty}\frac{(x+h)^n - x^n}{h} = nx^{n-1}$$

Hinweis: Benutzen Sie dazu den binomischen Lehrsatz: $(a+b)^n = \sum_{i=0}^n\binom{n}{i}a^ib^{n-i}$
