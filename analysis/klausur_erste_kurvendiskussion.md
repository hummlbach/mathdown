---
title: "1. Klausur"
author: "Name:"
date: 05.10.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte]
lang: de
header-center: "Analysis" 
footer-right: "Bearbeitungszeit 90 Minuten"
footer-center: 35 Punkte
---

Die Funktion

$$f(x) = x^4-8x^2$$

soll untersucht und ihr Graph $G_f$ gezeichnet werden.

1. Berechnen Sie die Schnittpunkte von $G_f$ mit der $x$-Achse. \hfill (__3P__)

2. Wo liegen die Extrema von $f$?
   a) Berechnen Sie die Koordinaten der Extrema. \hfill (__7P__)
   b) Legen Sie dar bei welchen Extrema es sich um Maxima bzw. Minima handelt. \hfill (__4P__)

3. Untersuchen Sie das Krümmungsverhalten von $G_f$.
   a) Wieviele Wendepunkte hat der Graph von $f$? Geben Sie deren Koordinaten an. \hfill (__7P__)
   b) Über welchen Intervallen ist $G_f$ links- über welchen rechtsgekrümmt? \hfill (__3P__)

4. Berechnen Sie $f(3)$ und $f(-3)$. \hfill (__2P__)

5. Zeichnen Sie $G_f$, unter Zuhilfenahme der Ergebnisse der bisherigen Aufgaben, in ein Koordinatensystem passender Größe mit $1cm$ pro Längeneinheit. \hfill (__5P__)

6. Wir nehmen nun an $f$ sei die Ableitung einer anderen Funktion $g$, also $g' = f$. Was verrät uns das über den Graph von $g$? \hfill (__4P__)

