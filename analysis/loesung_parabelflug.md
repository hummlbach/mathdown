---
title: "Lösung zur 3. Klassenarbeit"
author: "Klasse 12"
date: 19.02.2022
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

\Begin{center}

![](parabelflug_loesung.png){height=350px}

\End{center}

1. Die $x$-Achsenabschnitte können wir anhand der Periode einzeichnen, die $y$-Achsenabschnitte ergeben sich aus $h(0)$. Weiter ist nach der Amplitude gefragt.
   - Periode: $p=\frac{2\pi}{b}=\frac{2\pi}{\frac{\pi}{40}} = \frac{2\pi\cdot 40}{\pi} = 80$ (s.a. mathematische Merkhilfe)
   - $h(0) = -2 \cos(\frac{\pi}{40}\cdot 0)+7 = -2 \cos(0) +7 = -2 +7 = 5$
   - Amplitude: $A=|-2| = 2$

2. Ab $t=27$, also in der $28$. Sekunde, erleben die Insassen Schwerelosigkeit. Der Zeitraum, in dem im Flugzeug Mikrogravitation herrscht, erstreckt sich bis $t=53$ und dauert somit $l=53-27=26$ Sekunden an.

3. Die niedrigste Flughöhe ist $h_{\text{min}}= 5000$m, die maximale Flughöhe ist $h_{\text{max}} = 9000$m. Zum Auffinden des ersten Maximums geben wir $3$ mögliche Lösungen an:
   a) Erste Lösung ohne Ableitung:
      - Wir wissen (s.a. mathematische Merkhilfe), dass $\cos(x)$ sein erstes Minimum bei $x=\pi$ hat.
      - Somit hat $-\cos(x)$ (das Minus spiegelt den Graph an der $x$-Achse) sein erstes Maximum bei $x=\pi$.
      - Der Graph von $-\cos(\frac{\pi}{40}t)$ entsteht aus dem Graph von $-\cos(x)$ durch Streckung entlang der $x$-Achse um $\frac{40}{\pi}$, sodass $-\cos(\frac{\pi}{40}t)$ sein erstes Maximum bei $t=\pi\cdot\frac{40}{\pi}=40$
      - Streckung und Verschiebung in $y$-Richtung haben keinen Einfluss auf die $x$-Werte der Maxima/Minima, also hat $G_h$ seinen ersten Hochpunkt bei $x=40$.
   b) Zweite Lösung unter Verwendung der ersten Ableitung:
      - $h'(t) =  (-2)\cdot(-\sin(\frac{\pi}{40}t)\cdot\frac{\pi}{40} = \frac{\pi}{20}\sin(\frac{\pi}{40}t)$
      - Notwendige Bedingung für Maxima/Minima: $h'(t)=0$
      - $h'(t) = 0 \Leftrightarrow \sin(\frac{\pi}{40}t) = 0 \Leftrightarrow \frac{\pi}{40}t = k\cdot\pi \Leftrightarrow t = 40k$
      - $h(0) =-2 \cos(\frac{\pi}{40}0)+7 =-2 \cos(0)+7 = -2 + 7 = 5$
      - $h(40) = -2 \cos(\frac{\pi}{40}40)+7 = -2 \cos(\pi)+7 = (-2)\cdot (-1) + 7 = 9$ 
      - $h$ hat also Minima bei $t=80k$ und Maxima bei $t=40+80k$
   c) Dritte Lösung anhand Verschiebung und Streckung in $y$-Richtung:
      - Der $y$-Werte der Maxima des (unverschobenen, ungestreckten) $cos$ ist $1$. (s.a. mathematische Merkhilfe)
      - Durch die Streckung entlang der $y$-Achse um $2$ und die Verschiebung in $y$-Richtung um $7$ hat $h$ seine Maxima bei $y=9$.
      - Wir suchen also nach dem ersten $t$, sodass $h(t)=9$:
        $$h(t)=9 \Leftrightarrow -2 \cos(\frac{\pi}{40}t)+7 = 9 \Leftrightarrow \cos(\frac{\pi}{40}t) = -1 \Leftrightarrow \frac{\pi}{40}t = \pi+k2\pi \Leftrightarrow t = 40 +80k$$

4. Die Steigung ist gegeben durch die erste Ableitung $h'(t)$. Wir suchen also "das erste" Maximum von $h'(t)$.
   - Eine notwendige Bedingung für ein Maximum von $h'(t)$ ist, dass die erste Ableitung von $h'(t)$, also $h''(t)=0$ ist. (In den Wendepunkten wird die Steigung maximal bzw. minimal.)
   - Wir berechnen also die Ableitungen entsprechend der Kettenregel:
     - $h'(t) =  (-2)\cdot(-\sin(\frac{\pi}{40}t)\cdot\frac{\pi}{40} = \frac{\pi}{20}\sin(\frac{\pi}{40}t)$
     - $h''(t) = \frac{\pi}{20}\cos(\frac{\pi}{40}t)\cdot\frac{\pi}{40} = \frac{\pi}{800}\cos(\frac{\pi}{40}t)$
   - $h''(t) = 0 \Leftrightarrow \cos(\frac{\pi}{40}t) = 0 \Leftrightarrow \frac{\pi}{40}t = \frac{\pi}{2}+k\pi = 0 \Leftrightarrow t = (\frac{\pi}{2}+k\pi)\cdot\frac{40}{\pi} = 20+k40 \quad (k\in\mathbb{Z})$
   - $h'(20) = \frac{\pi}{20}\sin(\frac{\pi}{40}20) = \frac{\pi}{20}\sin(\frac{\pi}{2}) = \frac{\pi}{20}$, $h'(60)= \frac{\pi}{20}\sin(\frac{3\pi}{2}) = -\frac{\pi}{20}$ 
   $\Rightarrow$ Die Steigung wird also maximal $\frac{\pi}{20}$ groß und der Steigungswinkel des Graphen $G_h$ maximal $\alpha = \arctan{m} = \arctan{\frac{\pi}{20}}$.


5. Das Diagramm ist (sozusagen) ein $s$-$t$-Diagramm mit vertikaler Strecke. Die Steigung von $G_h$, also die Ableitung des $s$-$t$-Zusammenhangs ist ein Geschwindigkeits-Zeit-Zusammenhang (Strecke durch Zeit ist Geschwindigkeit). Die Steigung des Graphen $G_h$ ist also die Steig- bzw. Sinkgeschwindigkeit des Flugzeugs. Die maximale Steiggeschwindigkeit ist somit
   $$v_{max} = \frac{\pi}{20}\cdot 1000 m/s \approx 0,15 \cdot 1000 m/s = 150 m/s \approx 525 km/h$$

\pagebreak 

6. Wir bestimmen zunächst eine Stammfunktion von $h$:
   $$H(t) = -2\frac{40}{\pi}\sin\left(\frac{\pi}{40}t\right)+7t = -\frac{80}{\pi}\sin\left(\frac{\pi}{40}t\right)+7t$$
   Und rechnen nun das Integral $\int_{0}^{40}h(t)dt$, das aufgrund der Achsensymmetrie von $G_h$ zu $t=40$ gleich dem Integral $\int_{40}^{80}h(t)dt$ ist, aus:
   $$
   \begin{array}{ll}
   \int_0^{40} h(t) dt &= H(40) - H(0)
    =-\frac{80}{\pi}\sin\left(\frac{\pi}{40}\cdot 40\right)+7\cdot 40 - (-\frac{80}{\pi}\sin\left(\frac{\pi}{40}\cdot 0\right)+7\cdot 0) \\
    & = -\frac{80}{\pi}\sin(\pi)+280 +\frac{80}{\pi}\sin(0) = 280
   \end{array}
   $$
7. a) Die durchschnittliche Flughöhe über die gesamte Flugdauer ist gleich der durchschnittlichen Flughöhe über eine Periode oder auch über die erste Hälfte einer Periode (letzeres wieder aufgrund der Achsensymmetrie). Weiter erhält man die mittlere Flughöhe, indem man über alle Flughöhen summiert und durch die Flugdauer teilt. D.h. wir müssen den Wert des Integrals aus 6. noch mit $1000$m multiplizieren und durch $40$ teilen:
$$h_{avg} = \frac{280\cdot 1000\text{m}}{40} = 7000\text{m}$$

   b) Um die durchschnittliche Flughöhe während der Mikrogravitationsphase zu berechnen, verfahren wir analog:
      $$
   \begin{array}{ll}
   \int_{27}^{40} h(t) dt &= H(40) - H(27)
    =-\frac{80}{\pi}\sin\left(\frac{\pi}{40}\cdot 40\right)+7\cdot 40 - (-\frac{80}{\pi}\sin\left(\frac{\pi}{40}\cdot 27\right)+7\cdot 27) \\
    & = -\frac{80}{\pi}\sin(\pi)+280 +\frac{80}{\pi}\sin(\frac{27\pi}{40}) -189 \approx 91 + 21,7 = 112,7
   \end{array}
      $$
      Und somit ist die durchschnittliche Flughöhe während der Mikrogravitationsphase:
      $$\frac{112,7\cdot 1000\text{m}}{13} \approx 8669\text{m}$$
      
