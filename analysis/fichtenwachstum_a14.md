---
title: "Fichtenwachstum"
author: [Klasse 12]
date: 26.04.2022
keywords: [Logarithmen]
lang: de
header-center: "Analysis" 
---

## Wahlteil (mit WTR und Merkhilfe)

Durch die Funktion $f$ mit $f(t)=0,02t^2\cdot e^{-0,1\cdot t}$ wird das Wachstum einer Fichte in Abhängigkeit von der Zeit $t$ (gemessen in Jahren) beschrieben. Dabei gibt $f(t)$ nicht die Höhe, sondern die Wachstumsgeschwindigkeit in Metern pro Jahr (zum Zeitpunkt $t$) an. Der Graph von $f$ ist durch folgende Abbildung gegeben:

\Begin{center}

![](fichtenwachstum.png){}

\End{center}

Zum Zeitpunkt $t=0$ hat eine frisch eingepflanzte Fichte eine Höhe von $20$cm.

a) Berechnen Sie den Funktionswert von $f$ an der Stelle $t=30$ und interpretieren Sie das Ergebnis im Sachzusammenhang.
b) Beschreiben Sie anhand des Graphen von $f$, wie sich die Fichte im Laufe der Jahre entwickelt.
c) Begründen Sie anhand des Graphen, dass die Fichte nach $20$ Jahren weniger als $20$ Meter hoch ist.
d) Zeigen Sie, dass durch $F(t) = -0,2\cdot (t^2+20t+200)\cdot e^{-0,1 \cdot t}$ eine Stammfunktion von $f$ gegeben ist. \newline Berechnen Sie die zu erwartende Höhe der Fichte nach $80$ Jahren.
e) Formulieren Sie eine Fragestellung im Sachzusammenhang, die auf die Gleichung $F(t+10) = F(t) + 5$ führt. \newline Beschreiben Sie, wie man mithilfe der Abbildung eine Lösung dieser Gleichung ermitteln kann.

## Pflichtteil (HMF)

1. Gegeben sind die Funktionen $g_1$ und $g_2$ mit $g_1(x)=e^x$ und $g_2(x)=-e^{-x}+2$.
   a) Beschreiben Sie, wie der Graph von $g_2$ aus dem Graphen von $g_1$ entsteht.
   b) Zeigen Sie, dass sich die Graphen von $g_1$ und $g_2$ im Punkt $P(0|1)$ berühren.

2. Gegeben ist die Funktion $h$ mit $h(x)=e^{2\cdot x}-4\cdot x$; $x \in \mathbb{R}$.
   a) Bestimmen Sie den Punkt, an dem das Schaubild von $h$ eine waagrechte Tangente hat.
   b) Ermitteln Sie eine Stammfunktion von $h$, deren Schaubild durch den Punkt $P(0|5)$ verläuft.

$$\text{ }$$

Diese Aufgaben sind (fast so) als "Analysis A14" im Buch "Erfolg im Mathe-Abi (Baden-Württemberg), Mündliche Prüfung Basisfach 2022" bzw. als "1 HMF Analysis 8" und "1 HMF Analysis 11" im Buch "Erfolg im Mathe-Abi (Baden-Württemberg), Schriftliche Prüfung Basisfach 2021" zu finden. Beide Bücher sind erschienen im Freiburger Verlag.
