---
title: "4. Klausur"
author: "Name:"
date: 10.05.2022
keywords: [Exponentialfunktion]
lang: de
header-center: "Analysis" 
footer-center: "13,5 VP" 
footer-right: "Bearbeitungszeit: 80 Minuten" 
geometry:
  - top=2.4cm
  - bottom=2.7cm
  - left=2.5cm
  - right=2.5cm
---

\Begin{tcolorbox}
### Aufgabe 1

Ein Klimaforscher beschreibt die Entwicklung der globalen Durchschnittstemperatur modellhaft durch die Funktion $f$ mit
$$f(t)=2,8\cdot e^{0,008t}-0,03t+11,1 \qquad (0\leq t \leq 200)$$
Dabei gibt $t$ die Zeit in Jahren seit Beginn des Jahres $1900$ und $f(t)$ die globale Durchschnittstemperatur in Grad Celsius an. \newline
Bearbeiten Sie die folgenden Teilaufgaben anhand dieses Modells. \newline
Der Graph von $f$ ist durch folgende Abbildung gegeben:

![](abi_2018_analysis_a2.png)

a) Geben Sie die globale Durchschnittstemperatur zu Beginn des Jahres 1900 an. \newline
   Berechnen Sie die niedrigste globale Durchschnittstemperatur seit $1900$. \newline
   Bestimmen Sie, in welchem Jahr die globale Durchschnittstemperatur $16,0^\circ$ überschreitet. \newline
   Ermitteln Sie die momentane Änderungsrate der globalen Durchschnittstemperatur zu Beginn des Jahres $2000$.
   \newline
   \phantom{a}\hfill (5,5 VP)

b) Formulieren Sei eine Fragestellung im Sachzusammenhang, die auf die Gleichung \newline $f(t+10)-f(t) = 0,5$ führt. \newline
   Nachdem die globale Durchschnittstemperatur ihren niedrigsten Wert erreicht hat, steigt sie immer weiter an. \newline
   Zeigen Sie, dass dieser Anstieg immer schneller verläuft.
   \newline
   \phantom{a}\hfill (2,5 VP)

c) Im Jahr $2020$ werden Klimaschutzmaßnahmen durchgeführt. Ab diesem Zeitpunkt bleibt die momentane Änderungsrate der globalen Durchschnittstemperatur konstant bei dem Wert, der durch das Modell des Klimaforschers vorausgesagt wird. \newline
   Bestimmen Sie die globale Durchschnittstemperatur, die man im Jahr $2050$ nach diesem Modell erwartet.
   \newline
   \phantom{a}\hfill (3 VP)

\End{tcolorbox}

\Begin{tcolorbox}

### Aufgabe 2
Zwei ideale Würfel werden gleichzeitig geworfen.

a) Bestimmen Sie die Wahrscheinlichkeit dafür, dass zwei verschiedene Augenzahlen fallen.
b) Berechnen Sie die Wahrscheinlichkeit, dass man eine "$1$" und eine "$2$" erhält.
c) Betimmen Sie die Wahrscheinlichkeit, dass die Würfel zwei aufeinanderfolgende Zahlen zeigen.

\hfill (2,5 VP)

\End{tcolorbox}


