---
title: "Kettenregel"
author: [Klasse 12]
date: 10.01.2022
keywords: [Ableitung, Kettenregel]
lang: de
header-center: "Analysis" 
---

# I Starters

Verketten Sie die Funktionen $u$ und $v$ zu $u\circ v$ und berechnen Sie die Ableitung der Verkettung $(u\circ v)'$.
Wie ergibt sich $(u\circ v)'$ aus $u$, $v$, $u'$ und $v'$?

1. Im Folgenden bewirkt die Verkettung eine Verschiebung von $u$:

$u(x)$      $v(x)$      $\quad (u\circ v)(x)\quad$   $\quad u'(x)\quad$    $\quad v'(x)\quad$     $\quad (u\circ v)'(x)\quad$
----        ----        -----                        ----                  -----                  -----
$x^2$       $x+3$       
$x^2-1$     $x-3$
$-x^2$      $x+1$
$x^2+x$     $x+5$
$x^3$       $x-5$
$\sin(x)$   $x-2$


2. In der ersten, zweiten und vierten Zeile ist die Verkettung eine Streckung/Stauchung:

$u(x)$      $v(x)$          $\quad (u\circ v)(x)\quad$   $\quad u'(x)\quad$     $\quad v'(x)\quad$      $\quad (u\circ v)'(x)\quad$
----        ----            -----                       ----                    -----                   -----
$x^2$       $\frac{1}{3}x$
$x^3$       $\frac{1}{2}x$
$x^3$       $x^2$
$\sin x$    $2x$
$x^2$       $\sin(x)$
$x^3$       $\sin(x)$


# II Hauptgang

1. Berechnen Sie die Ableitung anhand der Kettenregel. Die innere Funktion $v$ ist hier stets linear:

   \Begin{multicols}{2}
   a) $f(x)=\sqrt{3x+1}$
   b) $f(x)=\sin{(6x-1)}$
   c) $f(x)=\cos{(2x+3)}$
   d) $f(x)=\sin{(\pi x +2)}$
   e) $f(x)=\sqrt{-\frac{1}{2}x+1}$
   f) $f(x)=\sin{(-3x+5)}$

   \End{multicols}

2. Wenden Sie wieder die Kettenregel an:

   \Begin{multicols}{2}
   a) $a(x)=\sin(x^2)$
   b) $b(x)=\sin(\frac{1}{x})$
   c) $c(x)=\sin{(-x)}$
   d) $h(x)=\cos{(x^3)}$
   e) $g(x)=\cos{(3x^2-2x+5)}$
   f) $f(x)=\cos(\sqrt{x})$

   \End{multicols}


# III Dessert

Buntest Gemischtes - wenden Sie alle Ableitungsregeln an, die Sie kennen:

\Begin{multicols}{2}

a) $f(x)=\sin(x-1)\cdot x$
b) $f(x)=\sin(3x+1)\cdot\cos(x)$
c) $f(x)=\frac{1}{\sin(x)}$
c) $f(x)=\sin(-3x+1)\cdot\sqrt{x}$
d) $f(x)=\cos(\sqrt[3]{x})\cdot x$
e) $f(x)=\sin(x^2)\cdot\cos{(5x-3)}$
f) $f(x)=\frac{\cos(x)}{\sin(x)}$
g) $f(x)=\sqrt{\sin(x^2+5x)}$
c) $f(x)=\frac{1}{\sin(x^2)}$
f) $f(x)=\frac{\cos(3x+1)}{\sin(5x-1)}$

\End{multicols}

\vfill

p.s. Im Basisfach brauchen Sie wahrscheinlich nur die Vorderseite dieses Blattes... ;-)

