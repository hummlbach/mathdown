---
title: "3. Klassenarbeit"
author: "Name:"
date: 08.02.2022
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte, Integral, Fläche]
lang: de
header-center: "Analysis" 
footer-right: "Bearbeitungszeit 60 Minuten"
---

Der Graph $G_h$ der Funktion $h(t)=-2 \cos(\frac{\pi}{40}t)+7$ soll idealisiert die Höhe eines Flugzeugs im Parabelflug nach der Zeit darstellen. Die Zeit $t$ ist dabei in Sekunden angegeben, die Höhe in Vielfachen von $1000$ Metern.

\Begin{center}

![](parabelflug.png){}

\End{center}

1. Beschriften Sie die Achsen und geben Sie die Periode sowie die Amplitude von $h$ an.

2. Im Schaubild ist lila gekennzeichnet, wann die Flugzeuginsassen kaum noch Erdanziehungskraft spüren. Entnehmen Sie dem Schaubild diesen Zeitraum und seine Länge.

2. Wie würden Sie ohne Kenntnis des Schaubildes bestimmen, nach wievielen Sekunden das Flugzeug zum ersten mal die maximale Flughöhe erreicht? Führen Sie eine entsprechende Rechnung aus und geben sie die höchste, sowie die niedrigste (dargestellte) Flughöhe an.

3. Untersuchen Sie das Krümmungsverhalten von $G_h$: Wie groß wird der Steigungswinkel des Graphen $G_h$ maximal?

5. Interpretieren Sie, was die Steigung von $G_h$ für den Flug physikalisch bedeutet.

6. Berechnen Sie die Fläche zwischen $G_h$ und der $x$-Achse über dem Interval $[40;80]$.

7. Wie groß ist die durschschnittliche/mittlere Flughöhe?
   a) Über die gesamte Flugdauer (ohne Start und Landung)
   b) Während des Zeitraums aus 2., in dem im Flugzeug Mikrogravitation herrscht.


