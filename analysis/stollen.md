---
title: "Der Stollen"
author: [Klasse 13]
date: 04.02.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

Der Querschnitt eines Berstollens wird beschrieben durch einen Teil der $x$-Achse (Boden) und den Teil des Graphen der Funktion $f$ mit $f(x) = 8-\frac{1}{2}x^2$, der oberhalb der $x$-Achse verläuft (Stollenwände).

\Begin{center}

![](stollen.png)

\End{center}

1. Beschriften Sie die Achsen mit Namen und Achsenabschnitten und benennen Sie die Form, die die Stollenwand (im Querschnitt) bildet.
3. Geben Sie die $x$-Werte an, an denen die Stollenwände am steilsten verlaufen und Begründen Sie Ihre Angabe.
2. Berechnen Sie den Winkel, den die Wände und der Boden des Stollens einschließen.
4. Die Wand eines etwas kleineren Stollens weiter östlich ist gegeben durch den Graphen der Funktion $g(x)=-\frac{1}{2}x^2-10x-44$. Nutzen Sie die Ableitungen von $g$, um die Koordinaten des höchsten Punkts der Stollendecke anzugeben und den Querschnitt des Stollens zu zeichnen.
5. Der Bergstollen ist $50$m lang und läuft voll mit Wasser. Verwenden Sie das bestimmte Integral $\int_{-4}^4{f(x)dx}$ um das Wasservolumen im Stollen zu berechnen.
6. Eine Weile nach Inbetriebnahme der Pumpen, ist der Wasserstand im Stollen auf $3,5$m gesunken. Bestimmen Sie das Volumen des noch im Stollen verbliebenen Wassers.
7. Ein würfelförmiger Behälter soll in den Stollen gestellt werden. Ermitteln Sie die maximal mögliche Breite des Behälters.

