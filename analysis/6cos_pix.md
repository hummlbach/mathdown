---
title: "Übung zur mündlichen Abi-Prüfung"
author: [Klasse 12]
date: 08.02.2022
keywords: [Integral, Fläche, Cosinus, Steigung]
lang: de
header-center: "Analysis" 
---

\Begin{multicols}{2}

Inspiriert durch "Analysis A 8" aus "Erfolg im Mathe-Abi, Baden-Württemberg, Mündliche Prüfung Basisfach" - kaufen Sie dieses Buch!

Gegeben ist der Graph $G_f$ der Funktion
$$f(x) = 6 \sin (\pi\cdot x) \quad \text{ mit }\quad x \in [0;k]$$

![](6cos_pix.png)

\End{multicols}


a) Ermitteln Sie die Periode, die Amplitude, die Nullstellen von $f$ und den Wert $k$, der die rechte Intervallgrenze bestimmt.

b) Ergänzen Sie die $x$- und die $y$-Achse gemäß den Ergebnissen aus a) so, dass die vorgegebene Kurve $G_f$ den Graphen von $f$ darstellt.

c) Beschreiben Sie, wie $G_f$ aus dem Graphen der Funktion $g$ mit $g(x)=sin(x)$ hervorgeht.

d) Begründen Sie anhand der Abbildung, dass gilt: $\int_0^4 f(x) dx = 0$

e) Berechnen Sie die Fläche die $G_f$ und die $x$-Achse über dem Intervall $[2;3]$ einschließen.

f) Zeigen Sie rechnerisch, wo $f$ am stärksten wächst und geben Sie die Steigung von $G_f$ an dieser Stelle an.

\pagebreak

Die Funktionsvorschrift zu $G_h$ hat die Form $h(x)=2x(x-a)(x-b)$. 

\Begin{center}
![](2xhoch3-xhoch2-6x.png)
\End{center}

a) Entnehmen Sie dem Schaubild die Nullstellen von $h$, um $a$ und $b$ zu bestimmen.

b) Berechnen Sie $f(1)$ und $f(2)$ und verwenden Sie die Funktionswerte, um die $y$-Achse zu beschriften/skallieren.

c) Beschreiben Sie, wie Sie (ohne Kenntnis des Graphen, rechnerisch) die Extrema der Funktion $h$ ausfindig machen würden.

d) Untersuchen Sie bei welchem $x$-Wert aus dem Intervall $[-1;1,5]$ die Funktionswerte von $h$ am stärksten wachsen.

e) Nutzen Sie die Integralrechnung, um den zwischen $x$-Achse und $G_h$ eingeschlossenen Fläacheninhalt zu ermitteln. (Über dem Intervall $[-1,5;2]$)
