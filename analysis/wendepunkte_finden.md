---
title: "Wendepunkte finden"
author: [Klasse 12]
date: 22.09.2021
keywords: [Extrema, Minimum, Maximum, Terassenpunkt, Kurvendiskussion]
lang: de
header-center: "Analysis" 
---

![](wendepunkt_flow.png){height=560px}
