---
title: "Drei Graphen und fünf Flächen"
author: [Klasse 12]
date: 25.02.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

Gegeben sind die drei Funktionen

- $f(x) = 2\sqrt{2x+4}$
- $g(x) = -\left(\frac{8}{27}x-2\right)^3+2$
- $h(x) = 5 \sin\left(\frac{\pi}{4}x\right)$

Ihre Graphen sind im folgenden Schaubild zu sehen:

\Begin{center}

![](drei_graphen_zum_integrieren.png){height=500px}

\End{center}

Berechnen Sie den Inhalt der Fläche...

a) die vom Graphen von $g$, der $x$-Achse und der $y$-Achse eingeschlossen wird.
b) die vom Graphen von $f$, der $x$-Achse und der $y$-Achse eingeschlossen wird.
c) die vom Graphen von $f$, der $x$-Achse und der Gerade $a$ begrenzt wird.
c) die von $G_g$, der $x$-Achse, sowie den Gerade $a$ und $b$ begrenzt wird.
d) zwischen $G_h$ und der $x$-Achse über dem Intervall $[3;8]$.

