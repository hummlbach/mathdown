---
title: "Bakterielles Wachstum"
author: [Klasse 12]
date: 08.04.2022
keywords: [Logarithmen]
lang: de
header-center: "Analysis" 
---

In einem Labor wird erforscht, wie sich Bakterien entwickeln. Betrachtet wird der Flächeninhalt der von den Bakterien eingenommenen Fläche. Bei ungehinderter Vermehrung wird der Flächeninhalt während der ersten zwölf Stunden beschrieben durch die Funktion $f$ mit
$$f(t) = 20\cdot 1,5^{t} \quad \text{($t$ in Stunden nach Beobachtungsbeginn, $f(t)$ in Quadtratmillimetern).}$$

a) Bestimmen Sie den Flächeninhalt drei Stunden nach Beobachtungsbeginn.
b) Berechnen Sie den Zeitpunkt, zu dem sich der Flächeninhalt im Vergleich zum Beobachtungsbeginn verdreifacht hat.
c) Fertigen Sie eine Skizze des Graphen von $f$ an.
d) Erläutern Sie im Sachzuammenhang, was durch die folgende Gleichung berechnet wird:
   $$f(t+2) = 2 \cdot f(t)$$
e) Erläutern Sie, wie man die momentane Änderungsrate des Flächeninhalts zwei Stunden nach Beobachtungsbeginn bestimmen kann.
f) Beurteilen Sie die Aussage: "Jede ganzrationale Funktion 4. Grades hat eine Extremstelle"

Frei nach Analysis A13 aus "Erfolg im Mathe-Abi (Baden-Württemberg), Mündliche Prüfung Basisfach" erschienen im Freiburger Verlag.
