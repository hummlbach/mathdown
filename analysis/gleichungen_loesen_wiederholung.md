---
title: "Gleichungen & Nullstellen"
author: [Klasse 13]
date: 17.09.2022
keywords: [Nullstellen, Satz vom Nullprodukt, Mitternachtsformel]
lang: de
header-center: "Analysis" 
geometry:
 - top=3cm
 - bottom=3.5cm
 - left=2cm
 - right=2cm
---

## 0 Quadratische Gleichungen

Naja Mitternachtsformel oder Satz von Vita oder... (siehe auch "Nullstellen & Vorzeichenuntersuchung") \newline
Wir skippen das hier...

## I Exponentialgleichungen

Exponentialgleichungen werden durch Logarithmieren (Umkehrfunktion der Exponentialfunktion) gelöst:

\Begin{multicols}{2}
**Beispiel 1:**
$$\Begin{array}{rll}
5e^{4x+3} -2 &= 1 \quad &| \, +2 \\
5e^{4x+3}  &= 3 \quad &| \, :5 \\
e^{4x+3}  &= \frac{3}{5} \quad &| \, \ln{(\cdot)} \\
4x+3 &= \ln{(\frac{3}{5})} \quad &| \, -3 \\
4x &= \ln{(\frac{3}{5})} -3 \quad &| \, :4 \\
x &= \frac{\ln{(\frac{3}{5})} -3}{4} \approx -0,88 & \\
\End{array}
$$

**Übung 1:** $2e^{3x-4}+5 = 7$

**Beispiel 2:**
$$\Begin{array}{rll}
e^{(x^2-4)} &= 1 \quad &| \, \ln{(\cdot)} \\
x^2-4 &= \ln{(1)} = 0 \quad &
\End{array}
$$

Durch Mitternachtsformel, 3. binomische Formel oder Wurzelziehen erhält man $x=\pm 2$...\newline
.oO(Okay, hätte man so sehen können, aber wenn auf der rechten Seite Drölf oder Einunddranzig steht...!?!)

**Übung 2:** $e^{(x^2-7x+15)} = 20$

\End{multicols}

## II Lösung durch Substitution

Manchmal kann man eine Gleichung (von der man erstmal nicht weiß wie man sie löst) vereinfachen, indem man einen Ausdruck (z.B. $x^2$) durch einen Buchstaben (z.B. $z$) ersetzt, sodass sich eine Gleichung ergibt, für die ein Lösungsverfahren bekannt ist.

\Begin{multicols}{2}

**Beispiel 1 (Polynom mit geraden Exponenten):**
$$\Begin{array}{rll}
x^4+x^2+19 &= 1 \quad &| \, z=x^2 \\
z^2+z-19 &= 1 \quad &| \, -1 \\
z^2+z-20 &= 0 \quad &| \, \text{(Satz von Vieta)} \\
(z-4)(z+5) &= 0 \quad &\\
\End{array}
$$
D.h. $z_1=4$ und $z_2=-5$, also $x^2 = 4$. Somit ist $x_1=2$ und $x_2=-2$. Wäre $z_2$ auch positiv ergäben sich zwei weitere Lösungen der Gleichung. Wären sowohl $z_1$ als auch $z_2$ negative gäbe es keine Lösung.

**Übung 1:** $\frac{1}{5}x^4-x^2+1 = -0,2$\newline

**Beispiel 2 ($e$-Funktion mit geraden Exponenten):**
$$\Begin{array}{rll}
e^{2x}-6e^x &= -8 \quad &| \, z=e^x \\
z^2-6z &= -8 \quad &| \, +8 \\
z^2-6z + 8 &= 0 \quad &| \, \text{(Satz von Vieta)} \\
(z-4)(z-2) &= 0 \quad &\\
\End{array}
$$
D.h. $z_1=4$ und $z_2=2$ und wir müssen noch resubstituieren. Es ist also $e^x = 4$ beziehungsweise $e^x=2$ und damit $x_1=\ln{(2)}$ und $x_2=\ln{(4)}$.

**Übung 2:** $e^{2x}+2e^x-3 = 0$ \newline

\End{multicols}

## III Trigonometrische Gleichungen

\Begin{multicols}{2}

**Beispiel 1 (Cosinus ungestaucht in $x$-Richtung):**
$$\Begin{array}{rll}
4\cos{(x)} - 3&= 0 \quad &| \, +3 \\
 \cos{(x)} &=  3 \quad &| \, :4 \\
\cos{(x)} &=  \frac{3}{4} \quad &| \, \arccos{(\cdot)} \\
   x_0 &=  \arccos{(\frac{3}{4})} \approx 0,72& 
\End{array}
$$

$\text{ }$ \newline

Der Arcuscosinus liefert die eindeutige Lösung aus dem Intervall $[0,\pi]$.
Da der Cosinus spiegelsymmetrisch zu $y$-Achse ist, ist auch $x_1=-\arccos{(\frac{3}{4})}$ eine Lösung der Gleichung. Aufgrund der Periodizität des Cosinus lösen also alle $x$-Werte der folgenden Form die Gleichung:
$$2k\pi\pm\arccos{(\frac{3}{4})}$$

\End{multicols}

\Begin{center}
![](cosinusgleichung.png){height=180px}
\End{center}

\Begin{multicols}{2}

**Beispiel 2 (Sinus ungestaucht in $x$-Richtung):**
$$\Begin{array}{rll}
3\sin{(x)} - 2&= -1 \quad &| \, +2 \\
3\sin{(x)} &=  1 \quad &| \, :3 \\
\sin{(x)} &=  \frac{1}{3} \quad &| \, \arcsin{(\cdot)} \\
   x &=  \arcsin{(\frac{1}{3})} \approx 0,34 & 
\End{array}
$$

Nun haben wir _eine_ Lösung $x_0 = \arcsin{(\frac{1}{3})}$ der Gleichung (nämlich die zwischen $-\frac{\pi}{2}$ und $\frac{\pi}{2}$) gefunden.
Wir müssen jetzt noch der Periodizität des Sinus rechnungtragen...
Der (in $x$-Richtung) ungestauchte Sinus hat eine Periode von $2\pi$, d.h. auch die $x$-Werte $\arcsin{(\frac{1}{3})}+k\cdot 2\pi$ lösen die Gleichung. \newline


Außerdem ist der Sinus symmetrisch zu $x=\frac{\pi}{2}$ und somit ist auch $x_1 = \frac{\pi}{2} + (\frac{\pi}{2} - x_0) = \pi - x_0$ eine Lösung der Gleichung. Und wieder müssen wir die Periodizität berücksichtigen, d.h. alle $x$-Werte der Form $\pi - x_0 + k\cdot 2\pi$ lösen ebenfalls die Gleichung. **Puh!!**

![](sinusgleichung.png)

\End{multicols}

Wir fassen das Beispiel nochmal zusammen. Die Lösungen der Gleichung sind also 
$$2k\pi+\arcsin{(\frac{1}{3})} \quad \text{ und } \quad (2k+1)\pi - \arcsin{(\frac{1}{3})}$$

\Begin{multicols}{2}
**Übung 1:** $42\cos{(x)} -23 = 0$ \newline
**Übung 2:** $\frac{1}{2}\sin{(x)} - 1 = 0$
\End{multicols}


\Begin{multicols}{2}
**Beispiel 3 (Beliebiger Sinus):**
$$\Begin{array}{rll}
5\sin{(4x-3)} + 2&= 1 \quad &| \, -2 \\
5\sin{(4x-3)} &=  -1 \quad &| \, :5 \\
\sin{(4x-3)} &=  -\frac{1}{5} \quad &| \, \arcsin{(\cdot)} \\
4x-3 &=  \arcsin{(-\frac{1}{5})} \quad & \\
\End{array}
$$

$\text{ }$ \newline
Wir erinnern uns ans vorherige Beispiel. Neben $\arcsin{(-\frac{1}{5})}$ könnte auf der rechten Seite also auch $2k\pi + \arcsin{(-\frac{1}{5})}$ oder $(2k+1)\pi- \arcsin{(-\frac{1}{5})}$ stehen.
\End{multicols}

Auf beiden Seiten $3$ addiert und durch $4$ geteilt ergeben sich schlussendlich insgesamt die Lösungen $$\frac{2k\pi+\arcsin{(-\frac{1}{5})} + 3}{4} \quad \text{ sowie } \quad \frac{(2k+1)\pi- \arcsin{(-\frac{1}{5})} +3}{4}$$

\Begin{multicols}{2}
**Übung 3:** $3\cos{\left(\frac{\pi}{6}(x-3)\right)}+\frac{1}{2} = 2$ \newline
**Übung 4:** $2\sin{\left(\frac{\pi}{4}x\right)}+3 = 5$
\End{multicols}


## IV Satz vom Nullprodukt

Ein Produkt $a\cdot b$ ist genau dann $0$ wenn $a=0$ oder $b=0$ ist. Entsprechend ist das Produkt zweier Funktionen $f(x)\cdot g(x)$ genau bei den $x$ gleich $0$ für die entweder $f(x)=0$ oder $g(x) = 0$ ist.

\Begin{multicols}{2}

**Beispiel 1 (Produkt von Funktionen):** \newline
$$\Begin{array}{rclll}
(x-1)&\cdot& (e^{2x+1}-1)&= 0 \, & \\
x-1 = 0 &\vee& e^{2x+1}-1 &= 0\, &| \, +1 \\
x = 1 &\vee&  e^{2x+1} &= 1\, & \\
x = 1 &\vee& 2x+1 &= 0\, & \\
x = 1 &\vee& x &= -\frac{1}{2}\, & \\
\End{array}
$$

**Übung 1:** $(x^2-9)\cdot \cos{(2x)} = 0$

**Beispiel 2 (Quadratisches Polynom mal $x^k$):** \newline
$$\Begin{array}{rll}
x^5-2x^4-3x^3 &= 0 &| \, x^3 \text{ ausklammern} \\
x^3\cdot (x^2-2x-3) &= 0 &| \, \text{S. v. Nullprodukt} \\
x^3 = 0 \text{ oder } x^2-2x-3 &= 0 &| \, \text{Satz von Vieta} \\
x_1 = 0,  x_2=1, x_3 =-3 & & \\
\End{array}
$$

**Übung 2:** $(x^4-3x^3+2x^2)\cdot (x^2-9) = 0$

\End{multicols}


