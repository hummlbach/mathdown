---
title: "Vom Differenzen- zum Differentialquotient"
author: [Klasse 11]
date: 21.05.2021
keywords: [Funktionen, Steigung, Differenzenquotient, Differentialquotient]
lang: de
header-center: "Analysis" 
---

## Die Ableitung von $f(x)=x^2$

   \Begin{multicols}{2}
   ![](parabel_goes_diffquot1.png){}
   ![](parabel_goes_diffquot2.png){}
   \End{multicols}

   \Begin{multicols}{2}
   ![](parabel_goes_diffquot3.png){}
   ![](parabel_goes_diffquot4.png){}
   \End{multicols}

1. Berechnen Sie die Steigung von $g_1$.
2. Berechnen Sie die Steigung von $g_2$.
3. Berechnen Sie die Steigung von $g_3$.
4. Was ist die Steigung von $t$?

