---
title: "Integralrechnung Einstieg"
author: [Klasse 12]
date: 13.01.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

# I Starters

 
Skizzieren und Berechnen Sie die Fläche zwischen $x$-Achse und dem Graphen von der $y$-Achse bis zum angebenen $x$-Wert $x_1$.

   $f(x)$          $x_1$
   ----            ---- 
   $4x$            $2$       
   $\frac{1}{2}x$  $5$
   $x+1$           $4$
   $2x+1$          $2$
   $3x+2$          $2$
   $-2x+6$         $2,5$
   $-x^2+4$        $2$
   $-x^3+8$        $1$
   $sin(x)$        $\pi$
   $-3$            $4$
   $x-4$           $2$, $4$, $6$ und $8$
   $x-3$           $5$

\pagebreak

# II Hauptgang

Skizzieren und Berechnen Sie die Fläche zwischen $x$-Achse und dem Graphen vom Anfangs-$x$-Wert $a$ bis zum End-$x$-Wert $b$.

   $f(x)$               $a$                 $b$
   ----                 ----                ---
   $2x$                 $1$                 $3$
   $-x^2+9$             $2$                 $3$
   $-\frac{1}{2}x^2+4x$ $3$                 $6$
   $-2x$                $-2$                $0$
   $3x^2$               $-1$                $0$
   $x+1$                $-1$                $2$
   $-x^2+4$             $-1$                $2$
   $-x^4+8$             $-1$                $1$
   $cos(x)$             $\frac{\pi}{2}$     $\pi$


# III Dessert

Zeichnen Sie den Graph der Funktion $f(x) = -\frac{1}{10}x^4+x^3-\frac{12}{5}x^2$ (Wo sind Nullstellen, Extrema und Wendepunkte?) und berechnen Sie die vom Graphen $G_f$ und der $x$-Achse eingeschlossene Fläche (zwischen $x=0$ und $x=6$).
