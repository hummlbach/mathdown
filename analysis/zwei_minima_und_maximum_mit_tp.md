---
title: "Noch zwei Kurvendiskussionen"
author: "Klasse 12"
date: 28.09.2021
keywords: [Ableitung, Extrema, Minimum, Wendepunkte]
lang: de
header-center: "Analysis" 
---

1. Die Funktion

   $$f(x) = \frac{(x^2-4)(x^2-64)}{4x^2}$$

   soll untersucht und ihr Graph $G_f$ gezeichnet werden.

   a) Geben Sie die Definitionsmenge von $f$ an.
   b) Berechnen Sie die Schnittpunkte von $G_f$ mit der $x$-Achse.
   c) Multiplizieren Sie den Funktionsterm aus, um dann $f'$ und $f''$ zu berechnen.
   d) Berechnen Sie die Koordinaten der Extrempunkte und geben Sie die Art der Extrema an.
   e) Untersuchen Sie den Graph von $f$ auf Wendepunkte.
   f) Berechnen Sie $f(1,5)$ und $f(-1,5)$ sowie $f(10)$ und $f(-10)$ und zeichnen Sie $G_f$.

2. Gegeben sei die Funktion $h$ und ihre erste Ableitung $h'$ wie folgt:

   $$\begin{array}{rr}
   h(x) =& \frac{1}{3}(6-x)(x-2)^3 \\
   h'(x) =& -\frac{4}{3}(x-2)^2(x-5)
   \end{array}
   $$

   a) Geben Sie die Schnittpunkte des Graphs von $h$, genannt $G_h$, mit den Koordinatenachsen an.
   b) Zeigen Sie an welcher Nullstelle von $h'$ ein Hochpunkt, an welcher ein Tiefpunkt und an welcher ein Terrassenpunkt von $G_h$ zu finden ist.
   c) Legen Sie dar, dass $G_h$ zwei Wendepunkte hat und berechnen Sie deren Koordinaten.
   d) Berechnen Sie noch $h(6,5)$ und zeichnen Sie den Graph von $h$.
