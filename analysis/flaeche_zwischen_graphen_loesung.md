---
title: "Lösungen zu Fläche zwischen Graphen"
author: [Klasse 12]
date: 29.03.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
geometry:
  - top=3cm
  - bottom=3cm
  - left=3cm
  - right=3cm
---

5. Der Scheitelpunkt der abgebildeten Parabel liegt bei $S(-2/5)$. (D.h. die Parabel ist im Vergleich zur Normalparabel um $2$ nach rechts und um $5$ nach oben verschoben.) Außerdem ist die Parabel nach unten geöffnet. Wir starten mit der Scheitelpunktsform und erhalten folgende Gleichung der Parabel: $q(x)=-(x+2)^2+5 = -(x^2+4x+4)+5 = -x^2-4x-4+5 = -x^2-4x+1$.

   a) Wir berechnen die Fläche, die von $G_q$ und $G_{p_1}$ eingeschlossen wird.
      - Gleichsetzen um Schnittpunkte der Graphen zu finden:
        $$
        \Begin{array}{rll}
        -x^2-4x+1 &= x^2+1 \quad &| -x^2-1 \\
        -2x\cdot(x+2) = -2x^2 -4x &= 0 & \\
        x = -2 \text{ oder } x=0 &
        \End{array}
        $$
      - Die Differenz der Funktionen ist: $h(x) = q(x)-p_1(x) = -2x^2-4x$
      - Deren Stammfunktion ist $H(x)=-\frac{2}{3}x^3-2x^2$
      - Wir Integrieren über $h(x)$ von $0$ bis $-2$:
        $$\int_{-2}^0 h(x) dx = \left[-\frac{2}{3}x^3-2x^2\right]_{-2}^0 = 0-\left(-\frac{2}{3}(-2)^3-2\cdot (-2)^2\right) = 8-\frac{16}{3} = 2,\bar{6}...$$
   b) - $G_q \cap G_{p_2}$: 
        $$
        \Begin{array}{rll}
        -x^2-4x+1 &= -\frac{1}{4}x^2-x+1 \quad &| +\frac{1}{4}x^2+x-1 \\
        -3x\cdot\left(\frac{1}{4}x+1\right)=-\frac{3}{4}x^2-3x &= 0 & \\
        x = -4 \text{ oder } x=0 &
        \End{array}
        $$
      - Differenz: $h(x) = q(x)-p_2(x) =-\frac{3}{4}x^2-3x$
      - Stammfunktion: $H(x)=-\frac{1}{4}x^3-\frac{3}{2}x^2$
      - Integrieren:
        $$\int_{-4}^0 h(x) dx= \left[-\frac{1}{4}x^3-\frac{3}{2}x^2\right]_{-4}^0 = 0-\left(-\frac{1}{4}(-4)^3-\frac{3}{2}\cdot (-4)^2\right) = -16+24 = 8$$
   c) - $G_q \cap G_{p_3}$: 
        $$
        \Begin{array}{rll}
        -x^2-4x+1 &= \frac{1}{2}x^2+2x+1 \quad &| -\frac{1}{2}x^2-2x-1 \\
        -3x\cdot\left(\frac{1}{2}x+2\right)=-\frac{3}{2}x^2-6x &= 0 & \\
        x = -4 \text{ oder } x=0 &
        \End{array}
        $$
      - Differenz: $h(x) = q(x)-p_2(x) =-\frac{3}{2}x^2-6x$
      - Stammfunktion: $H(x)=-\frac{1}{2}x^3-3x^2$
      - Integrieren:
        $$\int_{-4}^0 h(x) dx = \left[-\frac{1}{2}x^3-3x^2\right]_{-4}^0 = 0-\left(-\frac{1}{2}(-4)^3-3\cdot (-4)^2\right) = -32+48=16$$

6. a) Zur Diskussion der Kurve $G_h$ brauchen wir die Ableitungen von $h$. \newline
      $h'(x) = \frac{3}{4}x^2-2x$; \newline
      $h''(x) = \frac{3}{2}x-2$; \newline
      $h'''(x) = \frac{3}{2}$;
      - Extrempunkte:
        - $h'(x) = 0 \Leftrightarrow 0 = \frac{3}{4}x^2-2x = x \cdot \left( \frac{3}{4}x-2\right) \Leftrightarrow x=0 \text{ oder } x=\frac{8}{3}$
        - $h''(0) = -2$ und $h''(\frac{8}{3}) = \frac{3}{2}\frac{8}{3} -2 = 4-2=2$ 
        - $h(0) = 4$ und $h(\frac{8}{3}) = \frac{128}{27}-\frac{64}{9} +4 = -\frac{64}{27} +4 = 2-\frac{10}{27} \approx 1,\bar{6}...$
        - Daraus folgt: $Max(0|4)$ und $Min(\frac{8}{3}|1,\bar{6}...)$
      - Wendepunkte:
        - $h''(x) = 0 \Leftrightarrow \frac{3}{2}x-2 = 0 \Leftrightarrow x = \frac{4}{3}$
        - $h'''(\frac{4}{3}) = \frac{3}{2} \neq 0$
        - $h(\frac{4}{3}) = \frac{16}{27}-\frac{16}{9}+4 = \frac{16}{27}-\frac{48}{27}+4 = -\frac{32}{27}+4 \approx 3$
        - Es gibt also einen Wendepunkt ungefähr hier: $WP(\frac{4}{3}|2,8)$

      \Begin{center}
      ![](flaeche_zw_polynom_und_winkelhalbierender.png){height=200px}
      \End{center}

   b) - Wir müssen von Schnittpunkt zu Schnittpunkt integrieren, wissen aber nicht, wie wir die Gleichung $\frac{1}{4}x^3-x^2 + 4 = x$ dazu lösen können. Wir lesen die Schnittpunkte daher aus dem Schaubild. Die aus der Kurvendiskussion resultierende grobe Zeichnung, lässt Schnittpunkte von $G_h$ und $y=x$ bei $x=\pm 2$ und  $x=4$ vermuten. Dies kann noch durch einsetzen dieser $x$-Werte in $h(x)$ überprüft werden ($h(-2) = \frac{1}{4}\cdot(-2)^3-(-2)^2+4 = -2$, $h(2) = \frac{1}{4}\cdot 2^3-2^2+4 = 2$, $h(4)=\frac{1}{4}4^3-4^2+4 = 4$).
      - Die Differenz der Funktionen ist $g(x) = h(x)-x = \frac{1}{4}x^3-x^2-x+4$
      - Stammfunktion: $G(x) = \frac{1}{16}x^4-\frac{1}{3}x^3-\frac{1}{2}x^2+4x$
      - $\int_{-2}^2 g(x) dx = \left[\frac{1}{16}x^4-\frac{1}{3}x^3-\frac{1}{2}x^2+4\right]_{-2}^2 = \frac{1}{16}(-2)^4-\frac{1}{3}(-2)^3-\frac{1}{2}(-2)^2+4 - \left( \frac{1}{16}\cdot 2^4-\frac{1}{3}\cdot 2^3-\frac{1}{2}\cdot 2^2+4 \right)= 1+\frac{8}{3} - \frac{1}{2}\cdot 4 + 4 - 1+\frac{1}{3} \cdot 8 +2 - 4 = \frac{16}{3} = 5,\bar{3}...$
      - $\int_{2}^4 g(x) dx = \left[\frac{1}{16}x^4-\frac{1}{3}x^3-\frac{1}{2}x^2+4\right]_{2}^4 = \frac{1}{16}4^4-\frac{1}{3}4^3-\frac{1}{2}4^2+4 - \left( \frac{1}{16}\cdot 2^4-\frac{1}{3}\cdot 2^3-\frac{1}{2}\cdot 2^2+4 \right)= 16 - \frac{64}{3} - 8 + 4 - 1 + \frac{8}{3} + 2 - 4 = 9 - \frac{56}{3} = -9,\bar{6}...$
      - Der gesuchte Flächeninhalt ist demnach $A=5,\bar{3}... + 9,\bar{6}... = 15$
