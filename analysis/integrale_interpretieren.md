---
title: "Integrale interpretieren"
author: [Klasse 12]
date: 27.04.2022
keywords: [Stammfunktion, Integral, Fläche, Mittelwert, Rekonstruierter Bestand]
lang: de
header-center: "Analysis" 
---

## Starters

1. Es gilt: $\int_{-\frac{\pi}{2}}^\frac{\pi}{2} \cos(x)dx = 2$.

   \Begin{center}

   ![](cos_-5_to_5.png){height=120px}

   \End{center}

   Bestimmen Sie ohne Rechnung folgende Integrale und geben Sie eine kurze Begründung für Ihre Antwort an:
   \Begin{multicols}{3}
   a) $\int_0^\frac{\pi}{2} \cos(x)dx$
   b) $\int_0^\frac{\pi}{2} 2\cdot \cos(x)dx$
   c) $\int_0^\pi \cos(x)dx$

   \End{multicols}

2. Gegeben ist der Graph einer Funktion $f$:

   \Begin{center}

   ![](minus_x_squared_plus_4.png){height=160px}

   \End{center}

   Begründen Sie, dass gilt: $\int_0^2 f(x) dx \approx 5$.

3. Erläutern Sie anhand einer Skizze, ob der Wert des Integrals $\int_0^{\frac{3}{2}\pi} \sin(x)dx$ größer, kleiner oder gleich Null ist.

4. Gegeben ist der Graph der Funktion $f(x)=e^x$.

   \Begin{center}

   ![](e-funktion.png){height=140px}

   \End{center}

   Begründen Sie ohne Rechnung, dass gilt: $\int_{-1}^0 e^x dx < \int_0^1 e^x dx$.

## Hauptgang

1. Die Produktionskosten eines Werkstücks in Abhängigkeit von der produzierten Stückzahl werden durch die Funktion $P$ mit $P(x) = 20 + 10\cdot e^{-0,5x}$; $x \ge 0$ beschrieben. \newline ($x$: Stückzahl, $P(x)$: Herstellungskosten des $x$-ten Werkstücks in Euro.) \newline
   Erläutern Sie, was durch das Integral
   $$\int_0^{50} \left(20+10\cdot e^{-0,5x}\right)dx$$
   berechnet wird.

2. Die momentane Änderungsrate des Wasservolumens in einem Wassertank wird modellhaft beschrieben durch die Funktion $f$ mit $f(t)=(t^2-15t+44)\cdot e^{0,2t}$ ($t$ in Tagen, $f(t)$ in Liter pro Tag).

   Erläutern Sie die Bedeutung der Rechnung $\int_0^{12}\left(t^2-15t+44\right) \cdot e^{0,2t}dt \approx -128,5$ im Sachzusammenhang.

3. Die momentane Zuwachsrate von Bakterien wird modellhaft beschrieben durch die Funktion $f(t) = 300\cdot e^{0,02t}$ ($t$ in Tagen, $f(t)$ in Anzahl der Bakterien pro Tag). Zu Beginn gibt es $500$ Bakterien.
   a) Erläutern Sie die Bedeutung des Integrals $\int_0^{30} 300 \cdot e^{0,02t} dt$.
   b) Geben Sie einen Rechenausdruck an, wie man die Anzahl der Bakterien nach $15$ Tagen berechnen kann.

4. Die wöchentlichen Verkaufszahlen von Zahnpasta in einem Supermarkt werden durch die Funktion $f(t)$ beschrieben. \newline
   Dabei ist $t$ die Anzahl der Wochen ab dem 1. Januar eines Jahres. \newline
   Erläutern Sie die Bedeutung folgender Integrale:
   $$\int_0^{52}f(t)dt \text{ und } \frac{1}{52}\cdot \int_0^{52}f(t)dt$$

Diese Beispiele sind dem Buch "Erfolg im Mathe-Abi (Baden-Württemberg), Grundwissen Basisfach 2022", erschienen im Freiburger Verlag, entnommen.
