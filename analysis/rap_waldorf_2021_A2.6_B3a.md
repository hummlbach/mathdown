---
title: "Waldorf RAP 2021 A2.6 & B2a"
author: [Klasse 12]
date: 21.11.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Graph]
lang: de
header-center: "Analysis" 
---

Eine Funktion $f$ hat die Gleichung:

$$f(x)=\frac{1}{12}x^3-x^2+3x$$

Ihr Schaubild heiße $K_f$.

a) Berechnen Sie die Funktionswerte bei $x_0=0$, $x_1=1$ und $x_2=2$.
b) Geben Sie die Schnittpunkte von $K_f$ mit den Koordinatenachsen an.
c) Die Ursprungsgerade $g$ hat die Steigung $m=\frac{1}{3}$. Die Gerade $g$ schneidet $K_f$ in drei Punkten. Berechnen Sie deren Koordinaten.
e) Geben Sie die Koordinaten und die Art der Extrema von $f$ an.
d) Zeigen Sie, dass einer der Schnittpunkte von $g$ und $K_f$ der Wendepunkt von $K_f$ ist.
b) Die Tangente $t$ an $K_f$ hat den Berührpunkt $B(1|f(1))$. Stellen Sie die Geradengleichung von $t$ auf.
f) Zeichnen Sie $t$, $K_f$ und $g$ in ein rechtwinkliges Koordinatensystem ein.

