---
title: "Soziales Netzwerk"
author: [Klasse 12]
date: 08.05.2022
keywords: [Exponentialfunktion]
lang: de
header-center: "Analysis" 
geometry:
  - top=3.5cm
  - bottom=4cm
  
---

Der Graph $G_f$ der Funktion $f(t)=0,0063\cdot t^6\cdot e^{-0,25t}$ ($t \ge 4$) stellt näherungsweise die Anzahl der Benutzer eines bekannten sozialen Netzwerks über die Zeit dar. (Benutzer in Millionen, Zeit in Jahren ab dem Jahr $2000$.) Die Kurve ab 2015 ist angelehnt an eine Prognose von Randall Munroe aus dem Jahr 2014, basierend auf den Schicksalen früherer sozialer Netzwerke dar. (Siehe auch "What if?" erschienen im Knaus Verlag)

![](facebook_user.png)

a) Geben Sie die Anzahl der Benutzer des sozialen Netzwerks zu Beginn des Jahres 2010 an. \newline
   Bestimmen Sie das Jahr, in dem die Nutzerzahl die $1$-Milliarde-Marke überschreitet.

b) Zeigen Sie, dass die erste Ableitung von $f$ gegeben ist durch:
   $$f'(t) = 0,0063\cdot (6t^5-0,25t^6)\cdot e^{-0,25t}$$
   Ermitteln Sie die momentane Änderungsrate der Nutzerzahlen zu Beginn des Jahres 2014.
   Berechnen Sie die hypothetische Anzahl der Nutzer zu Beginn diesen Jahres, wenn sich die Nutzerzahlen mit der Rate von Anfang 2014 weiterentwickelt hätten.

c) Die zweite Ableitung von $f$ ist gegeben durch $f''(t)=0,0063\cdot (\frac{1}{16}t^6-3t^5+30t^4)\cdot e^{-0.25t}$.
   Berechnen Sie die maximale Nutzerzahl gemäß der Prognose. \newline

e) Formulieren Sie eine Fragestellung im Sachzusammenhang, die auf die Gleichung \newline $f(t+2)-f(t)=500$ führt. \newline

d) Zeigen Sie, dass bis 2014 der Nutzerzuwachs (=Änderungsrate) von Jahr zu Jahr immer größer wird und ab 2014 rückläufig ist. \newline
   Geben Sie den Zeitpunkt der größten momentanen Nutzereinbußen an.

\pagebreak

Tipps:

a) $f(10)$ und dann graphisch $t$ finden wo $f(t)=1000$ 
b) $f$ ableiten, $f'(14)$, Tangente am Punkt $(14,f(14))$ aufstellen und $22$ einsetzen
c) $f'(t)=0$ setzen, $f''(t) >0$ bei gefundenem $t$ zeigen und $f(t)$ ausrechnen
d) $t$ ist der Zeitpunkt, von dem an die Zahl der Nutzer binnen $2$ Jahren um $500$ Millionen wächst.
e) Vorzeichenuntersuchung der zweiten Ableitung, um zu zeigen wo die Änderungsrate wächst und wo sie fällt.
   Alternativ argumentieren Sie, dass die Änderungsrate in den Wendepunkten minimal/maximal wird. Vor/zwischen/nach den Wendepunkten muss die Änderungsrate dann entsprechend fallen/wachsen. \newline

