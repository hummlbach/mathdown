---
title: "Monotonie, Extrempunkte, Krümmung"
author: [Klasse 11]
date: 20.05.2021
keywords: [Funktionen, Monotonie, Extrempunkte, Krümmung]
lang: de
header-center: "Analysis" 
---
1. Stellen Sie sich die Funktionsgraphen als Flugbahn, in Seitenansicht, vor (die von links nach rechts abgeflogen wird).

   \Begin{multicols}{2}
   ![](symmetrie_1.png){}
   ![](symmetrie_2.png){}
   \End{multicols}

   \Begin{multicols}{2}
   ![](symmetrie_3.png){}
   ![](symmetrie_4.png){}
   \End{multicols}

a) Markieren Sie die Teile der Graphen grün, die einen Steigflug darstellen.
b) Markieren Sie alle Teile blau, die in diesem Bild einen Sinkflug bedeuten.
c) Färben Sie alle Stellen rot, an denen das Flugzeug weder sinkt noch steigt.
d) Schreiben Sie die Koordinaten der höchsten und der tiefsten Punkte der Graphen auf.

\pagebreak

2. Stellen Sie sich die Funktionsgraphen nun als Kurvenfahrt (Draufsicht von oben) vor. Der Graph wird so abgefahren, dass die $x$-Werte immer größer werden.

   \Begin{multicols}{2}
   ![](symmetrie_1.png){}
   ![](symmetrie_2.png){}
   \End{multicols}

   \Begin{multicols}{2}
   ![](symmetrie_3.png){}
   ![](symmetrie_4.png){}
   \End{multicols}

a) Markieren Sie alle Rechtskurven rot.
b) Färben Sie alle Linkskurven blau.
c) An welchen Stellen fährt man (zumindest kurz) gerade aus?

