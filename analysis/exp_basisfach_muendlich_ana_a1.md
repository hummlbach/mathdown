---
title: "Analysis A1"
author: [Klasse 12]
date: 26.04.2022
keywords: [Logarithmen]
lang: de
header-center: "Analysis" 
---

## Pflichtteil (HMF)

Eine Funktion $g$ ist durch $g(x)=2\cdot e^{\frac{1}{2}x}-1$ gegeben.

a) Ermitteln Sie die Nullstelle der Funktion $g$.
b) Die Tangente an den Graphen von $g$ im Punkt $S(0|1)$ begrenzt mit den beiden Koordinatenachsen ein Dreieck. \newline
   Weisen Sie nach, dass dieses Dreieck gleichschenklig ist.

## Wahlteil (mit WTR und Merkhilfe)

Gegeben ist die Funktion $f$ durch $f(x)=3-\frac{1}{2}e^{-x}$; $x\in \mathbb{R}$. Ihr Graph sei $K_f$.

\Begin{center}

![](abi_bw_basis_muendlich_ana_a1.png)

\End{center}

a) Berechnen Sie die Schnittpunkte von $K_f$ mit den Koordinatenachsen und bestimmen Sie die Gleichung der Asymptote von $K_f$.
b) Beschreiben Sie, wie $K_f$ aus dem Graphen der Funktion $e^x$ hervorgegangen ist.
c) Begründen Sie (mit einer Rechnung), dass $f$ streng monoton wachsend ist.
d) Erläutern Sie, wie man den Flächeninhalt der Fläche zwischen dem Graphen von $f$ der Geraden mit der Gleichung $y=3$, der $y$-Achse und der Geraden $x=4$ bestimmen kann.
e) Die Gerade mit der Gleichung $y=\frac{1}{2}x+ \frac{5}{2}$ ist eine Tangente an $K_f$. Berechnen Sie die Koordinaten des zugehörigen Berührpunktes $B$.
f) Beurteilen Sie die Aussage: "Es gibt ganzrationale Funktionen vierten Grades, deren Graphen drei Wendepunkte besitzen."

Frei nach "Analysis A1" aus "Erfolg im Mathe-Abi (Baden-Württemberg), Mündliche Prüfung Basisfach 2022" und "1 HMF Analysis 3)" aus "Erfolg im Mathe-Abi (Baden-Württemberg), Schriftliche Prüfung Basisfach 2021". Beide Bücher sind im Freiburger Verlag erschienen.
