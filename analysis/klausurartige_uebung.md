---
title: "Klausuratiges Übungsdings"
author: [Klasse 12]
date: 23.09.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Graph]
lang: de
header-center: "Analysis" 
---

Gegeben sei die Funktion

$$f(x)=x^5-\frac{5}{2}x^4$$

1. Berechnen Sie die Schnittpunkte des Graphen von $f$ mit den Koordinatenachsen.
   a) Werten Sie $f$ an der Stelle $x=0$ aus, um die Höhe des Schnittpunkts mit der $y$-Achse zu erhalten.
   b) Setzen Sie $f(x)$ gleich $0$ und lösen Sie nach $x$ auf, um die Schnittpunkte mit der $x$-Achse ausfindig zu machen.

2. Wo hat $f$ Extrema?
   a) Berechnen Sie die Koordinaten der Extrema.
   b) Legen Sie dar, dass es sich um Extrema und nicht um Terassenpunkte handelt.

3. An welchem Punkt $(x_w, y_w)$ ändert sich die Krümmung des Graphen von rechts auf links?

4. Berechnen Sie $f'''(0)$ und $f'''(1,5)$.

5. Zeichnen Sie den Graph von $f$.

