---
title: "Stollenlösung"
author: [Klasse 12]
date: 04.02.2022
keywords: [Stammfunktion, Integral, Fläche]
lang: de
header-center: "Analysis" 
---

\Begin{center}

![](bergstollen_loesung.png)

\End{center}

1. Die Stollenwand bildet im Querschnitt die Form einer nach unten geöffneten, (mit $\frac{1}{2}$) gestauchten Parabel. Um die Achsen beschriften zu können, berechnen wir die Schnittpunkte von $G_f$ mit den Achsen:
   - Schnittpunkte mit der $x$-Achse:
     $f(x)= 0 \Leftrightarrow 8-\frac{1}{2}x^2 \Leftrightarrow x^2 = 16 \Leftrightarrow x = \pm 4$
   - Schnittpunkt mit der $y$-Achse:
     $f(0) = 8-\frac{1}{2}0^2 = 8$
2. Die Ableitung von $f$ ist $f'(x) = -x$. Je größer der Betrag (also der Wert ohne das Vorzeichen) von $f'(x)$, desto steiler die Wand. Mit dem Betrag von $x$ wird auch der Betrag von $f'(x)$ immer größer. (Also je weiter wir und von $x=0$ entfernen, umso größer wird der Betrag der Ableitung.) Die (betragsmäßig) größten $x$-Werte, die im Anwendungskontext Sinn ergeben sind $\pm 4$, dort verlaufen die Stollenwände am steilsten.
3. $\tan (\alpha) = f'(-4) = 4 \Leftrightarrow \alpha = \tan^{-1}(4) \approx 75,96^\circ$
4. Am Scheitelpunkt der Parabel bzw. des Stollens ist die Ableitung von $g$ gleich $0$. Wir leiten also ab, setzen gleich $0$ und lösen nach $x$ auf:
   - Ableitung: $g'(x) = -x-10$
   - Nullsetzen und nach $x$ auflösen: $-x-10 = 0 \Leftrightarrow x=-10$

   Der kleinere Stollen ist also $g(-10) = -\frac{1}{2} (-10)^2 - 10\cdot (-10) -44 = -50+100-44=6$ Meter hoch. (Scheitelpunkt bei $(0,6)$)
5. Die Stammfunktion von $f$ ist $F(x) = -\frac{1}{6}x^3 +8x$. Wir berechnen die Fläche des Querschnitts:
   $$\begin{array}{ll}
   A&=\int_{-4}^4{f(x)dx} = F(4) - F(-4) = -\frac{1}{6}4^3+8\cdot 4 -(-\frac{1}{6}(-4)^3+8\cdot (-4)) \\
   & = -\frac{64}{6}+32 -\frac{64}{6}+32 = 64-\frac{64}{3} = \frac{128}{3}
    \end{array}$$
   Um das Volumen zu erhalten, müssen wir die Querschnittsfläche noch mit der Länge des Stollens multiplizieren:
   $$V= A m^2\cdot 50m = 2133\frac{1}{3}m^3$$
6. Wir müssen das Volumen, das sich aus der Querschnittsfläche oberhalb der Gerade $h:y=3,5$ ergibt abziehen. 
   - Schnittpunkte von $h$ und $G_f$:
     $$-\frac{1}{2}x^2+8 = 3,5 \Leftrightarrow \frac{1}{2}x^2 = 4,5 \Leftrightarrow x^2 = 9 \Leftrightarrow x = \pm 3$$
   - Querschnittsfläche unterhalb von $G_f$ über $[-3;3]$:
     $$\begin{array}{ll}
     \int_{-3}^3{f(x)dx} & = F(3) - F(-3) = -\frac{1}{6}3^3+8\cdot 3 -(-\frac{1}{6}(-3)^3+8\cdot (-3)) \\
     & = -\frac{27}{6}+24 -\frac{27}{6}+24 = 48-\frac{27}{3} = 39
     \end{array}$$
   - Fläche unterhalb von $h$ über dem Intervall $[-3;3]$ ist $6\cdot 3,5 = 21$
   - Querschnittsfläche zwischen $G_f$ und $h$: $39-21 = 18$
   $\Rightarrow$ Das Wasservolumen ist also auf $2133\frac{1}{3}m^3 - 18m^2\cdot 50m = 1233\frac{1}{3}m^3$ gesunken.
7. Tipp: Der Behälter soll würfelförmig sein. Das heißt die Breite ist gleich der Höhe. Weiter ist die maximale Höhe als "$f(\text{halber Kantenlänge})$" gegeben.
   

