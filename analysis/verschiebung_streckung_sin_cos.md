---
title: "Verschiebung und Streckung von Sinus und Cosinus"
author: [Klasse 12]
date: 14.12.2021
keywords: [Sinus, Cosinus, Streckung, Stauchung, Verschiebung, Periode]
lang: de
---

# Verschiebung und Streckung von Sinus und Cosinus

Verschobene und gestreckte Sinuse bzw. Cosinüsse sehen im Allgemeinen so aus:

$$f(x) = a+b\cdot\sin{(c\cdot x +d)}$$
$$g(x) = a+b\cdot\cos{(c\cdot x +d)}$$

Dabei spielen die Parameter $a$, $b$ und $d$ für den Graphen von $f$ bzw. $g$ - wie schon von Parabeln bekannt - die folgenden Rollen:

- $a$ ist die Verschiebung des Graphen (von Sinus bzw. Cosinus) in $y$-Richtung
- $b$ streckt bzw. staucht den Graphen des entlang der $y$-Achse (Änderung der **Amplitude**)
- und (positives) $d$ verschiebt den Graphen in $x$-Richtung nach links (**Phasenverschiebung**)

Neu ist dagegen die Streckung bzw. Stauchung des Graphen entlang der $x$-Achse durch $c$. (Änderung von **Periode** und **Frequenz**.)

## Markante "Stellen" von Sinus und Cosinus

Wenn wir mit diesen "allgemeinen", veschobenen, gestreckten Sini und Cosini umgehen wollen ist es günstig zu wissen, wo der "normale" Sinus und Cosinus Maxima, Minima und Nullstellen haben:

\Begin{center}
![](sinus_cosinus.png){width=450px}
\End{center}

      $\qquad$ Minima $\qquad\qquad$        Maxima$\qquad\qquad$     Nullstellen$\qquad\qquad$ 
---   -------                               ------                   -------
sin
cos

## Periode

Stellen wir den Streckungsfaktor $c$ in der Form $\frac{2\pi}{p}$ dar, so können wir die Periode $p$ des gestreckten bzw. gestauchten Sinus/Cosinus ablesen. Die altbekannte Periode $p=2\pi$ des ungestreckten Sinus/Cosinus ergibt gerade $c=1$.

Wir können also die Periode des "allgemeinen" Sinus/Cosinus berechnen, indem wir den Vorfaktor $c$ gleich $\frac{2\pi}{p}$ setzen

$$ c = \frac{2\pi}{p}$$

und nach $p$ auflösen:

$$p = \frac{2\pi}{c}$$

Kennen wir die "neue" Periode des gestreckten Sinus/Cosinus, so können wir den Streckungsfaktor ausrechnen, indem wir überlegen, mit was die alte Periode multipliziert werden muss, um die neue Periode zu erhalten:

$$2\pi \cdot s = p$$

Umgestellt nach dem Streckungs/Stauchungsfaktor $s$:

$$s= \frac{p}{2\pi}$$

**Beispiel**: \newline
$$f(x) = \frac{1}{2} + \frac{1}{2} \sin{\left(\frac{\pi}{15}x\right)}$$

Der Graph des Sinus wird also um $\frac{1}{2}$ entlang der $y$-Achse gestaucht, sodass die Maxima und Minima nicht mehr bei $\pm 1$ sondern bei $\pm \frac{1}{2}$ liegen:
\Begin{center}
![](sinus_gestaucht_in_y.png){width=450px}
\End{center}

Weiter wird der Graph um $\frac{1}{2}$ nach oben verschoben:

\Begin{center}
![](sinus_gestaucht_verschoben_in_y.png){width=450px}
\End{center}

Nun zur Streckung entlang der $x$-Achse. Die Periode des in $x$-Richtung gestreckten Sinus ist:

$$p = \frac{2\pi}{\frac{\pi}{15}} = \frac{2\pi \cdot 15}{\pi} = 2\cdot 15 = 30$$

Die Funktionswerte "wiederholen sich also zum ersten mal" bei $x=30$:

\Begin{center}
![](sinus_gestaucht_verschoben.png){width=450px}
\End{center}

Der Streckungsfaktor ist:

$$s=\frac{30}{2\pi} = \frac{15}{\pi}$$

## $x$-Werte zu gewissen $y$-Werten finden

Häufig stellt sich die Frage bei welchen $x$-Werten ein "allgemeiner" Sinus/Cosinus einen vorgegebenen $y$-Wert $y_0$ annimmt:

$$a+b \cdot \sin{\left(\frac{2\pi}{p}x +d\right)} = y_0$$

Wir fangen an nach $x$ umzustellen, indem wir $a$ von beiden Seiten abziehen und durch $b$ teilen:

$$\sin{\left(\frac{2\pi}{p}x +d\right)} = \frac{y_0 -a}{b}$$

Nun können wir die Umkehrfunktion des Sinus (bzw. des Cosinus) also den $arcsin$ (bzw. den $arccos$) anwenden

$$\frac{2\pi}{p}x +d = arcsin{\left(\frac{y_0 -a}{b}\right)}$$

um schlussendlich nach $x$ aufzulösen:

$$x_0 = \frac{p}{2\pi}\cdot \left(arcsin{\left(\frac{y_0 -a}{b}\right)} -d\right)$$

Nun kann uns die Umkehrfunktion lediglich den ersten $x$-Wert zu $y_0$ liefern und wir wissen, dass sich der Funktionswert mit der Periode $p$ wiederholt, also $x_0+k\cdot p$ stets der Funktionswert $y_0$ auftritt (für alle ganzen Zahlen $k$). Allerdings tritt der Funktionswert nicht nur nach der Periode wieder auf, sondern auch "auf der anderen Seite" des Minimums bzw. des Maximums. Jetzt wirds so allgemein ein bisschen schwierig, also zurück zum

**Beispiel**: \newline

$$f(x) = \frac{1}{2} + \frac{1}{2} \sin{\left(\frac{\pi}{15}x\right)}$$
Gesucht seien nun die $x$-Werte, sodass $f(x) = 0,95$, also:

$$\frac{1}{2} + \frac{1}{2} \sin{\left(\frac{\pi}{15}x\right)} = 0,95$$

Wir ziehen $\frac{1}{2}$ ab und multiplizieren mit $2$
 
$$\sin{\left(\frac{\pi}{15}x\right)} = 0,9$$

und wenden die Umkehrfunktion des Sinus an:

$$\frac{\pi}{15}x = arcsin(0,9)$$

Zu guter Letzt multiplizieren wir mit $\frac{15}{\pi}$, um nach $x$ aufzulösen:

$$x_0 = arcsin(0,9)\cdot\frac{15}{\pi} \approx 5,33$$

Da die Periode $p=30$ ist, nimmt die Funktion $f$ bei allen $x$-Werten der Form $x=x_0+k\cdot 30$, also $-25,33$, $5,33$, $35,33$, $65,33$ usw. den Wert $0,95$ an.

Nun ist der Graph allerdings achsensymmetrisch zu den senkrechten Geraden durch die Minima und Maxima, sodass der $y$-Wert $0,95$ zwei mal auftritt - einmal vor dem Maximum bei $x=7,5$ und einmal nach dem Maximum. Wir nennen diesen zweiten $x$-Wert $x_1$. Der Abstand von $x_0$ zum $x$-Werte des Maximum ist $d\approx 7,5-5,33 = 2,17$ und der Abstand vom Maximum zu $x_1$ ist genauso groß, d.h. $x_1=7,5+d = 9,67$.
