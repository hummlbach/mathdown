---
title: "Ableitung"
author: [Klasse 12]
date: 17.09.2021
keywords: [Ableitung, Extrema, Minimum, Maximum]
lang: de
header-center: "Analysis" 
---

# I Starters

Berechnen Sie die Ableitung der folgenden Funktionen:

\Begin{multicols}{3}
a) $f(x)=5x$
b) $g(x)=5x^2$
c) $h(x)=\frac{3}{4}x^2$
d) $q(x)=-3x^2$
e) $c(x)=42$
f) $s(x)=x^2-4x+4$
g) $p(x)=\frac{1}{3}x^3+2x^2+\frac{1}{2}x$
h) $t(x)=2x^5$
i) $u(x)=\sqrt{x}$

\End{multicols}

# II Hauptgang

1. Finden Sie den Scheitelpunkt der folgenden Parabeln:
   a) $p(x)=-x^2-4x$
   b) $q(x)=\frac{1}{2}x^2-x-3$

2. Die ideale Schachtel

   \Begin{multicols}{2}
    Aus einem quadratischen Stück Pappe mit der Seitenlänge $21$ cm soll ein Kaddon (schwäbisch für offene Schachtel aus Pappe) hergestellt werden. An den Ecken der Pappe sollen dazu quadratische Stücke mit Seitenlänge $h$ herausgeschnitten werden.

   Für welchen Wert von $h$ ergibt sich ein Kaddon mit maximalem Volumen?

   ![](kaddon.png){}

   \End{multicols}

