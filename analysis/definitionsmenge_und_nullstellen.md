---
title: "Funktionen"
author: [Klasse 12]
date: 13.09.2021
keywords: [Funktionen, Definitionsmenge, Wertetabelle, Bildmenge]
lang: de
header-center: "Analysis" 
---

# I Starters

1. Zeichnen Sie den Graph des Sinus in ein Koordinatensystem ein.
   - Die horizontale Achse erstrecke sich dabei vom Ursprung $18$ cm nach rechts.
   - Die vertikale Achse reiche mindestens $5$ cm nach oben und unten.
   - Tragen Sie die Winkel in Zehnerschritten (pro Kästchen) auf der horizontalen Achse, und in "$y$-Richtung" jeweils den Wert des Sinus an.
   - In der Vertikalen soll dabei ein Kästchen $0,1$ Längeneinheiten entsprechen.

2. Ein Ball wird aus reichlich Höhe fallen gelassen.

   a) Wie schnell ist er nach $5$ Sekunden?
   b) Wie schnell nach $3,5$ Sekunden?
   c) Nach wievielen Sekunden ist der Ball $65$ Meter pro Sekunde schnell?
   d) Wie tief ist der Ball nach $4$ Sekunden gefallen?
   e) Wann ist der Ball $100$ Meter tief gefallen?

# II Hauptgang

1. Ein Gegenstand wird vom Boden aus senkrecht nach oben geschleudert. Der Zusammenhang zwischen verstrichener Zeit und der Höhe des Gegenstandes ist gegeben durch die Funktion $f(t) = -5t^2+50t$ wobei $0 \le t \le 10$.
   a) Wie hoch ist der Gegenstand nach einer Sekunde?
   b) Wann ist der Gegenstand in einer Höhe von $120$ Metern?
   c) Wann hat der Gegenstand seine maximale Höhe erreicht und wie hoch ist er dann?

2. Dem Buch "Grundlegende Begriffe der Mathematik: Entstehung und Entwicklung..." zufolge, extrapolierte Halley Messungen zum Luftdruck und vermutete zunächst (fälschlicher Weise), der Luftdruck folge dem positiven Ast einer Hyperbel. (Das Buch referenziert eine zweifelhafte Quelle.) Der Wert entspräche demnach in der Höhe von $h$ Kilometern grob dem Wert von $p(h)=\frac{4}{h+4}$ Bar.
   a) Ab $4500$ Metern Höhe werden (bei mangelhafter Akklimatisation) $50$ bis $85$% der Bergsteiger höhenkrank. Wie hoch ist der Luftdruck in dieser Höhe?
   b) In welcher Höhe ist der Luftdruck geringer als $300$ mbar, sodass technisch gesehen ein sogenanntes Grobvakuum herscht?
   c) Für welche Höhen ergeben sich auf jeden Fall keine sinnvollen Luftdruckwerte? Für welche Höhe ergibt sich rein rechnerisch kein Wert?

# III Dessert

1. Jeder Zahl kann man ihre Quadratwurzel zuordnen:
   $$x\mapsto \sqrt{x}$$
   a) Füllen Sie die folgende Wertetabelle aus:
      
      $x$           $\,\frac{1}{9}\,$     $\,\frac{1}{4}\,$     $\,\frac{1}{2}\,$     $\,1\,$   $\,2\,$   $\,4\,$   $\,9\,$
      ----          ----------            ----------            -------               ---       ---       ---       ---
      $\sqrt{x}$

   b) Zeichnen Sie den Graph der Quadratwurzel in ein Koordinatensystem ein.
   c) Von welchen Zahlen können Sie die Quadratwurzel berechnen und von welchen nicht?
   d) Die Menge welcher Zahlen erhalten Sie als Ergebnis beim Wurzelziehen?

2. Aus der analytischen Geometrie wissen Sie wie man in der Ebene den Abstand zweier Punkte ($A=(x_A,y_A)$ und $B=(x_B,y_B)$) voneinander berechnet ($\left\Vert A,B\right\Vert=\sqrt{(x_A-x_B)^2+(y_A-y_B)^2}$). Daraus ergibt sich für den Abstand eines Punktes $(x,y)$ vom Ursprung folgende Funktion:
$$ \begin{array}{lccc}
d: &\mathbb{R}^2 & \rightarrow & [0,\infty[ \\
   &(x,y)        & \mapsto & \sqrt{x^2+y^2}
\end{array}
$$

   a) Was ist der Abstand des Punktes $(8,15)$ vom Ursprung?
   b) Was ist der Abstand des Punktes $(40,-9)$ vom Ursprung?
   c) Zeichnen Sie alle Punkte in ein Koordinatensystem, deren Abstand vom Urspung $1$ beträgt.
   
