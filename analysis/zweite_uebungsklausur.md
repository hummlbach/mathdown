---
title: "2. Übungsklausur"
author: "Name:"
date: 21.10.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte]
lang: de
header-center: "Analysis" 
footer-right: "Bearbeitungszeit 75 Minuten"
footer-center: 26 Punkte
---

Die Funktion

$$f(x) = \frac{x^2}{4}+\frac{4}{x}$$

soll untersucht und ihr Graph $G_f$ gezeichnet werden.


1. Wo schneidet $G_f$ die $x$-Achse? \hfill __3P__

2. Untersuchen Sie $f$ auf Extrema und geben Sie die Koordinaten an. \hfill __6P__

3. Wieviele Wendepunkte weißt $G_f$ auf? Geben Sie deren Koordinaten an. \hfill __6P__

4. Füllen Sie die folgende Wertetabelle: \hfill __3P__

   x     -4   -1   1   2   4
   ---  ---  --- --- --- ---
   f(x)

5. Ermitteln Sie das Verhalten von $G_f$ an der Asymptote $x=0$. \hfill __3P__

6. Zeichnen Sie $G_f$, unter Zuhilfenahme der Ergebnisse der bisherigen Aufgaben, in ein Koordinatensystem passender Größe mit $1cm$ pro Längeneinheit. \hfill __5P__

