---
title: "Funktionen"
author: [Klasse 12]
date: 13.09.2021
keywords: [Funktionen]
lang: de
header-center: "Analysis" 
---

\AtBeginEnvironment{table}{\small}

## Überreste der Stadt Larsa im heutigen Irak

![](larsa.jpg){}

# Plimpton 322

![](plimpton_322.jpg){}


# Die Entschlüsselung {.t}

## Keilschrift

![](keilzeichen1.png){width=200px} \ ![](keilzeichen2.png){width=200px}


# Die Entschlüsselung {.t}

## Keilschrift

![](keilzeichen1.png){width=200px} \ ![](keilzeichen2.png){width=200px}

## Zeile 12

![](plimpton_322_line12.jpg){}


# Die Entschlüsselung {.t}

## Keilschrift

![](keilzeichen1.png){width=200px} \ ![](keilzeichen2.png){width=200px}

## Zeile 12

![](plimpton_322_line12.jpg){}

## Sexagesimalsystem

![](plimpton12.png){}


# Die Entschlüsselung {.t}

## Keilschrift

![](keilzeichen1.png){width=200px} \ ![](keilzeichen2.png){width=200px}

## Zeile 12

![](plimpton_322_line12.jpg){}

## Sexagesimalsystem

![](plimpton12.png){}

![](plimpton12tab.png){}


# Die Entschlüsselung {.t}

## Keilschrift

![](keilzeichen1.png){width=200px} \ ![](keilzeichen2.png){width=200px}

## Zeile 12

![](plimpton_322_line12.jpg){}

## Sexagesimalsystem

![](plimpton12.png){}

![](plimpton12tab.png){}

$27\, 59 = 27 \cdot 60^1 + 59 \cdot 60^0 = 1679 \quad 48\, 49 = 48 \cdot 60^1 + 49 \cdot 60^0 = 2929$

$29\cdot 60^{-1} + 21 \cdot 60^{-2} + 54 \cdot 60^{-3} + 2 \cdot 60^{-4} + 15 \cdot 60^{-5} = 0,489417$


# Die älteste Wertetabelle der Erde {.t}

\Begin{multicols}{2}
\tiny
?                               Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 

\End{multicols}

# Die älteste Wertetabelle der Erde in dezimal {.t}

\Begin{multicols}{2}
\tiny
                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1

\End{multicols}

\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# Die älteste Wertetabelle der Erde in dezimal {.t}

\Begin{multicols}{2}
\tiny
                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1
$(1).9491586$ 	    $3367$ 	        $4825$ 	        2

\End{multicols}

\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# Die älteste Wertetabelle der Erde in dezimal {.t}

\Begin{multicols}{2}
\tiny
                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1
$(1).9491586$ 	    $3367$ 	        $4825$ 	        2
$(1).9188021$ 	    $4601$ 	        $6649$ 	        3

\End{multicols}

\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# Die älteste Wertetabelle der Erde in dezimal {.t}

\Begin{multicols}{2}
\tiny
                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1
$(1).9491586$ 	    $3367$ 	        $4825$ 	        2
$(1).9188021$ 	    $4601$ 	        $6649$ 	        3
$(1).8862479$ 	    $12709$ 	    $18541$ 	    4

\End{multicols}

\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# Die älteste Wertetabelle der Erde in dezimal

\Begin{multicols}{2}

\tiny

                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1
$(1).9491586$ 	    $3367$ 	        $4825$ 	        2
$(1).9188021$ 	    $4601$ 	        $6649$ 	        3
$(1).8862479$ 	    $12709$ 	    $18541$ 	    4
$(1).8150077$ 	    $65$ 	        $97$ 	        5

\End{multicols}

\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# Die älteste Wertetabelle der Erde in dezimal {.t}

\Begin{multicols}{2}

\tiny
                                Breite          Diagonale       Zeile
------------                    ---------       -----------     ------
(1) 59 00 15 	                1 59 	        2 49 	        1st
(1) 56 56 58 14 **50 06** 15 	56 07 	        **3 12 01** 	2nd
(1) 55 07 41 15 33 45 	        1 16 41 	    1 50 49 	    3rd
(1) 53 10 29 32 52 16 	        3 31 49 	    5 09 01 	    4th
(1) 48 54 01 40 	            1 05 	        1 37 	        5th
(1) 47 06 41 40 	            5 19 	        8 01 	        6th
(1) 43 11 56 28 26 40 	        38 11 	        59 01 	        7th
(1) 41 33 **59** 03 45 	        13 19 	        20 49 	        8th
(1) 38 33 36 36 	            **9** 01        12 49 	        9th
(1) 35 10 02 28 27 24 26 40 	1 22 41 	    2 16 01 	    10th
(1) 33 45 	                    45 	            1 15 	        11th
(1) 29 21 54 02 15 	            27 59 	        48 49 	        12th
(1) 27 00 03 45 	            **2 41** 	    4 49 	        13th
(1) 25 48 51 35 06 40 	        29 31 	        53 49 	        14th
(1) 23 13 46 40 	            **56**          53              15th 


\tiny

$\frac{d^2}{l^2}$   $s$              $d$            Zeile
-----------         ------          -----           ----
$(1).9834028$ 	    $119$ 	        $169$ 	        1
$(1).9491586$ 	    $3367$ 	        $4825$ 	        2
$(1).9188021$ 	    $4601$ 	        $6649$ 	        3
$(1).8862479$ 	    $12709$ 	    $18541$ 	    4
$(1).8150077$ 	    $65$ 	        $97$ 	        5
$(1).7851929$ 	    $319$ 	        $481$ 	        6
$(1).7199837$ 	    $2291$ 	        $3541$ 	        7
$(1).6927094$ 	    $799$ 	        $1249$ 	        8
$(1).6426694$ 	    $481$ 	        $769$ 	        9
$(1).5861226$ 	    $4961$ 	        $8161$ 	        10
$(1).5625$ 	        $45$ 	        $75$  	        11
$(1).4894168$ 	    $1679$ 	        $2929$ 	        12
$(1).4500174$ 	    $161$ 	        $289$ 	        13
$(1).4302388$ 	    $1771$ 	        $3229$ 	        14
$(1).3871605$ 	    $56$  	        $106$  	        15

\End{multicols}


\footnotesize

Die kurze Kathete sei mit $s$, die lange mit $l$ und die Hypothenuse mit $d$ bezeichnet.


# 2000 Jahre später

![](trigonometry_table.png){}


# Der Graph des Sinus

![](sinus.png){}


# Inklinition der Planeten um 1000 (nach Chrsitus) {.t}

![](planeten_inklination_mittelalter.jpg){}


# Inklinition der Planeten um 1000 (nach Chrsitus) {.t}

![](planeten_inklination_mittelalter.jpg){}

Die maximale Höhe verschiedener Himmelskörper über einem Beobachtungspunkt auf der Erde in Abhängigkeit von der Zeit.


# Galilei und der freie Fall (1609) {.t}

Galilei stellte fest, dass Körper unabhängig von ihrem Gewicht im Schwerefeld der Erde über die Zeit gleichmäßig beschleunigt werden.
Für die Geschwindigkeit $v$ im freien Fall gilt:

$$v(t) = 9,81 \frac{m}{s^2} \cdot t$$

# Galilei und der freie Fall (1609) {.t}

Galilei stellte fest, dass Körper unabhängig von ihrem Gewicht im Schwerefeld der Erde über die Zeit gleichmäßig beschleunigt werden.
Für die Geschwindigkeit $v$ im freien Fall gilt:

$$v(t) = 9,81 \frac{m}{s^2} \cdot t$$

Durch die zunehmende Geschwindigkeit wächst die zurückgelegte Strecke im freien Fall quadratisch mit der Zeit:

$$s(t) = \frac{1}{2}\cdot 9,81\frac{m}{s^2} \cdot t^2$$

# Zusammenhang zwischen Höhe und Luftdruck nach Halley (1686)

![](halleys_pressure_vs_altitude_1686.png){height=250px}

# Der Kamm von Dirichlet (1829)

$$
\text{dir}(x) = \left\{
\begin{array}{l}
0 \quad \text{ für } x \in \mathbb{Q} \\
1 \quad \text{ für } x \notin \mathbb{Q}
\end{array} \right.
$$

# Geometrische Folge {.t}
\begin{multicols}{2}
$$
\begin{array}{lccl}
a_n:& \mathbb{N} & \rightarrow & \mathbb{R} \\
    &  n & \mapsto & \frac{1}{2^n}
\end{array}
$$

Oder kürzer: $$a_n = \frac{1}{2^n}$$
\end{multicols}

# Geometrische Folge {.t}

\begin{multicols}{2}
$$
\begin{array}{lccl}
a_n:& \mathbb{N} & \rightarrow & \mathbb{R} \\
    &  n & \mapsto & \frac{1}{2^n}
\end{array}
$$

Oder kürzer: $$a_n = \frac{1}{2^n}$$
\end{multicols}

$$a_1 = \frac{1}{2^1} = \frac{1}{2}$$

# Geometrische Folge {.t}

\begin{multicols}{2}
$$
\begin{array}{lccl}
a_n:& \mathbb{N} & \rightarrow & \mathbb{R} \\
    &  n & \mapsto & \frac{1}{2^n}
\end{array}
$$

Oder kürzer: $$a_n = \frac{1}{2^n}$$
\end{multicols}

$$a_1 = \frac{1}{2^1} = \frac{1}{2}$$
$$a_2 = \frac{1}{2^2} = \frac{1}{4}$$

# Geometrische Folge {.t}

\begin{multicols}{2}
$$
\begin{array}{lccl}
a_n:& \mathbb{N} & \rightarrow & \mathbb{R} \\
    &  n & \mapsto & \frac{1}{2^n}
\end{array}
$$

Oder kürzer: $$a_n = \frac{1}{2^n}$$
\end{multicols}

$$a_1 = \frac{1}{2^1} = \frac{1}{2}$$
$$a_2 = \frac{1}{2^2} = \frac{1}{4}$$
$$a_6 = \frac{1}{2^6} = \frac{1}{64}$$

# Geometrische Folge {.t}

\begin{multicols}{2}
$$
\begin{array}{lccl}
a_n:& \mathbb{N} & \rightarrow & \mathbb{R} \\
    &  n & \mapsto & \frac{1}{2^n}
\end{array}
$$

Oder kürzer: $$a_n = \frac{1}{2^n}$$
\end{multicols}

$$a_1 = \frac{1}{2^1} = \frac{1}{2}$$
$$a_2 = \frac{1}{2^2} = \frac{1}{4}$$
$$a_6 = \frac{1}{2^6} = \frac{1}{64}$$

$$\lim_{n\rightarrow\infty} a_n = 0$$

# Geometrische Reihe {.t}

$$
s_n = \sum_{i=1}^{n} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots + \frac{1}{2^n}
$$

# Geometrische Reihe {.t}

$$
s_n = \sum_{i=1}^{n} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots + \frac{1}{2^n}
$$

$$s_4 = \sum_{i=1}^4 = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} = \frac{15}{16}$$

# Geometrische Reihe {.t}

$$
s_n = \sum_{i=1}^{n} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots + \frac{1}{2^n}
$$

$$s_4 = \sum_{i=1}^4 = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} = \frac{15}{16}$$

$$
\lim_{n\rightarrow\infty} s_n = \sum_{i=1}^{\infty} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots =
$$

# Geometrische Reihe {.t}

$$
s_n = \sum_{i=1}^{n} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots + \frac{1}{2^n}
$$

$$s_4 = \sum_{i=1}^4 = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \frac{1}{16} = \frac{15}{16}$$

$$
\lim_{n\rightarrow\infty} s_n = \sum_{i=1}^{\infty} a_i = \frac{1}{2} + \frac{1}{4} + \frac{1}{8} + \cdots = 1
$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$\begin{array}{ll}
\mathcal{P}(\{1,2,3,4\}) =  &\{ \{1\}, \{2\}, \{3\}, \{4\}, \\
                            & \{1,2\}, \{1,3\}, \{1,4\}, \{2,3\}, \{2,4\}, \{3,4\}, \\
                            & \{1,2,3\}, \{1,2,4\}, \{1,3,4\}, \{2,3,4\}, \{1,2,3,4\}, \emptyset \}
\end{array}
$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = $$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4} $$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) = $$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{2}{4} =\frac{1}{2}$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{1}{2}, \,  P(\{1,3\}) =\frac{1}{2}, \,  P(\{1,4\}) =\frac{1}{2}, \,  P(\{2,3\}) =\frac{1}{2}, \cdots$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{1}{2}, \,  P(\{1,3\}) =\frac{1}{2}, \,  P(\{1,4\}) =\frac{1}{2}, \,  P(\{2,3\}) =\frac{1}{2}, \cdots$$
$$ P(\{1,2,3\}) =\frac{3}{4}, \,  P(\{1,2,4\}) =\frac{3}{4}, \,  P(\{1,3,4\}) =\frac{3}{4}, \,  P(\{2,3,4\}) =\frac{3}{4}$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{1}{2}, \,  P(\{1,3\}) =\frac{1}{2}, \,  P(\{1,4\}) =\frac{1}{2}, \,  P(\{2,3\}) =\frac{1}{2}, \cdots$$
$$ P(\{1,2,3\}) =\frac{3}{4}, \,  P(\{1,2,4\}) =\frac{3}{4}, \,  P(\{1,3,4\}) =\frac{3}{4}, \,  P(\{2,3,4\}) =\frac{3}{4}$$
$$ P(\{1,2,3,4\}) =\frac{4}{4}$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{1}{2}, \,  P(\{1,3\}) =\frac{1}{2}, \,  P(\{1,4\}) =\frac{1}{2}, \,  P(\{2,3\}) =\frac{1}{2}, \cdots$$
$$ P(\{1,2,3\}) =\frac{3}{4}, \,  P(\{1,2,4\}) =\frac{3}{4}, \,  P(\{1,3,4\}) =\frac{3}{4}, \,  P(\{2,3,4\}) =\frac{3}{4}$$
$$ P(\{1,2,3,4\}) =\frac{4}{4} = 1$$

# Wahrscheinlichkeitsverteilung beim Wurf eines Tetraeders {.t}

\begin{multicols}{2}
$$\begin{array}{lccl}
P: & \mathcal{P}(\{1,2,3,4\}) & \rightarrow & [0,1] \\
   & A & \mapsto & \frac{|A|}{4}
\end{array}
\qquad\text{oder abgekürzt:} \quad P(A) = \frac{|A|}{4}
$$
\end{multicols}
$$ P(\{1\}) = \frac{1}{4}, \, P(\{2\}) = \frac{1}{4}, \, P(\{3\}) = \frac{1}{4}, \, P(\{4\}) = \frac{1}{4} $$
$$ P(\{1,2\}) =\frac{1}{2}, \,  P(\{1,3\}) =\frac{1}{2}, \,  P(\{1,4\}) =\frac{1}{2}, \,  P(\{2,3\}) =\frac{1}{2}, \cdots$$
$$ P(\{1,2,3\}) =\frac{3}{4}, \,  P(\{1,2,4\}) =\frac{3}{4}, \,  P(\{1,3,4\}) =\frac{3}{4}, \,  P(\{2,3,4\}) =\frac{3}{4}$$
$$ P(\{1,2,3,4\}) =\frac{4}{4} = 1, \, P(\emptyset) = 0$$

