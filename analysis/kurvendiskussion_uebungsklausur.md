---
title: "Übungsklausur"
author: "Name:"
date: 24.09.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte]
lang: de
header-center: "Analysis" 
footer-right: "Bearbeitungszeit 90 Minuten"
footer-center: 34 Punkte
---

Die Funktion

$$f(x) = 3x^5-5x^3$$

soll untersucht und ihr Graph $G_f$ gezeichnet werden.

1. Berechnen Sie die Schnittpunkte von $G_f$ mit der $x$-Achse. \hfill (**4P**)

2. Untersuchen Sie (rechnerisch), ob der Graph von $f$ Extrema oder Terrassenpunkte aufweist. 
   a) Geben Sie deren Koordinaten an. \hfill (**5P**)
   b) Legen Sie dar, welcher der Punkte ein Maximum, welcher ein Minimum und welcher ein Terrassenpunkt ist. \hfill (**5P**)

3. Untersuchen Sie das Krümmungsverhalten von $G_f$
   a) Ermitteln Sie die Wendepunkte von $G_f$. \hfill (**8P**)
   b) Wo ist der Graph von $f$ links- und wo rechtsgekrümmt? \hfill (**3P**)

4. Berechnen Sie $f(1,5)$ und $f(-1,5)$. \hfill (**1P**)

5. Zeichnen Sie $G_f$, unter Zuhilfenahme der Ergebnisse der bisherigen Aufgaben, in ein Koordinatensystem passender Größe mit $2$ cm pro Längeneinheit. \hfill (**3P**)

6. Wir nehmen nun an $f$ sei die Ableitung einer anderen Funktion $g$, also $g' = f$. Was lässt sich daraus für $g$ ableiten? \hfill  (**3P**)
