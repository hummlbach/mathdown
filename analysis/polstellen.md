---
title: "Polstellen und Verhalten 'im Unendlichen'"
author: [Klasse 12]
date: 23.09.2021
keywords: [Polstellen, Definitionslücken, Graph]
lang: de
header-center: "Analysis" 
---

1. Wir betrachten die Funktion

   $$f(x)=\frac{1}{x}+1$$

   a) Zeigen Sie, dass die Funktion keine Extrema besitzt.
   b) Hat der Graph $G_f$ der Funktion $f$ Schnittpunkte mit der $x$-Achse?
   c) Wie verhalten sich die Funktionswerte in der Nähe von $x=0$?
   d) Wie verhalten sich die Funktionswerte für sehr große $x$-Werte?
   e) Untersuchen Sie, wie sich $f'$ für sehr große und sehr kleine $x$-Werte verhält.
   f) Zeichnen Sie $f$ und $f'$ in ein Koordinatensystem ein.

2. Gegeben sei die Funktion
   
   $$g(x)=\frac{1}{x}+x$$

   a) Hat der Graph von $g$ Schnittpunkte mit der $x$-Achse?
   b) Weist der Graph von $g$ Minima oder Maxima auf?
   c) Wie verhalten sich die $x$-Werte von $g$ in der Nähe von $x=0$? (Berechnen Sie zum Beispiel $g(\pm \frac{1}{2})$ und $g(\pm \frac{1}{4})$.)
   d) Wie verhält sich der Graph für große $x$-Werte? ($g(\pm 2)$, $g(\pm 4)$?)
   e) Wie verhält sich der Graph der Ableitung $g'$ für sehr kleine und große $x$-Werte?
   f) Zeichnen Sie $g$ und $g'$ in ein Koordinatensystem ein.

