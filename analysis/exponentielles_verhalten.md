---
title: "Exponentielles Verhalten: Zinseszins"
author: [Klasse 12]
date: 01.04.2022
keywords: [Logarithmen]
lang: de
header-center: "Analysis" 
---

# I Starters

Josef von Nazaret legt im Jahre 0 bei der Sparkasse Jerusalem den Betrag von $1$ Euro-Cent auf einem Sparbuch an.
Die Verzinsung beträgt jährlich $5\%$. Die Zinsgewinne werden wieder angelegt. Im Jahre 2000 finden seine Nachkommen in einer verbeulten Blechbüchse sein Sparbuch. Bei einer nun unternommenen Reise nach Israel entdecken sie, dass es die Sparkasse Jerusalem heute immer noch gibt. Sie gehen hinein und bitten die Angestellte, doch die Zinsen für die vergangenen 2000 Jahre nachzutragen.

a) Wie müssen Sie rechnen, um auf das aktuelle Guthaben zu kommen?
b) Wie wäre das aktuelle Guthaben ohne Zinseszins?

# II Hauptgang


Frida, Max und Greta haben fest angelegtes Geld von Ihren Großeltern geerbt. (Irre unrealistisch aber egal...)
Das aktuelle Kapital ist mit $K_0$ bezeichnet und $p$ ist der Zinssatz (siehe Tabelle)

\Begin{multicols}{2}

a) Berechnen Sie jeweils die Höhe des Kapitals nach $20$, $40$, $60$, $80$ und $100$ Jahren.
b) Berechnen Sie jeweils das Kapital vor $20$, $40$, $60$, $80$ und $100$ Jahren.
c) Stellen Sie die Kapitalzuwächse in einem Schaubild dar.
d) Wann werden sich die aktuellen Kapitäler verdoppelt haben?

Anleger     $K_0$       $p$     
-----       -----       -----  
Frida       $20000$     $5$          
Max         $50000$     $1$              
Greta       $10000$     $2$              

\End{multicols}


