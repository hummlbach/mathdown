---
title: "Wiederholung Ableiten"
author: [Klasse 13]
date: 17.09.2022
keywords: [Kettenregel, Produktregel, Summenregel, Faktorregel]
lang: de
header-center: "Analysis" 
---

## 0 Potenzfunktionen ableiten

Hochzahl als Faktor vorne dran und dann von der Hochzahl $1$ abziehen:

$$f(x)=ax^k \Rightarrow f'(x)= kax^{k-1}$$

**Beispiele:**

- Ist $f(x)=5x^3$, dann ist $f'(x)=3\cdot 5\cdot x^2 = 15x^2$
- Ist $f(x)=\frac{1}{2}x^4$, dann ist $f'(x)=4 \cdot \frac{1}{2}\cdot x^3 = 2x^3$

**Übung 1:**
Leiten Sie $f(x) = \frac{1}{5}x^5$ ab.

## 1 Wurzeln ableiten

Die Wurzel wird als Hochzahl geschrieben und dann mittels der Potenzableitungsregel abgelitten:

$$\sqrt[n]{x} = x^{\frac{1}{n}} \, \text{ oder etwas allegemeiner } \sqrt[n]{x^k} = x^\frac{k}{n}$$

**Beispiele:**

- $f(x) = \sqrt{x} = x^\frac{1}{2} \, \Rightarrow \, f'(x) = \frac{1}{2} x^{\frac{1}{2}-1} = \frac{1}{2} x^{-\frac{1}{2}} = \frac{1}{2} \cdot \frac{1}{x^\frac{1}{2}} = \frac{1}{2} \cdot \frac{1}{\sqrt{x}} = \frac{1}{2\sqrt{x}}$
- $f(x)=\sqrt[3]{x} = x^{\frac{1}{3}} \, \Rightarrow \, f'(x) = \frac{1}{3}x^{\frac{1}{3}-1} = \frac{1}{3}x^{-\frac{2}{3}} = \frac{1}{3}\cdot \frac{1}{x^{\frac{2}{3}}} = \frac{1}{3}\cdot\frac{1}{\sqrt[3]{x^2}} = \frac{1}{\sqrt[3]{3x^2}}$

**Übung 2:**
Leiten Sie $f(x) = \sqrt[4]{x}$ ab.

## 2 Summenregel

Bei Summen werden die Ableitungen der Summanden gebildet und addiert:

$$(f(x)+g(x))' = f'(x)+g'(x)$$

**Beispiele:**

- $f(x) = x^3 + \frac{1}{2}x^2 + 3x + 5 \, \Rightarrow \, f'(x) = 3x^2+x+3$
- $f(x) = 6x^7 + \sqrt{x} \, \Rightarrow \, f'(x) = 42x^6 + \frac{1}{2\sqrt{x}}$

**Übung 3:**
Leiten Sie $f(x) = 5,75x^4 + 11,5x^2 - 23x$ ab.

## 3 $e$-Funktion, Sinus, Cosinus und natürlicher Logarithmus

- $(e^x)' = e^x$ für später: $a^x = e^{\ln{(a)}\cdot x}$
- $\sin'(x) = \cos(x)$
- $\cos'(x) = -\sin(x)$
- $\ln'(x) = \frac{1}{x}$

## 4 Kettenregel

Sei $f$ eine Funktion die eine Hintereinanderausführung/Verkettung/Verschachtelung zweier Funktionen $u$ und $v$ ist: $f(x)=u(v(x))$ \newline
Dann ist die Ableitung von $f$ wie folgt: $$f'(x) = u'(v(x))\cdot v'(x)$$
Merksatz: "Äußere Ableitung mal innere Ableitung"

**Beispiele:**

$f(x)$          $u(x)$      $v(x)$          $u'(x)$         $v'(x)$         $f'(x)$
-----           -----       ----            ------          ------          ------
$e^{\ln(a)x}$   $e^x$       $\ln(a)x$       $e^x$           $\ln(a)$        $\ln(a) e^{\ln(a)x}$
$(x^2+4)^3$     $x^3$       $x^2+4$         $3x^2$          $2x$            $3(x^2+4)^2\cdot 2x = 6x(x^2+4)^2$
$\sin(x^4)$     $\sin(x)$   $x^4$           $\cos(x)$       $4x^3$          $\cos(x^4)\cdot 4x^3 = 4x^3\cos(x^4)$
$(\cos(x))^2$   $x^2$       $\cos(x)$       $2x$            $-\sin(x)$      $2\cos(x)\cdot(-\sin(x))=-2\cos(x)\sin(x)$

**Übung 4:**

Als Folgerung aus der Kettenregel erhalten wir noch die **Faktorregel**, die besagt, dass ein Faktor vor einer Funktion beim Ableiten erhalten bleibt:
$$(a\cdot f(x))' = a\cdot f'(x)$$

**Beispiel:** \newline
$(3\cdot sin(x))' = 3\cos(x)$

## 5 Produktregel


