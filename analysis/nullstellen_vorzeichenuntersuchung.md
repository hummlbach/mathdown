---
title: "Nullstellen & Vorzeichenuntersuchung"
author: [Klasse 12]
date: 17.09.2021
keywords: [Nullstellen, Satz vom Produkt, Mitternachtsformel, Satz von Vieta]
lang: de
header-center: "Analysis" 
---

Die Abschnitte, die mit einem Sternchen (*) gekennzeichnet sind, können Sie als "nice to have" betrachten. Wobei der Satz von Vieta (erster Sternchenabschnitt) fürs Verständnis hier aber wichtig/hilfreich ist.

# Quadratische Gleichungen

Zur Lösung quadratischer Gleichungen ist Ihnen die Mitternachtsformel wohlvertraut:

Die Lösungen $x_1$ und $x_2$ zu $ax^2+bx+c=0$ sind gegeben durch
$$x_{1/2} = \frac{-b\pm\sqrt{b^2-4ac}}{2a}$$

Ist $b$ oder $c$ gleich Null, so ergeben sich Spezialfälle, für deren Lösung es schöne Alternativen zur Mitternachtsformel gibt.

## b=0

Ist $b=0$, fehlt also das $bx$ in der quadratischen Gleichung, so können wir "einfach" nach $x^2$ umstellen und die Wurzel ziehen:

**Beispiel**: \newline
$$
\begin{array}{lll}
4x^2-16 & = 0 & \qquad | \, +16\\
4x^2 & = 16 & \qquad | \, :4 \\
x^2 & = 4 & \qquad | \, \sqrt{\cdot} \\
x & = \pm 2 &  \\
\end{array}
$$

## c=0

Liegt eine quadatische Gleichung ohne konstanten Anteil vor, so können wir $x$ ausklammern und den Satz vom Produkt (siehe auch unten) anwenden:

**Beispiel**: \newline
$$
\begin{array}{ll}
&0 = 4x^2+16x = x\cdot (4x+16) \\
\Leftrightarrow & x = 0 \, \text{ oder }\,  4x+16 = 0 \qquad | \, :4,\, -4\\
\Leftrightarrow & x = 0 \, \text{ oder }\,  x = -4
\end{array}
$$

## Satz von Vieta*

Haben wir das Glück, dass die gesuchten Lösungen halbwegs ganze/runde Zahlen sind, so können wir sie durch "scharfes Hinschauen" mit dem Satz von Vieta finden. Wir starten zur Herleitung "von der anderen Seite":

$$(x+3)(x+7) = x^2 + 7x +3x + 3\cdot 7 = x^2 + (3+7)x + 3\cdot 7 = x^2 +10x + 21$$

Was fällt Ihnen auf? Der Vorfaktor vor dem $x$ ist die Summe aus den Lösungen (mit umgedrehten Vorzeichen) und die Konstante ist das Produkt der Lösungen (mit umgedrehten Vorzeichen). Normalerwiese liegt uns natürlich nicht die Form links, sondern die Form rechts vor. Wenn es uns gelingt zwei Zahlen $x_1$ und $x_2$ zu finden, deren Summe den Vorfaktor vor $x$ und deren Produkt der Konstante entspricht, so haben wir die Lösungen gefunden:

**Beispiel**: \newline
$$x^2+7x+12=0$$

Wir fragen uns also: Welche Zahlen ergeben addiert $7$ und multipliziert $12$? $3\cdot 4 =12$ und $3+4=7$. D.h. die Lösungen der Gleichung sind $x_1 = -3$ und $x_2 = -4$. Wir überprüfen das noch: $-3$ ist "Nullstelle" von $(x+3)$ und $-4$ von $(x+4)$ und:
$$(x+3)\cdot (x+4) = x^2+3x+4x+3\cdot4 = x^2 +7x +12$$
Daraus sehen wir nebenbei, dass sich ein quadratischer Term $ax^2+bx+c$, der bei $x_1$ und $x_2$ Null wird, immer als $(x-x_1)\cdot(x-x_2)$ schreiben lässt. Also $ax^2+bx+c = (x-x_1)\cdot (x-x_2)$. (Man sagt der quadratische Term zerfällt in seine Linearfaktoren.)


# Schlimmer als quadratisch

Für Gleichungen dritten und vierten Grades (den höchsten vorkommenden Exponenten bezeichnet man als Grad der Gleichung) gibt es zwar allgemeine Lösungsformeln, diese sind aber recht kompliziert (und nicht Gegenstand des Schulstoffes). Für Gleichungen noch höheren Grades (also höher als $5$) gibt es sogar gar keine allgemeinen Lösungsformeln mehr. Wir betrachten daher nur noch zwei Spezialfälle.

## Satz vom Produkt

Generell gilt: ein Produkt $a\cdot b$ ist $0$ genau dann, wenn $a=0$ oder $b=0$ - also einer der Faktoren Null ist. Das können wir bei Gleichungen höheren Grades (ohne Konstante) nutzen. $x$ hoch der kleinste Exponent kann ausgeklammert werden und anschließend kann man untersuchen, wann die Faktoren Null werden (um herauszufinden wann das Produkt $0$ wird):

**Beispiel**: \newline
$$
\begin{array}{ll}
&0 = 3x^7-27x^5 = x^5\cdot (3x^2-27) = 0 \\
\Leftrightarrow & x^5 = 0 \, \text{ oder }\,  3x^2-27 = 0 \qquad | \, :3,\, +9\\
\Leftrightarrow & x = 0 \, \text{ oder }\,  x^2 = 9 \qquad | \, \sqrt{\cdot} \\
\Leftrightarrow & x = 0 \, \text{ oder }\,  x = \pm 3 
\end{array}
$$

## Substitution*

Eine Gleichung vom Grad $4$, in der nur gerade Hochzahlen vorkommen, wird zu einer Gleichung vom Grad $2$, wenn wir $x^2$ durch $z$ ersetzen (das nennt man Substitution). Die entstehende Gleichung ist quadratisch und kann mit der Mitternachtsformel gelöst werden. Die Wurzeln der Lösungen der quadratischen Gleichung sind dann die Lösungen der ursprünglichen Gleichung.

**Beispiel**: \newline
$$
x^4-3x^2 -4 = 0
$$
Wir ersetzen $x^2$ durch $z$, erhalten damit die folgende Gleichung

$$z^2-3z-4=0$$

und wenden die Mitternachtsformel an, um deren Lösungen zu finden:

$$
z_{1/2} = \frac{-(-3)\pm \sqrt{(-3)^2-4\cdot 1 \cdot (-4)}}{2}
        = \frac{3 \pm \sqrt{9+16}}{2} = \frac{3 \pm 5}{2}
$$

Also $z_1 = 4$ und $z_2 = -1$. Da $z = x^2$ ist also $x=\sqrt{z}$ und somit sozusagen auch $x_1 = \sqrt{z_1}$ und $x_2 = \sqrt{z_2}$. Aus $z_2=-1$ können wir die Wurzel nicht ziehen, sodass sich daraus keine Lösungen für die ursprüngliche Gleichung ergeben. Damit hat die ursprüngliche Gleichung zwei Lösungen:
$$x_{1/2} = \sqrt{z_1} = \sqrt{4} = \pm 2$$ 


# Vorzeichenuntersuchung

Wie zum Beispiel beim Satz von Vieta gesehen, kann man ganzrationale Terme höheren Grades häufig darstellen als Produkte von einfacheren Termen niedrigeren Grades. Sodann kann man untersuchen, wann welcher der Faktoren positiv bzw. negativ ist und daraus schließen wann das Produkt (und damit der eigentliche Term) positiv bzw. negativ ist:

**Beispiel**: \newline
Wann ist $f(x)=x^2-3x+4$ positiv und wann negativ? Wir haben oben schon gesehen, dass $f$ seine Nullstellen bei $x=-1$ und $x=4$ hat. Der Satz von Vieta sagt uns, dass $f(x) = (x-4)\cdot (x-(-1))$. (Wir prüfen das kurz nach: $(x-4)\cdot (x+1) = x^2-4x+x-4 = x^2 -3x -4$ - passt also...)
D.h. $f$ ist das Produkt auch $(x-4)$ und $(x+1)$ und wir könnene herausfinden wann $f(x)$ welches Vorzeichen trägt, indem wir uns ansehen, wann die Faktoren welches Vorzeichen haben:

$x$     $-----$  $-1$    $-----$ $0$  $+++++$ $4$  $+++++$
---     ------- ------   ------- ---  ------- ---  -------
$(x-4)$ $-----$  $-$     $-----$ $-$  $-----$ $0$  $+++++$ 
$(x+1)$ $-----$  $0$     $+++++$ $+$  $+++++$ $+$  $+++++$
$f(x)$  $+++++$  $0$     $-----$ $-$  $-----$ $0$  $+++++$ 

**Beispiel**: \newline
Wir wollen wissen, wann die Funktion $g(x)=4x^5-x^3$ positiv bzw. negativ ist. Wie oben analog besprochen, lässt sich die kleinste Potenz von $x$ (also $x^3$) hier ausklammern: $g(x)=x^3\cdot (4x^2-1)$. Der rechte Faktor wird $0$ bei $x=\pm\frac{1}{2}$ und wir können ihn noch weiter zerlegen: $4x^2-1 = 4(x+\frac{1}{2})\cdot (x-\frac{1}{2})$. Insgesamt erhalten wir also: $g(x) = x^3\cdot 4(x+\frac{1}{2})\cdot (x-\frac{1}{2})$

Die Vorzeichenuntersuchung sieht damit dermaßen aus:

$x$                 $-----$  $-\frac{1}{2}$  $-----$ $0$  $+++++$ $\frac{1}{2}$  $+++++$
---                 ------- ---------------  ------- ---  ------- -------------  -------
$x^3$               $-----$      $-$         $-----$ $0$  $+++++$    $+$         $+++++$ 
$4(x+\frac{1}{2})$  $-----$      $0$         $+++++$ $+$  $+++++$    $+$         $+++++$
$(x-\frac{1}{2})$   $-----$      $-$         $-----$ $-$  $-----$    $0$         $+++++$
$g(x)$              $-----$      $0$         $+++++$ $0$  $-----$    $0$         $+++++$ 

Hier werden also die Zeilen 2, 3 und 4, die das Vorzeichenverhalten der drei Faktoren widerspiegeln, "zusammenmmultipliziert", um dazulegen wann deren Produkt - also $g$ - positiv bzw. negativ ist.
