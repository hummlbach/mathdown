---
title: "Waldorf RAP 2020 P5/W3"
author: [Klasse 12]
date: 9.11.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Graph]
lang: de
header-center: "Analysis" 
---

Eine Funktion $f$ hat die Gleichung:

$$f(x)=\frac{1}{2}x^3-x^2-2x+4$$

Ihr Schaubild heiße $K_f$

1. Frei nach Aufgabe P 5:
   a) Berechnen Sie die Funktionswerte bei $x=-2$ und $x=2$
   b) Die Gerade $y=4$ schneidet $K_f$ in drei Punkten. Berechnen Sie diese.
   c) Berechnen Sie die Koordinaten der Extrempunkte von $K_f$. Handelt es sich dabei um Hoch- oder Tiefpunkte?
   d) Tragen Sie die berechneten Punkte in ein Koordinatensystem ($1$LE = $1$cm) ein und zeichnen Sie $K_f$.

2. Frei nach Aufgabe W 3:
   a) Die Tangente $t_1$ an $K_f$ hat den Berührpunkt $P_1(-1|f(-1))$. Die Tangente $t_2$ an $K_f$ ist parallel zu $t_1$. Berechnen Sie die Koordinaten des Berührpunkts $B$ (von $t_2$ und $K_f$) sowie die Gleichung von $t_2$.
   b) Die Gerade $g$ gehe durch die Punkte $P_1$ und $P_2(0|f(0))$. Berechnen Sie die Gleichung von $g$ und den dritten Schnittpunkt $P_3$ von $g$ mit $K_f$.

3. Im Bild unten ist der Graph $K_g$ von $g(x)=\frac{f(x)}{x^3}$ zu sehen. Untersuchen die das Verhalten von $g$ an den Rändern und vergleichen Sie mit dem Bild.

   ![](Tritratrullala.png){height=300px}
