---
title: "Wiederholung Parabeln"
author: [Klasse 12]
date: 25.01.2021
keywords: [Parabeln, Scheitelform, Normalform]
lang: de
header-center: "Analysis" 
---

## I Starters

1. Zeichnen Sie die Graphen der folgenden Funktionen in ein Koordinatensystem ein:

   \Begin{multicols}{2}

   a) $p_1(x)=x^2$
   b) $p_2(x)=(x+3)^2$
   c) $p_3(x)=x^2-4$
   d) $p_4(x)=(x+3)^2+2$


   \End{multicols}

2. Zeichnen Sie die Graphen der Funktionen $q_i$ in ein **weiteres** Koordinatensystem ein:

   \Begin{multicols}{2}

   a) $q_1(x)=\frac{1}{4}x^2$
   b) $q_2(x)=2x^2$
   c) $q_3(x)=-x^2+3$
   d) $q_4(x)=-\frac{1}{2}(x-4)^2+3$

    
   \End{multicols}

## II Hauptgang

1. Zeichnen Sie die Graphen der Funktionen wiederum in ein neues Koordinatensystem ein:

   \Begin{multicols}{2}
   
   a) $f_1(x)=x^2+4x+4$
   b) $f_2(x)=x^2-6x+9$
   c) $f_3(x)=x^2+4x+6$
   d) $f_4(x)=x^2+4x+6$
   e) $f_5(x)=2x^2-4x+6$

    
   \End{multicols}

2. Zeichnen Sie die Graphen der Funktionen $f$ und $g$ in ein Koordinatensystem:
   a) $f(x)=-\frac{1}{2}x^2+4x+6$
   b) $g(x)=-2x^2+x+1$

## III Dessert

1. Berechnen Sie die Schnittpunkte von ...

   \Begin{multicols}{2}

   a) ... $f$ mit der $y$-Achse
   b) ... $g$ mit der $y$-Achse
   c) ... $f$ mit der $x$-Achse
   d) ... $g$ mit der $x$-Achse
   e) ... von $f$ und $g$

    
   \End{multicols}
