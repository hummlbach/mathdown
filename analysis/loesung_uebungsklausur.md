---
title: "Lösung zur Übungsklausur"
author: "Klasse 12"
date: 01.10.2021
keywords: [Ableitung, Extrema, Minimum, Maximum, Wendepunkte]
lang: de
header-center: "Analysis" 
---

1. Die Schnittpunkte mit der $x$-Achse sind die Nullstellen der Funktion. Wir suchen also $x$-Werte sodass $f(x)=0$.
   $$\begin{array}{ll}
   &0 = 3x^5 - 5x^3 = x^3 (3x^2-5) \\
   \Leftrightarrow &x^3 = 0 \text{ oder } 3x^2-5 = 0 \qquad |\, +5, :3 \\
   \Leftrightarrow &x = 0 \text{ oder } x^2 = \frac{5}{3} \qquad | \, \sqrt{\cdot} \\
   \Leftrightarrow &x = 0 \text{ oder } x = \pm\sqrt{\frac{5}{3}}
   \end{array}$$

   $G_f$ hat drei Schnittpunkte mit der $x$-Achse: $S_1(-\sqrt{\frac{5}{3}}|0)$, $S_2(0|0)$ und $S_3(\sqrt{\frac{5}{3}}|0)$.

2. Ein notwendiges Kriterium dafür, dass $f$ bei $z$ ein Minimum oder Maximum hat, ist dass die erste Ableitung $f'$ bei $z$ "verschwindet". Um Kandidaten für Extrema zu finden, suchen wir also zunächst Nullstellen der ersten Ableitung $f'(x) = 15x^4-15x^2$:

   a)
   $$\begin{array}{ll}
   &0 = 15x^4 - 15x^2 = 15x^2 (x^2-1) \\
   \Leftrightarrow &15x^2 = 0 \text{ oder } x^2-1 = 0 \qquad |\, +1,  \sqrt{\cdot} \\
   \Leftrightarrow &x = 0 \text{ oder } x = \pm 1
   \end{array}$$

   Wir haben damit potenziell drei Extrempunkte mit den $y$-Werten $f(0)=0$, $f(-1)=2$ und $f(1)=-2$, also: $E_1(-1|2)$, $E_2(0|0)$ und $E_3(1|-2)$.

   b) Wir versuchen anhand der zweiten Ableitung $f''(x) = 60x^3 - 30x$ herauszufinden, bei welchem Punkt es sich um ein Minimum oder Maximum handelt:
      $$f(-1) = 60(-1)^3 - 30 (-1) = -60 +30 = -30 < 0 \quad \Rightarrow \quad E_1 \text{ ist ein Maximum}$$
      $$f(1) = 60(1)^3 - 30 (1) = 60 -30 = 30 > 0 \quad \Rightarrow \quad E_3 \text{ ist ein Minimum}$$
      Über $E_2$ können wir anhand der zweiten Ableitung keine Aussage treffen, da diese bei $x=0$ auch $0$ ist. Wir müssen also eine Vorzeichenuntersuchung von $f'(x) = 15x^4-15x^2 = 15x^2(x^2 -1) = 15$ durchführen:

      \pagebreak

      $x$     $-----$  $-1$    $-----$ $0$  $+++++$ $1$  $+++++$
      ---     ------- ------   ------- ---  ------- ---  -------
      $15x^2$ $+++++$  $+$     $+++++$ $0$  $+++++$ $+$  $+++++$
      $(x-1)$ $-----$  $-$     $-----$ $-$  $-----$ $0$  $+++++$ 
      $(x+1)$ $-----$  $0$     $+++++$ $+$  $+++++$ $+$  $+++++$
      $f'(x)$ $+++++$  $0$     $-----$ $-$  $-----$ $0$  $+++++$ 

      Es findet also kein Vorzeichenwechsel der ersten Ableitung bei $0$ statt, woraus wir schließen können, dass es sich um bei $E_2$ um einen Terrassenpunkt handelt.
     
3. a) Ein notwendiges Kriterium für einen Wendepunkt ist, dass die zweite Ableitung am betreffenden $x$-Wert $0$ ist. Wir suchen also nach Nullstellen der zweiten Ableitung:
      $$\begin{array}{ll}
      &0 = 60x^3 - 30x = 30x (2x^2-1) \\
      \Leftrightarrow &30x = 0 \text{ oder } 2x^2-1 = 0 \qquad |\, +1,  :2, \sqrt{\cdot} \\
      \Leftrightarrow &x = 0 \text{ oder } x = \pm\sqrt{\frac{1}{2}}
      \end{array}$$
      Um sicherzustellen, dass es sich bei den gefundenen Nullstellen $-\sqrt{\frac{1}{2}}$, $0$ und $\sqrt{\frac{1}{2}}$ tatsächlich um Wendepunkte handelt, rechnen wir nach, dass die dritte Ableitung $f'''(x) = 180x^2 -30$ dort ungleich $0$ ist:
      $$f'''(-\sqrt{\frac{1}{2}}) = 180 (-\sqrt{\frac{1}{2}})^2-30 = 180 \cdot \frac{1}{2} -30 = 60$$
      $$f'''(\sqrt{\frac{1}{2}}) = 60$$
      $$f'''(0) = -30$$
      An allen drei $x$-Werten liegen also Wendepunkte vor. Wir rechnen die zugehörigen $y$-Koordinaten aus:
      $$f(-\sqrt{\frac{1}{2}}) \approx 1,24$$
      $$f(0) = 0 $$
      $$f(\sqrt{\frac{1}{2}}) \approx -1,24$$
      Der Graph von $f$ hat also die folgenden drei Wendepunkte: $W_1(-\sqrt{\frac{1}{2}}|1,24 )$, $W_2(0|0)$ und  $W_3(\sqrt{\frac{1}{2}}|-1,24)$

   b) Der Graph von $f$ ist linksgekrümmt, wannimmer $f''$ positiv ist, und rechtsgekrümmt, wenn die zweite Ableitung kleiner als $0$ ist. Wir betrachten also das Vorzeichen der zweiten Ableitung $f''(x) = 30x(2x^2-1) = 30x\cdot 2 \cdot (x+\sqrt\frac{1}{2}) \cdot (x-\sqrt\frac{1}{2})$:

      $x$                     $-----$  $-\sqrt{\frac{1}{2}}$    $-----$ $0$  $+++++$ $\sqrt{\frac{1}{2}}$   $+++++$
      ---                     ------- ------                    ------- ---  ------- ---                    -------
      $60x$                   $-----$  $-$                      $-----$ $0$  $+++++$ $1$                    $+++++$
      $(x-\sqrt\frac{1}{2})$  $-----$  $-$                      $-----$ $-$  $-----$ $0$                    $+++++$ 
      $(x+\sqrt\frac{1}{2})$  $-----$  $0$                      $+++++$ $+$  $+++++$ $+$                    $+++++$
      $f''(x)$                $-----$  $0$                      $+++++$ $0$  $-----$ $0$                    $+++++$ 

      $G_f$ ist also rechtsgekrümmt über den Intervallen $]-\infty;-\sqrt\frac{1}{2}[$ sowie $]0;\sqrt\frac{1}{2}[$ und linksgekrümmt in den Bereichen $]-\sqrt\frac{1}{2};0[$ sowie $]\sqrt\frac{1}{2};\infty[$

4. $f(1,5) \approx -5,9$ und $f(-1,5) \approx 5,9$

5. (Die Vorgabe, dass eine Längeneinheit genau $2$ cm entsprechen soll, wird hier nicht berücksichtigt.)

   ![](min_max_tp_uebklausur.png){}

6. Wenn $f$ die Ableitung von $g$ ist, so können wir aus den Nullstellen und dem Vorzeichenverhalten von $f$ schließen, wo $g$ Extrema hat. $f$ (also die Ableitung von $g$) ist in den Intervallen $]-\infty;-\sqrt\frac{5}{3}[$ sowie $]0;\sqrt\frac{5}{3}[$ negativ (Graph ist unter der $x$-Achse zu finden), woraus folgt, dass $g$ dort streng monoton fallend ist. In den Intervallen $]-\sqrt\frac{5}{3};0[$ sowie $]\sqrt\frac{5}{3};\infty[$ ist $f$ positiv und $g$ somit streng monoton wachsend. An den Nullstellen von $f$, nämlich $-\sqrt\frac{5}{3}$, $0$ und  $\sqrt\frac{5}{3}$ finden jeweils Vorzeichenwechsel, erst von $-$ nach $+$, dann von $+$ nach $-$ und schließlich wieder von $-$ nach $+$, statt. D.h. $g$ hat Minima bei $x=\pm\sqrt\frac{5}{3}$ und ein Maximum bei $x=0$.
