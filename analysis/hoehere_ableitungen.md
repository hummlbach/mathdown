---
title: "Höhere Ableitungen"
author: [Klasse 12]
date: 17.09.2021
keywords: [Extrema, Minimum, Maximum, Wendepunkt, Kurvendiskussion]
lang: de
header-center: "Analysis" 
---

# I Starters

Die zurückgelegte Strecke in Metern der (vollständig wiederverwendbaren) ersten Stufe einer Rakete folgt sehr grob der folgenden Funktion in Abhängigkeit von der Zeit in Sekunden: $s(t)=\frac{1}{30}t^3$

a) Welche Strecke legt die Rakete in den ersten $60$ Sekunden zurück?
b) Wie groß ist die Geschwindigkeit der Rakete nach $80$ Sekunden?
c) Wann erreicht die Rakete eine Geschwindigkeit von $490\frac{m}{s}$?
d) Welche durchschnittliche Beschleunigung weißt die Rakete in den ersten $60$ Sekunden auf?
e) Was ist die durchschnittliche Beschleunigung zwischen dem Ende der $60.$ und dem Ende der $90.$ Sekunde?
f) Wie groß ist die "Momentanbeschleunigung" der Rakete am Ende der $60.$ Sekunde?

# II Hauptgang

1. Gegeben sei die Funktion $f(x) = \frac{1}{13}x^3-\frac{15}{13}x^2+\frac{63}{13}x$.
   a) Wo hat der Graph von $f$ Hoch- oder Tiefpunkte? Geben Sie Koordinaten (also sowohl $x$- als auch $y$-Wert) an.
   c) Füllen Sie die folgende Wertetabelle aus und zeichnen Sie den Graph von $f$ in ein Koordinatensystem ein.

      $x$       $-1$    $0$     $1$     $2$     $5$     $8$     $9$     $10$
      ---       ----    ---     ---     ---     ---     ---     ---     ---
      $f(x)$

   d) Zeichnen Sie den Graph der ersten Ableitung $f'$ in das Koordinatensystem ein.
   e) Berechnen Sie die Ableitung von $f'$ - namentlich $f''$ - und zeichnen Sie ihren Graph ebenfalls ins Koordinatensystem ein.

2. Gegeben sei die Funktion $g(x) = -\frac{1}{144}x^3-\frac{13}{48}x^2-2x$.
   a) Wo hat der Graph von $f$ Hoch- oder Tiefpunkte? Geben Sie Koordinaten (also sowohl $x$- als auch $y$-Wert) an.
   b) In welchen Bereichen "fällt" und in welchen "steigt" der Graph?
   c) Füllen Sie die folgende Wertetabelle aus und zeichnen Sie den Graph von $g$ in ein Koordinatensystem (mit $3$ Längeneinheiten pro $cm$) ein.

      $x$       $-3$    $0$     $3$     $6$     $9$     $15$    $18$    $24$    $27$    $30$
      ---       ----    ---     ---     ---     ---     ---     ---     ---     ---     ---
      $g(x)$

   d) Zeichnen Sie den Graph der ersten Ableitung $g'$ in das Koordinatensystem ein.
   e) Berechnen Sie die Ableitung von $g'$ - namentlich $g''$ - und zeichnen Sie ihren Graph ebenfalls ins Koordinatensystem ein.

3. Stellen Sie sich die Funktionsgraphen als Kurvenfahrt (Draufsicht von oben) vor. Der Graph wird so abgefahren, sodass die $x$-Werte immer größer werden.

   \Begin{multicols}{2}
   ![](symmetrie_1.png){}
   ![](symmetrie_2.png){}
   \End{multicols}

   \Begin{multicols}{2}
   ![](symmetrie_3.png){}
   ![](symmetrie_4.png){}
   \End{multicols}

a) Markieren Sie alle Rechtskurven rot.
b) Färben Sie alle Linkskurven blau.
c) Sehen Sie einen Zusammenhang mit Hoch- und Tiefpunkten?
d) In welchen Bereichen nimmt die Steigung zu? In welchen ab?


