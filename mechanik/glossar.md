---
title: "Mechanik"
author: [Klasse 10]
date: 01.05.2021
keywords: [Mechanik, Kraft, Dynamik]
lang: de
---


# Glossar

Formelzeichen   "Definition"              Bedeutung           Einheit                     Name der Einheit
-------------   ------------              ---------           ---------------             -----------------
$s$                                       Strecke             $m$                         Meter
$t$                                       Zeit                $s$                         Sekunde
$v$             $\frac{s}{t}$             Geschwindigkeit     $\frac{m}{s}$
$a$             $\frac{v}{t}$             Beschleunigung      $\frac{m}{s^2}$
$g$                                       Erdbeschleunigung   $\frac{m}{s^2}$
$m$                                       Masse               $kg$                        Kilogramm
$F$             $m\cdot a$                Kraft               $N=kg\cdot\frac{m}{s^2}$    Newton
$F_G$           $m\cdot g$                Gewichtskraft       $N$                         Newton
$F_H$                                     Hangabtriebskraft   $N$                         Newton
$W$             $F\cdot s$                Arbeit              $J=N\cdot m$                Joule
$P$             $\frac{W}{t}$             Leistung            $W=\frac{J}{s}$             Watt
$E_{kin}$       $\frac{m\cdot v^2}{2}$    Kinetische Energie  $J$                         Joule
$E_{pot}$       $m\cdot g\cdot h$         Potentielle Energie $J$                         Joule

