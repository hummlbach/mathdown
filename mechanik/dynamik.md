---
title: "Mechanik"
author: [Klasse 10]
date: 21.04.2021
keywords: [Mechanik, Kraft, Dynamik]
lang: de
---


# Übungen zur Dynamik

1. Zur Rettung aus Gletscherspalten (z.B.), kann man aus Seilen und Karabinern einen notdürftigen Flaschenzug bauen. Die Karabiner übernehmen dabei die Rolle der Rollen. Wieviele Karabiner braucht man mindestens, damit eine $50$ kg schwere Person eine $105$ kg schwere Person retten kann und wieviel Gewicht, muss die rettende Person dabei ziehen?

2. Beim Bau der Pyramiden haben die alten Ägypter (mutmaßlich) Rampen verwendet, um die mehrere Tonnen schweren Steine vom Hafen zum Bauplatz zu befördern. Wieviel $kg$ müssen die Arbeiter dabei ziehen bzw. drücken, während sie einen $2,5$ t schweren Steinblock eine um $5$ Grad geneigte Rampe hinaufbefördern?

3. \Begin{multicols}{2}

   Ein Ballonfahrer fürchtet an einer Felsspitze hängen zu bleiben. Auf seinen Ballon wirken, wim im Bild eingezeichnet, drei Kräfte: Die Gewichtskraft $\vec{S}$, die Kraft eines Aufwindes $\vec{W}$, und die Auftriebskraft $\vec{A}$:

   a) Konstruieren Sie die aus den $3$ Kräften resultierende Kraft.
   b) Wird der Ballon am Felsen hängen bleiben?


   ![](ballon.png){height=200px}

   \End{multicols}


4. Wieso haben alle Hubschrauber (bzw. Multicopter) mindestens $2$ Rotoren?

5. Ein Tennisball wird so geschmettert, dass er mit durchschnittlich $180$ km/h (anfänglich) horizontal fliegt. Wie weit fliegt der Ball, bevor er auf dem Boden aufkommt (wenn wir den Luftwiderstand vernachlässigen)?



