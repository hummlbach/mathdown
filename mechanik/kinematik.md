---
title: "Mechanik"
author: [Klasse 10]
date: 09.05.2022
keywords: [Mechanik, Kinematik, Beschleunigung]
lang: de
---


# Übungen zur Kinematik

1. Nürnberg und München liegen (ungefähr) $150$ km voneinander entfernt. Der ICE braucht für diese Strecke knapp $1$ Stunde und $10$ Minuten. Was ist die Durchschnittsgeschwindigkeit des ICEs auf dieser Strecke?

2. Kurt läuft einen Cooper-Test (lauf $12$ Minuten so weit wie Du kannst). Aus dem Training weiß Kurt, dass er über diese Dauer ein Tempo von $18$ km/h durchhalten kann. Wie weit kommt er?

6. Ein Urlauber plant täglich mit einer Durchschnittgeschwindigkeit von $80$ km/h zu fahren. Wie weit kommt er in einer Woche, wenn er jeden Tag $9$ h hinter dem Steuer sitzt? 

7. Ein Auto benötigt von $0$ auf $100$ km/h eine Zeit von $9$ s. Mit welcher Beschleunigung (in m/s²) wurde das Auto beschleunigt?

3. \Begin{multicols}{2}
   
   Der Mond kreist in ca. $390000$ km Entfernung um die Erde. Die Apollo-Raumschiffe wurden für ihre Reisen zum Mond auf eine Geschwindigkeit von $39400$ km/h beschleunigt. Die erste Stufe der Saturn-V-Rakete beschleunigte das Raumschiff dabei binnen $2,5$ Minuten auf $8600$ km/h.

   a) Wie lange dauerte die Reise zum Mond bei einer Durchschnittsgeschwindigkeit von $5130$ km/h? (Wir vernachlässigen, dass die Flugbahn einer Ellipse folgt.)
   b) Wie groß ist die Beschleunigung der ersten Stufe im Schnitt?


      ![](saturn_v.png){height=280px}

   \End{multicols}


4. Auf trockener Straße, geht man im besten Fall von einer Bremsbeschleunigung von $-8$ m/$\text{s}^\text{2}$ aus. Wie lang ist der Bremsweg bei einer Vollbremsung aus einer Geschwindigkeit von $50$ km/h?

5. Ein Stein wird in einen Brunnen geworfen. Nach $3$ Sekunden hören wir, wie der Stein ins Wasser platscht. Wie tief ist der Brunnen?

6. Ein Kunstspringer springt vom $10$-Meter-Brett. Mit welcher Geschwindigkeit taucht er ins Wasser ein?

