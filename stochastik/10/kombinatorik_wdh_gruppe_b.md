---
title: "Gruppenarbeit zur Kombinatorik"
author: "Gruppe B: Variation ohne Wiederholung"
date: 07.07.2022
keywords: [Variation]
lang: de
header-center: "Stochastik" 
---

1. Bringen Sie in Erfahrung was Variation ohne Wiederholung bedeutet, so dass Sie es im Vortrag definieren können.

2. Geben Sie die Formel zur Berechnung der Variationen $k$-ter Klasse aus $n$ Dingen ohne Wiederholung an und erläutern Sie sie zum Beispiel an einem Urnenexperiment in dem mehrmals gezogen wird.

3. Gegeben sei ein Kartenspiel mit $32$ paarweise verschiedenen Karten - je $8$ Karten von Herz, Kreuz, Pik und Karo. Berechnen Sie die Wahrscheinlichkeit, dass $4$ zufällig gezogene Karten alle von der Farbe Herz sind.

4. Berechnen Sie die Wahrscheinlichkeit dafür mit $5$ Würfeln eine große Straße - also $5$ verschiedene Zahlen - zu würfeln.

5. Rechnen Sie Beispiel 3 oder 4 vor und überlegen Sie sich ein weiteres, das Sie ihre Mitschüler rechnen lassen.


\pagebreak

### Tipps zu ...

3. - Gehen Sie davon aus, dass die Reihenfolge relevant ist.
   - Wieviele Möglichkeiten gibt es insgesamt $4$ von $32$ Karten zu ziehen?
   - Wieviele Möglichkeiten gibt es $4$ von den $8$ Herzkarten zu ziehen?

4. $|\Omega| = 6^5$

