
---
geometry: margin=2.5cm
papersize: a4
--- 

## Das Ziegenproblem

Das Ziegenproblem - auch Monty-Hall-Problem genannt - erlangte durch einen Artikel, der 1990 in der Zeitschrift Parade erschien, Berümtheit. Das Problem zeichnet sich dadurch seine Kontraintuitivität aus. Der naive, intuitive Gedankengang führt zu falschen Ergebnissen. Tausende Leserbriefe aber Anekdoten zu Folge auch hochrangige Mathematiker (zumindest einer, nämlich Paul Erdösz), bezweifelten die im Artikel (richtig) angegebene Lösung. Der Artikel (von Marilyn vos Savants) beschrieb das Problem wie folgt: \newline

> "Nehmen Sie an, Sie wären in einer Spielshow und hätten die Wahl zwischen drei Toren. Hinter einem der Tore ist ein Auto, hinter den anderen sind Ziegen. Sie wählen ein Tor, sagen wir, Tor Nummer 1, und der Showmaster, der weiß, was hinter den Toren ist, öffnet ein anderes Tor, sagen wir, Nummer 3, hinter dem eine Ziege steht. Er fragt Sie nun: ‚Möchten Sie das Tor Nummer 2?‘ Ist es von Vorteil, die Wahl des Tores zu ändern?"

Die Intuition sagt wohl den allermeisten Menschen, es spiele hier keine Rolle, ob man wechselt oder nicht. Gegeben der Showmaster muss in jedem Fall, gleich ob hinter dem zuerst gewählten Tor eine Ziege oder das Auto ist, eins der anderen Tore öffnen und den Wechsel anbieten, verspricht der Wechsel tatsächlich eine Erfolgswahrscheinlichkeit von $\frac{2}{3}$. Wie kann man diese Einsicht erlangen?

Genauer folge das Spiel den folgenden Regeln:

1. Ein Auto und zwei Ziegen werden zufällig auf drei Tore verteilt.
2. Zu Beginn des Spiels sind alle Tore verschlossen, sodass Auto und Ziegen nicht sichtbar sind.
3. Der Kandidat, dem die Position des Autos völlig unbekannt ist, wählt ein Tor aus, das aber vorerst verschlossen bleibt.
4. Fall A: Hat der Kandidat das Tor mit dem Auto gewählt, öffnet der Moderator eines der beiden anderen Tore, hinter dem sich immer eine Ziege befindet. Wir gehen davon aus, dass der Moderator rein zufällig entscheidet, welches der beiden Tore er öffnet.
5. Fall B: Hat der Kandidat ein Tor mit einer Ziege gewählt, dann muss der Moderator dasjenige der beiden anderen Tore öffnen, hinter dem die zweite Ziege steht.
6. Der Moderator bietet dem Kandidaten an, seine Entscheidung zu überdenken und das andere ungeöffnete Tor zu wählen.
7. Das vom Kandidaten letztlich gewählte Tor wird geöffnet, und er erhält das Auto, falls es sich hinter diesem Tor befindet.

Wir können das Spiel als mehrstufiges Zufallsexperiment auffassen. 

1. Zuerst wird zufällig gleichwahrscheinlich das Auto hinter einem der Tore geparkt.
2. Der Spieler wählt (zufällig) eines der Tore.
3. Der Showmaster öffnet eines der verbleibenden Tore hinter dem kein Auto steht. (Hat er mehrere zur Auswahl, trifft er die Enscheidung wiederum zufällig.)
4. Zuletzt entscheidet der Spieler, ob er wechselt oder nicht.

Dies können wir auf zwei wesentliche Stufen reduzieren. Die ersten beiden Schritte bedeuten zusammengefasst, dass der Spieler eines der Tore wählt und sich mit Wahrscheinlichkeit $\frac{1}{3}$ ein Auto dahinter befindet. Die zweite Stufe ist das öffnen eines der beiden nicht gewählten Tore durch den Showmaster. Es ergibt sich folgendes Baumdiagramm:

![](baumdiagramm_ziegenproblem.png){height=200px}

Die Situationen entsprechen in etwa den folgenden (von Wikipedia geklauten) Bildern:

![](Ziegenproblem_1.png){height=75px} \ $\quad\quad$ ![](Ziegenproblem_2.png){height=75px} \ $\quad\quad$ ![](Ziegenproblem_3.png){height=75px} \ $\quad\quad$ ![](Ziegenproblem_4.png){height=75px}

Anhand der Pfadregeln können wir die Wahrscheinlichkeiten für die verschiedenen Situationen ausrechnen. Wir landen mit Wahrscheinlichkeit $\frac{1}{3}\cdot\frac{1}{2}$ in der ersten Situation (ganz links), ebenso mit Wahrscheinlichkeit $\frac{1}{3}\cdot\frac{1}{2}$ in der zweiten Situation. In diesen Situationen, also zusammengenommen mit einer Wahrscheinlichkeit von $\frac{1}{3}$, ist der Wechsel des Tores unvorteilhaft. Die beiden anderen Fälle treten zusammen mit Wahrscheinlichkeit $\frac{2}{3}$ auf.

