---
title: "Prozentrechnung"
author: [Klasse 10]
date: 27.06.2022
keywords: [Pythagoras, Dreiecke, Winkelsumme, Kongruenz]
lang: de
header-center: "Stochastik" 
geometry:
  - top=3.5cm
  - bottom=4cm
---

Teile der Aufgaben sind aus dem Buch "Schnittpunkt 6 Mathematik", erschienen im Klett Verlag, geklaut.

1. Gehaltserhöhung
   a) Frau Hamann verdient monatlich $3215,62$ Euro. Sie erhält eine Gehaltserhöhung von $2,3$%. Wieviel verdient sie danach?
   b) Ein Kollege von Frau Hamann verdient nach der Erhöhung um $2,3$% jetzt $3056,26$ Euro. Berechnen sie die Gehaltserhöhung in Euro.
   c) Bei Frau Kleiner machte die Gehaltserhöhung von $2,3$% genau $66,61$ Euro aus. Berechnen Sie das Gehalt vor der Erhöhung.

2. Diagramme
   \Begin{multicols}{2}

   ![](beliebtestehaustiere.jpg){}
   ![](co2ausstoss.jpg){}
   ![](brot_fuer_die_welt.png){}
   ![](dwo_weltbevoelkerung.jpg){}

   \End{multicols}
   a) Wieviel Prozent der Haustiere in Deutschland sind Hunde? Wieviele sind Katzen?
   b) Berechnen Sie wieviel von Ihrem Geld tatsächlich "in Brot investiert" wird, wenn Sie $75$ Euro an "Brot für die Welt" spenden.
   c) Berechnen Sie Deutschlands Anteil am europäischen $\text{CO}_2$-Ausstoß.
   d) Um wieviel Prozent, wird die Weltbevölkerung im Jahr $2100$ seit "Heute", laut der Vorhersage, gewachsen sein?
   e) Berechnen Sie die Anzahl der Europäer "Heute", sowie $2100$ (nach Vorhersage).

\pagebreak

3. Der Preis eines Einzelfahrscheins für drei Zonen stieg zu Jahresbeginn von $3,30$ Euro auf $3,60$ Euro und im Oktober nochmals um denselben Betrag.
   Berechnen Sie die jeweilige prozentuale Erhöhung. Um wieviel Prozent stieg der Preis insgesamt?

4. Vom 1. Juni bis zum 31. August 2022 gilt eine befristete Senkung der Energiesteuer auf Krafstoffe in Deutschland.
   Der Energiesteuersatz für Benzin wurde von $65,45$ Cent/Liter auf $35,9$ Cent/Liter um $29,55$ Cent/Liter reduziert.
   a) Geben Sie an um wieviel Prozent der Steuersatz gesenkt wurde.
   b) Welcher prozentuale Preisnachlass, würde sich bei einem Benzinpreis von $2$ Euro, ergeben wenn der Nachlass (in voller Höhe) an den Kunden weitergegeben würde?

5. Der Preis eines Snowboards wird zuerst um $10$% und dann im Räumungsverkauf nochmals um $40$% gesenkt. Jetzt (nach den Preissenkungen) kostet es $269$ Euro.
   a) Wie hoch war der ursprüngliche Preis?
   b) Wieviel Euro machten die einzelnen Preissenkungen aus?
   c) Berechnen den gesamten Preisnachlass in Prozent.



