---
title: "Baumdiagramme"
author: [Klasse 10]
date: 10.07.2022
keywords: [Baumdiagramme, Mehrstufige Zufallsexperimente]
lang: de
header-center: "Stochastik" 
---

1. Aus einer Urne, in der sich $2$ grüne und $3$ rote Kugeln befinden, werden $4$ Kugeln nacheinander mit Zurücklegen gezogen.
   a) Malen Sie ein Baumdiagramm zu diesem Experiment (mit den Wahrscheinlichkeiten für die Ergebnisse der jeweiligen Stufe an den Kannten).
   b) Geben Sie die Wahrscheinlichkeit dafür an, dass nur rote Kugeln gezogen werden.
   b) Berechnen Sie die Wahrscheinlichkeit dafür, dass beim ersten und beim letzten Zug eine grüe Kugel gezogen wird.

2. Ein Tetraeder wird zwei mal geworfen. (Ein Tetraeder ist eine gleichseitige Pyramide mit dreieckiger Grundfläche. D.h. ein Tetraeder hat insgesamt $4$ Seiten.)
   a) Zeichnen Sie ein Baumdiagramm  zu diesem mehrstufigen Zufalls-Experiment.
   b) Wie groß ist die Wahrscheinlichkeitm dafür, dass beim ersten Wurf die $1$ und beim zweiten die $2$ fällt.
   c) Berechnen Sie die Wahrscheinlichkeitm dafür, dass der Tetraeder beim ersten Wurf auf eine gerade Zahl und beim zweiten Wurf auf die $1$ fällt.
   d) Berechnen Sie die Wahrscheinlichkeit für das Ereignis "Der zweite Tetraeder fällt auf die $4$".

3. In einer Urne befinden sich eine blaue, eine rote, eine gelbe, eine schwarze und eine weiße Kugel. Aus der Urne werden sukzessive $3$ Kugeln ohne Zurücklegen gezogen.
   a) Malen Sie ein Baumdiagramm, das dieses Experiment darstellt.
   b) Berechnen Sie die Wahrscheinlichkeit dafür, dass zuerst die gelbe, dann die blaue und zuletzt die rote Kugel gezogen wird.
   c) Berechnen Sie die Wahrscheinlichkeit des Ereignisses "Die rote Kugel wird gezogen".

4. In einer Urne befinden sich eine gelbe, eine blaue, eine rote und eine weiße Kugel. Es werden $2$ Kugeln gleichzeitig gezogen. Ermitteln Sie die Wahrscheinlichkeit für das Ereignis "Die gelbe Kugel wird gezogen".

5. Zwei unterscheidbare Würfel - einer rot, einer weis - werden geworfen.
   a) Berechnen Sie die Wahrscheinlichkeit $P(A)$ des Ereignisses $A=\text{"Augensumme $6$"}$.
   b) Berechnen Sie die Wahrscheinlichkeit dafür, dass rote Würfel eine gerade Zahl der weise Würfel eine Zahl größer als $3$ zeigt.

6. In einer Sockenschublade befinden sich einzeln $4$ verschiedene Paar Socken. Berechnen Sie die Wahrscheinlichkeit
   a) bei $2$ entnommennen Socken schon ein Paar zusammen zu haben.
   b) bei $4$ entnommennen Socken noch kein Paar zusammen zu haben.


