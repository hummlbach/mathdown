---
title: "Relative Häufigkeit"
author: [Klasse 10]
date: 13.07.2022
keywords: [Wahrscheinlichkeit, Baumdiagramme]
lang: de
footer-center: "Stochastik" 
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=2.5cm
  - right=2.5cm
---


## I Basketball-Freiwurfquote

Aleksi und Paul werfen Freiwürfe. Während Aleksi von $40$ Würfen $8$ trifft, versenkt Paul $5$ von $20$ Freiwürfe.
Berechnen Sie die relativen Häufigkeiten eines Treffers für die beiden Versuchsreihen. Wer ist der bessere Werfer?

## II Reisnagel

Ermitteln Sie wie wahrscheinlich es ist, dass der gegebene Reisnagel auf dem Kopf landet.

### III Fairer Würfel?

Ein Würfel wird $200$ mal mit folgenden Ergebnissen geworfen:

Ergebnis                1       2       3       4       5       6
---------               ----    ---     ----    ----    ----    ----
Absolute Häufigkeit     12      30      44      37      49      28

a) Berechnen Sie die relatien Häufigkeiten der Ergebnisse.
b) Diskutieren Sie, ob der Würfel fair ist oder nicht.

## VI Buchstabenhäufigkeit

a) Geben Sie an, welche Buchstaben jeweils wie oft in diesem Satz vorkommen. Und bestimmen Sie dann
die relative Häufigkeit von Vokalen und Konsonanten:

+---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Buchstaben    |A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|R|S|T|U|V|W|Z|
+---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Anzahl        | | | | | | | | | | | | | | | | | | | | | | |
+---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| $h_{60}$ in % | | | | | | | | | | | | | | | | | | | | | | |
+---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

b) Entschlüsseln Sie den folgenden Text:

   Qvrfre Grkg jne EBG-Qervmrua-irefpuyhrffryg. Qnf urvffg zna puvssevreg ormvruhatfjrvfr qrpuvssevreg Anpuevpugra, vaqrz zna vz Nycunorg qervmrua Ohpufgnora jrvgremnruyg, nyfb fbmhfntra qnf Nycunorg hz qervmrua Ohpufgnora qerug (Ebgngvba). Qvrfr Irefpuyhrffryhat vfg rva Fcrmvnysnyy qre Zrgubqr, qvr Pnrfne ibe zrue nyf mjrvgnhfraq Wnuera orahgmgr haq jveq qnure nhpu rvar Pnrfne-Irefpuyhrffryhatra tranaag. Fvr vfg bssrafvpugyvpu avpug orfbaqref fvpure... ;-)

   Tipp: Häufigkeitsanalyse

