---
title: "Ergebnisse und Ereignisse"
author: [Klasse 10]
date: 04.07.2022
keywords: [Ergebnisse, Ereignisse, Mengen]
lang: de
header-center: "Stochastik" 
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=2cm
  - right=2cm
---

## I Starters
1. Ein Tetraeder wird zwei mal geworfen. (Ein Tetraeder ist eine gleichseitige Pyramide mit dreieckiger Grundfläche. D.h. ein Tetraeder hat insgesamt $4$ Seiten.)
   a) Zeichnen Sie ein Baumdiagramm, das dieses Experiment darstellt und geben Sie den Ergebnisraum $\Omega$ an. 
   b) Wieviele Elemente hat das Ereignis $A$ "Der zweite Tetraeder fällt auf die $4$". Schreiben Sie $A$ hin.
2. Ein Dodekaeder ($12$ gleiche Flächen) wird geworfen. Was ist die Schnittmenge $A\cap B$ von den Ereignissen $A$ eine Zahl kleiner als $9$ wird geworfen und dem Ereignis $B$ eine Zahl größer als $3$ wird geworfen?
3. Ein Ikosaeder ($20$ Flächen) wird geworfen. Was ist die Vereinigung $A \cup B$ der Ereignisse $A$ "Die geikosaederte Zahl ist größer als $13$" und $B$ "Die vom Ikosaeder gezeigte Zahl ist gerade"?

## II Hauptgang
1. Aus einer Urne, in der sich $10$ grüne und $10$ rote Kugeln befinden, werden $4$ Kugeln sukzessive (nacheinander) gezogen.
   a) Malen Sie ein Baumdiagramm zu diesem Experiment und geben Sie den Ergebnisraum $\Omega$ an.
   c) Wieviele Möglichkeiten gibt es dafür, dass beim letzten Zug eine grüne Kugel gezogen wird?
   d) Schreiben Sie das Ereignis "Es wird keine grüne Kugel gezogen" hin.
2. In einer Urne befinden sich eine blaue, eine rote, eine gelbe, eine schwarze und eine weiße Kugel. Aus der Urne werden sukzessive $3$ Kugeln ohne Zurücklegen gezogen.
   a) Malen Sie ein Baumdiagramm, das dieses Experiment darstellt.
   b) Wie groß ist der Ergebnisraum $\Omega$?
   b) Wieviele Elemente hat das Ereignis "Die rote Kugel wird gezogen"?
3. In einer Urne befinden sich eine gelbe, eine blaue, eine rote und eine weiße Kugel. Es werden $2$ Kugeln gleichzeitig gezogen.
   a) Geben Sie den Ergebnisraum an.
   b) Schreiben Sie das Ereignis "Die gelbe Kugel wird gezogen" als Menge auf.
3. Zwei unterscheidbare Würfel - einer rot, einer weis - werden geworfen.
   a) Berechnen Sie die Mächtigkeit des Ergebnisraums $\Omega$.
   b) Wieviele Elemente hat das Ereignis $A$ "Augensumme $7$"? Schreiben Sie das Ereignis als Menge.
   c) Was ist der Schnitt $A \cap B$ der Ereignisse $A$ "Der rote Würfel zeigt eine gerade Zahl" und $B$ "Der weise Würfel zeigt eine Zahl größer als $3$"?
   d) Was ist die Vereinigung der Ereignisse $A$ "Der weise Würfel zeigt eine $6$" und dem Ereignis $B$ "Der rote Würfel zeigt eine $6$"?

## III Dessert

1. In wieviel Prozent der Fälle, fällt ein Würfel...
   a) auf die $6$?
   b) nicht auf die $6$?
   c) auf $1$ oder $2$?
   d) auf eine ungerade Zahl?

2. In wieviel Prozent der Fälle, fällt beim Werfen zweier (unterscheidbarer) Würfel...
   a) ein Pasch? 
   b) Augenzahl $7$?

3. In wieviel Prozent der Fälle, fällt beim Zufallsexperiment aus I 1. der Tetraeder beim ersten Wurf auf eine gerade Zahl und beim zweiten Wurf auf die $1$?

4. In wieviel Prozent der Fälle, tritt beim Experiment aus II 2. das in c) beschriebene Ereignis ein?

5. In wieviel Prozent der Fälle, tritt beim Experiment aus II 3. das in b) beschriebene Ereignis ein?

1. Ein Mensch-Ärger-Dich-Nicht-Spiel beginnt. Schreiben Sie das Ereignis "Der Spieler schafft es aus seinem Haus" hin oder berechnen Sie die Mächtigkeit des Ereignisses. (Ein vollständiger Zug kann bis zu $4$ Würfelwürfe umfassen.)
2. Wie groß ist der Ereignisraum, beim Werfen eines Würfels?
3. Russelsche Antinomie: Sei $A$ die Menge aller Mengen, die sich selbst nicht enthalten. Ist $A$ in sich selbst enthalten?

