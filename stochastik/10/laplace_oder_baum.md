---
title: "Laplace oder Baum"
author: [Klasse 10]
date: "08.07.2022"
keywords: [Kombinatorik, Laplace, Baumdiagramm]
lang: de
header-center: "Stochastik" 
geometry:
  - top=3.5cm
  - bottom=3.5cm
  - left=2cm
  - right=2cm
---

## I Starters

1. Berechnen Sie die Wahrscheinlichkeit beim Roulette für das Ereignis...
   \Begin{multicols}{2}
   a) $17$
   b) "impair" (ungerade Zahl)
   f) "Transversale $19$-$21$"
   e) "Les $3$ premiers"

   \End{multicols}

2. Berechnen Sie die Wahrscheinlichkeit, dafür beim Blackjack mit den ersten beiden Karten bereits $21$ auf der Hand zu halten.

## II Hauptgang

1. Eine Münze wird $4$ mal geworfen. Berechnen Sie die Wahrscheinlichkeit dafür, dass...
   a) alle Münzen auf Kopf fallen.
   d) die Münze genau ein mal auf Zahl fällt.
   c) die erste und die letzte Münze auf Kopf fallen.
   a) die letzte Münze Kopf zeigt.

   Zeichnen Sie nun ein Baumdiagramm zum Experiment und betrachten Sie die Teilaufgaben nochmal:

   a) In wieviel Prozent der Fälle, zeigt die erste Münze Kopf? Zu welchem Teil dieser Fälle, fällt anschließend die zweite Münze auf Kopf? (Anders gefragt: zu wieviel Prozent von dieser Prozentzahl zeigt die zweite Münze auch Kopf?) Wie kommen Sie von den Einzelwahrscheinlichkeiten der einzelnen Stufen auf die in a) gesuchte Wahrscheinlichkeit?
   b) Berechnen Sie die in b) gesuchte Wahrscheinlichkeit aus den Wahrscheinlichkeiten der Ergebnisse der einzelnen Stufen. (Können Sie das gerade für a) beschriebene Verfahren verallgemeinern?)
   c) Können Sie die Wahrscheinlichkeit dafür, dass die erste und die letzte Münze auf Kopf fallen analog anhand des Baumdiagramms berechnen?
   d) Berechnen Sie die Wahrscheinlichkeit, dass die letzte Münze Kopf zeigt anhand des Baumdiagramms.

2. Es werde zuerst ein Reisnagel geworfen. Dieser lande zu $40$% auf der Spitze und zu $60$% auf dem Kopf. Landet der Reisnagel auf der Spitze, so werde anschließend ein Tetraeder, sonst ein Würfel geworfen.
   a) Wenn der Reisnagel zu $40$% Prozent auf der Spitze landet. Zu welchem Teil dieser $40$% fällt der Tetraeder anschließend auf die $1$?
   b) Was ist die Wahrscheinlichkeit dafür, dass der Reisnagel auf den Kopf und anschließend der Würfel auf $1$ oder $6$ fällt?
   c) Berechen Sie die Wahscheinlichkeit, dafür dass eine Zahl, die größer als $3$ ist, gewürfelt oder getraedert wird.

2. Es wird mit unterscheidbaren Würfeln gewürfelt. Vielleicht hilft ein Baumdiagramm beim Berechnen der Wahrscheinlichkeit...
   a) für einen Dreierpasch beim Würfeln mit $3$ Würfeln.
   c) für einen Zweierpasch beim Werfen von $3$ Würfeln.
   d) für eine (kleine oder große) Straße. (Es werden $5$ Würfel geworfen. Vier aufeinanderfolgende Zahlen sind eine kleine Straße.)
   e) dafür beim Mensch-Ärger-Dich-Nicht nicht aus dem Häuschen zu kommen.

3. In einer Sockenschublade befinden sich einzeln $5$ verschiedene Paar Socken. Berechnen Sie die Wahrscheinlichkeit
   a) bei $2$ entnommennen Socken schon ein Paar zusammen zu haben.
   b) bei $5$ entnommennen Socken noch kein Paar zusammen zu haben.

4. Anna, Bertram, Christof, Dagmar und Eduard setzen sich zufällig in einen Kreis. Wie groß ist die Wahrscheinlichkeit, dass Anna neben Bertram sitzt?


