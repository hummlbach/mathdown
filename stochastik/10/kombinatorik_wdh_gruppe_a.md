---
title: "Gruppenarbeit zur Kombinatorik"
author: "Gruppe A: Variation mit Wiederholung"
date: 07.07.2022
keywords: [Variation]
lang: de
header-center: "Stochastik" 
---

1. Bringen Sie in Erfahrung was Variation mit Wiederholung bedeutet, so dass Sie es im Vortrag definieren können.

2. Geben Sie die Formel zur Berechnung der Variationen $k$-ter Klasse aus $n$ Dingen mit Wiederholung an und erläutern Sie sie zum Beispiel an einem Urnenexperiment in dem mehrmals gezogen wird.

3. Berechnen Sie die Wahrscheinlichkeit dafür einen Kniffel zu würfeln (mit einem Wurf). (D.h. es werden $5$ Würfel geworfen und man möchte, dass alle Würfel die gleiche Zahl zeigen.)

4. Berechnen Sie die Wahrscheinlichkeit dafür beim ersten Zug im Mensch-Ärger-Dich-Nicht nicht aus dem Häuschen zu kommen. (Sie dürfen dreimal Würfeln und brauchen eine $6$.)

5. Rechnen Sie Beispiel 3 oder 4 vor und überlegen Sie sich ein weiteres, das Sie ihre Mitschüler rechnen lassen.
