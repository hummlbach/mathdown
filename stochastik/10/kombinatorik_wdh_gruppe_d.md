---
title: "Interessante Probleme"
author: "Klasse 10"
date: 14.07.2022
keywords: [Variation]
lang: de
header-center: "Stochastik" 
---

1. Wir betrachten eine Klasse mit $36$ Schülern. Berechnen Sie die Wahrscheinlichkeit, dass alle $36$ Schüler an verschiedenen Tagen Geburtstag haben. (Wir gehen davon aus, dass die Geburten übers Jahr gleichverteilt sind.)

2. Nach dem zweiten Wurf beim Kniffel, liegen die Würfel wie folgt: \newline
   \Begin{center}
   ![](dice-6-6-4-3-2.png){height=50px}
   \End{center}

   a) Berechnen Sie die Wahrscheinlichkeit dafür eine Straße zu Würfeln, indem man die Würfel, die eine $6$ zeigen noch einmal wirft.
   b) Berechnen Sie die Wahrscheinlichkeit dafür nach dem Wurf der $3$ Würfel, die keine $6$ zeigen, mindestens $3$ Sechser zu haben (also wenigstens noch eine weitere $6$ zu erhalten).

3. Berechnen Sie die Wahrscheinlichkeit beim Lotto ($6$ aus $49$) genau $3$ Richtige zu bekommen.

4. Buchstabenhäufigkeit
   a) Geben Sie an, welche Buchstaben jeweils wie oft in diesem Satz vorkommen. Und bestimmen Sie dann die relative Häufigkeit von Vokalen und Konsonanten:

      +---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      | Buchstaben    |A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|R|S|T|U|V|W|Z|
      +---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      | Anzahl        | | | | | | | | | | | | | | | | | | | | | | |
      +---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
      | $h_{60}$ in % | | | | | | | | | | | | | | | | | | | | | | |
      +---------------+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    b) Entschlüsseln Sie den folgenden Text:

       Qvrfre Grkg jne EBG-Qervmrua-irefpuyhrffryg. Qnf urvffg zna puvssevreg ormvruhatfjrvfr qrpuvssevreg Anpuevpugra, vaqrz zna vz Nycunorg qervmrua Ohpufgnora jrvgremnruyg, nyfb fbmhfntra qnf Nycunorg hz qervmrua Ohpufgnora qerug (Ebgngvba). Qvrfr Irefpuyhrffryhat vfg rva Fcrmvnysnyy qre Zrgubqr, qvr Pnrfne ibe zrue nyf mjrvgnhfraq Wnuera orahgmgr haq jveq qnure nhpu rvar Pnrfne-Irefpuyhrffryhatra tranaag. Fvr vfg bssrafvpugyvpu avpug orfbaqref fvpure... ;-)

\pagebreak


### Tipps zu...

3. - Wieviele Möglichkeiten gibt es insgesamt $6$ (unterscheidbare) Kugeln aus $49$ auszuwählen?
   - Genau $3$ richtige heißt $3$ aus den $6$ die gezogen wurden. Für jede dieser Möglichkeiten müssen noch $3$ aus den $46$, die nicht gezogen wurden ausgewählt

4. Häufigkeitsanalyse/relative Häufigkeiten

