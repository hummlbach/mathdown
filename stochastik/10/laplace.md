---
title: "Laplace-Experimente"
author: [Klasse 10]
date: 06.07.2022
keywords: [Laplace, Wahrscheinlichkeit]
lang: de
header-center: "Stochastik" 
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=2.5cm
  - right=2.5cm
---

## I Starters

1. In einer Urne befinden sich $5$ Kugeln - je eine der Farben Weiß, Schwarz, Rot, Gelb und Blau.
   a) Geben Sie die Wahrscheinlichkeit an die blaue Kugel zu ziehen.
   b) Berechnen Sie die Wahrscheinlichkeit weder die schwarze noch die weiße Kugel zu ziehen.

2. In einer Urne befinden sich $15$ Kugeln: $5$ weiße und $10$ schwarze. Geben Sie die Wahrscheinlichkeit an, dass eine schwarze gezogen wird.

3. In einer Urne befinden sich $4$ gelbe, $5$ blaue und $6$ rote Kugeln.
   a) Geben Sie die Wahrscheinlichkeit an eine rote Kugel zu ziehen.
   b) Mit welcher Wahrscheinlichkeit wird keine blaue Kugel gezogen?
   c) Wieviele rote Kugeln müssen zusätzlich in die Urne gelegt werden, damit die Wahrscheinlichkeit eine rote Kugel zu ziehen mindestens $66$% beträgt?

## II Hauptgang

1. Geben Sie die Wahrscheinlichkeit dafür an, dass ein (fairer) Würfel...
 
   a) auf die $6$ fällt.
   b) nicht auf die $6$ fällt.
   c) auf $1$ oder $2$ fällt.
   d) auf eine ungerade Zahl fällt.

2. Zwei unterscheidbare Würfel werden geworfen. Ermitteln Sie die Wahrscheinlichkeit dafür Augensumme...

    \Begin{multicols}{4}
    a) $12$
    b) $4$
    c) $7$ 
    d) $1$ 

    \End{multicols}

    zu würfeln? (Wie groß ist der Ereignisraum $\Omega$? Wie groß die Ereignisse?)

3. Die $12$ durchnummerierten, gleichgroßen Felder eines (fairen) Glücksrads seien wie folgt eingefärbt:

    - Die Felder mit Zahlen der $3$er-Reihe seien rot
    - Die Felder mit Zahlen aus $5$er- oder $7$er-Reihe seien blau
    - Die restlichen Felder seien gelb

    Berechnen Sie die Wahrscheinlichkeiten dafür, dass das Glücksrad auf einem der folgenden Felder stehen bleibt:

    \Begin{multicols}{2}
    a) Rot
    b) Gelb
    c) Blaues oder rotes Feld
    d) Rot oder eine gerade Zahl

    \End{multicols}

## III Dessert

Zwei nicht unterscheidbare Würfel werden geworfen.

a) Geben Sie die Wahrscheinlichkeit an einen $1$er-Pasch zu würfeln.
b) Wie hoch die die Wahrscheinlichkeit eine $1$ und eine $2$ zu würfeln?
c) Handelt es sich um ein Laplace-Experiment?


