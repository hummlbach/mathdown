---
title: "Lösungen zur Prozentrechnung"
author: [Klasse 10]
date: 30.06.2022
keywords: [Pythagoras, Dreiecke, Winkelsumme, Kongruenz]
lang: de
header-center: "Stochastik" 
---

## Lösungen

2. Diagramme

   a) $34,31$% der (beliebtesten) Haustiere sind Katzen, $22,59$% sind Hunde.
   b) $61,65$ ihrer $75$ Euro kommen tatsächlich im Projekt an.
   c) $22,86$% der europäischen Emissionen entfallen auf Deutschland.
   d) $52,87$%
   e) Heute sind es $744,08$ Millionen Europäer, $2100$ werden es voraussichtlich $735,9$ Millionen sein.

3. Die erste Erhöhung betrux $9,09$%, die zweite $8,34$%. Beide Erhöhungen zusammen betrugen $16,67$%


4. a) $45,15$%
   b) $14,78$%

5. a) Vor der zweiten Preissenkung lag das Snowboard bei $448,34$ Euro. Der Originalpreis war also $498,15$ Euro.
   b) $49,81$ und $179,34$ Euro
   c) Der gesamte Nachlass in Prozent beträgt $44,19$.


