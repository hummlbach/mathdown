---
title: "Gruppenarbeit zur Kombinatorik"
author: "Gruppe C: Kombination (ohne Wiederholung)"
date: 07.07.2022
keywords: [Kombination (ohne Wiederholung)]
lang: de
header-center: "Stochastik" 
---


1. Bringen Sie in Erfahrung was Kombination (ohne Wiederholung) bedeutet, so dass Sie es im Vortrag definieren können.

2. Geben Sie die Formel zur Berechnung der Kombinationen $k$-ter Klasse aus $n$ Dingen (ohne Wiederholung) an und erläutern Sie sie zum Beispiel an einem Urnenexperiment (in dem mehrere Kugeln gleichzeitig gezogen werden).

3. Eine (faire) Münze wird $10$ mal geworfen. Berechnen Sie die Wahrscheinlichkeit dafür, dass die Münze genau $3$ mal Kopf zeigt.

4. Rechnen Sie 3. vor und überlegen Sie sich ein weiteres Beispiel, das Sie ihre Mitschüler rechnen lassen.

\pagebreak

### Tipps zu...

3. Wieviele Möglichkeiten gibt es von den $10$ Münzwürfen $3$ auszuwählen (bei denen die Münze Kopf zeigt)?
