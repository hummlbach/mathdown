---
title: "Axiome ebener euklidischer Geometrie"
author: [Johannes Renkl]
date: 15.12.2020
keywords: [Euklidisch, Geometrie, Ebene, Axiome]
lang: de
---

# 0 Relationen

Relationen, beschreiben sozusagen die Beziehungen von Elementen einer Menge untereinander. Beispiele für Relationen könnten sein "befreundet sein", "lieben" oder "mögen". Wählen wir als Beispiel letzteres. Hat man nun zwei Menschen $m_1$ und $m_2$ aus einer Gruppe $M$, dann hieße $m_1 \sim m_2$, dass der Mensch $m_1$ den Menschen $m_2$ mag. Eine Relation ist gegeben durch eine Teilmenge des kartesischen Produktes: $N\subset M\times M$. ($M\times M$ ist die Menger alle möglichen Paare von Elementen aus $M$. Ist z.B. $M={1,2,3}$ dann ist $M\times M = {(1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3)}$). Man schreibt $m_1\sim m_2$ wenn $(m_1,m_2)\in N$ für $m_1,m_2\in M$.

**Definition** (Eigenschaften von Relationen): \newline
Eine Relation $\sim$ auf $M$ heißt

- **reflexiv**, wenn $m\sim m$ für alle $m\in M$
- **symmetrisch**, wenn aus $m_1 \sim m_2$ schon $m_2\sim m_1$ für alle $m_1, m_2 \in M$ folgt
- **transitiv**, wenn aus $m_1 \sim m_2$ und $m_2 \sim m_3$ folgt, dass $m_1 \sim m_3$ für alle $m_1, m_2, m_3 \in M$
- **antisymmetrisch**, wenn $m_1 = m_2$ aus $m_1 \sim m_2$ und $m_2\sim m_1$ folgt für alle $m_1, m_2 \in M$
- **total**, wenn $m_1 \sim m_2$ oder $m_2\sim m_1$ für alle $m_1, m_2 \in M$

Man überlege sich, welche Eigenschaften, die oben genannten Relationen, aus dem Leben haben.


**Definition** (Äquivalenzrelation): \newline
Eine Relation $\sim$ auf $M$ heißt **Äquivalenzrelation**, wenn sie reflexiv, symmetrisch und transitiv ist.

Äquivalenzrelationen beschreiben soetwas wie Ähnlichkeit und unterteilen eine Menge in Gruppen, deren Elemente aufgrund irgendeines Aspektes zusammengehören.


**Definition** (Totale Ordnung): \newline
Eine $\text{Relation } < \text{ auf} M$ nennt man **totale Ordnung**, wenn sie reflexiv, antisymmetrisch, transitiv und total ist.


# 1 Hilberts Axiome in der Ebene

Gegeben seien eine Menge von Punkten $\mathcal{P}$, eine Menge von Geraden $\mathcal{G}$, eine Relation $I$ ($\subset \mathcal{P}\times\mathcal{G}$), die angibt welche Punkte auf welchen Geraden liegen (Inzidenz), zu jeder Geraden $g$ eine $\text{totale Ordnung } < \text{ auf}$ den Punkten der Gerade ($\{P \in \mathcal{P} \, | \, P I g \}$), sowie eine Äquivalenzrelation $\cong$, die besagt welche Strecken und welche Winkel gleich lang bzw. groß sind (Kongruenz - eigentlich handelt es sich um zwei Relationen, es wird aber für beide das selbe Symbol verwendet), sodass die im Folgenden beschriebenen Axiome erfüllt sind.

## 1.1 Inzidenz

Der Begriff der **Inzidenz** fasst die Beziehungen zwischen Geraden und Punkten, die üblicherweise durch die Sprechweisen "$g$ geht durch $P$", "$g$ verbindet $P$ und $Q$", "$P$ liegt auf $g$", "$P$ ist ein Punkt von $g$", "auf $g$ gibt es den Punkt $P$" usw. ausgedrückt werden, zusammen. Die Inzidenzrelation $I$ muss folgenden Axiomen genügen:

- **I.1** Zwei verschiedene Punkte liegen stets auf genau einer Gerade. (Für $P, Q \in \mathcal{P}$ existiert genau ein $g\in\mathcal{G}$ mit $P I g$ und $Q I g$.)
- **I.2** Auf jeder Gerade liegen mindestens zwei Punkte. (Für alle $g \in \mathcal{G}$ existieren $P\neq Q \in \mathcal{P}$ mit $P I g$ und $Q I g$.)

## 1.2 Anordnung

**Definition** (Strecke): \newline
Seien zwei Punkte $P$ und $Q$ gegeben. Die Menge aller Punkte zwischen $P$ und $Q$ heißt **Strecke** zwischen $P$ und $Q$ und wird mit $PQ$ bzw., $QP$ bezeichnet. (Seien $A, C \in \mathcal{P}$ mit $AIg$ und $CIg$ für $g\in\mathcal{G}$, dann setzen wir $AC:= { B I g \, | \, A< B< C}$.)

Die so definierten Strecken müssen das **Axiom von Pasch** erfüllen:

- **II.4** Es seien $A, B, C$ drei nicht in gerader Linie gelegene Punkte und $g$ eine Gerade, die keinen dieser Punkte trifft. Wenn $g$ durch die Strecke $AB$ geht, so geht sie auch durch $BC$ oder durch $AC$. (Existiert ein Punkt $X\in AB$ mit $XIg$, so gibt es auch einen Punkt $Y\in BC\cup AC$ mit $YIg$.)

Das Axiom von Pasch besagt im Wesentlichen, dass jede Gerade die Ebene in zwei Gebiete teilt.

## 1.3 Kongruenz

- **III.1** Zu jedem Punkt $P$ auf jeder Gerade $g$ gibt es einen Punkt $Q$ auf $g$, sodass $PQ$ kongruent zu einer vorgegebenen Strecke ist.
- **III.4** Zu jeder Gerade $g$ gibt es eine weitere Gerade $h$ die $g$ in einem vorgegebene Winkel schneidet.
- **III.6** Sind je zwei Seiten und die dazwischenliegenden Winkel zweier Dreiecke kongruent, so sind die Dreiecke kongruent.

## 1.4 Parallelen

Das **Euklidische Axiom** birgt die "Seele des euklidischen":

- **IV** Sei $g$ eine Gerade und $P$ ein Punkt außerhalb von $g$, dann gibt es höchstens eine Gerade die durch $P$ geht und $g$ nicht schneidet. (Für $g \in \mathcal{G}$ und $P\in\mathcal{P}$ mit $\neg P I g$ existiert höchstens ein $h\in\mathcal{G}$ mit $P I h$ und $\neg Q I h$ für alle $Q I g$.)

## 1.5 Stetigkeit

Zuletzt soll noch das **Archimedische Axiom** sowie das **Axiom der linearen Vollständigkeit** gelten:

- **V.1** Jede Strecke kann durch Verfielfachung länger als jede andere Strecke gemacht werden.
- **V.2** Jede Gerade ist vollständig.

Letzteres besagt, dass Geraden so kontinuierlich sind, wie es unserer Anschauung nach in der natürlichen Welt zu sein scheint.

# 2 Analytische Geometrie in der Ebene

Die analytische Geometrie in der Ebene erfüllt Hilberts Axiome:

















