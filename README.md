# mathdown

Lets write math using markdown, dot, gnuplot and a bit of python.

A lot of the figures were drawn/constructed using geogebra: https://www.geogebra.org/u/hummlbach

# credits

Some of the pictures were taken (unmodified) from wikipedia and credits go to:

| User         | For          |
|--------------|--------------|
| Martin Thoma | https://commons.wikimedia.org/wiki/File:Perceptron-xor-task.svg |
| Psychonaut   | https://de.wikipedia.org/wiki/Manhattan-Metrik#/media/Datei:Manhattan_distance.svg |
| Olaf Tausch  | https://de.wikipedia.org/wiki/Edfu#/media/Datei:Edfu_Tempel_01.jpg |
| Jleedev      | https://commons.wikimedia.org/wiki/File:Trilateration.png |
| Ad Meskens   | https://commons.wikimedia.org/wiki/Category:Hieroglyphs_of_Egypt:_Numerals#/media/File:Edfu_Egyptian_numerals.JPG |
| Rolfcosar    | https://commons.wikimedia.org/wiki/File:Pyramiden_von_Gizeh.jpg |
| Carroy       | https://de.wikipedia.org/wiki/Datei:Druselturm_kassel_top.jpg |
| Honina       | https://commons.wikimedia.org/wiki/File:Ebene_polarkoordinaten.svg |
| NASA         | https://commons.wikimedia.org/wiki/File:Apollo_11_Launch_-_GPN-2000-000630.jpg |

(Follow the links to see the licenses under which the pictures were published.)

Some of the pictures were "stolen" somewhere else and credits go to:

| Source       | For          |
|--------------|--------------|
| Industrieverband für Heimtierbedarf e.V. | https://www.deinhaustier.de/wp-content/uploads/2013/04/beliebtestehaustiere.jpg |
| WikiReal     | https://wikireal.info/wiki/Datei:Schiefbahnhof,_21_gute_Gr%C3%BCnde_f%C3%BCr_S21,_S._21.png |
| W. Hutter    | Various drawings |
| D. Laiblin   | Various drawings |


