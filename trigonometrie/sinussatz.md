---
title: "Übungsblatt 6"
author: [Klasse 10]
date: 23.03.2022
keywords: [Sinus, Winkelsumme, Sinussatz]
lang: de
header-center: "Trigonometrie" 
---

# I Starters
\Begin{multicols}{2}
Berechnen Sie alle fehlenden Größen.

a) Gegeben: $a=87$m, $b=54$m und $\alpha=63^\circ$
b) Gegeben: $c=832,4$m, $\beta=55,6^\circ$ und $\alpha=45,8^\circ$

   ![](sinsatz_warmup.png){}

\End{multicols}


# II Hauptgang
\Begin{multicols}{2}
1. Ein Architekturstudent steigt auf den schiefen Turm von Pisa (Punkt $B$ im Dreieck unten). Ein zweiter Student befindet sich unten auf dem Vorplatz (Punkt $A$) in einer Entfernung von $b=29,57$m vom Fußpunkt $C$. Durch gegenseitiges Anpeilen messen sie die beiden Winkel $\alpha=29,5^\circ$ und $\beta=65^\circ$. Berechnen Sie wie hoch der Turm war, als er noch senkrecht stand.

   ![](turm_pisa_schematisch.png){}

   ![](turm_pisa.png){height=400px}

\End{multicols}

\pagebreak

\Begin{multicols}{2}
2. Zwei Leute beobachten jeweils von ihrem Wohnort aus einen Heißluftballon und messen, die in der Abbildung gezeigten Winkel. Wie hoch befindet sich der Ballon über dem Erdboden?
   ![](ballonhoehe.png){}


3. Berechnen Sie, wie weit die Insel von der Gastätte am Ufer entfernt ist.

   ![](insel_entfernung.png){}

\End{multicols}

4. Gegeben seien folgende Skizzen:

   ![](regatta.png){}
   \Begin{multicols}{3}

   Wie weit ist es, von $B$ nach $P$, wenn $s=243,5$m, $\alpha=51,216^\circ$ und $\beta=28,783^\circ$ ist?
   \newline
   Berechnen Sie die Länge des Regattakurses.
   $$\text{ }$$
   Wie hoch ist der Berg, wenn $s=494,4$m, $\alpha=57,3^\circ$, $\beta=81,5^\circ$ und $\delta=34,1^\circ$ ist?

   \End{multicols}


# III Dessert

\Begin{multicols}{3}
![](schiff_leuchttuerme.png){}

Von einem Schiff (Position C) werden gleichzeitig ein Leuchtturm A am westlichen Ufer einer Hafeneinfahrt unter S $37^\circ$ W und ein zweiter Leuchtturm B am östlichen Ufer unter S $57^\circ$ O angepeilt. Nach der Seekarte beträgt die Entfernung zwischen den beiden Leuchttürmen $c = 2,47$km. Die Verbindungslinie der beiden Leuchttürme verläuft genau von West nach Ost.  Wie weit ist das Schiff von jedem Leuchtturm entfernt?

\End{multicols}
