---
title: "Übungsblatt 2"
author: [Klasse 10]
date: 09.03.2022
keywords: [Pythagoras, Dreiecke, Winkelsumme, Kongruenz]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

1. Wiederholung des Satzes von Pythagoras.

   \Begin{multicols}{2}
   ![](uebung_pythagoras.png){} 
   a) Berechnen Sie die Länge der Hypothenuse des Dreiecks $\overline{ABC}$.
   b) Berechnen Sie die Länge der Kathete $b$ des Dreiecks $\overline{DEF}$.
   c) Berechnen Sie die Länge der Seite $c$ der Figur rechts.

   **-> mehr davon**

   ![](pythagoras_ums_eck.png){}

   \End{multicols}

2. Ermitteln Sie die Werte der Winkel, die mit Namen (griechische Buchstaben) beschriftet sind. **-> nächstes Blatt**

   \Begin{multicols}{2}
   a) ![](aufwaermuebung_winkelsumme.png){height=100px}
   b) ![](winkeladdition_und_subtraktion.png){}
   c) ![](winkelsumme_und_180_grad.png){}
   d) ![](stufen_und_wechselwinkel.png){}

   \End{multicols}

\pagebreak

# II Hauptgang

**1. Innenwinkel im Dreieck ausmessen**

**2. Gleichheit von Stufenwinkel und Wechselwinkel erkennen**

**3. Innenwinkelsumme im Vieleck ausmessen**

Zeichnen Sie Dreiecke gemäß den folgenden Angaben. Jedes Dreieck soll dabei **mit kräftigen Linien gezeichnet eine Seite eines einzelnen, losen Din A4 Blattes** einnehmen. **-> nächstes Blatt**
 
a) Die Seiten des Dreiecks haben die Längen $8$ cm, $15$ cm und $17$ cm.
b) Die Seiten des Dreiecks haben die Längen $17$ cm, $25$ cm und $10$ cm.
c) Ein Winkel des Dreiecks messe $80^\circ$ und die beiden an den Winkel anliegenden Seiten seien $15$ und $20$ cm lang.
d) Eine der Seiten sei $15$ cm lang, die beiden (Innen-)Winkel an dieser Seite seien $40^\circ$ groß.
e) Das Dreieck habe einen $45^\circ\text{-Winkel}$ und die dem Winkel gegenüberliegende Seite sei $15$ cm lang. Eine weitere Seite sei $20$ cm lang.
f) Das Dreieck habe einen $45^\circ\text{-Winkel}$. Die dem Winkel gegenüberliegende Seite sei nun $20$ cm und eine weitere Seite $15$ cm lang.

# III Dessert

\Begin{multicols}{2}

1. Berechnen Sie das Maß des Winkels $\xi$.

   ![](winkelraetsel.png){width=200px}

2. Berechnen Sie die Seitenlängen $a$ und $b$ der Dreieckspyramide.

   ![](pythagoras_gleichungssystem.png){}

\End{multicols}
