---
title: "Kurztest Trigonometrie"
author: "Name:"
date: 22.03.2022
keywords: [Sinus, Cosinus, Tangens, Winkelfunktionen, Pythagoras]
lang: de
footer-center: "15 Punkte"
header-center: "Klasse 10"
footer-right: "Bearbeitungszeit: 40 Minuten"
geometry:
  - top=2.5cm
  - bottom=3cm
  - left=2.5cm
  - right=2.5cm
---

1. Definition der Winkelfunktionen \hfill __(2P)__

   \Begin{multicols}{2}
   In einem rechtwinkligen Dreieck ist $d$ die Gegenkathete zu $\delta$, $f$ die Ankathete zu $\delta$ und $e$ die Hypothenuse. Ergänzen Sie folgenden Ausdrücke:

   a) $\cos{\varphi}=$
   b) $\tan{\delta}=$
   c) $\frac{f}{}=\sin\text{ }$

   ![](rechtwinkliges_dreieck_anders_bezeichnet.png){}

   \End{multicols}


2. Trockenübung mit Standardbezeichnungen \hfill __(8P)__

   \Begin{multicols}{2}
   Wir betrachten rechtwinklige Dreiecke, die wie üblich bezeichnet seien, das heißt $c$ ist die Hypothenuse, $a$ die Gegenkathete und $b$ die Ankathete zu $\alpha$. In der Tabelle rechts sind je Zeile zwei Größen eines solchen Dreiecks angegeben. Berechnen Sie daraus jeweils die gesuchte Größe.

    Teilaufgabe        Gegeben                          Gesucht
   -------------       ---------                       ----------
   a)                  $c=9$cm; $\alpha=86^\circ$       $a$
   b)                  $a=22$m; $\beta=21^\circ$        $b$
   c)                  $b=36$m; $c=360$m                $\alpha$
   d)                  $a=30$cm; $\alpha=48^\circ$      $c$

   \End{multicols}

3. Schiefbahnhof \hfill __(2P)__
   \Begin{multicols}{2}

   ![](Schiefbahnhof.png)

   Aus den Bauplänen des neuen stuttgarter Tiefbahnhofs ist ersichtlich, dass Gleise und Bahnsteige um $0,85^\circ$ zur Waagerechten geneigt sein werden. Berechnen Sie den Höhenunterschied zwischen den beiden Enden des $400$ Meter langen Bahnsteigs.
   
   \End{multicols}


4. Gorch Fock \hfill __(3P)__

   \Begin{multicols}{2}
   Felix ist Marine-Fan und beobachtet vom Deich aus, wie die Gorch Fock nach ihrer $135$ Millionen Euro teuren Rundumsanierung in ihren Heimathafen Kiel geschleppt wird. Er weiß, dass die Gorch Fock $89,31$ Meter lang ist, und von seinem Beobachtungspunkt beträgt der Winkel zwischen Bug und Heck $60^\circ$. Berechnen Sie die Entfernung $d$ von Felix zur Gorch Fock. (Felix steht im rechten Winkel zur Schiffsmitte und blickt waagerecht.)

   ![](gorchfock.png)

   \End{multicols}

\Begin{center}
![](use_bananas_for_scale.png)

\End{center}
