---
title: "Übungsblatt 4"
author: [Klasse 10]
date: 15.03.2021
keywords: [Dreiecke, Tangens]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

\Begin{multicols}{2}

1. Messen Sie, an $4$ rechtwinkligen Dreiecken, die im Winkel $\alpha$ übereinstimmen aber unterschiedlicher Größe seien, die Länge der Katheten. 
2. Teilen Sie jeweils (für alle $4$ Dreiecke) die Länge der Kathete, die gegenüber des Winkels $\alpha$ liegt, durch die Kathete, die an $\alpha$ anliegt; bilden Sie also den Quotient $\frac{a}{b}$.

![](rechtwinkliges_dreieck.png){}

\End{multicols}

$\alpha$ (in $^\circ$)  $5$   $10$  $15$  $20$ $25$ $30$ $35$  $40$ $45$  $50$  $55$ $60$  $65$ $70$  $75$   $80$ $85$
-------------           ----- ----- ----- ---- ---- ---- ----- ---- ----  ----- ---- ----  ---- ----- ------ ---- -----
$\frac{a}{b}$

# II Hauptgang

1. Gegeben sei ein rechtwinkliges Dreieck. Berechnen Sie die gesuchte Größe anhand der gegebenen.
   a) Wie lang ist $a$, wenn $\alpha=35^\circ$ und $b=5$
   b) Wie lang ist $b$, wenn $\alpha=80^\circ$ und $a=8$
   c) Wie groß ist der Winkel $\alpha$, wenn $a=7$ und $b=7$?
   d) Wie groß ist der Winkel $\beta$, wenn $b=4,25$ und $a=16$?

2. Ein Flugzeug steigt in einem Winkel von $8^\circ$. Berechnen Sie...
   a) wieviel es an Höhe gewinnt, auf einer (waagrecht gemessenen) Strecke von $1000$ Metern?
   b) wieviel Meter über Grund muss das Flugzeug zurücklegen, um $200$ Meter zu steigen?

# III Dessert

\Begin{multicols}{2}

Ein Turm wird aus $e=120$m Entfernung betrachtet. Der Winkel zwischen der Sichtlinie auf die Turmspitze und der Waagerechten beträgt $\alpha=25^\circ$. Die Augenhöhe des Betrachters betrage $1,60$m. Berechnen Sie die Höhe des Turms.

![](nicos_turm.png){}

\End{multicols}
