---
title: "Übungsblatt 7"
author: [Klasse 10]
date: 28.03.2022
keywords: [Cosinussatz, Sinussatz, Allgemeine Dreiecke, Winkelsumme]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

Die Angaben in folgender Tabelle gehören zu Dreiecken, die gemäß der Abbildung unten links bezeichnet sind.

\Begin{multicols}{2}

![](allgemeines_dreieck.png)

 Aufgabe             $a$     $b$         $c$         $\alpha$    $\beta$     $\gamma$
-------------        ----    ------      -----       -----       -----       -----
a)                   $36$cm                          $22^\circ$  $130^\circ$
b)                                       $62$dm      $52^\circ$              $63^\circ$
c)                   $35$km                          $38^\circ$  $70^\circ$
d)                           $114$m      $108$m                  $58^\circ$

\End{multicols}

# II Hauptgang

\Begin{multicols}{2}

Im Dreieck $ABC$ gilt:

- $\overline{AC} = \overline{CE} = 9,2 \text{cm}$
- $\alpha = 64^\circ$
- $\beta = 40^\circ$

Berechnen Sie den Umfang des Dreicks $EBC$.

$$\text{ }$$

![](rap_2015_p1.png)

\End{multicols}

# III Dessert

Zeichnen Sie ein Quadrat und verbinden Sie die Ecken des Quadrats jeweils mit den Mittelpunkten der gegenüberliegenden Seiten des Quadrats. Teile der Verbindungslinien und ihre innersten Schnittpunkte bilden ein Achteck.
Handelt es sich dabei um ein regelmäßiges Achteck? Belegen Sie Ihre Aussage anhand von Rechnungen.

\pagebreak

\Begin{center}
![](kein_reg_achteck.png){}

\End{center}

