---
title: "Beispieltest Trigonometrie"
author: "Name:"
date: 21.03.2021
keywords: [Sinus, Cosinus, Tangens, Winkelfunktionen, Pythagoras]
lang: de
footer-center: "18 Punkte"
header-center: "Klasse 10"
---

\Begin{multicols}{2}
1. In einem rechtwinkligen Dreieck ist $a$ die Gegenkathete zu $\alpha$, $b$ die Ankathete zu $\alpha$ und $c$ die Hypothenuse. Ergänzen Sie folgende Ausdrücke:

   a) $\sin{\alpha}=$
   b) $\tan{\alpha}=$
   c) $c=\frac{b}{\text{  }}$

   ![](rechtwinkliges_dreieck.png)

\End{multicols}

\Begin{multicols}{2}
2. In der Tabelle unten sind je Zeile zwei Größen eines rechtwinkligen Dreiecks (wie jenes, das rechts oben abgebildet ist) gegeben. Berechnen Sie daraus jeweils die gesuchte Größe.

   Teilaufgabe      Gegeben                            Gesucht
   ----------       ---------                       ----------
   a)               $b=23$m; $\alpha=23^\circ$      $a$
   b)               $c=7$km; $\alpha=2^\circ$       $a$
   c)               $a=13$m; $c=370$m               $\beta$
   d)               $a=31$cm; $\alpha=51^\circ$     $c$

   $\text{ }$

3. Ein Haus ist $b=9$m breit und der Winkel des Daches $\alpha$ beträgt $40^\circ$.
   a) Wie lang ist die Dachschräge?
   b) Berechen Sie die Höhe $h$ des Dachs.

   ![](Hausdach.png)

\End{multicols}


\Begin{multicols}{2}

4. Von einem Fenster aus ($h=13$m) erscheint der Fuß eines Turms unter dem Tiefenwinkel $\alpha=27,5^\circ$ und die Spitze unter dem Höhenwinkel $\beta=41,98^\circ$. Berechnen Sie die Höhe des Turms.

   ![](turm_hoehen_und_tiefenwinkel.png)

\End{multicols}

