---
title: "Anwendung der Winkelfunktionen Sinus, Cosinus und Tangens"
author: [Klasse 10]
date: 21.03.2022
keywords: [Sinus, Cosinus, Tangens, Winkelfunktionen]
lang: de
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=3cm
  - right=3cm
---

# Anwendung der Winkelfunktionen Sinus, Cosinus und Tangens

Die Winkelfunktionen Sinus, Cosinus und Tangens ermöglichen es uns fehlende Seiten und Winkel in gegebenen rechtwinkligen Dreiecken zu berechnen.
Zu den vier "Grundaufgaben" der Dreiecksberechnung betrachten wir je ein Beispiel.

## 1. Fall: gegeben sind eine Kathete und die Hypotenuse
Gegeben sind die Seiten $c=8$cm und $a=3$cm.
\newline
Gesucht werden die Winkel $\alpha$ und $\beta$ sowie die Seite $b$.
\Begin{multicols}{2}
![](kathete_3_hypotenuse_8.png)

- Um $\alpha$ zu berechnen, nutzen wir den Sinus
  $\sin\alpha = \frac{a}{c} = \frac{3\text{cm}}{8\text{cm}} = 0,375$
  \newline
  und wenden dann den Arcussinus an, um an den Winkel heranzukommen:
  \newline
  $\alpha = \arcsin 0,375 \approx 22^\circ$

- Über die Winkelsumme
  \newline
  $\alpha + \beta + 90^\circ = 180^\circ$
  \newline
  erhalten wir
  \newline
  $\beta = 180^\circ - 90^\circ - \alpha = 90^\circ - 22^\circ = 68^\circ$
  \newline
  (Wir hätten auch Cosinus und Arcuscosinus verwenden können.)

- Die noch fehlende Seite $b$ kann nun anhand des Satzes von Pythagoras oder mittels Tangens/Sinus/Cosinus. Wir nutzen Letzteres:
  \newline
  $b = \sin{\beta} \cdot c = \sin{68^\circ} \cdot 8 \text{cm} \approx 7,42 \text{cm}$

\End{multicols}

## 2. Fall: gegeben sind die zwei Katheten
Die zwei Katheten sind $a=8$cm und $b=3$cm.
\newline
Gesucht sind die Winkel $\alpha$ und $\beta$ sowie die Hypotenuse $c$.
\Begin{multicols}{2}
![](kathete_3_und_8.png){height=250px}

- Wir nutzen den Tangens, um $\alpha$ ins Spiel zu bringen:
  $\tan\alpha = \frac{a}{b} = \frac{8\text{cm}}{3\text{cm}}$.
  \newline
  Anwendung des Arcustangens auf beiden Seiten:
  $\alpha = \arctan{\frac{8}{3}} \approx 69,45^\circ$
  \newline
  (Wir hätten auch erst $c$ mit dem Satz von Pythagoras berechnen und dann Sinus oder Cosinus verwenden können um $\alpha$ zu berechnen.

- $\beta = 90^\circ - \alpha = 90^\circ - 69,45^\circ = 20,55^\circ$

- $c = \sqrt{a^2+b^2} = \sqrt{8^2+3^2} = \sqrt{64+9} = \sqrt{73} \approx 8,54 \text{cm}$

\End{multicols}

## 3. Fall: gegeben sind ein Winkel und die Hypotenuse 

Gegeben sind $\alpha=53^\circ$ sowie $c=10cm$.
\newline
Gesucht sind nun der Winkel $\beta$ und die Katheten $a$ und $b$.

\Begin{multicols}{2}

![](winkel_53_hypotenuse_10.png)

- Wir setzen die gegebenen Werte in $\sin\alpha = \frac{a}{c}$ ein:
  \newline
  $\sin 53^\circ = \frac{a}{10\text{cm}}$
  und stellen nach $a$ um: 
  $a = 10 \text{cm} \cdot \sin 53^\circ \approx 7,99 \text{cm}$

- $b = \sqrt{c^2 - a^2} \approx \sqrt{10^2 - 7,99^2} \approx 6,01 \text{cm}$
  \newline
  Oder alternativ zum Beispiel über den Cosinus:
  \newline
  $b = \cos\alpha\cdot c = \cos 53^\circ \cdot 10 \text{cm} \approx 6,02 \text{cm}$

- $\beta = 90^\circ - \alpha = 90^\circ - 53^\circ = 37^\circ$

\End{multicols}

## 4. Fall: gegeben ist ein Winkel und eine Kathete

Gegeben ist nun zum Beispiel der Winkel $\alpha=52^\circ$ sowie die Ankathete $b=6,5$cm.
\newline
Gesucht sind $\beta$ sowie $a$ und $c$.

\Begin{multicols}{2}

![](winkel_52_kathete_6_5.png)

- $\beta = 90^\circ - \alpha = 90^\circ - 52^\circ = 38^\circ$

- Wir stellen $\tan\alpha = \frac{a}{b}$ nach $a$ um und setzen ein:
  \newline
  $a = \tan\alpha \cdot b = \tan 52^\circ \cdot 6,5 \text{cm} \approx 8,32 \text{cm}$

- Wir stellen $\cos\alpha = \frac{b}{c}$ nach $c$ um und setzen ein:
  \newline
  $c = \frac{b}{\cos{\alpha}} = \frac{6,5\text{cm}}{\cos 52^\circ} \approx 10,56 \text{cm}$

\End{multicols}

