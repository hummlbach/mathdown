---
title: "Lösungen zu Blatt 9"
author: [Klasse 10]
date: 02.05.2022
keywords: [Cosinussatz, Sinussatz, Allgemeine Dreiecke, Winkelsumme, Sinus, Cosinus, Tangens]
lang: de
header-center: "Trigonometrie" 
---

# Hinweise in Prosa

## 2014 Aufgabe P1

- Aus $\overline{AE}$ und $\varepsilon$, kann mit Kosinus $\overline{DE}$ berechnet werden.
- Sodann kann aus $\overline{CD}$ und $\overline{DE}$ die Länge der Seite $CE$ (z.B. Pythagoras) sowie mit dem Arcustangens der Winkel $\varepsilon_2$ berechnet werden.
- Mit den Winkeln $\varepsilon_1$ und $\varepsilon_2$ ist auch $\varepsilon_3$ bekannt (Halbkreis hat $180^\circ$). Aus $\varepsilon_3$ und $\overline{CE}$ können schließlich, mit Kosinus und Sinus, die Längen von $EB$ und $BC$ berechnet werden.

## 2014 Aufgabe P2

- Aus $\alpha$ und $\overline{AB}$ kann mit dem Kosinus $\overline{AC}$ berechnet werden.
- Anhand von $\overline{AM} = \frac{\overline{AC}}{2}$ ($M$ ist der Mittelpunkt der Seite $AC$) und $\alpha$ kann mit dem Kosinus die Länge der Strecke $AM$ und damit $\overline{BD}$ ausgerechnet werden.
- Mit $\alpha$ ist durch die Winkelsumme auch $\beta$ bekannt und aus dem Tangens von $\beta$ und $\overline{BD}$ ergibt sich schließlich $\overline{DE}$.

# Formeln zu den Hinweisen

## 2014 Aufgabe P1

- $\cos\varepsilon_1 = \frac{\overline{AE}}{\overline{DE}}$
- $\overline{CE}^2 = \overline{DE}^2+\overline{CD}^2$
- $\tan\varepsilon_2 = \frac{\overline{CD}}{\overline{DE}}$
- $\varepsilon_1 + \varepsilon_2 + \varepsilon_3 = 180^\circ$
- $\sin\varepsilon_3 = \frac{\overline{BC}}{\overline{CE}}$
- $\cos\varepsilon_3 = \frac{\overline{EB}}{\overline{CE}}$

## 2014 Aufgabe P2

- $\cos\alpha = \frac{\overline{AC}}{\overline{AB}}$
- $\cos\alpha = \frac{\overline{AD}}{\overline{AM}}$ 
- $\overline{AD} + \overline{BD} = \overline{AB}$
- $\alpha + \beta + 90^\circ = 180^\circ$
- $\tan\beta = \frac{\overline{DE}}{\overline{BD}}$

\pagebreak

# Passend umgestellte Formeln zu den Hinweisen

## 2014 Aufgabe P1

- $\overline{DE} = \frac{\overline{AE}}{\cos\varepsilon_1}$
- $\overline{CE} = \sqrt{\overline{DE}^2+\overline{CD}^2}$
- $\varepsilon_2 = \arctan{\frac{\overline{CD}}{\overline{DE}}}$
- $\varepsilon_3 = 180 - \varepsilon_2 - \varepsilon_1$
- $\overline{BC} = \sin\varepsilon_3\cdot\overline{CE}$
- $\overline{EB} = \cos\varepsilon_3\cdot\overline{CE}$
- $U_{EBC} = \overline{BC} + \overline{EB} + \overline{CE}$

## 2014 Aufgabe P2

- $\overline{AC} = \cos\alpha\cdot\overline{AB}$
- $\overline{AD} = \cos\alpha\cdot\overline{AM}$ 
- $\overline{BD} = \overline{AB} - \overline{AD}$
- $\beta = 180^\circ - \alpha - 90^\circ$
- $\overline{DE} = \tan\beta \cdot \overline{BD}$


# Ergebnisse und Zwischenergebnisse

\Begin{multicols}{2}

## 2014 Aufgabe P1

- $\overline{DE}=5,52 \text{ cm}$
- $\overline{CE}=8,01 \text{ cm}$
- $\overline{AE}=1,73 \text{ cm}$
- $\varepsilon_2 = 46,6^\circ$
- $\varepsilon_3 = 79,0^\circ$
- $\overline{BE}=1,53 \text{ cm}$
- $\overline{BC}=7,68 \text{ cm}$
- $U_{BCE} = 17,4 \text{ cm}$

## 2014 Aufgabe P2

- $\overline{AC}=5,00 \text{ cm}$
- $\beta = 53,8^\circ$
- $\overline{AM}=2,50 \text{ cm}$
- $\overline{AD}=2,02 \text{ cm}$
- $\overline{BD}=4,18 \text{ cm}$
- $\overline{DE}=5,71 \text{ cm}$

\End{multicols}
