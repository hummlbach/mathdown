---
title: "Übungsblatt 3"
author: [Klasse 10]
date: 14.03.2022
keywords: [Pythagoras, Dreiecke, Winkelsumme, Kongruenz]
lang: de
header-center: "Trigonometrie" 
---

# I Winkel

1. Bei einer Landvermessung wurden folgende Polygone vermessen. Ermitteln Sie die fehlenden Winkelgrößen?

   ![](Feldmesspolygone.png){}
3. Berechnen Sie die Größe eines Innenwinkels in einem regelmäßigen Fünfeck.
2. Wieviele Ecken hat ein Vieleck mit einer Innenwinkelsumme von $2160^\circ$?
4. In den untenstehenden Figuren schneiden sich mehrere Geraden von denen manche parallel zueinandern sind. In jeder Figur ist eine Winkelweite angegeben. Bestimmen Sie jeweils die Weiten der Winkel $\alpha$, $\beta$ und $\gamma$.

   ![](stufen_wechsel_gegen_winkel.png){}

\pagebreak

# II Konstruktion von Dreiecken

1. Konstruieren Sie Dreiecke gemäß der folgenden Zeichnung mit den Angaben aus der nebenstehenden Tabelle.
   \Begin{multicols}{2}

   ![](allgemeines_dreieck.png){}

   (Alle Längenangaben in Zentimetern.)

   $a$    $b$    $c$        $\alpha$        $\beta$        $\delta$
   ----   ----   ----    -----------     ----------     -----------
   $7$    $10$   $15$    
   $4$    $9$            $105^\circ$                
                 $5$      $23^\circ$     $115^\circ$
          $7$    $6$                                     $50^\circ$
   $8,5$         $12,5$                                  $77^\circ$ 
                 $4$      $33^\circ$                     $90^\circ$

   \End{multicols}

2. Füllen Sie die Tabelle oben, indem Sie die fehlenden Werte an Ihren Konstruktionen messen.

# III Pythagoras

1. Berechnen Sie die fehlenden Seiten eines rechtwinkligen Dreiecks mit folgenden Angaben:

a) $a=6$cm, $b=8$cm
b) $a=3,6$cm, $b=7,7$cm
c) $a=20$cm, $c=29$cm

\Begin{multicols}{2}

2. Berechnen Sie die nicht gegebenen Längen der Seiten des folgenden Dreiecks...

   a) wenn $c=8$ ist.
   b) wenn $a=5$ ist.

   ![](gleichschenklig_rechtwinklig.png){}

\End{multicols}

