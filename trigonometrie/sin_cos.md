---
title: "Übungsblatt 5"
author: [Klasse 10]
date: 17.03.2022
keywords: [Pythagoras, Dreiecke, Winkelsumme, Kongruenz]
lang: de
header-center: "Trigonometrie" 
geometry:
- top=3.25cm
- bottom=3.75cm
- left=2.5cm
- right=2.5cm
---

# I Starters
\Begin{multicols}{2}

            Gegeben                               Gesucht    
--------    ----------  ----                    ---------
Tangens     $b=7$m      $\alpha=35^\circ$       $a$
            $a=11$m     $\alpha=17,2^\circ$     $b$
            $a=12$m     $b=23$m                 $\alpha$
Sinus       $c=13,3$m   $\alpha=48^\circ$       $a$
            $a=55$m     $\alpha=16^\circ$       $c$
            $a=15$m     $c=55$m                 $\alpha$
Cosinus     $c=25$m     $\alpha=77^\circ$       $b$
            $b=112$m    $\alpha=12^\circ$       $c$
            $b=109$m    $c=144$m                $\alpha$
Pythagoras  $a=14$m     $b=16$m                 $c$
            $a=11$m     $c=27$m                 $b$
            $b=17$m     $c=20$m                 $a$

![](rechtwinkliges_dreieck_hochkant.png){}
\End{multicols}

# III Dessert
Aristarchos von Samos erkannte, dass der Halbmond dadurch entsteht, dass das Sonnenlicht genau senkrecht zu unserer Blickrichtung auf den Mond fällt. Das Dreieck Sonne, Mond, Erde weist bei Halbmond also einen rechten Winkel auf.

![](aristarch_ago-sternwarte.png){height=130px} \  ![](aristarch_sarganserland-walensee.png){height=130px}

a) Die Größe des Winkels $\alpha$ zwischen Sonne und Mond hat Aristarch experimentell zu mindestens $87^\circ$ bestimmt. Um wieviel größer ist die Sonne als der Mond demnach mindestens?
b) Der tatsächliche (nicht leicht zu messende) Winkel zwischen Sonne und Mond beträgt sogar $89,85^\circ$. Was bedeutet das für den Größenunterschied von Mond und Sonne?

# II Hauptgang
\Begin{multicols}{2}
1. Ein Sendemast soll mit Seilen von je $l=40$ m Länge gehalten werden. Der Neigungswinkel $\alpha$ der Seile soll $55^\circ$ betragen. In welcher Höhe $h$ müssen die Seile befestigt werden?

   ![](Sendemast.png){height=130px}

2. Eine $5,40$m lange Leiter lehnt an einer Hauswand. Die Leiter bildet mit der Hauswand einen Winkel von $20^\circ$.
   a) Fertigen Sie eine Skizze an!
   a) Wie groß ist der Abstand des unteren Leiterendes von der Wand?
   b) Berechnen Sie, die Höhe der obersten Leitersprosse.
   c) Ermitteln Sie, den Abstand des oberen Leiterendes zum Boden bei einem Winkel von $30^\circ$.

\End{multicols}

\Begin{multicols}{3}
3. Die Burg des Ritter Rost ist umgeben von einem Burggraben, der $5$ Meter breit ist. Bei herabgelassener Zugbrücke sind $10$ Meter der die Brücke haltenden Kette sichtbar. \newline Berechnen Sie den Winkeln, den Zugbrücke und Kette bilden? 

   ![](zugbruecke.png){height=260px}

4. Der Schatten eines $4,50$m hohen Baumes in Stuttgart ist $6$m lang.
a) Wie hoch steht die Sonne, d.h. unter welchem Winkel treffen die Sonnenstrahlen auf den Boden?
b) In welchem Monat wird der Schatten gemessen? (Sonnenhöchststand in Stuttgart: $65^\circ$).

\End{multicols}

5.          a)          b)          c)              d)                      e)                  f)              g)          
-----       --------    ----------  -----------     --------                --------            --------        --------- 
$a$         $5,1$cm     $4,34$cm                    $7,23$cm              
$b$         $7,21$cm                $8,11$m                                 $9,47$km                            $12$m
$c$                     $10,58$cm   $12,51$m                                                    $3,32$cm
$\alpha$                                            $28,5^\circ$                                                $60,25^\circ\qquad\qquad\qquad$
$\beta$                                                                     $17,6^\circ$        $41,3^\circ$

$\quad$Jede Spalte der vorherigen Tabelle enthält zwei Daten eines rechtwinkligen Dreiecks.

$\quad$Berechnen Sie jeweils die fehlenden Werte.

