---
title: "Übungsblatt 1"
author: [Klasse 10]
date: 21.10.2021
keywords: [Wechselwinkel, Stufenwinkel, Winkel, Höhe, Fläche, Dreiecke, Winkelsumme]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

\Begin{multicols}{2}

1. 
   a) Zeichnen Sie in die abgebildeten Dreiecke jeweils die drei Höhen ein. (Zur Erinnerung: in jedem Dreieck gibt es drei Höhen.)
   b) Berechnen Sie die Flächen der Dreiecke.

   ![](hoehen_und_flaechen_von_dreiecken.png){}

\End{multicols}

2. Scheren Sie das...
   a) links unten abgebildeten Rechteck parallel zur Seite $g$, sodass $\alpha$ zu einem $45^\circ$ Winkel wird.
   b) in a) entstandene Parallelogramm parallel zu einer der Diagonalen, sodass ein Rechteck entsteht, das auf der Ecke $B$ steht.
   c) rechts unten abgebildete Rechteck analog zum ersten, wie in a) und b) beschrieben (aber gespiegelt), sodass das Rechteck am Ende auf der Ecke $F$ steht.

![](rechtecke_zum_scheren.png){}

# II Hauptgang

1. Zeichnen Sie mit dem Geodreieck folgende Winkel

   \Begin{multicols}{4}

   a) $37^\circ$
   b) $60^\circ$
   c) einen rechten
   d) $125^\circ$

   \End{multicols}

2. Zeichnen Sie (möglichst groß auf ein einzelnes Din A4 Blatt) zwei parallele Geraden und eine dritte Gerade, die die ersten beiden Geraden schneidet.

3. Zeichnen Sie ein Dreieck (sauber mit einem Lineal), messen Sie die (Innen-)Winkel mit dem Geodreieck und addieren Sie die Gradzahlen zusammen.

# III Dessert

Welches Dreieck hat eine Winkelsumme von $270^\circ$?

\pagebreak

# IV Tipps fürs Dessert

He Sie da! Knobeln Sie erstmal ein bisschen bevor Sie die Tipps lesen!

1. Das Dreieck darf nicht eben sein.
2. .leguK renie fua ekceierD eiS nethcarteB

