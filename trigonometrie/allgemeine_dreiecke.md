---
title: "Übungsblatt 8"
author: [Klasse 10]
date: 04.04.2022
keywords: [Cosinussatz, Sinussatz, Allgemeine Dreiecke, Winkelsumme]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

Die Angaben in folgender Tabelle gehören zu Dreiecken, die gemäß der Abbildung unten links bezeichnet sind.

\Begin{multicols}{2}

![](allgemeines_dreieck.png)

Fall     $a$      $b$         $c$         $\alpha$    $\beta$     $\gamma$
-----    ----     ------      -----       -----       -----       -----
SSS      $4$      $7$         $9$        
SWS      $9$cm                $10$cm                  $61^\circ$
WWS      $4,2$                            $10^\circ$  $55^\circ$
WSW               $51$km                  $48^\circ$              $98^\circ$
SsW      $6,4$m               $12,8$m                             $80^\circ$
sSW      $10,2$cm $7,5$cm                             $40^\circ$

\End{multicols}

# II Hauptgang

1. Welcher Fall ist welcher? Wenden Sie den Sinus- oder den Kosinussatz an!
   \Begin{multicols}{2}
   a) Berechnen Sie die Längen $a$ und $b$ wenn $\alpha=59^\circ$ und $\beta = 41^\circ$. 
      ![](entfernung_boot.png)

   b) Wie weit ist es von Seeufer $A$ zu $B$?
      ![](entfernung_seeufer.png)

   c) Berechnen Sie die Entfernung $\overline{PB}$.
      ![](ssw_buntstifte.png)

   d) \Begin{multicols}{2}
      Berechnen Sie den Winkel $\alpha$.
      ![](wuerfel_diagonale_winkel.png){height=100px}

      \End{multicols}

   \End{multicols}

2. Realschulabschlussaufgabe von Annodazumal

   \Begin{multicols}{2}

   Im Viereck $ABCD$ sind gegeben:

   - $\overline{AB} = 26,8 \text{cm}$
   - $\overline{BD} = 35,2 \text{cm}$
   - $\delta = 88,5^\circ$

   Berechnen Sie den Winkel $\gamma$ und den Umfang des Vierecks.

   $$\text{ }$$

   ![](rsa_annodazumal.png)

   \End{multicols}

# III Dessert

\Begin{multicols}{2}

1. Von einem $70$m hohen Turm sieht man eine Wolke unter dem Höhenwinkel $45^\circ$ und ihr Spiegelbild unter dem Tiefenwinkel von $50^\circ$. (Skizze rechts) Berechnen Sie die Höhe der Wolke. (Beachten Sie, dass der Einfallswinkel von Licht gleich dem Ausfallswinkel ist.)

2. Berechnen Sie die Länge der Landebahn?

   ![](landebahn_laenge.png){}

   ![](wolkenhoehe.png){height=300px}

\End{multicols}

