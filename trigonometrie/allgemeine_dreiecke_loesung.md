---
title: "Lösungen zum Übungsblatt 8"
author: [Klasse 10]
date: 07.04.2022
keywords: [Cosinussatz, Sinussatz, Allgemeine Dreiecke, Winkelsumme]
lang: de
header-center: "Trigonometrie" 
---

# I Starters

## SSS

- Kosinussatz: $c^2 = a^2 + b^2 -2ab\cos\gamma$ \newline
  - Nach $\gamma$ auflösen:
    $$\Begin{array}{rll}
    2ab\cos\gamma & = a^2 + b^2 -c^2     \quad &| \, :2ab \\
    \cos\gamma &= \frac{a^2+b^2-c^2}{2ab} \quad &| \, \cos^{-1}(\cdot) \\
    \gamma &= \cos^{-1}\left(\frac{a^2+b^2-c^2}{2ab}\right)
    \End{array}$$
  - Einsetzen: $\gamma = \cos^{-1}\left(\frac{4^2+7^2-9^2}{2\cdot 4\cdot 7}\right) = \cos^{-1}\left(\frac{-16}{8\cdot 7}\right) = \cos^{-1}\left(\frac{-2}{7}\right) \approx 106,6^\circ$
- Wir rechnen $\beta$ mit dem Sinussatz aus:
  $$\Begin{array}{rll}
  \frac{b}{\sin\beta} &= \frac{c}{\sin\gamma} \quad &| \, (\cdot)^{-1} \\
  \frac{\sin\beta}{b} &= \frac{\sin\gamma}{c} \quad &| \, \cdot b \\
  \sin\beta &= \frac{\sin\gamma \cdot b}{c}   \quad &| \sin^{-1}(\cdot) \\
  \beta &= \sin^{-1}\left( \frac{\sin\gamma\cdot b}{c} \right) \quad & \text{(wir setzen ein)} \\
        &\approx \sin^{-1}\left(\frac{\sin 106,6^\circ \cdot 7}{9}\right) \approx 48,19^\circ
  \End{array}$$
- Aus der Winkelsumme ergibt sich $\alpha$:
  $$\alpha = 180^\circ - \gamma - \beta \approx 180^\circ - 48,19^\circ - 106,6^\circ = 25,21^\circ$$

## SWS

- $b^2=a^2+c^2-2ac\cos\beta$, also ist: \newline
  $$\Begin{array}{ll}
  b&=\sqrt{a^2+c^2-2ac\cos\beta} = \sqrt{81\text{cm}^2+100\text{cm}^2-2\cdot 9\text{cm}\cdot 10\text{cm}\cdot\cos 61^\circ}\\
   &= \sqrt{181\text{cm}^2-180\text{cm}^2\cdot\cos 61^\circ} \approx 9,68\text{cm}
  \End{array}$$
- $\frac{a}{\sin\alpha} = \frac{b}{\sin\beta}$ stellen wir um nach $\alpha$:
  $$\Begin{array}{rll}
  \frac{\sin\alpha}{a} &= \frac{\sin\beta}{b} \quad &| \, \cdot a \\
  \sin\alpha &= \frac{\sin\beta \cdot a}{b}   \quad &| \sin^{-1}(\cdot) \\
  \beta &= \sin^{-1}\left( \frac{\sin\beta\cdot a}{b} \right) \quad & \text{(einsetzen)} \\
        &\approx \sin^{-1}\left(\frac{\sin 61^\circ \cdot 9}{9,68}\right) \approx 54,41^\circ
  \End{array}$$
- $\gamma = 180^\circ - \alpha - \beta \approx 180^\circ -61^\circ - 54,41^\circ = 64,59^\circ$

## WWS

- $\frac{b}{\sin\beta} = \frac{a}{\sin\alpha}$ nach $b$ umstellen:
  $b = \frac{\sin\beta \cdot a}{\sin\alpha} \stackrel{\text{einsetzen}}{=} \frac{4,2\cdot \sin 55^\circ}{\sin 10^\circ} \approx 19,81$
- $\gamma = 180^\circ - 10^\circ - 55^\circ = 115^\circ$
- $\frac{c}{\sin\gamma} = \frac{a}{\sin\alpha} \Leftrightarrow c=\frac{a\cdot\sin\gamma}{\sin\alpha} = \frac{4,2\cdot\sin 115^\circ}{\sin 10^\circ} \approx 21,92$

## WSW

- $\beta = 180^\circ - 98^\circ - 48^\circ = 34^\circ$
- $\frac{a}{\sin\alpha} = \frac{b}{\sin\beta} \Leftrightarrow a=\frac{b\cdot\sin\alpha}{\sin\beta} = \frac{51\text{km}\cdot\sin 48^\circ}{\sin 34^\circ} \approx 67,78\text{km}$
- $\frac{c}{\sin\gamma} = \frac{b}{\sin\beta} \Leftrightarrow a=\frac{b\cdot\sin\gamma}{\sin\beta} = \frac{51\text{km}\cdot\sin 98^\circ}{\sin 34^\circ} \approx 90,32\text{km}$

## SsW
 
- $\frac{6,4\text{m}}{\sin\alpha} = \frac{12,8\text{m}}{\sin 80^\circ} \Leftrightarrow \sin\alpha = \frac{6,4\cdot \sin 80^\circ}{12,8} \Leftrightarrow \alpha = \sin^{-1} \left(\frac{6,4\cdot\sin 80^\circ}{12,8}\right) \approx 29,5^\circ$
- $\beta \approx 180^\circ - 80^\circ - 29,5^\circ = 70,5^\circ$
- $\frac{b}{\sin\beta} = \frac{c}{\sin\gamma} \Leftrightarrow b = \frac{c\cdot\sin\beta}{\sin\gamma} \approx \frac{12,8\text{m}\cdot\sin 70,5^\circ}{\sin 80^\circ} \approx 12,25\text{m}$

# II Hauptgang

1. a) Fall WSW, $\gamma = 80^\circ$, $b\approx 79,94$m, $a\approx 104,45$m
   b) Fall SWS, $c\approx 79,27$m
   c) Fall sSW
      - $\beta \approx 62,68^\circ$ oder $\beta \approx 117,32^\circ$
      - $\alpha \approx 71,02^\circ$ oder $\alpha\approx 16,38^\circ$
      - Und schließlich $a\approx 108,56$m oder $a\approx 32,38$m
   d) Fall SSS, aber das links unten ist ein rechter Winkel und die Anwenung des Tangens ist viel einfacher, als die Anwendung des Kosinussatzes, wahrscheinlich $\alpha \approx 35,26^\circ$

2. (Zwischen-)Ergebnisse:
   - $\overline{AD} \approx 22,82$cm
   - $\delta_u \approx 49,58^\circ$, $\delta_o \approx 38,92^\circ$
   - $\gamma \approx 51,08^\circ$
   - $\overline{CD} = 45,24$cm, $\overline{BC} \approx 28,42$cm
   - $U=\overline{AB}+\overline{AD}+\overline{CD}+\overline{BC} \approx 123,28$cm
