---
title: "Kurztest Allgemeine Dreiecke"
author: "Name:"
date: 11.04.2022
keywords: [Sinussatz, Kosinussatz, Winkelfunktionen, Pythagoras]
lang: de
footer-center: "15 Punkte"
header-center: "Klasse 10"
footer-right: "Bearbeitungszeit: 45 Minuten"
geometry:
- top=3.5cm
---

\Begin{multicols}{2}

1. Gegeben sei ein beliebiges Dreieck, wie rechts zu sehen. Vervollständigen Sie die folgenden Ausdrücke zu mathematisch wahren Aussagen. \hfill (__3P__)

   a) $\frac{d}{\sin\delta} = \frac{}{\sin\phantom{\beta}}$
   b) $\phantom{\frac{}{\sin\delta}} = \cos^{-1}\left(\frac{\phantom{f^2}+b^2-\phantom{d^2}}{2fb}\right)$
   c) $\frac{f}{\phantom{\sin\delta}} = \frac{\sin\varphi}{}$

   ![](allgemeines_dreieck_unusual.png){height=160px}

\End{multicols}

2. Berechnen Sie... 

   \Begin{multicols}{2}

   a) die Länge der Seite $b$. \hfill (__2P__)
      ![](kurztest_sws.png){height=200px}
   b) die Größe des Winkels $\gamma$. \hfill (__2P__)
      \newline
      $\text{ }$ \newline
      ![](kurztest_ssw.png){}

   \End{multicols}

3. Gegeben sind das gleichschenklige Dreieck $ABC$ und das rechtwinklige Dreieck $AEC$. \hfill (__5P__)
   \Begin{multicols}{2}

   ![](rsa_2018_p2.png)

   Es gilt:

   - $\overline{AE} = 9,4 \text{cm}$
   - $\varepsilon = 55^\circ$
   - $\overline{AC} = \overline{BC}$

   Berechnen Sie die Länge von $\overline{BE}$.

   \End{multicols}

\Begin{multicols}{2}

4. Ein Baum steht an einem Berghang, der um $\beta=25^\circ$ zur Waagerechten geneigt ist. Wenn die Sonnenstrahlen in einem Winkel von $\alpha=12^\circ$ (wiederum auf die Waagrechte bezogen) einfallen, wirft der Baum einen $x=26$m langen Schatten. Berechnen Sie die Höhe $h$ des Baumes. Tipp: $\beta$ tritt hier als Wechsel- und als Stufenwinkel auf. \hfill (__3P__)

![](baum_am_hang.png)

\End{multicols}
