---
title: "Übungsblatt 4"
author: [Klasse 9]
date: 26.04.2021
keywords: [Volumen, Kegel, Cavalieri]
lang: de
header-center: "Stereometrie" 
---


# I Starters

1. Wir betrachen die Pyramide $A$ mit rechteckiger Grundfläche und Höhe $5$, sowie den gleichhohen Kegel $B$. Eine Seite der Pyramiden-Grundfläche habe die Länge $\pi$, die zweite Seite sei $4$ lang. Der Radius der Kegelgrundfläche betrage $2$.

   a) Berechnen Sie die Grundflächen der Pyramide und des Kegels.
   b) Berechnen Sie die Fläche der (horizontalen) Querschnitte von Pyramide und Kegel auf halber Höhe. 
   c) Geben Sie eine Formel für die Fläche des Querschnitts in Abhängigkeit von der Höhe $h$ an.
   d) Haben Sie eine Vermutung für das Volumen des Kegels?

![](pyramide_und_zwei_kegel.png){}

\Begin{multicols}{2}

2. Gegeben sei ein Kreis mit Radius $5$ in dem ein Sektor von $144$ Grad fehlt.

   a) Wie groß ist die Fläche des beschriebenen unvollständigen Kreises?
   b) Wie hängt die beschriebene Figur mit der Mantelfläche des Kegels $C$ zusammen?
   c) Geben Sie den Flächeninhalt der Oberfläche von $C$ an.

![](kegelmantel_abgerollt.png){height=250px}


\End{multicols}

# II Hauptgang

1. Berechnen Sie das Volumen von Kegel $C$ sowie die Oberfläche des Kegels $B$.

\Begin{multicols}{3}

2. Das Dach eines runden Turms (Druselturm in Kassel) mit $9,20$ Metern Durchmesser soll neu gedeckt werden. Insgesamt ist der Turm $44$ Meter hoch und bis zur Dachkannte sind es $27,10$ Meter vom Boden aus. Berechnen Sie die Oberfläche des Daches, um die Zahl der benötigten Dachschindeln abschätzen zu können. Sie können sich dabei an den Schritten rechts orientieren.

   ![](Druselturm_kassel_top.jpg){height=330px}


   a) Wie hoch ist der Kegel, den das Dach bildet?
   b) Berechnen Sie die Länge der Mantellinie, also die Länge von der Dachkannte bis zur Turmspitze.
   c) Jetzt können Sie die Formel zur Berechnung der Kegeloberfläche anwenden, müssen aber den Kegelboden weglassen bzw. wieder abziehen.

\End{multicols}

# III Desert

Es sei eine Ellipse $E$ mit kleiner Halbachse $b=3$ und großer Halbachse $a=5$ gegeben. Die Fläche der Ellipse berechnet man wie folgt: $F_E = a\cdot b \cdot \pi$. Wie groß ist das Volumen des Ellipsenkegels mit der Ellipse $E$ als Grundfläche und einer Höhe von $10$.


