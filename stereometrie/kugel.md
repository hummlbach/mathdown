---
title: "Übungsblatt 5"
author: [Klasse 9]
date: 21.05.2021
keywords: [Volumen, Kegel, Kugel, Cavallieri]
lang: de
header-center: "Stereometrie" 
---

# I Starters

1. Berechnen Sie

   a) die Volumina der folgenden Kugeln.
   b) die Oberflächen der beiden linken Kugeln.

   ![](kugeln.png){height=200px}


# II Hauptgang

1. Der Erdumfang beträgt ca. $40000$ km.
   a) Wie groß ist die Erdoberfläche?
   b) Wie groß ist das Volumen der Erde?

2. Um $1$ kg mit einem Helium-Balon anzuheben, braucht man ungefähr $1$ Kubikmeter Helium.
   a) Wie groß muss der Durchmesser des Ballons sein, wenn Sie $63$ kg anheben wollen?
   b) Wieviele Ballons brauchen wir, um $63$ kg zu heben, wenn die Ballons einen Durchmesser von $30$ cm haben?

3. Eine halbe Hohlkugel aus Eisen hat einen Durchmesser von $5$ cm und eine $1$ cm starke Wand.
   a) Wie groß ist da Volumen der halben Hohlkugel? (Nicht das Volumen, das in die Halbkugel passt, sondern das Volumen, das das Material der Halbkugel einnimmt.)
   b) Wie groß ist die Oberfläche der halben Hohlkugel?

\pagebreak

0. Wir betrachten eine Halbkugel mit Radius $r$ und einen auf der Spitze stehenden Kegel mit gleichem Radius sowie der Höhe $r$ wie in folgender Abbildung:

   ![](summary/kugelvolumen.png){}

   a) Berechnen Sie die Fläche des Halbkugelquerschnitts auf halber Höhe.
   b) Was ist die Summe aus der Halbkugelquerschnittsfläche und der Kegelquerschnittsfläche auf halber Höhe?
   c) Geben Sie eine Formel für die Halbkugelquerschnittsfläche in Abhängigkeit von der Höhe an.
   d) Was ist die Summe aus Halbkugelquerschnittsfläche und Kegelquerschnittsfläche in Höhe $h$?
   e) Haben Sie eine Vermutung für das Volumen der Halbkugel?


