---
title: "Übungsblatt 1"
author: [Klasse 9]
date: 02.02.2021
keywords: [Volumen, Würfel, Quader]
lang: de
header-center: "Stereometrie" 
---


# I Starters

1. Berechnen Sie das Volumen und die Oberfläche eines Würfels mit der Kantenlänge

   \Begin{multicols}{2}
   a) $4$ cm
   b) $9$ mm
   c) $2,1$ m
   d) $\frac{3}{4}$ m
   e) $\sqrt{2}$ cm
   f) $\sqrt[3]{5}$ cm


   \End{multicols}


2. Berechnen Sie die Volumina und Oberflächen der folgenden Quader:

   ![](quader.png){}


# II Hauptgang

1. Gertrude möchte einen Kleiderschrank aufstellen. Der Schrank ist $60$ cm tief, $1,80$ m breit und $2,21$ m hoch. Die Deckenhöhe in Gertrudes Wohnung beträgt $2,28$ m. Kann der Schrank im Liegen zusammengebaut werden (oder sollte das lieber im Stehen geschehen)?

2. $2$ Ster ($1$ Ster auch als Raummeter bezeichnet, ist die Menge Holz, die in einen Würfel von $1$ m Kantenlänge passt) Holz sollen auf einer Länge von $4$ m an eine Wand geschlichtet werden. Wie hoch wird der "Holzstoß" ungefähr, wenn die Holzscheite $40$ cm lang sind?

3. Wieviele Kubikcentimeter fasst eine Milchpackung ($1$ l)?

\pagebreak

# III Dessert

Der Bodensee hat ein Volumen von $48 \text{ km}^3$.

   a) Wieviel Liter Wasser sind ungefähr im Bodensee?
   b) Wieviele Menschen können sich in den Bodensee stellen/stapeln, wenn ein Mensch ungefähr das Volumen eines Quaders von $30$ mal $30$ mal $180$ cm einnimmt?
   c) Wäre der Bodensee überall gleichtief, dann betrüge seine Tiefe ca. $90$ m (mittlere Tiefe). Wieviele Mensch könnten dann auf dem Grund des Bodensees nebeneinander stehen? Passt das zu einer Fläche von $536$ Quadratkilometern?


