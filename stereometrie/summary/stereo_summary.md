---
title: "Stereometrie"
author: [Klasse 9]
date: 20.05.2021
keywords: [Volumen, Kegel, Kugel, Cavalieri]
lang: de
---

# Zusammenfassung zur Stereometrie

## 0 Einleitung

Stereometrie ist ein Teilgebiet der Geometrie und befasst sich mit geometrischen Gebilden im (dreidimensionalen) Raum.
Zur Stereometrie gehört unter anderem die Berechnung der Oberflächen und der Volumen von geometrischen Körpern im Raum; mit der wir uns im Speziellen befassen wollen.


## 1 Würfel, Quader und Einheiten

So wie man ein (zweidimensionales) Quadrat von $1 cm$ Kantenlänge, Quadratzentimeter nennt, so nennen wir einen kleinen (dreidimensionalen) Würfel von einem Zentimeter Kantenlänge Kubikcentimeter. Das Volumen eines Würfels oder Quaders gibt man an, indem man zählt wieviele dieser Kubikzentimeter in den gegebenen Würfel oder Quader passen.

\Begin{multicols}{3}

![](wuerfel.png){height=100px}

\Begin{multicols}{2}

Volumen: \newline
Oberfläche: \newline
Raumdiagonale: \newline 

$V=a^3$ \newline
$O=6\cdot a^2$ \newline
$d=a\cdot \sqrt{3}$

\End{multicols}

\End{multicols}

Das Volumen eines Quaders ergibt sich, indem man Höhe, Breite und Tiefe miteinander multipliziert. (Breite mal Tiefe ist die Anzahl der Kubikzentimeter, die auf den Boden des Quaders passen. Die Höhe gibt an wieviele dieser Schichten in den Quader passen.)

\Begin{multicols}{2}

![](quader.png){height=100px}

$V=a\cdot b \cdot c$ \newline
$O=2(ab+bc+ac)$ \newline
$d=\sqrt{a^2+b^2+c^2}$ \newline
$G = a\cdot b$ (Grundfläche)

\End{multicols}

Während $1 m = 100 cm$, ist $1 m^2$ nicht $100 cm^2$, sondern $100\cdot 100 cm^2 = 10000 cm^2$. Entsprechend ist $1 m^3 = 1000000 cm^3$. Man kann Kubikmeter in Kubikcentimeter umrechnen, indem man "stur" das $m$ für Meter durch $100 cm$ ersetzt:

**Beispiel**: \newline
$$0,1 m^3 = 0,1 (100 cm)^3 = 0,1 \cdot 100^3 cm^3 = 0,1 \cdot 1000000 cm^3 = 100000 cm^3$$
Oder andersherum:
$$1234567 cm^3 = 1234567 \left(\frac{1}{100} m\right)^3 = 1234567 \left(\frac{1}{100}\right)^3 m^3 = 1,234567 m^3$$

Werden also Flächen- bzw. Volumeneinheiten ineinander umgerechnet, so gehen die "Längenumrechnungsfaktoren" in der zweiten bzw. dritten Potenz ein.

**Definition** (Liter): \newline
Beim Ausmessen von 1-Liter-Getränke-Verbunstoffpackungen haben wir festgestellt, dass $1$ Liter als $1000$ Kubikzentimeter definiert ist:

$$ 1l = 1000 cm^3$$

**Beispiel** (Fildorado Springerbecken): \newline
Wieviele Liter Wasser sind im Springerbecken im Fildorado? \newline
Das Springerbecken im Fildo ist $10$ Meter breit, $10$ Meter lang und $5$ Meter tief:
$$V=10m\cdot 10m\cdot 5m = 500m^3 = 500 (100cm)^3 = 500 \cdot 100^3 cm^3 = 500000000 cm^3 = 500000000 l$$

## 2 Prismen und Zylinder

Ein (gerades) Prisma ist ein Körper der sich von einem Vieleck als Grundfläche senkrecht (zum Beispiel) nach oben erstreckt. Analog ist ein Zylinder ein säulenartiger Körper mit einem Kreis als Grundfläche. Die Volumina berechnen sich jeweils durch "Grundfläche mal Höhe".

\Begin{multicols}{3}

![](prisma.png){height=190px}

$V=G \cdot h$ \newline

![](zylinder.png){height=190px}

$$
\begin{array}{ll}
V & =G \cdot h = r^2\cdot\pi\cdot h \\
O &= 2\cdot r^2\cdot\pi + 2\cdot r\cdot\pi\cdot h \\
  & = 2r\pi (r+h)
\end{array}
$$

![](zylinderoberflaeche.png){height=300px}

\End{multicols}

Die Oberfläche des Zylinders setzt sich zusammen aus Boden, Deckel und Mantel des Zylinders. Wir müssen also zweimal den Flächeninhalt der Grundfläche und den Flächeninhalt des Mantels zusammenzählen. Den Mantel können wir dazu mit einem senkrechten Schnitt aufschneiden und "abrollen" - es ergibt sich ein Rechteck mit den beiden Kantenlängen $h$ und $2\pi r$. (Die Länge des Rechtecks entspricht dem Umfang des Zylinders.)

## 3 Pyramiden

Volumen und Oberflächen von Pyramiden mit quadratischen Grundflächen berechnen sich wie folgt:

\Begin{multicols}{3}

![](pyramide.png){height=120px}

$$
\begin{array}{ll}
V &= \frac{h\cdot a^2}{3} \\
O &= a^2 + 4\cdot\frac{a\cdot c}{2} \\
  &= a^2 + a\sqrt{4h^2+a^2}
\end{array}
$$

\End{multicols}

Für Pyramiden mit beliebigen Grundflächen ergibt sich das Volumen analog:

$$V=\frac{G\cdot h}{3}$$

(wenn $G$ die Fläche des Pyramidenbodens bezeichnet.)

## 4 Das Prinzip von Cavallieri

Kann man zwei Körper $A$ und $B$ so nebeneinander stellen, dass für jede waagerechte Ebene die Schnittfläche des Körpers $A$ mit der Ebene genau so groß ist, wie die Schnittfläche des Körpers $B$ mit der Ebene, so haben die beiden Körper das gleiche Volumen.

   ![](cavallieri_pyramide_kegel.png){height=200px}

Auf diese Weise kann man sehen, dass die Formel fürs Kegelvolumen (im Prinzip) die gleiche ist, wie die für das Pyramidenvolumen.

## 5 Kegel

Das Volumen des Kegels berechnet sich also analog zum Volumen der Pyramide.
Der Oberflächeninhalt des Kegels setzt sich zusammen aus dem Flächeninhalt des Kegelbodens $G$, sowie der Kegelmantelfläche $M$.
Auf der rechten Seite ist der entlang einer Mantellinie aufgeschnittene und abgerollte Kegelmantel dargestellt.
Die Fläche dieses Kreissegments ergibt sich, indem man die Fläche des ganzen Kreises mit Radius $m$ durch dessen Umfang $2m\pi$ teilt und mit dem Umfang des Kegelbodens $2r\pi$ multipliziert: $M = \frac{m^2\pi}{2m\pi}\cdot 2r\pi = mr\pi$

\Begin{multicols}{3}

![](kegel.png){height=120px}

$$
\begin{array}{ll}
V &= \frac{r^2 \cdot \pi \cdot h}{3} \\
O &= G + M \\
  &= r^2\cdot\pi + r\cdot\pi\cdot m \\
  &= r\pi(r+\sqrt{r^2+h^2})
\end{array}
$$

   ![](kegelmantel_abgerollt.png){height=120px}

\End{multicols}

## 6 Kugel

Auch das Kugelvolumen können wir uns mit dem Satz von Cavallieri erschließen.

![](kugelvolumen.png){height=200px}

Wir berechnen die Flächeninhalte der abgebildeten Schnittkreise in Höhe $h$:

\Begin{multicols}{3}
Da die Höhe des Kegels seinem Radius entspricht, Kegelhöhe und Radius mit der Mantellinie also ein gleichschenkliges Dreieck bildet, sind auch $r_1$ und $h$ gleichlang, und die Fläche des grünen Kreises ist: 
$$F_1 = h^2\cdot\pi$$

Der Radius $r_2$ bildet mit der Höhe $h$ und dem Radius $r$ ein rechtwinkliges Dreieck. Aus dem Satz von Pythagoras folgt $r_2 = \sqrt{r^2-h^2}$ und der Flächeninhalt des roten Kreises ist:
$$F_2 = \sqrt{r^2-h^2}^2\cdot\pi = (r^2-h^2)\pi$$

Wir addieren die Flächeninhalte der beiden Kreise:
$$
\begin{array}{ll}
F_1+F_2 &= h^2\cdot\pi+(r^2-h^2)\cdot\pi \\
        &= h^2\pi+r^2\pi-h^2\pi \\
        &= r^2\pi = F_3
\end{array}
$$
Der grüne und der rote Kreis sind zusammen also so groß wie der blaue Kreis.
\End{multicols}

Nach dem Satz von Cavallieri sind die Volumina des Kegels und der Halbkugel zusammen so groß wie das Volumen des Zylinders. Außerdem wissen wir schon, dass das Kegelvolumen ein Drittel des Zylindervolumens ist. Das Halbkugelvolumen muss also zwei Drittel des Volumens des Zylinders mit Höhe $r$ sein. Das Kugelvolumen ist dann nocheinmal das doppelte des Halbkugelvolumens: 
$$V = 2\cdot\frac{2}{3}\pi \cdot r^2 \cdot r = \frac{4}{3} \pi r^3$$

Um zu sehen wie groß die Oberfläche der Kugel ist, stellen wir uns die Kugel aus vielen Pyramiden der Höhe $r$ (Kugelradius), mit der Spitze in der Kugelmitte, zusammengebaut vor. Natürlich ergibt das nur eine Annäherung an die Kugel, aber je mehr Pyramiden und kleiner deren Grundflächen desto mehr nähert sich das Konstrukt der Kugel an. Wir können die gerade gefundene Formel fürs Kugelvolumen der Summe der Pyramidenvolumen gegenüberstellen. Letzteres hängt von den Grundflächen der Pyramiden - die hier die Kugeloberfläche annähern - ab:

$$\frac{1}{3}\cdot r \cdot G_1 + \frac{1}{3}\cdot r \cdot G_2 + \dots + \frac{1}{3}\cdot r \cdot G_n = \frac{4}{3}\cdot r^3\cdot \pi$$

Wir können auf der linken Seite $\frac{1}{3}r$ ausklammern:
$$\frac{1}{3} r (G_1 +  G_2 + \dots + G_n) = \frac{4}{3}r^3\pi$$

Fassen wir noch die Grundflächensumme $G_1+\dots +G_n$ zur Kugeloberfläche $O$ zusammen, teilen durch $r$ und multiplizieren mit $3$, so erhalten wir:

$$O = 4 r^2 \pi$$

Wir halten Zum Abschluss fest, wie sich Oberfläche und Volumen der Kugel berechnen lassen:

\Begin{multicols}{3}

![](kugel.png){height=120px}

$$
\begin{array}{ll}
V &= \frac{4}{3}\pi r^3 \\
O &= 4\pi r^2
\end{array}
$$

\End{multicols}


