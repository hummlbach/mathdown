---
title: "Abschlussübungsblatt"
author: [Klasse 9]
date: 18.06.2021
keywords: [Volumen, Oberflächen, Cavallieri, Würfel, Quader, Zylinder, Kegel, Pyramide, Kugel]
lang: de
header-center: "Stereometrie" 
---


# I Starters

1. Berechnen Sie das Volumen und die Oberfläche...
   a) ...eines Würfels mit Kantenlänge $3$ cm.
   b) ...eines Quaders, der $50$ cm tief, $1$ m hoch und $20$ cm breit ist.
   c) ...einer Kugel mit einem Radius von $1000$ km.

2. Berechnen Sie das Volumen eines $90$ cm langen Zylinders, dessen Grundfläche einen Radius von $3$ cm hat.

3. Gegeben sei eine $2,5$ Meter hohe Pyramide, deren quadratische Grundfläche eine Seitenlänge von $2$ Metern hat. Wie sind ihr Volumen und ihre Oberflächen?

4. Gegeben sei ein Kegel mit Höhe $h=2$, dessen Grundfläche ein Kreis mit Radius $r=1$ bildet. Wie groß ist das Volumen und die Oberfläche des Kegels?

# II Hauptgang

1. Was ist die Kantenlänge eines Würfels mit Volumen $V=64$?
2. Von einer Kugel ist bekannt, dass sie eine Oberfläche von ca. $12,56 \text{cm}^2$ hat. Wie groß ist ihr Radius?
3. Berechnen Sie die Höhe der folgenden Körper:

   ![](hoehe_zylinder_quader_pyramide.png){}

\pagebreak

4. Vervollständigen Sie die folgende Tabelle mit Angaben über drei Kegel:

   Kegelname     Grundflächenradius     Höhe                Volumen                Oberfläche
   ----------    -------------------    -----               --------               -----------
   $A$           $10$ cm                                    $209,4 \text{cm}^3$
   $B$                                  $150 \text{cm}^2$   $1,575 \text{m}^3$
   $C$           $3$                                                                $75,6$

# III Desert

1. Sie möchten gerne die Kantenlänge eines Würfels messen, haben aber kein Lineal zur Hand. Wie können Sie mit Hilfe eines Messbechers dennoch seine Kantenlänge bestimmen?


2. Ein Kegel habe ein Volumen von $\pi$ Kubikmetern. Nennen Sie zwei Paare aus Höhe und Grundflächenradius, die zum angegebenen Volumen passen.

\Begin{multicols}{2}
3. Gegeben sei der unten dargestellte Pilz. Wie groß ist
   a) sein Volumen?
   b) seine Oberfläche?


![](Pilz.png){}


4. Napoleon soll bei der Besichtigung der Cheops-Pyramide gesagt haben, man könne aus den Steinen der Pyramide eine Mauer um ganz Frankreich errichten. Stimmt das? Wie dick wäre die Mauer, wenn Sie $2$ Meter hoch sein soll? Die Cheops-Pyramide ist $146$ Meter hoch bei einer ursprünglich quadratischen Grundfläche von ca. $230$ Metern Kantenlänge. Die Grenzen Frankreichs zu anderen europäischen Ländern haben zusammengenommen eine Länge von $2889$ km. Die Küstenlinie des französischen europäischen Mutterlandes ist $3427$ km lang.

\End{multicols}


