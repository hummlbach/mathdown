---
title: "Übungsblatt 3"
author: [Klasse 9]
date: 10.04.2021
keywords: [Volumen, Pyramide, Kegel]
lang: de
header-center: "Stereometrie" 
---


# I Starters

![](Pyramiden.png){}

1. a) Berechnen Sie das Volumen des Würfels $A$ aus obigem Bild.
   b) Wie ist das Volumen der Pyramide $B$?

2. a) Berechnen Sie die Höhe einer Pyramidenseite der Pyramide $C$.
   b) Wie ist der Flächeninhalt der Pyramidenseite?
   c) Geben Sie die Oberfläche der Pyramide $C$ an.

3. Berechnen Sie das Volumen der Pyramide $D$.

# II Hauptgang

\Begin{multicols}{2}

Die drei markanten, großen Pyramiden von Gizeh (Cheops, Chephren und Mykerinos, im Bild von rechts nach links) waren ursprünglich $146,6$, $143,5$ bzw. $65$ Meter hoch, und haben in etwa quadratische Grundflächen mit Seitenlängen von $230,3$, $215,25$ bzw. $103,4$ Metern. 

Name        Höhe     Seitenlänge
----------- -------  -----------
Mykerinos   $65$     $103,4$ 
Chephren    $143,5$  $215,25$ 
Cheops      $146,6$  $230,3$ 

\End{multicols}

![](Pyramiden_von_Gizeh.png){}

Berechnen sie die Volumina und die Oberflächen der Pyramiden.

# III Desert

Gegeben Sei der folgende Tetraeder (links) und die Dreieckspyramide (rechts):

![](dreieckspyramide_und_tetraeder.png){}

a) Berechnen Sie Volumen und Oberfläche der Dreieckspyramide.
b) Berechnen Sie Volumen und Oberfläche des Tetraeders.

