---
title: "Übungsblatt 2"
author: [Klasse 9]
date: 02.02.2021
keywords: [Volumen, Prisma, Kegel]
lang: de
header-center: "Stereometrie" 
---


# I Starters

1. Berechnen Sie das Volumen eines Quaders mit...

   a) einer Grundfläche von $4$ cm mal $3$ cm und $10$ cm Höhe.
   b) einer Grundfläche von $50$ cm mal $30$ cm und $2$ m Höhe.
   c) einer Grundfläche von $40$ mm mal $3$ cm und $0,1$ m Höhe.


2. Berechnen Sie die Flächeninhalte der folgenden geometrischen Figuren:

   ![](flaechen.png){height=180px}


3. Berechnen Sie die Volumina und Oberflächen der Körper $A$ und $E$ aus dem folgenden Bild. Die Grundflächen sind dabei die Figuren aus 2. (Die Höhen sind in rot angegeben.) 

   ![](prismen_und_kuchen.png){}

# II Hauptgang

1. Berechnen Sie die Volumina und Oberflächen der Körper $B$, $C$ und $D$ aus dem vorherigen Bild.

2. Berechnen Sie die Volumina und die Oberflächen einer Konservenbüchse mit...

   a) $9$ cm Durchmesser und $16$ cm Höhe
   b) einer Höhe von $11$ cm und einem Durchmesser von $11$ cm
   c) einem Durchmesser von $12$ cm und einer Höhe von $9,2$ cm

   Welche der drei Büchsengrößen würden Sie als Hersteller verwenden?

3. Für eine Rakete ist ein Durchmesser von $10$ Metern vorgesehen. Um eine Nutzlast von $100$ Tonnen in einen niedrigen Erdorbit zu bringen, sind ungefähr $7$ Millionen Liter Treibstoff erforderlich. Wie hoch muss die Rakete mindestens sein, wenn für die Nutzlast noch $1000$ Kubikmeter Stauraum zur Verfügung stehen sollen?

4.
   a) Berechnen Sie die Flächen der Figuren $A$ bis $D$ aus dem folgenden Bild:

      ![](zusammengesetzte_flaechen.png){height=200px}

   b) Berechnen Sie die Volumen und Oberflächen der Prismen $A$ bis $D$ aus dem folgenden Bild. Die Grundflächen sind dabei die gefüllten Vielecke aus dem vorherigen Bild mit den gleichen Bezeichnungen.

   ![](prismen.png){}

# III Dessert

Berechnen Sie die Oberflächen und die Volumina der Prismen $E$ und $F$ aus dem vorherigen Bild.

