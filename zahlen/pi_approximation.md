---
title: "Übungsblatt 7"
author: [Klasse 9]
date: 04.12.2020
keywords: [Irrationale Zahlen, PI, Approximation, Polygone]
lang: de
header-center: "Zahlen" 
---

## I Starters (**ins Übheft**)

1. Den Mittelpunkt zwischen zwei Punkten $A$ und $B$ kann man zeichnerisch bestimmen, indem man die Mittelsenkrechte konstruiert:
   a) Stellen Sie den Zirkel auf die Entfernung der Punkte ein. (Irgendetwas mehr als die Hälfte der Entfernung, würde es auch tun.)
   b) Markieren Sie zwei Punkte ($A$ und $B$). Stechen Sie den Zirkel bei Punkt $A$ ein und ziehen Sie den Kreis mit dem eingestellten Radius.  Wiederholen sie das für den Punkt $B$.
   c) Verbinden Sie die beiden Schnittpunkte der Kreise (mit einem Geraden Strich).
   d) Der Schnittpunkt der geraden Strecke zwischen $A$ und $B$ mit der Strecke aus c) ist der Mittelpunkt zwschen $A$ und $B$.

2. Eine Tangente an einen Kreis - der Berührpunkt heiße $B$ - konstruiert man wie folgt:
   a) Zeichnen Sie (einen Kreis und) einen Durchmesser durch den Punkt $B$ und verlängern Sie diesen, um ein paar Zentimeter über die Kreislinie hinaus.
   b) Stechen Sie den Zirkel am Punkt $B$ ein und zeichnen Sie einen Kreis, sodass Sie Schnittpunkte mit dem in a) gezeichneten Durchmesser erhalten.
   c) Die gesuchte Tangente ist nun die Mittelsenkrechte zwischen den Schnittpunkten aus b).

3. Strahlensatz: Wenn zwei durch einen Punkt - den sogenannten Scheitel - verlaufende Geraden von zwei parallelen Geraden wie folgt geschnitten werden:

   ![](Strahlensatz.png){height=200px}

   Dann gilt $\frac{a+b}{a} = \frac{d}{c}$ sowie $\frac{a+b}{d} = \frac{a}{c}$.
   
   Sei $a=4$, $b=1$ und $c=2$. Wie lang ist $d$?
\pagebreak

## II Hauptgang

1. Zeichnen Sie
   a) in die obere Hälfte der nächsten freien Seite des Epochenheftes einen Kreis mit $12$ cm Durchmesser;
   b) nun die Ecken eines Quadrats auf die Kreislinie; (Schnittpunkte zweier senkrecht aufeinander stehenden Durchmesser mit dem Kreis.)
   c) die ($4$) Mittelsenkrechten zwischen den Ecken des Quadrats;
   d) das Achteck aus den Quadratecken aus b) sowie den Schnittpunkten der Mittelsenkrechten mit der Kreislinie.

2. Zeichnen Sie wieder einen Kreis mit $12$ cm Durchmesser in die obere Hälfte der nächsten freien Epochenheftseite und konstruieren Sie, analog zum Achteck ein $16$-Eck in den neuen Kreis.

3. Rechnen Sie **im Übheft** den Umfang des Achtecks aus. Rechnen Sie in LE; eine LE sei $6$ cm. \newline $\left(\text{Ergebnis: }\, \sqrt{2-\sqrt{2}}\right)$

4. Rechnen Sie **im Übheft** den Umfang des $16$-Ecks aus. Rechnen Sie in LE; eine LE sei $6$ cm. \newline $\left(\text{Ergebnis: }\, \sqrt{2-\sqrt{2+\sqrt{2}}}\right)$

## III Dessert

1. Zeichnen Sie um den Kreis aus II 1. ein umschließendes Achteck, das den Kreis in $8$ Punkten berührt. Die Ecken des umschließenden Achtecks sollen auf den (verlängerten) Durchmessern liegen auf denen auch die Ecken des inneren Achtecks liegen.

2. Konstruieren Sie analog zu 3. ein umschließendes $16$-Eck um den zweiten Kreis (also den aus 2.)

3. Berechnen Sie den Umfang des umschließenden Achtecks und den Umfang des umschließenden $16$-Ecks (in LE, Tipp: Strahlensatz).

4. Wie kann man den Umfang des (in einen Kreis mit Radius $1$) eingeschriebenen $2^n$-Ecks aus dem Umfang des $2^{n-1}$-Ecks errechnen?

