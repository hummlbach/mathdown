#!/usr/bin/python3

import math

s=[]
t=[]
h=[]

s.append(math.sqrt(2))
h.append(1/math.sqrt(2))
t.append(84)

for i in range(20):
    s.append(math.sqrt(2-math.sqrt(4-s[i]*s[i])))
    h.append(math.sqrt(1-math.sqrt(s[i+1]*s[i+1]/4)))
    t.append(s[i+1]/h[i+1])

for i in range(0,10):
    s[i] = s[i] * math.pow(2,(i+2))
    t[i] = t[i] * math.pow(2,(i+2))

print("s: " + str(s))
print("t: " + str(t))
