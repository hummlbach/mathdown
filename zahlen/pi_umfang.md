---
title: "Übungsblatt 6"
author: [Klasse 9]
date: 26.11.2020
keywords: [Irrationale Zahlen, PI]
lang: de
header-center: "Zahlen" 
---

## Vorbemerkung

Rechnen Sie mit $3,141$ oder mit $\frac{22}{7}$ als Näherung für $\pi$.

## I Starters

1. Ein Kreis habe einen Durchmesser von

   \Begin{multicols}{3}

   a) $10$ cm
   b) $21$ m
   c) $3,5$ km

   \End{multicols}

   Wie groß ist der Umfang?

2. Ein Kreis habe einen Umfang von

   \Begin{multicols}{3}

   a) $31,41$ cm
   b) $22$ m
   c) $9,423$ km

   \End{multicols}

   Wie groß ist der Durchmesser?


## II Hauptgang
 
1. Die Achsen zweier paralleler Rollen seien $2$ m voneinander entfernt und der Durchmesser der beiden Rollen betrage $20$ cm. Über die beiden Rollen soll ein Fließband geführt werden (siehe Bild). Wie lang muss das Band sein?

   ![](fliessband.png)

2. Der Erddurchmesser (Entfernung von zwei auf dem Erdball gegenüberliegenden Punkten) beträgt ca. $12700$ km. (Wir nehmen an die Erde wäre eine Kugel.)

   a) Wie lang müsste eine Schnur sein, um einmal um den Äquator zu reichen?
   b) Wieviel "Luft" ist zwischen Schnur und Äquator, wenn wir die Schnur um $3141$ m verlängern?
   c) Um wieviele Meter muss man die Schnur verlängern, um $1$ m Luft zwischen Äquator und Schnur zu bekommen?

3. Ein Auto fährt $100$ km. Welche Streckenlänge misst der Kilometerzähler, wenn der Durchmesser der Reifen wegen Abnutzung statt $50$ cm nur noch $49,5$ cm beträgt?

