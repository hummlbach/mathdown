---
title: "Übungsblatt 3"
author: [Klasse 9]
date: 19.11.2020
keywords: [Rationale Zahlen]
lang: de
header-center: "Zahlen" 
---

Rechnen Sie in eine "Kommazahl" um:

1. Brüche mit Nennern aus den Primfaktoren $2$ und $5$:

   \Begin{multicols}{3}

   a) $\frac{1}{2}$
   b) $\frac{1}{4}$
   c) $\frac{1}{5}$
   d) $\frac{1}{8}$
   e) $\frac{1}{10}$
   f) $\frac{1}{20}$
   g) $\frac{1}{25}$
   h) $\frac{1}{50}$
   i) $\frac{7}{16}$
   j) $\frac{3}{25}$
   k) $\frac{21}{50}$
   l) $\frac{129}{100}$

   \End{multicols}

2. Brüche mit Nennern ohne die Primfaktoren $2$ und $5$:

   \Begin{multicols}{3}

   a) $\frac{1}{3}$
   b) $\frac{1}{7}$
   c) $\frac{1}{9}$
   d) $\frac{1}{11}$
   e) $\frac{1}{21}$
   f) $\frac{1}{27}$
   g) $\frac{1}{33}$
   h) $\frac{1}{49}$
   i) $\frac{11}{100}$
   j) $\frac{7}{16}$
   k) $\frac{3}{25}$
   l) $\frac{53}{50}$

   \End{multicols}

3. Brüche mit gemischten Nennern aus den Faktoren $2$, $5$ und anderen:

   \Begin{multicols}{3}

   a) $\frac{1}{14}$
   b) $\frac{1}{15}$
   c) $\frac{1}{30}$
   d) $\frac{1}{12}$
   e) $\frac{1}{60}$
   f) $\frac{1}{75}$
   g) $\frac{1}{150}$
   h) $\frac{1}{375}$
   i) $\frac{1}{24}$

   \End{multicols}

Sehen Sie einen Zusammenhang zwischen Nachkommastellen und den Faktoren der Nenner?
