---
title: "Übungsblatt 2"
author: [Klasse 9]
date: 18.11.2020
keywords: [Zahlen]
lang: de
header-center: "Zahlen" 
---

1. Zerlegen Sie die folgenden Zahlen in ihre Primfaktoren:

   \Begin{multicols}{3}
   a) $6$
   b) $8$
   c) $15$
   d) $12$
   e) $24$
   f) $18$
   g) $56$
   h) $98$
   i) $125$
   j) $210$
  
   \End{multicols}

2. Kürzen Sie die folgenden Brüche:

   \Begin{multicols}{3}
   a) $\frac{6}{8}$
   b) $\frac{6}{15}$
   c) $\frac{12}{15}$
   d) $\frac{15}{24}$
   e) $\frac{18}{24}$
   f) $\frac{12}{56}$
   g) $\frac{12}{56}$
   h) $\frac{24}{98}$
   h) $\frac{56}{98}$
   j) $\frac{56}{210}$
   k) $\frac{98}{210}$
   l) $\frac{125}{210}$
  
   \End{multicols}


3. Bringen Sie die Brüche auf den kleinsten gemeinsamen Nenner:

   \Begin{multicols}{3}
   a) $\frac{1}{6}$ und $\frac{1}{8}$
   b) $\frac{5}{6}$ und $\frac{2}{15}$
   c) $\frac{5}{12}$ und $\frac{3}{8}$
   d) $\frac{1}{12}$ und $\frac{2}{15}$
   e) $\frac{5}{18}$ und $\frac{2}{15}$
   f) $\frac{3}{18}$ und $\frac{6}{24}$
   g) $\frac{12}{56}$ und $\frac{2}{15}$
   h) $\frac{12}{56}$ und $\frac{2}{15}$
   i) $\frac{2}{56}$ und $\frac{56}{98}$
   j) $\frac{12}{56}$ und $\frac{98}{210}$
   k) $\frac{15}{125}$ und $\frac{14}{210}$
  
   \End{multicols}

4. Addieren bzw. Subtrahieren Sie:

   \Begin{multicols}{3}
   a) $\frac{1}{6} + \frac{1}{8}$
   b) $\frac{5}{6} - \frac{2}{15}$
   c) $\frac{5}{12} + \frac{3}{8}$
   d) $\frac{1}{12} + \frac{2}{15}$
   e) $\frac{5}{18} - \frac{2}{15}$
   f) $\frac{3}{18} - \frac{6}{24}$
   g) $\frac{12}{56} + \frac{2}{15}$
   h) $\frac{12}{56} + \frac{2}{15}$
   i) $\frac{2}{56} - \frac{56}{98}$
   j) $\frac{12}{56} + \frac{98}{210}$
   k) $\frac{15}{125} - \frac{14}{210}$
  
   \End{multicols}


