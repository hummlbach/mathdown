---
title: "Übungsblatt 4"
author: [Klasse 9]
date: 23.11.2020
keywords: [Rationale Zahlen]
lang: de
header-center: "Zahlen" 
---

## I Starters

1. Addieren Sie:

   \Begin{multicols}{3}

   a) $\frac{2}{3}+\frac{3}{4}$
   b) $\frac{11}{42}+\frac{5}{15}$
   c) $\frac{28}{49} + \frac{24}{84}$

   \End{multicols}

2. Subtrahieren Sie:

   \Begin{multicols}{3}

   a) $\frac{1}{3} - \frac{1}{5}$
   e) $\frac{23}{66} - \frac{7}{30}$
   f) $2 - \frac{8}{5}$

   \End{multicols}

3. Multiplizieren Sie:

   \Begin{multicols}{3}

   a) $\frac{3}{8}\cdot\frac{2}{9}$
   h) $\frac{3}{8}\cdot\frac{2}{9}$
   i) $\frac{3}{8}\cdot\frac{2}{9}$

   \End{multicols}

4. Dividieren Sie:

   \Begin{multicols}{3}

   a) $\frac{2}{3}:\frac{3}{4}$
   k) $\frac{9}{14}:\frac{6}{7}$
   l) $\frac{8}{55}:\frac{14}{15}$

   \End{multicols}


## II Hauptgang

1. Rechnen Sie in eine Kommazahl um:

   \Begin{multicols}{3}

   a) $\frac{2}{9}$
   b) $\frac{123}{999}$
   c) $\frac{123}{9990}$
   d) $\frac{123}{99900}$
   e) $\frac{45}{99}$
   f) $\frac{45}{999}$
   g) $\frac{45}{9999}$
   h) $\frac{45}{99990}$
   i) $\frac{45}{999900}$

   \End{multicols}


2. Rechnen Sie in einen Bruch um:

   \Begin{multicols}{3}

   a) $0,\overline{815}\dots$
   b) $0,\overline{0815}\dots$
   c) $0,0\overline{815}\dots$
   d) $0,1\overline{815}\dots$
   e) $0,23\overline{815}\dots$
   f) $1,23\overline{815}\dots$

   \End{multicols}

3. Multiplizieren Sie die Dezimalbrüche:

   \Begin{multicols}{3}

   a) $1,5 \cdot 1,5$
   b) $1,28 \cdot 1,28$
   c) $1,234 \cdot 1,234$
   d) $2,56 \cdot 3,2$
   e) $0,12 \cdot 0,12$
   f) $0,123 \cdot 0,45$

   \End{multicols}

\pagebreak

3. Rechnen Sie mit dem Satz des Pythagoras die fehlende Dreiecksseite aus:

   \Begin{multicols}{2}

   a) ![](triangle_6_8_10.png){height=130px}
   b) ![](triangle_5_12_13.png)

   \End{multicols}

   c) ![](triangle_8_15_17.png){height=150px}

## III Dessert

Finden Sie ganze Zahlen $x$, $y$ und $z$, sodass die Gleichung

a) $x^2+y^2=z^2$
b) $x^3+y^3=z^3$
c) $x^4+y^4=z^4$

erfüllt ist?

