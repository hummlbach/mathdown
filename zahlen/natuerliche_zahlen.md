---
title: "Übungsblatt 1"
author: [Klasse 9]
date: 17.11.2020
keywords: [Zahlen]
lang: de
header-center: "Zahlen" 
---

## Starters

1. Lesen Sie die folgenden Binomialkoeffizienten am Pascalschen Dreieck ab:

   \Begin{multicols}{3}
   a) $\binom{7}{4}$
   b) $\binom{8}{6}$
   c) $\binom{9}{4}$
   d) $\binom{10}{3}$
   e) $\binom{11}{4}$
   f) $\binom{12}{4}$

   \End{multicols}

2. Berechnen Sie die obigen Binomialkoeffizienten anhand der Definition über die Fakultäten.
3. Berechnen Sie:

   \Begin{multicols}{3}
   a) $12^5$
   b) $13^4$
   c) $105^3$

   \End{multicols}

    Tipp: Biomischer Lehrsatz

## Hauptgang (Sieb des Erathostenes)

Anbei finden Sie die Zahlen von $1$ bis $500$ abgedruckt.

1. Markieren Sie alle
   a) geraden Zahlen außer der $2$ in einer Farbe,
   b) dann alle Vielfachen der $3$ (die noch nicht markiert sind) in einer anderen Farbe,
   c) dann alle Zahlen der $5$er-Reihe außer der $5$,
   d) alsdann alle Vielfachen von $7$-, $11$ und $13$ in je einer anderen Farbe (jeweils ohne die Zahl selber zu markieren).
2. Wie lange muss man das fortführen, bis keine neuen Zahlen mehr markiert werden?
3. Welche Zahlen bleiben übrig?
4. Ein Primzahlzwilling sind zwei Primzahlen, zwischen denen nur eine andere (ganze) Zahl liegt. Wieviele Primzahlzwillinge finden Sie im Sieb?

\pagebreak

## Dessert

1. Wieviele Primzahlen gibt es?
2. Zählen Sie die ersten $2$ ungeraden Zahlen zusammen. Addieren Sie jetzt die ersten $3$ ungeraden Zahlen zusammen. Nun die ersten $4$ ungeraden Zahlen, anschließend die ersten $5$ ungeraden Zahlen und dann die ersten $6$ ungeraden Zahlen (addieren). Erkennen Sie ein Muster in den Summen der ungeraden Zahlen? Ist das immer so?

3. Die Zahl $4$ ist Summe zweier Primzahlen, nämlich $4=2+2$. Auch $6$ ist Summe zweier Primzahlen: $3+3=6$. Weiter: $8=5+3$, $10=5+5$ und $12=5+7$. Ist jede gerade Zahl (größer als $2$) Summe von zwei Primzahlen?
4. Wieviele Primzahlzwillinge gibt es?
