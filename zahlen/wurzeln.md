---
title: "Übungsblatt 5"
author: [Klasse 9]
date: 24.11.2020
keywords: [Irrationale Zahlen]
lang: de
header-center: "Zahlen" 
---

1. Bestimmen Sie $\sqrt{3}$ durch Einschachtelung auf $5$ Dezimalstellen genau. Zur Erinnerung:
   
   - man startet wie folgt:

   $$1 < \sqrt{3} < 2 \qquad \text{ denn } \qquad 1^2=1 < 3 \, \text{ und } \, 2^2 = 4 > 3$$

   - und macht weiter mit:

   $$1,7 < \sqrt{3} < 1,8 \qquad \text{ denn } \qquad 1,7^2=2,89 < 3 \, \text{ und } \, 1,8^2 = 3,24 > 3$$

   usw...

2. Löse folgende Gleichungen (auf $4$ Dezimalstellen genau):

   \Begin{multicols}{2}

   a) $x^2 = 25$
   b) $x^2 = 81$
   c) $x^2 = 196$
   d) $x^2 = 2$
   e) $(x+3)^2 = 2$
   f) $(x+4)^2 = 2$
   g) $x^2 = 3$
   h) $(x+5)^2 = 3$
   i) $(x+11)^2 = 3$
   j) $(x+3)^2 -1 = 1$
   k) $(x+11)^2 -9 = -6$
   l) $(x+4)^2 + 128 = 130$

   \End{multicols}

   (Hinweis: es gibt meist zwei Lösungen)
