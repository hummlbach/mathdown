---
title: "Übungsblatt 8"
author: [Klasse 9]
date: 15.12.2020
keywords: [Irrationale Zahlen, PI]
lang: de
header-center: "Zahlen" 
---


Vorbemerkung: Sie können mit $3,15$ oder mit $\frac{22}{7}$ als Näherung für $\pi$ rechnen.

1. Der Erddurchmesser (Entfernung von zwei auf dem Erdball gegenüberliegenden Punkten) beträgt ca. $12700$ km. (Wir nehmen an die Erde wäre eine Kugel.)

   a) Wie lang müsste eine Schnur sein, um einmal um den Äquator zu reichen?
   b) Um wieviele Meter muss man die Schnur verlängern, um rund um die Erde $1$ m Luft zwischen Äquator und Schnur zu bekommen?

2. Rechnen Sie die Flächen und die Umfänge (auch innen liegende Ränder zählen mit) der folgenden Figuren aus:

   ![](Kreisgebilde.png)

   ($\pi$ darf als Konstante im Ergebnis stehen bleiben.)

3. Gegeben sei ein Kreis mit einer Fläche von $12,6 \text{ cm}^2$. Wie groß ist sein Radius?

4. Die Pupillengröße eines jungen Menschen reicht von $1,5$ mm (Tagsehen) bis hin zu $8$ mm (Nachtsehen). Wieviel mal mehr Licht, als bei minimaler Öffnungsweite, kann bei maximaler Öffnungsweite ins Auge fallen?

5. Die Skizze zeigt ein Fußballfeld (blau) und eine Laufbahn, wie man sie häufig zusammen antrifft. Die Innenbahn (rote Linie) ist für gewöhnlich $400$ Meter lang und das Fußballfeld $100$ Meter lang. Wie breit sind Fußballfelder in etwa? 

   ![](Fussballfeld_und_Laufbahn.png)
