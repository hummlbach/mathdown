ROOT_DIR = $(realpath ./)
TOOLS_DIR = $(realpath ./tools/)
PANDOC = pandoc
PANDOC_TEMPLATE = $(TOOLS_DIR)/eisvogel.latex
TEX_PREAMBLE = $(TOOLS_DIR)/preamble.tex
PANDOC_ARGS = --template=$(PANDOC_TEMPLATE) -H $(TEX_PREAMBLE)
PDFLATEX = pdflatex
GNU_PLOT_HISTOGRAM = $(TOOLS_DIR)/probability_histogram.sh
DOT = dot

export

.PHONY: analysis analytische_geo/10 analytische_geo/12 geometrie kombinatorik linalg mechanik neuronale_netze potenzrechnung stereometrie trigonometrie zahlen stereometrie/summary stochastik/10

all: analysis analytische_geo/10 analytische_geo/12 geometrie kombinatorik linalg mechanik neuronale_netze potenzrechnung stereometrie trigonometrie zahlen stereometrie/summary stochastik/10

analysis:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

analytische_geo/11:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

analytische_geo/12:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

geometrie:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

kombinatorik:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

linalg:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

mechanik:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

neuronale_netze:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

potenzrechnung:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

stereometrie:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

trigonometrie:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

stereometrie/summary:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

stochastik/10:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile

zahlen:
	$(MAKE) -C $@ -f $(TOOLS_DIR)/Makefile
