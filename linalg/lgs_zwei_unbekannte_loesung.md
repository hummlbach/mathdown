---
title: "Auffrischen und Vertiefen"
author: [Klasse 11]
date: 07.04.2021
keywords: [Lineare Gleichungssysteme, Geraden, Determinante, Trilateration]
lang: de
header-center: "Lineare Gleichungssysteme" 
---

## Lösungen zu Starters I

### Zur Lösbarkeit der Gleichungssysteme...

#### ... durch Berechnung von Determinanten.

Die Determinante ist definiert durch: "Vorfaktor von $x$ in der ersten Gleichung mal Vorfaktor von $y$ aus der zweiten Gleichung minus Vorfaktor von $x$ in der zweiten Gleichung mal Vorfaktor von $y$ aus der ersten Gleichung."
In Formeln $D = a\cdot d - c \cdot b$, wenn das Gleichungssystem, wie folgt gegeben ist:
$$\begin{array}{rll}
\text{I: }&  ax + by = s \\
\text{II: }& cx + dy = t
\end{array}$$

Wir bringen das Gleichungssystem aus a) auf diese Form:
$$\begin{array}{rrl}
\text{I: }&  3x - y = 2 \\
\text{II: }& x + y = 6
\end{array}$$

Die Determinante ist also $D = 3\cdot 1 - 1\cdot (-1) = 3 - (-1) = 3+1 = 4\neq 0$ und das Gleichungssystem ist eindeutig lösbar.

Für b) und c) ergeben sich die Determinanten $D= -3\cdot 1 - 2 \cdot 2 = -3 -4 = -7$ bzw. $D= (-3) \cdot (-1) - 3 \cdot 4 = 3 - 12 = -9$. Die Determinante ist also in beiden Fällen ungleich $0$ und die Gleichungssysteme sind eindeutig lösbar.

Für d) berechnen wir die Determinante nochmal ausführlich. Wir tauschen die Seiten der ersten Gleichung und ziehen auf beiden Seiten der zweiten Gleichung $3x$ ab und erhalten:
$$\begin{array}{rrlr}
\text{I: }& 6x - 4y &=& -2 \\
\text{II: }& -3x + 2y &=& 2
\end{array}$$

Die Determinante ist also $D = 6\cdot 2 - (-3) \cdot (-4) = 12 - 12 = 0$, was uns sagt, dass das Gleichungssystem nicht eindeutig lösbar ist.

Für e) ist die Determinante $D = (-6) \cdot (-6) - 9 \cdot 4 = 36 - 36 = 0$. Also ist auch das Gleichungssystem aus e) nicht eindeutig lösbar.

\pagebreak

#### ... durch Vergleich der Geraden:

Wir illustrieren noch die "Lösbarkeitsanalyse" anhand der "Geradennormalformen" für die Gleichungssysteme d) bis f). Wir addieren $4y+2$ auf beiden Seiten der ersten Gleichung von d) und teilen durch $4$ , um die Geradennormalform zu erhalten:
$$\begin{array}{rlll}
-2 &= 6x - 4y \qquad &| \quad +4y+2\\
4y &= 6x + 2 \qquad &| \quad :4 \\
y &= \frac{6}{4}x + \frac{2}{4} = \frac{3}{2}x + \frac{1}{2}
\end{array}$$

Die zweite Gleichung, müssen wir nur durch $2$ teilen, um Sie auf Normalform zu bringen:
$$\begin{array}{rll}
2y &= 3x + 2 \qquad | \quad :2 \\
y &= \frac{3}{2}x + 1
\end{array}$$

Die beiden Gleichungen stellen also zwei Geraden mit gleicher Steigung ($\frac{3}{2}$) aber unterschiedlichen $y$-Achsenabschnitten ($\frac{1}{2}$ bzw. $1$) dar. Wir haben demnach zwei parallele nicht identische Geraden, die also keinen Schnittpunkt haben, woraus wir schließen können, dass das Gleichungssystem nicht lösbar ist.

Im Fall von e) müssen wir in der ersten Gleichung $2$ abziehen und durch $-4$ teilen sowie die zweite Gleichung durch $6$ teilen und erhalten:
$$\begin{array}{rrll}
\text{I: }&  y  &=  \frac{6}{4} x - \frac{2}{4} &= \frac{3}{2} x - \frac{1}{2}\\
\text{II: }& y  &= \frac{9}{6} x + \frac{3}{6} &= \frac{3}{2} x - \frac{1}{2}\\
\end{array}$$

Die beiden Gleichungen des Gleichungssystems stellen also die gleiche Gerade dar und jeder Punkt $(x|y)$ auf dieser Gerade erfüllt das Gleichungssystem.

Um die Gleichungen in f) auf Normalform zu bringen, müssen wir auf beiden Seiten der ersten Gleichung $2x$ abziehen und die Gleichung durch $3$ teilen, sowie die zweite Gleichung durch $2$ teilen und erhalten:
$$\begin{array}{rrl}
\text{I: }& y &= - \frac{2}{3}x + \frac{5}{3} \\
\text{II: }& y &= \frac{5}{2}x + 8
\end{array}$$

Die Gleichungen stellen also zwei Geraden mit unterschiedlicher Steigung, die nicht parallel sind und sich somit irgendwo schneiden müssen, dar, woraus wir schließen, dass das Gleichungssystem eine eindeutige Lösung (nämlich den Schnittpunkt der Geraden) hat.

### Lösungen der Gleichungssysteme (aus I)

a) Um das Gleichungssystem in a) zu lösen, bietet sich das Gleichsetzungsverfahren an. Auf den beiden linken Seiten steht nur $y$ und wir können die rechten Seiten gleichsetzen:
   $$\begin{array}{rll}
3x - 2 &= 6 - x \qquad &| +x+2 \\
4x &= 8 \qquad &| :4 \\
x &= 2
\end{array}$$

   Mit $x$ in II ergibt sich $y=6-2=4$.

b) Lösen wir anhand des Einsetzungsverfahrens, ersetzen also $y$ in der ersten Gleichung durch die rechte Seite der zweiten Gleichung:
   $$\begin{array}{rrll}
\text{II in I: }& 2 (-2x+1) &= 3x+2 & \\
& 2-4x &= 3x+2 \qquad & | +4x-2 \\
& 0 &= 7x \qquad & | :7 \\
& x &= 0 \qquad &
\end{array}$$

   Setzen wir $x=0$ ein in II, erhalten wir: $y=(-2)\cdot 0 +1 = 1$. Die Lösung zu b) ist also $(0|1)$.

c) Hier liegt es nahe, das Additionsverfahren zu verwenden (da die $x$'e in den beiden Gleichungen bereits mit gleichem Vorfaktor vorkommen):
   $$\begin{array}{rrll}
\text{II + I: }& 2 + y &= 3x - 3x + 5 + 4y = 5 + 4y & | -y-5 \\
& -3 &= 3y \qquad & | :3 \\
& y &= -1 \\
\\
\text{$y=-1$ in II: }& -1 &= 3x+5 & | -5 \\
& 3x &= -6 \qquad & | :3 \\
& x &= -2 
\end{array}$$

   Die Lösung für das Gleichungssystem aus c) ist also $(-2|-1)$.

\addtocounter{enumi}{2}

f) ist etwas widerborstig, da sich keines der drei Verfahren unmittelbar anwenden lässt. Für jedes der drei Verfahren (Gleichsetungs-, Einsetzungs- bzw. Additionsverfahren) müssen wir das Gleichungssystem erst etwas umstellen. Wir multiplizieren die erste Gleichung mit $-2$ und die zweite Gleichung mit $3$ und addieren dann die resultierenden Gleichungen:
   $$\begin{array}{rrll}
\text{3II - 2I: }& 3 \cdot 2y -2 (2x + 3y) &= 3 (5x+16) - 2 \cdot 5 & \\
& -4x &= 15x + 48 -10 = 15x + 38 \qquad & | -15x \\
& -19x &= 38 \qquad & | :(-19) \\
& x &= -2
\end{array}$$
   $$\begin{array}{rrll}
\text{$x=-2$ in II: }& 2y &= 5(-2)+16 = -10 +16 = 6 \qquad & | :2 \\
& y &= 3 
\end{array}$$

   Die Lösung für f) ist also $(-2|3)$.

## Lösungen zu II

1. Der Schnittpunkt von $g$ und $h$ ist $(2|-1)$.

2. $$ \begin{array}{rll}
\text{I :}& 3m + c &= 0\\
\text{II :}& -m + c &= 4\\
\text{I-II :}& 4m &= -4 \qquad | :4 \\
& m &= -1 \\
\text{$m=-1$ in I :}& 3(-1) + c &= 0 \qquad | +3 \\
& c &= 3 \\
\end{array}$$

   Die gesuchte Geradengleichung ist also $h: \, y = -x +3$.

nicht3. Es sei $x$ die Zahl der Säcke, die der Esel und $y$ die Anzahl der Säcke die das Maultier trägt.
   - Der erste Satz des Maultiers in Formeln übersetzt bedeutet: $2 (x-1) = y+1$. Denn, wenn der Esel dem Maultier einen Sack abgibt, trägt der Esel nur noch $x-1$, das Maultier aber $y+1$ Säcke. Und da uns das Maultier verrät, dass es dann doppelt soviel trüge wie der Esel, ist $2$ mal die Last des Esels, also $2(x-1)$ gleich der Last des Maultiers, also $y+1$.
   - Der zweite Satz de Maultiers, heißt analog $y-1 = x+1$

   Als Lösung des Gleichungssystems
   $$\begin{array}{rll}
   \text{I: }&  y+1 = 2(x - 1) \\
   \text{II: }& y-1 = x + 1
   \end{array}$$

   erhält man schließlich $(5,7)$.

4. $x$ sei die Zahl der Hasen, $y$ die Anzahl an Hühnern. Jedes Tier hat einen Kopf (hoffentlicht): $x+y  = 35$ und alle Hasen haben $4$, jedes Huhn aber nur $2$ Beine: $4x+2y = 94$. Die Lösung des Gleichungssystems verrät uns: es sind $12$ Hasen und $23$ Hühner.

\pagebreak
 
## Lösung zu III

a) Um Abstände anzugeben ist in der Mathematik der Buchstabe $d$ (für *d*istance) üblich, und $d(H,A)$ bezeichnet den Abstand der Punkte $H$ und $A$.
   $$\begin{array}{rll}
   d(H, A) &= 7 \mu s \cdot 300 \frac{m}{\mu s} = 2100 m \\
   d(H, B) &= 4 \mu s \cdot 300 \frac{m}{\mu s} = 1200 m \\
   d(H, C) &= 3 \mu s \cdot 300 \frac{m}{\mu s} = 900 m 
   \end{array}$$

   Im eingezeichneten Koordinatensystem entsprechen diese Entfernungen $14$, $8$ bzw $6$ Längeneinheiten (die Entfernung wird jeweils durch $150$ geteilt, da $1$ Längeneinheit im Koordinatensystem $150$ Metern entsprechen.

b) Den Abstand zweier Punkte $P(x_1|y_1)$ und $Q(x_2|y_2)$ im Koordinatensystem berechnet man wie folgt $d(P, Q)= \sqrt{ (x_1 - x_2)^2 + (y_1 - y_2)^2}$. Damit ergeben sich aus den Entfernungen des Handys zu den drei Funkmasten die folgenden Gleichungen:
   $$\begin{array}{rll}
   \text{I: }&  14 &= d(H, A) = \sqrt{(x-(-8,5))^2+(y-8,5)^2} = \sqrt{x^2+17x+\frac{17^2}{4} + y^2-17y+\frac{17^2}{4}} \\
   \text{II: }&  8  &= d(H, B) = \sqrt{(x-0)^2+(y-0)^2} = \sqrt{x^2+y^2} \\
   \text{III: }&  6  &= d(H, C) = \sqrt{(x-(0,5))^2+(y-(-11,5))^2} = \sqrt{x^2-x+\frac{1}{4} + y^2+23y+\frac{23^2}{4}}
   \end{array}$$

c) Wir haben also ein nicht-lineares Gleichungssystem (indem die Unbekannten auch quadriert vorkommen) von dem wir erstmal nicht wissen, wie es zu lösen ist. Aber glücklicherweise fallen die Quadrate weg, wenn wir die Gleichungen quadrieren und dann eine der Gleichungen von einer anderen abziehen, und es ergibt sich ein lineares Gleichungssystem (von dem wir wissen, wie es zu lösen ist):
   $$\begin{array}{rll}
   \text{I}^2-\text{II}^2\text{: }&  14^2 - 8^2 &= x^2+y^2+17x-17y+\frac{17^2}{2} -x^2 -y^2 = 17x-17y+\frac{289}{2} \\
   \text{III}^2-\text{II}^2\text{: }& 6^2 - 8^2 &= x^2+y^2-x+23y+\frac{23^2+1}{4} -x^2 - y^2 = -x+23y+\frac{530}{4} 
   \end{array}$$

   Rechnen wir noch die linken Seiten aus und fassen die Konstanten auf einer Seite zusammen erhalten wir das Gleichungssystem:
   $$\begin{array}{rrlr}
   \text{I: }&  17x - 17y &=& 15,5 \\
   \text{II: }& x - 23y &=& 104,5
   \end{array}$$
 
d) Die Lösung des Gleichungssystems ist ungefähr $(-3,6|-4,7)$ und das Handy befindet sich also irgendwo zwischen Schulgarten und dem Sportplatz.


