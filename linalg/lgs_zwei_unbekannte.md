---
title: "Auffrischen und Vertiefen"
author: [Klasse 11]
date: 04.04.2021
keywords: [Lineare Gleichungssysteme, Geraden, Determinante, Trilateration]
lang: de
header-center: "Lineare Gleichungssysteme" 
---

## I Starters

Sind die folgenden Gleichungssysteme lösbar? Falls ja, verwenden Sie ein Verfahren ihrer Wahl, um eine Lösung zu finden und anzugeben.

\Begin{multicols}{3}

a) $$\begin{array}{rll}
   \text{I: }&  y &= 3x  -2 \\
   \text{II: }& y &= 6 -x \\
   \end{array}$$

b) $$\begin{array}{rrl}
   \text{I: }&  2y &=  3x + 2 \\
   \text{II: }& y  &= -2x + 1 \\
   \end{array}$$

c) $$\begin{array}{rll}
   \text{I: }&  2 &=  -3x + 4y \\
   \text{II: }& y  &= 3x  + 5 \\
   \end{array}$$

d) $$\begin{array}{rll}
   \text{I: }&  -2 &= 6x - 4y \\
   \text{II: }& 2y &= 3x + 2 \\
   \end{array}$$

e) $$\begin{array}{rrl}
   \text{I: }&  2 - 4y  &= - 6 x \\
   \text{II: }& 6y      &= 9 x + 3 \\
   \end{array}$$

f) $$\begin{array}{rrl}
   \text{I: }&  2x + 3y &= 5 \\
   \text{II: }& 2y &= 5x + 16 \\
   \end{array}$$


\End{multicols}

Um die Lösbarkeit der Gleichungssysteme a priori beurteilen zu können, berechnen Sie jeweils die Determinante, oder bringen Sie alternativ die Gleichungen auf "Geradennormalform" ($y=mx+c$) und beurteilen Sie die Lösbarkeit anhand der Steigung und des $y$-Achsenabschnitts. (Sind die beiden Geraden paralell oder gar identisch?)


## II Hauptgang

1. Berechnen Sie die Schnittpunkte der Geraden $h: \, y=-2x+3$ und $g: \, y+2 = \frac{1}{2}x$.

2. Gegeben seien die Punkte $A(3|0)$ und $B(-1|4)$. Stellen Sie eine Gleichung für die Gerade auf, die durch die Punkte $A$ und $B$ geht. Gehen Sie dabei wie folgt vor:
   - Jede Geradengleichung kann auf die Normalform $h: y = mx+c$ gebracht werden.
   - Wir wollen also $m$ und $c$ so bestimmen, dass $A$ und $B$ auf der durch $h$ gegebenen Gerade liegen.
   - $A$ und $B$ liegen genau dann auf $h$, wenn die Punktproben ($x$- und $y$-Koordinate des Punktes werden in die Geradengleichung eingesetzt) von $A$ bzw. $B$ mit $h$ erfolgreich (= die Gleichung ist erfüllt) sind. D.h. wir suchen $m$ und $c$, sodass
     $$ \begin{array}{rll}
   0 &= m\cdot 3 + c \text{ und} \\
   4 &= m\cdot (-1) + c
   \end{array}$$
     gilt.
   - Finden Sie $m$ und $c$, indem Sie dieses Gleichungssystem lösen.
   
3. Nach Euklid: Ein Maultier und ein Esel trotteten die Straße entlang, beide mehrere gleichschwere Säcke tragend, und der Esel sich immerzu furchtbar seufzend beschwerte. Das Maultier aber hatte des Esels Gejammere satt und sagte: "Worüber beschwerst Du Dich? Wenn Du mir nur einen Sack gäbst, hätte ich doppelt so viel Säcke zu tragen wie Du! Und wenn ich Dir einen Sack gäbe, trügen wir die gleiche Last..."
Wieviele Säcke trugen Sie je?

4. Aus einem chinesischen Rechenbuch um 2600 v. Chr.: In einem Käfig sind Hasen und Hühner eingesperrt. Die Tiere haben zusammen 35 Köpfe und 94 Füße. Wie viele Hasen und wie viele Hühner sind es? 

 
## III Dessert (Trilateration)

Wie funktionieren eigentlich GPS oder das Orten von Handys?

- Indem man die Laufzeit von Funksignalen zwischen Sender und Empfänger misst (beide kennen die Uhrzeit genau; der Sender schickt einen Zeitstempel; der Empfänger erhält dann die Laufzeit aus der Differenz von Ankunftszeit und Zeitstempel), kann man auf deren Entfernung voneinander zurückschließen.
- Funksignale bewegen sich annähernd mit Lichtgeschwindigkeit, also mit fast $300000$ km/s; oder in einer für unseren Zweck besser geeigneten Einheit: $300 m/\mu s$. ($1000000$ Mikrosekunden sind $1$ Sekunde).

\Begin{multicols}{2}

- Laufzeit mal Geschwindigkeit ergibt die Entfernung. Braucht das Signal zum Beispiel $4$ Mikrosekunden, so beträgt der Abstand von Sender und Empfänger ungefähr $4 \mu s \cdot 300 m/\mu s = 1200 m$


- Die Entfernung von etwas (in einer Ebene oder auf einer Kugeloberfläche zum Beispiel) zu $3$ gegebenen Punkten, bestimmt die Position des etwas, wie die Zeichnung rechts illustiert. Diese Art der Positionsbestimmung nennt man **Trilateration**.

![](Trilateration.png){width=250px}

\End{multicols}


Der folgende Kartenausschnitt der Bundesnetzagentur zeigt das Gebiet südlich von Bonlanden und alle Funkmasten in der Umgebung. Die Koordinatenachsen wurden nachträglich und willkürlich aber maßstabsgetreu hinzugefügt. Die Funkmasten $27010339$, $27012252$ und $750119$ haben in diesem Koordinatensystem ungefähr die Koordinaten $A(-8,5|8,5)$, $B(0|0)$ sowie $C(0,5|-11,5)$. 

   ![](bonlanden_funkmasten.png){height=500px}

Die Laufzeiten der Funksignale zwischen einem Handy und den drei Funkmasten $27010339$, $27012252$ und $750119$ seien $7 \mu s$, $3 \mu s$ bzw. $4 \mu s$. Wo befindet sich das Handy?

a) Berechnen Sie aus den Signallaufzeiten die Entfernungen der Funkmasten zum Handy.
b) Der Standort des Handys werde mit $H(x|y)$ bezeichnet. Wie würde man den Abstand des Handys zu den Funkmasten berechnen, wenn man die Koordinaten des Handys kennen würde? (In Ihrem Heft zur analytischen Geometrie finden Sie die Formel zur Berechnung des Abstands zweier Punkte.) Mit den Entfernungen aus a) ergeben sich daraus drei Gleichungen, stellen Sie diese auf.
c) Quadrieren Sie die Gleichungen aus b) und ziehen Sie dann von der ersten und der dritten Gleichung jeweils die zweite ab. Sie erhalten ein lineares Gleichungssystem.
d) Lösen Sie das folgende lineare Gleichungssystem, dass sich aus c) ergeben hat, um die Koordinaten des Handys zu erhalten:
   $$\begin{array}{rrlr}
   \text{I: }&  17x - 17y &=& 15,5 \\
   \text{II: }& x - 23y &=& 104,5
   \end{array}$$

   

