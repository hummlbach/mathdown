---
title: "Gruppenarbeit"
author: [Klasse 11]
date: 18.03.2021
keywords: [Additionsverfahren, Einsetzungsverfahren, Gleichsetzungsverfahren, Matrizen]
lang: de
header-center: "Lineare Gleichungssysteme" 
---


## I Einsetzungsverfahren und Probe

1. Recherchieren Sie, wie man mit dem Einsetzungsverfahren lineare Gleichungssysteme löst. (siehe z.B. Wikipedia)

2. Erklären und illustrieren Sie das Einsetzungsverfahren anhand des folgenden Beispiels:

   $$
\begin{array}{lr}
3x-6y &= -9 \\
-2x+3y &= 7
\end{array}
$$

3. Denken Sie sich ein lineares Gleichungssystem aus und lassen Sie es von Ihren Mitschülern mit dem Einsetzungsverfahren lösen.

4. Führen Sie die "Probe" durch und vor. Erläutern Sie also, wie man sicher gehen kann, dass die gefundene vermeintliche Lösung das Gleichungssystem tatsächlich löst.


## II Gleichsetzungsverfahren und Lösungsmenge

1. Recherchieren Sie, wie man mit dem Gleichsetzungsverfahren lineare Gleichungssysteme löst. 

2. Lösen Sie die folgenden drei linearen Gleichungssysteme. Was sind jeweils die Lösungsmengen?

   \Begin{multicols}{3}
   a) $$
\begin{array}{lr}
2x-y &= -3 \\
9x+3y &= 6
\end{array}
$$

   b) $$
\begin{array}{lr}
3x-y &= -3 \\
-9x+3y &= 6
\end{array}
$$

   c) $$
\begin{array}{lr}
3x-y &= -3 \\
-9x+3y &= 9
\end{array}
$$

   \End{multicols}


3. Zu einem Gleichungssystem mit Unbekannten $x$ und $y$, sowie Koeffizienten $a$, $b$, $c$ und $d$ wie folgt:
   $$
\begin{array}{lr}
ax+by &= s \\
cx+dy &= t
\end{array}
$$
   betrachte man die Zahl $ad-cb$ (grob gesprochen die sogenannte *Determinante* des Gleichungssystems). Sehen Sie einen Zusammenhang zur Lösbarkeit bzw. der Lösungsmenge des Gleichsungssystems?

4. Illustrieren und erklären Sie Ihren Mitschülern das Geichsetzungsverfahren und bringen Sie Ihnen näher, wie man erkennen kann, wieviele Lösungen ein lineares Gleichungssystem (mit zwei Gleichungen und zwei Unbekannten) haben kann und wie man erkennt, ob es lösbar ist oder nicht.


## III Additionsverfahren und gaußsches Eliminationsverfahren

1. Recherchieren Sie, wie man mit dem Additionsverfahren lineare Gleichungssysteme löst.

2. Lösen Sie das folgende lineare Gleichungssystem und erläutern Sie Ihren Mitschülern damit das Additionsverfahren.

   $$
\begin{array}{ll}
4y &= 2x-6 \\
9y+3x &= 6
\end{array}
$$

3. Lösen Sie das Gleichungssystem aus 2. nochmal in den folgenden Schritten:
   a) Bringen Sie in beiden Gleichungen die Konstanten auf die rechte und die $x$ sowie die $y$ auf die linke Seite, sodass die $x$ "an erster Stelle" und die $y$ "an zweiter Stelle stehen".
   b) Teilen Sie die erste Zeile durch den Vorfaktor von $x$ (in dieser Zeile)
   c) Addieren bzw. Subtrahieren Sie die erste Gleichung zu/von der zweiten Zeile so ("so oft"), dass die $x$ in der zweiten Zeile "wegfallen".
   d) Teilen Sie die zweite Gleichung durch den Vorfaktor von $y$.
   e) Setzen Sie den Wert von $y$, in die erste Gleichung ein und lösen Sie nach $x$ auf.

4. Recherchieren Sie, was das *gaußsche Eliminationsverfahren* ist und worin seine Vorteile liegen. Versuchen Sie das ihren Mitschülern näher zu bringen. (Wenigstens ein Beispiel vorrechnen.)

## IV Matrizen

1. Lösen Sie das folgende lineare Gleichungssystem mit dem Additionsverfahren. Bringen Sie vorher, in allen Gleichungen, die Unbekannten in die gleiche Reihenfolge und auf die linke Seite, sowie die Konstante auf die rechte Seite.

   $$
\begin{array}{ll}
3x + 9 &= 6y \\
3y &= 7 + 2x
\end{array}
$$

2. Schreiben Sie den Lösungsweg aus 1. nocheinmal auf (ab dem Punkt, an dem die Terme sortiert sind), aber entfernen Sie alle "=" sowie alle Unbekannten (also hier $x$ und $y$), lassen Sie Platz vor den Vorzeichen und schreiben Sie die $+$ und $-$ direkt vor die nachfolgenden Zahlen.

3. Recherchieren Sie was *Matrizen* sind und was diese mit linearen Gleichungssystemen zu tun haben. Lösen Sie ein weiteres Gleichungssystem ohne Unbekannte und Istgleiche "mitzuschleppen", indem Sie Matrizen verwenden.

4. Versuchen Sie die Erkenntnis aus 2. und 3. Ihren Mitschülern näher zu bringen. (Sie könnten ihre Mitschüler z.B. anleiten 1. und 2. durchzuführen.)


## V Drei Gleichungen und drei Unbekannte

Versuchen Sie das folgende Gleichungssystem zu lösen:

$$
\begin{array}{ll}
3x + 2y - z &= 1 \\
2x - 2y + 4z &= -2 \\
-x + \frac{1}{2}y - z &= 0
\end{array}
$$

1. Versuchen Sie das Gleichungssystem zu lösen.

2. Lösen Sie das Gleichungssystem, wie folgt:
   a) Lösen Sie zuerst eine Gleichung nach einer Unbekannten auf (Unbekannte steht alleine auf der linken Seite und der Rest steht auf der rechten Seite).
   b) Ersetzen Sie die Unbekannte in den beiden anderen Gleichungen durch die rechte Seite aus a).
   c) Lösen Sie das aus b) resultierende Gleichungssystem mit $2$ Gleichungen und $2$ Variabeln.
   d) Setzen Sie die Lösungen aus c) in eine der Ausgangsgleichungen ein, um die Lösung für die dritte Unbekannte zu erhalten.

3. Lösen Sie das Gleichungsystem, indem Sie
   a) Eine Zeile so mit einer Konstante multiplizieren, sodass vor $x$ eine $1$ steht;
   b) Addieren oder Subtrahieren Sie diese Zeile so zu den anderen beiden hinzu, dass in den anderen beiden Zeilen die $x$ verschwinden;
   c) Das aus b) resultierende Gleichungssystem mit $2$ Gleichungen und $2$ Variabeln lösen;
   d) Sie die Lösungen aus c) in eine der Ausgangsgleichungen einsetzen, um die Lösung für die dritte Unbekannte zu erhalten.

4. Präsentieren Sie die Lösung eines Gleichungssystems mit $3$ Gleichungen und $3$ Unbekannten und leiten Sie Klasse bei der Lösung eines anderen Gleichungssystems mit $3$ Gleichungen und $3$ Unbekannten an.

