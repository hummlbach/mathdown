---
title: "Urnenmodell"
author: [Klasse 9]
date: "12.11.2020"
keywords: [Kombinatorik, Urnenmodell]
lang: de
header-center: "Kombinatorik" 
---

## Urnenmodell

- Kombinatorische Situationen bzw. Zufallsexperimente können häufig als (mehrmaliges) Ziehen von Kugeln aus einer Urne modelliert werden.

- Beispiele zur Übersetzung in "Urnenexperimente":
  - Permutation: $8$ Bücher ins Bücherregal einräumen (wieviele Anordnungen?)

    - jede Kugel repräsentiert ein Buch - es sind also $8$ Kugeln in der Urne
    - wir ziehen für jeden Platz einmal, also insgesamt $8$ mal (alle Kugeln, bis alle Bücher eingeräumt sind)
    - es wird ohne Zurücklegen gezogen - jedes Buch kann nur einmal ins Regal gestellt werden
    - die Reihenfolge, in der gezogen wird, stellt die Anordnung der Bücher im Regal dar, wird also beachtet
    - Formel: \newline \newline

  - Variation ohne Wiederholung: $10$ Personen auf $5$ Stühle setzen

    - die Kugeln stellen die Personen dar - es liegen also $10$ Kugeln in der Urne
    - wir ziehen $5$ mal (für jeden Stuhl einmal)
    - es wird ohne Zurücklegen gezogen, da jede Person nur auf einem Stuhl sitzen kann
    - die Reihenfolge wird beachtet, da sie festlegt auf welchen Stuhl sich die gezogene Person setzt
    - Bei umgekehrt $10$ Stühlen und $5$ Personen würden die Kugeln die Stühle darstellen und wir zehen für jede Person sozusagen die Sitznummer
    - Formel: \newline

  - Kombination ohne Wiederholung: $5$er Team aus $20$ Personen bilden

    - $20$ Kugeln in der Urne, jede steht für eine Person
    - es wird wieder $5$ mal gezogen
    - ohne Zurücklegen, da eine Person nicht zweimal im Team sein kann
    - die Reihenfolge wird nicht berücksichtigt, da es egal in welcher Reihenfolge die Teammitglieder ausgewählt werden
    - Formel: \newline \newline

\pagebreak

  - Variation mit Wiederholung: $4$ stellige PIN

    - in der Urne sind $10$ Kugeln für die Zahlen $0$ bis $9$
    - wir ziehen $4$ mal
    - mit Zurücklegen, da die Zahl an der ersten Stelle auch an den weiteren Stellen wieder stehen kann
    - die Reihenfolge wird berücksichtigt, da es bei PINs einen Unterschied macht, an welcher Stelle eine Zahl steht
    - Formel: \newline \newline

  - (Haben wir nicht gemacht) $5$ gleiche Münzen werden (gleichzeitig) geworfen

    - in der Urne sind zwei Kugeln, eine für "Kopf" und eine für "Zahl"
    - es wird $5$ mal gezogen (da $5$ Münzen)
    - mit Zurücklegen, da bei jedem Münzwurf wieder "Kopf" oder "Zahl" fallen können
    - die Reihenfolge wird nicht berücksichtigt, da die Münzen simultan geworfen werden und nicht unterscheidbar sind

- Die folgenden Fragen können bei der Modellierung als Urnenexperiment "leiten":

  - wieviele Möglichkeiten ($n$) gibts beim (ersten) Ziehen? (Also die Anzahl der Kugeln in der Urne beim ersten Ziehen.)
  - wie oft wird gezogen ($k$)?
  - kommt es auf die Reihenfolge an?
  - werden die Kugeln zurückgelegt - also gibt es bei jedem Ziehen wieder alle Möglichkeiten, oder fällt bei jedem weiteren Ziehen eine Möglichkeit weg?

- Je ein Ergebnis eines $k$-maligen Ziehens aus $n$ Dingen, kann als $k$-Tupel mit Einträgen aus diesen $n$ Dingen dargestellt werden. ($k$-Tupel heißt man schreibt die $k$ Dinge durch Kommas getrennt nebeneinander.)

  - wenn die Reihenfolge nicht berücksichtigt wird, werden nur sortierte Tupel verwendet
  - wenn zurückgelegt wird, können sich die Einträge in den Tupeln wiederholen
  - wenn nicht zurückgelegt wird, sind die Einträge in den Tupeln paarweise verschieden


