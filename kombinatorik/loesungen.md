---
title: "Lösungen"
author: [Klasse 9]
date: "14.11.2020"
keywords: [Kombinatorik]
lang: de
header-center: "Kombinatorik" 
---

# Lösungen zu Blatt 1
## Starters

1. a) $P(3) = 3! = 3\cdot 2 \cdot 1 = 6$
   b) $P(4) = 4! = 4 \cdot 3 \cdot 2 \cdot = 24$

2. $P(4) = 4! = 4 \cdot 3 \cdot 2 \cdot 1 = 24$
3. $P(5) = 5! = 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1 = 120$
4. $P(5) = 5! = 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1 = 120$

## Hauptgang

1. Man hat $6$ Möglichkeiten den Knopf zu wählen, den man als erstes zuknöpft. Dann noch $5$ Möglichkeiten welchen Knopf man als zweites zuknöpft... Also: $$P(6) = 6! = 6\cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1 = 720$$

2. Für den Läufer der als erstes ins Ziel läuft gibt es $8$ Möglichkeiten. Für den zweiten noch $7$, da schon einer im Ziel ist. Für den dritten noch $6$ usw.: $P(8) = 8! = 40320$

\pagebreak

# Lösungen zu Blatt 2
## Starters

1. Wenn sich Ann und Bob ein Zugabteil mit
   - $4$ Plätzen setzen, kann Ann als erste (Ladys first) aus $4$ Plätzen wählen. Egal wo Ann sich hingesetzt hat, Bob hat jeweils noch $3$ Sitzmöglichkeiten: $4\cdot 3 = 12$
   - Bei $5$ Plätzen: $5 \cdot 4 = 20$
   - Bei $6$ Plätzen: $6 \cdot 5 = 30$

2. a) Für den ersten Streifen kann man aus $3$ Farben wählen, für den zweiten noch $2$ und für den dritten bleibt nur noch eine Farbe übrig: $P(3) = 3! = 6$
   b) Für den ersten Streifen kann man aus $4$ Farben wählen. Zu jeder Farbwahl für den ersten Streifen, gibt es $3$ Möglichkeiten für den zweiten Streifen. Zu jeder Farbkombination für die ersten zwei Streifen gibt es nochmal $2$ Farben für den dritten Streifen:
      $$V_3(4) = \frac{4!}{(4-3)!} = \frac{4!}{1!} = 4! = 4\cdot 3\cdot 2 \cdot 1 = 24$$
   c) $$V_3(5) = \frac{5!}{(5-3)!} = \frac{5!}{2!} = 5\cdot 4\cdot 3 = 60$$

3. a) $V_2(5) = \frac{5!}{(5-2)!} = \frac{5!}{3!} = 5\cdot 4 = 20$
   b) $V_3(5) = \frac{5!}{(5-3)!} = \frac{5!}{2!} = 5\cdot 4 \cdot 3 = 60$
   c) $V_4(5) = \frac{5!}{(5-4)!} = \frac{5!}{1!} = 5\cdot 4 \cdot 3 \cdot 2 = 120$

## Hauptgang

1. a) $V_4(6) = \frac{6!}{(6-4)!} = 6\cdot 5 \cdot 4 \cdot 3 = 360$
   b) $V_4(7) = \frac{7!}{(7-4)!} = 7\cdot 6\cdot 5 \cdot 4 = 840$

2. a) $P(8) = 8! = 40320$
   b) $8$ Läufer können als erstes ins Ziel laufen. Für jeden Sieger gibt es $7$, die auf den zweiten Platz kommen könnten und dann für jede Belegung der Plätze eins und zwei, gibt es noch $6$ Möglichkeiten für den Drittplatzierten.
      $$V_3(8) = \frac{8!}{(8-3)!} = \frac{8!}{5!} = 8\cdot 7\cdot 6 = 336$$

3. $V_{11}(36) = \frac{36!}{(36-11)!} = \frac{36!}{25!} = \dots$

\pagebreak

# Lösungen zu Blatt 3

## Starters

2. a) $\hat{V}_4(2) = 2^4 = 16$
   b) $\hat{V}_4(3) = 3^4 = 3\cdot 3 \cdot 3 \cdot 3 = 81$
   c) $\hat{V}_4(4) = 4^4 = 4\cdot 4 \cdot 4 \cdot 4 = 256$
   d) $\hat{V}_4(6) = 6^4 = 6\cdot 6 \cdot 6 \cdot 6 = 1296$

3. $\hat{V}_6(2) = 2^6 = 64$
4. a) $\hat{V}_2(4) = 4^2 = 4\cdot 4 = 16$
   b) $\hat{V}_3(4) = 4^3 = 4\cdot 4\cdot 4 = 64$
   c) $\hat{V}_4(4) = 4^4 = 4\cdot 4\cdot 4 \cdot 4= 256$

## Hauptgang

1. Es gibt $985824$ Entsperrmuster.
   a) Für eine $4$-stellige PIN gibt es $10000$ Möglichkeiten: $\hat{V}_4(10) = 10^4$. Für jede weitere Stelle wird die Anzahl der Möglichkeiten verzehnfacht. Es gibt also mehr $6$-stellige PINs als Entsperrmuster: $\hat{V}_6(10) = 10^6 = 1000000$.
   b) Bei einem Passwort reichen $3$ Stellen: $\hat{V}_3(100) = 100^3 = 1000000$

2. a) $\hat{V}_8(2) = 2^8 = 256$
   b) $\hat{V}_32(2) = 2^{32} = 4294967296$

\pagebreak

# Lösungen zu Blatt 4

## Starters

1. Es gibt $12$ Möglichkeiten für die Stunde, $60$ Möglichkeiten für die Minuten und $60$ Möglichkeiten für die Sekunden. Produktregel: $12\cdot 60 \cdot 60 = 12 \cdot 3600 = 36000 + 7200 = 43200$
2. Für die erste Stelle, gibt es $9$ Möglichkeiten ($0$ kann nicht an der ersten Stelle stehen). Für die zweite und dritte Stelle je $10$. Produktregel: $9\cdot 10 \cdot 10 = 900$ \addtocounter{enumi}{1}
4. $4\cdot 3 \cdot 3 = 36$ oder wenn man davon ausgeht, dass auch unterschiedliche Schuhe getragen werden: $4\cdot 3\cdot 3 \cdot 3 = 36\cdot 3 = 108$
5. $P(6)\cdot P(5) = 6!\cdot 5! = 720 \cdot 120 = 72000 + 14400 = 86400$

## Hauptgang

1. a) Wenn sich Robert als erster setzt, hat er $6$ Plätze zur Auswahl. Anna hat anschließend nur $3$ Möglichkeiten sich zu setzen, wenn sie nicht neben Robert sitzen mag. Die $4$ anderen haben dann noch $P(4) = 4! = 24$ Möglichkeiten sich zu setzen. Insgesamt: $6\cdot 3\cdot 24 = 144\cdot 3 = 432$
   b) Wenn sich Eva als erste setzt, hat sie $6$ Plätze zur Auswahl. Daniel hat dann $2$ Plätze um neben Eva zu setzen und die verbleibenden $4$ haben wieder $4!$ Möglichkeiten die verbleibenden Plätze zu besetzen: $6\cdot 2\cdot 24 = 288$
   c) Setzt sich Eva als erste, so hat sie $6$ Plätze zur Auswahl und Daniel $2$ Möglichkeiten sich neben Eva zu setzen. Setzt sich Robert nun neben Daniel oder Eva ($2$ Möglichkeiten), dann hat Anna anschließend $2$ Plätze zur Auswahl, die nicht neben Robert sind. Christina kann dann noch zwischen $2$ Plätzen wählen und Florian setzt sich auf den letzten freien Platz. Das macht $6\cdot 2\cdot 2 \cdot 2 \cdot 2 \cdot 1 = 96$ Möglichkeiten. Setzt sich Robert aber nicht neben Daniel oder Eva (wieder $2$ Möglichkeiten), bleibt Anna nur $1$ Platz der nicht neben Robert ist. Für Christian und Florian verbleiben wieder $2$ Sitzmöglichkeiten: $6\cdot 2\cdot 2 \cdot 1 \cdot 2 = 48$. Wir müssen die Möglichkeiten beider Fälle addieren: $96+48 = 144$
   d) Wenn es nur darauf ankommt wer der linke und wer der rechte Sitznachbar vom jeweils anderen ist. Sind jeweils $6$ der $6!$ Sitzordnungen in diesem Sinne gleich (eine Drehung der Sitzordnung erhält die Sitznachbarn und man kann $6$ mal drehen): $\frac{6!}{6} = 5! = 120$ \addtocounter{enumi}{1}

3. Es gibt $3\cdot 4!$ Wörter, die mit einem Buchstaben anfangen der alphabetisch vor O liegt (A, E und N). Außerdem gibt es $1 \cdot 3 \cdot 3!$ Wörter die mit O beginnen, deren zweiter Buchstaben aber alphabetisch vor Z kommt (O steht schon an erster Stelle in diesem Fall). Außerdem gibt es noch $1 \cdot 1 \cdot 1 \cdot 2!$ Wörter die mit OZ beginnen, deren dritter Buchstabe aber im Alphabet vor $E$ kommt, nämlich "Ozaen" und "Ozane". Es liegen also $3 \cdot 4! +3\cdot 3! + 2 = 72 + 18 +2 = 92$ Wörter lexikographisch vor dem Wort "Ozean" und "Ozean" ist das $93$. Wort.

\pagebreak

# Lösungen zu Blatt 6

## Starters

1. (In der Angabe fehlt, dass $3$ verschiedene Sorten bestellt werden.)
   a) $V_3(5) = \frac{5!}{(5-3)!} = \frac{5!}{2!} = 5 \cdot 4 \cdot 3 = 60$
   b) Es kommt nur auf die Wahl der Sorten, nicht auf die Reihenfolge an. Für jede Sorten-Wahl gibt es $3!$ Reihenfolgen. D.h. wir müssen das Ergebnis von a) noch durch $3!$ teilen, um die Reihenfolge "los zu werden":
      $$K_3(5) = \frac{5!}{(5-3)!\cdot 3!} = \frac{5!}{2!\cdot 3!} = \frac{5\cdot 4}{2} = 10$$ \addtocounter{enumi}{1}

3. a) $K_2(6) = \binom{6}{2} = \frac{6!}{4!\cdot 2!} = 15$
   b) $K_3(6) = \binom{6}{3} = \frac{6!}{3!\cdot 3!} = 10$
   b) $K_4(6) = \binom{6}{4} = \frac{6!}{2!\cdot 4!} = 15$

## Hauptgang

1. $K_5(20) = \binom{20}{5} = \frac{20!}{15!\cdot 5!} = \frac{20 \cdot 19 \cdot 18 \cdot 17 \cdot 16}{5\cdot 4 \cdot 3 \cdot 2 \cdot 1} = 19 \cdot 3 \cdot 17 \cdot 16 = 15504$
2. $K_5(32) = \binom{32}{5} = \frac{32\cdot 31 \cdot 30 \cdot 29 \cdot 28}{5\cdot 4 \cdot 3 \cdot 2 \cdot 1} = 201376$
3. $K_2(6) = \binom{6}{2} = \frac{6!}{4!\cdot 2!} = 15$
4. a) $K_3(6) = 10$
   b) $K_3(12) = \binom{12}{3} = \frac{12!}{9!\cdot 3!} = \frac{12\cdot 11\cdot 10}{3\cdot 2\cdot 1} = 2\cdot 11 \cdot 10 = 220$
5. $K_6(49) = \binom{49}{6} = \frac{49!}{43!\cdot 6!} = \frac{49\cdot 48\cdot 47\cdot 46\cdot 45 \cdot 44}{6\cdot 5 \cdot 4 \cdot 3 \cdot 2 \cdot 1} = \frac{49\cdot 47\cdot 46\cdot 3 \cdot 44} = 13983816$

## Dessert

Der Taxifahrer fährt $12$ mal von Kreuzung zu Kreuzung. $6$ Strecken muss er nach Süden fahren. Welche $6$ der $12$ Strecken er nach Süden fährt kann er wählen. Er hat also $K_6(12) = \binom{12}{6} = \frac{12!}{6!\cdot 6!} =
\frac{12\cdot 11 \cdot 10 \cdot 9 \cdot 8 \cdot 7}{6\cdot 5 \cdot 4 \cdot 3 \cdot 2\cdot 1} =
12\cdot 11 \cdot 7= 924$
