---
title: "Übungsblatt 4"
author: [Klasse 9]
date: "05.11.2020"
keywords: [Kombinatorik, Produktregel]
lang: de
header-center: "Kombinatorik" 
---

## Starters

1. Wieviele Uhrzeiten gibt es?
2. Wieviele $3$-stellige Zahlen gibt es im Dezimalsystem?
3. Wieviele $4$-stellige, gerade Zahlen gibt es im Dezimalsystem?
4. Herr R. aus L. besitzt $4$ Oberteile, $3$ Hosen und $3$ Paar Schuhe. In wievielen Outfits kann er erscheinen?
5. Auf wieviele Arten können sich $6$ Damen und $5$ Herren für ein Gruppenfoto platzieren, wenn die Damen in der ersten Reihe sitzen und die Männer in der zweiten Reihe stehen sollen?

## Hauptgang

1. Anna, Robert, Christina, Daniel, Eva und Florian setzen sich an einen runden Tisch (mit $6$ Plätzen). Wieviele Sitzordnungen gibt es, wenn
   a) Anna nicht neben Robert sitzen mag?
   b) Daniel unbedingt neben Eva sitzen mag?
   c) Anna nicht neben Robert und Daniel unbedingt neben Eva sitzen mag? (hässlich)
   d) es nur darauf ankommt, wer neben wem sitzt und nicht darauf, wer auf welchem Stuhl sitzt?
2. Wie ändern sich die Situationen in 1a), 1b) und 1c), wenn sich die $6$ in eine Reihe setzen?
3. Das wievielte Wort is "Ozean" in der Reihe der lexikographisch geordneten Liste seiner Permutationen (also in der alphabetisch sortierten "Wörter" aus den Buchstaben A, E, O, N und Z)?

## Dessert (Stack-folding-challenge)

1. Falten Sie dieses DIN A4 Blatt entlang der Linien auf der Rückseite, sodass die Buchstaben auf der Rückseite in alphabetischer Reihenfolge aufeinander zu liegen kommen.
2. Beschriften Sie ein anderes Blatt analog, wie folgt, und falten Sie es wieder so, dass die Buchstaben alphabetisch hintereinander bzw. übereinander liegen.
   a)
    ```A H G D``` \newline
    ```B C F E```
   b)
    ```A H B G``` \newline
    ```D E C F```
3. Wieviele Möglichkeiten gibt es ein Blatt, wie in 1. und 2. zu falten? (Harte Nuss)
