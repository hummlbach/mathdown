---
title: "Übungsblatt 7"
author: [Klasse 9]
date: "12.11.2020"
keywords: [Kombinatorik, Laplace Wahrscheinlichkeitsraum]
lang: de
header-center: "Kombinatorik" 
---

## I Starters

1. Ein (faires) Glücksrad mit $10$ Feldern, einem roten, $3$ gelben und $6$ blauen wird gedreht. Wie groß ist die Wahrscheinlichkeit dafür, dass das Glücksrad auf einem der folgenden Felder stehen bleibt?
   a) Gelb
   b) Blau
   c) Gelb oder Rot

2. Würfeln
   a) Wieviele Ausgänge/Ergebnisse gibt es beim Werfen von
      (i) $2$
      (ii) $3$
      (iii) $5$
     
      (unterscheidbaren) Würfeln?
   b) Wie groß ist die Wahrscheinlichkeit beim Werfen mit $2$ (unterscheidbaren) Würfeln eine $6$ zu würfeln?
   c) Wie groß ist die Wahrscheinlichkeit beim Werfen mit $2$ (unterscheidbaren) Würfeln einen Pasch zu würfeln?

## II Hauptgang

1. Wie groß ist die Wahrscheinlichkeit, beim Würfeln mit $5$ Würfeln:
   a) Eine große Straße zu würfeln?
   b) Einen 3er Pasch zu würfeln?

2. Beim Mastermind mit $8$ Farben ohne Mehrfachverwendung von Farben: Wie hoch ist die Wahrscheinlichkeit auf Anhieb die richtigen Farben zu erraten (nicht unbedingt die richtigen Plätze)?
3. In einer Urne seien $9$ Kugeln, nummeriert von $1$ bis $9$. Wie groß ist die Wahrscheinlichkeit $3$ aus $9$ Kugeln in aufsteigender Nummerierung zu ziehen?
4. Wie Wahrscheinlich ist es im Lotto ("6 aus 49") wenigstens $3$ Richtige zu haben (bei einem Tipp)?
5. Wie hoch ist die Wahrscheinlichkeit, dass von $35$ Personen wenigstens $2$ am gleichen Tag Geburtstag haben?

\pagebreak


## III Dessert

1. In der vorletzten Runde eines Kniffelspiels hat man noch den $3$er-Pasch und die kleine Straße zu würfeln. Man hat bereits zwei mal gewürfelt und die Würfel zeigen eine $2$, eine $3$, eine $4$ sowie zwei $6$er. Wie groß sind die Chancen in dieser Runde nichts zu streichen, bei folgenden Strategien:
    (i) Die Würfel, die eine $6$ zeigen, nochmal werfen.
    (ii) Die Würfel, die keine $6$ zeigen, noch einmal zu werfen.
    (iii) Alle Würfel noch einmal zu werfen.

2. Eine Münze wird so lange geworfen bis sie auf Zahl fällt.
   a) Wie sieht der Ergebnisraum dieses Experiments aus?
   b) Wie groß ist die Wahrscheinlichkeit dafür, dass nach $10$ Würfen immer noch nicht Zahl "gefallen ist"?


## Tipps

- I 1. a) Variation mit Wiederholung
- I 1. b) $1 - \text{"Wahrscheinlickeit des Gegenereignisses"}$

- II 1. a) Variation ohne Wiederholung
- II 2. Ergebnisraum: Variation ohne Wiederholung, Ereignis: Permutation ohne Wiederholung 
- II 3. Ergebnisraum: Variation ohne Wiederholung, Ereignis: Kombination ohne Wiederholung
- II 4. Kombination ohne Wiederholung: $3$ Kugeln werden aus den $6$ richtigen gewählt. Die anderen $3$ Kugeln werden aus allen anderen $46$ Kugeln (außer den ersten $3$ - die sind ja schon gezogen) gewählt. 
- II 5. Ergebnisraum: Variation mit Wiederholung, Ereignis: Variation ohne Wiederholung, wie bei I 1. b) übers Gegenereignis
