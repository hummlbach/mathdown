---
title: "Übungsblatt 2"
author: [Klasse 9]
date: "03.11.2020"
keywords: [Kombinatorik, Permutation, Variation]
lang: de
header-center: "Kombinatorik" 
---

# Starters

1. Auf wieviele verschiedene Arten können sich Ann und Bob in ein Zugabteil mit $4$, $5$ bzw. $6$ Plätzen setzen?
2. Wieviele Flaggen aus $3$ verschiedenfarbigen, waagerechten, gleichdicken Streifen, lassen sich mit den folgenden Farben bilden?
   a) Rot, Blau und Weiß
   b) Rot, Blau, Weiß und Grün
   c) Rot, Blau, Weiß, Gelb und Schwarz
3. In einer Urne befinden sich $5$ Kugeln: $1$ rote, $1$ blaue, $1$ gelbe, $1$ schwarze und $1$ weiße. Wieviele verschiedene Möglichkeiten gibt es, die Kugeln zu ziehen? Wenn
   a) $2$
   b) $3$
   c) $4$

   Kugeln nacheinander (also unter Beachtung der Reihenfolge), ohne Zurücklegen gezogen werden.

# Hauptgang

1. Das Spiel Mastermind wird zu zweit gespielt. Während sich der eine Spieler einen vierstelligen Farbcode ausdenkt, muss der andere diesen Farbcode geschickt erraten. Normalerweise stehen $6$ Farben für den Farbcode zur Verfügung.
   a) Wieviele Farbcodes gibt es, wenn in jedem Farbcode jede Farbe höchstens $1$ mal vorkommen darf?
   b) Wieviele Codes sind möglich, wenn Farben (wieder) nicht mehrfach verwendet, aber ein Platz leer gelassen werden darf?
2. Bei einem 400-Meter-Lauf starten $8$ Läufer.
   a) Wieviele verschiedene Zieleinläufe (Platzierungen) gibt es?
   b) Wieviele verschiedene "Treppchen" (Belegungen der ersten $3$ Plätze) gibt es?
3. Angenommen $11$ Schüler der 9. Klasse fehlen. Wieviele Sitzordnungen gibt es dann? (Es gibt nach wie vor 36 Plätze!)
4. Auf wieviele verschiedene Weisen kann man $k$ von $n$ Dingen auswählen und anordnen?

# Dessert

Eine beliebte Methode, um Smartphones vor unbefugtem Zugriff zu schützen, sind "Entsperrmuster". Dabei werden (z.B.) $9$ Punkte auf dem Bildschirm angezeigt. Der Benutzer legt fest, welche (in der Regel mindestens $4$) der Punkte in welcher Reihenfolge verbunden werden müssen, um das Handy zu entsperren. Wieviele Entsperrmuster gibt es

a) wenn $4$ Punkte für das Muster verwendet werden?
b) wenn $8$ Punkte für das Muster verwendet werden?
c) insgesamt? (Die verwendete Zahl der Punkte also unbekannt bzw. beliebig ist.)

