---
title: "Übungsblatt 1"
author: [Klasse 9]
date: "02.11.2020"
keywords: [Kombinatorik, Permutation]
lang: de
header-center: "Kombinatorik" 
---
## Starters

1. Wieviele Sitzordnungen gibt es für
   a) $3$ Personen - sagen wir sie heißen Ann, Bob und Dan - auf $3$ Sitzplätzen?
   b) Ann, Bob, Dan und Eve an einem Tisch mit $4$ Sitzplätzen?
2. Wieviele "Wörter" kann man aus den Buchstaben $A$, $B$, $C$ und $D$ bilden, wenn jeder der Buchstaben genau einmal in jedem "Wort" vorkommt?
3. In einer Urne befinden sich $5$ Kugeln: $1$ rote, $1$ blaue, $1$ gelbe, $1$ schwarze und $1$ weiße. Alle $5$ Kugeln werden nacheinander, ohne Zurücklegen gezogen. Wieviele verschiedene Möglichkeiten gibt es, die Kugeln zu ziehen?
4. Wieviele Anagramme hat das Wort STIEL? (Und wieviele davon ergeben Sinn?)

## Hauptgang

1. In wievielen verschiedenen Reihenfolgen kann man ein Hemd mit 6 Knöpfen zuknöpfen?
2. Bei einem 400-Meter-Lauf starten $8$ Läufer. Wieviele verschiedene Zieleinläufe sind möglich?
3. Wieviele Sitzordnungen gibt es für die 9. Klasse ($36$ Personen und $36$ Plätze)?
4. Auf wieviele verschiedene Weisen kann man $n$ Dinge anordnen?

## Dessert 

1. Rätselgedicht von Friedrich Haug

   > Kein Mensch lebt ohne mich. Ist das nicht klar genug, \newline
   > So wisst: In mir steckt Erbgut und Betrug.

2. Problem der 100 Gefangenen nach Flajolet und Sedgewick (Harte Nuss)

   > Der Leiter eines Gefängnisses gibt $100$ zum Tode verurteilten Gefangenen mit den Nummern von $1$ bis $100$ eine letzte Chance. In einem Raum befindet sich ein Schrank mit $100$ Schubladen. Der Gefängnisleiter legt in jede Schublade die Nummer genau eines Gefangenen in zufälliger Reihenfolge und schließt die Schubladen daraufhin. Die Gefangenen betreten den Raum der Reihe nach. Jeder Gefangene darf $50$ Schubladen in einer beliebigen Reihenfolge öffnen und muss sie danach mit ihrem Inhalt wieder schließen. Finden dabei alle Gefangenen ihre eigene Nummer, werden sie begnadigt. Findet irgendein Gefangener seine Nummer nicht, müssen alle sterben. Bevor der erste Gefangene den Raum betritt, dürfen sich die Gefangenen beraten, danach ist keine Kommunikation mehr möglich. Was ist für die Gefangenen die beste Strategie? \newline

   Tipp: Werden Elemente "im Kreis vertauscht", so nennt man dies einen *Zyklus* oder eine *zyklische Permutation*.

