---
title: "Übungsblatt 5"
author: [Klasse 9]
date: "09.11.2020"
keywords: [Kombinatorik, Permutation mit Wiederholung]
lang: de
header-center: "Kombinatorik" 
---

## Starters

1. Wieviele Anagramme gibt es von den fogelnden Wörtern?
   a) EMMA
   b) ANAGRAMM
   c) MISSISSIPPI

2. In einer Urne befinden sich $5$ Kugeln: $3$ rote, $1$ blaue und $1$ grüne. Alle $5$ Kugeln werden nacheinander, ohne Zurücklegen gezogen. Wieviele verschiedene Möglichkeiten gibt es, die Kugeln zu ziehen?
3. Anna, Robert, Daniel und Eva gehen ins Kino und haben nur noch $2$ Sitze in Reihe A und $2$ Sitze in Reihe J bekommen. Wieviele Sitzmöglichkeiten gibt es, wenn es nur darauf ankommt wer in welcher Reihe sitzt (und nicht wer an welchem Platz)?

## Hauptgang

1. $9$ Personen bilden $3$ Teams zu je $3$ Personen, um $3$ verschiedene Arbeiten zu verrichten. Wieviele Teamein- bzw. Arbeitsaufteilungen gibt es?
2. $11$ Personen unternehmen mit $3$ Autos, mit $2$, $4$ und $5$ Plätzen einen Ausflug. Wieviele mögliche Fahrgemeinschaften gibt es (unabhängig von den Sitzordnungen in den Autos)?
3. Galileo Galilei 1610: 

   > "Cynthiae figuras aemulatur Mater Amorum"

   Deutsch: "Die Mutter der Liebe [gemeint ist der Planet Venus] ahmt die Gestalten der Mondgöttin [also die Mondphasen] nach". Galileo sandte diese Erkenntnis als Anagramm verschlüsselt an Johannes Kepler:

   > "Haec immatura a me iam frustra leguntur o y".

   Deutsch: "Dieses noch Unreife wird von mir bisher vergeblich vorgetragen".

   Kepler hat versucht Galileis Anagramm zu entschlüsseln und kam auf:

   > "Salue umbisineum gemnatum Martia proles."

   Deutsch: "Den beiden Freunden, Kinder des Mars, zum Gruß." was sich zugleich als falsch und richtig herausstellte.

   Wieviele Anagramme dieses Satzes gibt es? (Ansatz hinschreiben reicht.)

