---
title: "Unterrichtseffektivitätsevaluation"
author: ["Name:" ]
date: "16.11.2020"
keywords: [Kombinatorik]
lang: de
header-center: "Kombinatorik" 
---


## I Starters (4 Punkte)

1. In einer Urne liegen $101$ verschiedene Kugeln. Wieviele mögliche "Zugergebnisse" gibt es, wenn sukzessive (unter Beachtung der Reihenfolge) $3$ Kugeln ohne Zurücklegen gezogen werden?

2. Wieviele verschiedene Kombinationen gibt es, wenn von $11$ Kugeln in einer Urne

   \Begin{multicols}{3}
   a) $7$
   b) $4$

   \End{multicols}
   gleichzeitig (also ohne die Reihenfolge zu beachten) gezogen werden?

3. In einer Urne läge eine rote, eine gelbe, eine grüne, eine blaue und eine weiße Kugel. Wieviele Farbanordnungen sind möglich, wenn sukzessive (unter Beachtung der Reihenfolge) $3$ Kugeln mit Zurücklegen gezogen werden?

## II Hauptgang (5 Punkte)

<!--1. In einem Verein werden die $3$ (gleichberechtigten) Vorstände neu gewählt. $7$ Personen stellen sich zur Wahl. Wieviele Zusammensetzungen des Vorstandes sind möglich?-->
<!--1. Mit der additiver Farbmischung, kann man aus **R**ot, **G**rün und **B**lau "alle" Farben mischen.-->
1. In Computern (insbesondere im Internet) werden Farben oft als RGB-Tripel aus $3$ Zahlen zwischen $0$ und $255$ angegeben. Die erste Zahl gibt den **R**ot- die zweite den **G**rün- und die dritte den **B**lau-Anteil, die additiv gemischt werden, an. Wieviele Farben können so dargestellt werden? (Wenn Ihnen die Zahlen hier zu groß sind, rechnen Sie mit $0$ bis $5$ statt $0$ bis $255$.)
2. Beim Spiel Cluedo müssen die Spieler ermitteln wer, wo, wie den Mord an Graf Eutin begangen hat. Es kommen $6$ Täter, $9$ Tatorte und $6$ Tatwaffen in Frage. Wieviele Tathergänge sind möglich?
3. An einem Tanzkurs nehmen $8$ Mädels und $4$ Jungs teil.
   a) Es wird ein Paar zum Vortanzen gewählt. Wieviele (geschlechter-)gemischte Paare gibt es?
   b) Wieviele Möglichkeiten gibt es $4$ gemischte Paare zusammenzustellen? (Die Jungs "fordern auf" z.B.)
   c) Wieviele Möglichkeiten ein Paar zu bilden gibt es, wenn auch gleichgeschlechtliche Paare berücksichtigt werden?
<!-- d) Wieviel Möglichkeiten gibt es $6$ Paare (geschlechtsunabhängig) zusammenzustellen?-->

## III Dessert (1 Extra-Punkt)

Ein Känguruh hat $7$ Nirvana-Songs in seiner Playlist und überlegt in welcher Reihenfolge es die Lieder hören will.

   a) Wieviele Sortierungen der Playlist gibt es?
   b) Wenn eine Playlist in der Schleife gehört wird, gibt es keinen Unterschied zwischen den Playlisten $1$, $2$, $3$, $4$, $5$, $6$, $7$ und $6$, $7$, $1$, $2$, $3$, $4$, $5$ mehr (zum Beispiel). Die Liedreihenfolge (nach Lied $1$ kommt Lied $2$, nach $2$ kommt $3$...) ist bei beiden gleich. Wieviele in der Schleife unterschiedliche Playlists (aus den $7$ Songs) gibt es?
