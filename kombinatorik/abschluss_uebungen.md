---
title: "Übungsblatt 6"
author: [Klasse 9]
date: "10.11.2020"
keywords: [Kombinatorik, Kombination ohne Wiederholung]
lang: de
header-center: "Kombinatorik" 
header-includes:
    - \usepackage{multicol}
    - \newcommand{\hideFromPandoc}[1]{#1}
    - \hideFromPandoc{
        \let\Begin\begin
        \let\End\end
      }
---

## Starters

1. In einem Café gibt es $5$ Sorten Eis: Schoko, Pistazie, Erdbeere, Zitrone und Blaubeere.
   a) Wieviele verschiedene Eiswahlen gibt es, wenn $3$ Kugeln Eis bestellt werden (und dem Besteller wichtig ist in welcher Reihenfolge er die Sorten isst)?
   b) Wieviele verschiedene Wahlen gibt es, wenn aus den $3$ Kugeln Milchshake gemacht wird?

2. Wir bauen "Wörter" aus den Buchstaben L, E, G und O. Die Wörter sollen aus genau $3$ Buchstaben bestehen und jeder Buchstabe soll in jedem Wort höchstens einmal vorkommen. 
   a) Wieviele von diesen "Wörtern" gibt es?
   b) Wieviele bleiben übrig, wenn die Buchstaben im Wort alphabetisch sortiert sein sollen?

3. Aus einer Urne mit $6$ verschiedenfarbigen Kugeln werden gleichzeitig (bzw. ohne Zurücklegen und ohne Beachtung der Reihenfolge)
   a) $2$
   b) $3$
   c) $4$

Kugeln gezogen. Wieviele mögliche "Zugergebnisse" gibt es?

## Hauptgang

1. Aus $20$ Astronauten sollen $5$ für eine Marsmission ausgewählt werden. Wieviele verschiedene Missionsteams sind möglich?
2. Das Kartenspiel Maumau spielt man üblicherweise mit $32$ (paarweise verschiedenen) Karten. Wieviele Ausgangssituationen gibt es für einen Spieler, wenn man mit $5$ Startkarten spielt? (Die Reihenfolge, in der man die Karten erhält, ist unerheblich.)
3. $6$ Sportfreunde begrüßen sich mit Handschlag (jeder gibt jedem einmal die Hand). Wieviele Händedrücke werden ausgetauscht?
4. Wieviele Dreiecke kann man in ein
   a) $6$-Eck
   b) $12$-Eck

   zeichnen?
5. Beim Lotto "6 aus 49" befinden sich $49$ Kugeln in der Lostrommel, die von $1$ bis $49$ nummeriert sind. Davon werden $6$ ohne Zurücklegen und ohne Beachtung der Reihenfolge gezogen. Wieviele mögliche Lotto-Ziehungen gibt es?

## Dessert 

\Begin{multicols}{2}

![](manhattan_distance.png){height=210px}

Wieviele Wege kann ein Taxifahrer durch Manhattan nehmen, wenn er $6$ Blocks nach Westen und $6$ Blocks nach Süden muss? Wieviele Wege gibt es also im - durch Manhattan inspirierten - Bild (links) von der rechten oberen Ecke zur unteren linken Ecke?

\End{multicols}
