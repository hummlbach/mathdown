---
title: "Übungsblatt 3"
author: [Klasse 9]
date: "04.11.2020"
keywords: [Kombinatorik, Variation]
lang: de
header-center: "Kombinatorik" 
---

## Starters

1. Wieviele verschiedene Farbcodes sind beim Mastermind möglich, wenn vereinbart wurde, dass die Farben eines Codes verschieden sein müssen, und die Codes gebildet werden aus
   a) $4$ Farben?
   b) $8$ Farben?
2. Wieviele verschiedene Farbcodes sind beim Mastermind möglich, wenn Farben auch mehrfach in den Codes verwendet werden dürfen, und für die Codes
   a) $2$ Farben
   b) $3$ Farben
   c) $4$ Farben
   d) $6$ Farben

   verwendet werden können.
3. Die Zeichen in der Brailleschrift bestehen aus je $6$ Punkten (angeordnet in $2$ Spalten und $3$ Zeilen). Jede dieser $6$ Stellen eines Zeichens kann zu einer punktförmigen Erhöung ausgedrückt (oder eben/flach/glatt) sein. Wieviele Zeichen können so dargestellt werden?
4. In einer Urne seien $4$ Kugeln: $1$ rote, $1$ blaue, $1$ grüne sowie $1$ weiße. Aus der Urne werden nacheinander (also unter Beachtung der Reihenfolge) Kugeln gezogen. Die gezogene Kugel wird nach dem Ziehen jeweils wieder zurückgelegt. Wieviele verschiedene Farbvariationen ergeben sich, wenn
   a) $2$ mal
   b) $3$ mal
   c) $4$ mal

   gezogen wird?

\pagebreak

## Hauptgang

1. Wie lang muss 
   a) eine PIN 
   b) ein Passwort (gehen Sie von 100 möglichen Zeichen pro Stelle aus)

   sein, um mindestens so "sicher" zu sein, wie ein Entsperrmuster (siehe Übungsblatt 2 Dessert)?
2. Der Computer kennt im Grunde nur $0$ und $1$ (Strom fließt oder eben nicht). So eine "Stelle im Computer", die $0$ oder $1$ sein kann, heißt *Bit*. $8$ Bit werden zusammengefasst zu einem *Byte*. 
   a) Der sogenannte *ASCII-Code* legt fest welcher "Byte-Werte" welches Zeichen (Buchstabe, Zahl, Satzzeichen) darstellt. Wieviele Zeichen können mit einem Byte dargestellt werden?
   b) Um auch Emojis (sowie nicht-lateinische Buchstaben und andere Schriftzeichen) darstellen zu können, verwendet man heutzutag $4$ Byte pro Zeichen. Wieviele Zeichen kann man damit darstellen?
3. Auf den Karten des Kartenspiels Set sind Symbole abgebildet. Die Karten variieren in der Anzahl der abgebildeten Symbole, ihrer Form, ihrer Farbe sowie deren Füllung. Auf den Karten sind jeweils $1$, $2$ oder $3$ Symbole zu sehen. Die Symbole sind entweder Ellipsen, Rechtecke oder Dreiecke. Die Symbole auf einer Karte sind alle blau, alle rot oder alle grün und alle Symbole auf einer Karte sind gefüllt, leer oder schraffiert. Das Spiel beginnt indem (z.B. $9$) Karten offen ausgelegt werden. Sobald ein Mitspieler $3$ Karten ausmacht, bei denen jede der $4$ Eigenschaften, für sich betrachtet, entweder bei allen $3$ Karten gleich oder bei allen $3$ verschieden ist, darf er sich melden, das Set benennen und die Karten an sich nehmen. Dann werden Karten nachgelegt bis wieder ein Set zu finden ist.
   a) Wieviele rote Karten gibt es?
   b) Wieviele blaue, gefüllte Karten gibt es?
   c) Wieviele Karten gibt es insgesamt?
4. Es seien $k$ Stellen zu besetzen und für jede Stelle gäbe es (die) $n$ (gleichen) Dinge zur Auswahl. Wieviele Möglichkeiten gibt es die $k$ Stellen zu besetzen?

## Dessert

Weitere Fragen zum Spiel Set:

a) Wieviele Karten gibt es, deren Symbole entweder rot oder rechteckig sind?
b) Wieviele verschiedene Sets gibt es?
c) Wieviele Sets mit einer bestimmten Karte, z.B. der einzelnen, grünen, leeren Ellipse, gibt es?
d) Wieviele Karten muss man mindestens auslegen um sicher ein Set zu finden? (Harte Nuss!)

