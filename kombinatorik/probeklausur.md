---
title: "Lehrerqualitätskontrollenbeispiel"
author: ["Name:" ]
date: "13.11.2020"
keywords: [Kombinatorik]
lang: de
header-center: "Kombinatorik" 
---


## I Starters (8 Punkte)

1. Beim (hypothetischen) Mastermind Junior wird der Farbcode nur aus $4$ Farben gebildet und der Code hat nur $3$ Stellen. Farben dürfen aber mehrfach im Code verwendet werden. Wieviele Farbcodes sind möglich?
2. Bei einem 100-Meter-Lauf starten $5$ Läufer. Wieviele verschiedene "Treppchen" (also Belegungen der ersten $3$ Plätze) gibt es?
3. Ann, Bob, Dan, Eve und Flo bereiten ein Referat vor. Sie losen aus, wer von Ihnen vortragen darf. Wieviele verschiedene Möglichkeiten gibt es zwei Vorträger auszuwählen?
4. In einem Restaurant gibt es ein $4$-Gänge-Menü. Zum ersten Gang kann man eine Suppe oder einen Salat wählen. Zum zweiten Gang kann man aus $3$ Vorspeisen wählen. Der dritte Gang besteht einem von $2$ Hauptgerichten. Schließlich kann man noch eines von $4$ Desserts wählen. Wieviele verschiedene Bestellmöglichkeiten gibt es?

## II Hauptgang (10 Punkte)

1. Aus einer Urne mit $6$ Kugeln werden alle sukzessive (ohne Zurücklegen) gezogen. Wieviele mögliche Zugergebnisse gibt es, wenn
   a) alle Kugeln voneinander unterscheidbar sind?
   b) drei Kugeln blau, eine rot, eine gelb und eine grün ist?

2. In einer Urne liegen $11$ paarweise verschiedene Kugeln. Wieviele Möglichkeiten gibt es, sukzessive (unter Beachtung der Reihenfolge)
   a) $3$ Kugeln ohne Zurücklegen zu ziehen?
   b) $3$ Kugeln mit Zurücklegen zu ziehen?

3. Wieviele verschiedene Kombinationen gibt es, wenn von $50$ Kugeln in einer Urne, $47$ gezogen werden ohne die Reihenfolge zu beachten?

## III Dessert (6 Punkte)

1. Ein Multiple Choice Test bestehe aus $8$ Fragen mit je $3$ Antworten, von denen genau eine angekreuzt werden darf. Wieviele Antwortmöglichkeiten gibt es insgesamt?

2. $6$ Personen begrüßen sich mit Handschlag (jeder gibt jedem die Hand). Wieviele Händedrücke werden ausgetauscht?

3. Wieviele Möglichkeiten gibt es, einen Springer, einen Turm und einen Läufer auf ein Schachbrett zu stellen? (Ein Schachbrett hat $64$ Felder, auf einem Feld darf immer nur eine Figur stehen. Sie dürfen den letzten Rechenschritt weglassen.)

