---
title: "Lösungen zum Operatorenblatt"
author: [Klasse 12]
date: 13.06.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
---

1. a) $u=\left(\Begin{array}{c}-2\\ 0\\ 2\End{array}\right)$, $v=\left(\Begin{array}{c}-2\\ 2\\ 2\End{array}\right)$, $w=\left(\Begin{array}{c}0\\ 2\\ 0\End{array}\right)$
   b) $\text{ }$ \newline ![](drei_vektoren_in_wuerfel_konkateniert.png)
   c) Indem man sie addiert...

2. Wir betrachten die vier Vektoren $\vec v_1$, $\vec v_2$, $\vec v_3$ sowie $\vec v_4$.

   a) $v_1=\left(\Begin{array}{c}-3\\ -2\\ 4\End{array}\right)$, $v_2=\left(\Begin{array}{c}-3\\ 0\\ 0\End{array}\right)$, $v_3=\left(\Begin{array}{c}0\\ 2\\ 0\End{array}\right)$, $v_4=\left(\Begin{array}{c}2\\ 2\\ -1\End{array}\right)$

   $\text{ }$

   c) \Begin{multicols}{2}
      (i)   $\vec v_1 - \vec v_2=\left(\Begin{array}{c}0\\ -2\\ 4\End{array}\right)$
      (ii)  $\vec v_4 - \vec v_3=\left(\Begin{array}{c}2\\ 0\\ -1\End{array}\right)$
      (iv)  $\vec v_4 - \vec v_2=\left(\Begin{array}{c}5\\ 2\\ -1\End{array}\right)$
      (v)  $\vec v_1 - \vec v_4=\left(\Begin{array}{c}-5\\ -4\\ 5\End{array}\right)$

      \End{multicols}

   $\text{ }$

   b) $\text{ }$ \newline ![](zwei_quader_vier_vektoren_differenzen.png)

3. a) Skip!
   b) $\left|\vec u_1 \right| = \sqrt{0^2+2^2+0^2} = \sqrt{4} = 2$ \newline $\left|\vec u_2 \right| = \sqrt{4^2+0^2+(-3)^2} = \sqrt{25} = 5$ \newline $\left|\vec u_3 \right| = \sqrt{1^2+(-2)^2+2^2} = \sqrt{9} = 3$
   c) \Begin{multicols}{3}
      (i) $\left(\Begin{array}{c}0\\ 1\\ 0\End{array}\right)$
      (i) $\left(\Begin{array}{c}8\\ 0\\ -6\End{array}\right)$
      (i) $\left(\Begin{array}{c}3\\ -6\\ 6\End{array}\right)$

      \End{multicols}
   d) $1$, $10$, $9$; die Ergebnisvektoren sind um den Faktor mit dem der jeweilige ursprüngliche gegebene Vektor skalarmultipliziert wurde länger/kürzer.



