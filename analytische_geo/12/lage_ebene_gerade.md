---
title: "Lage von Gerade und Ebene"
author: [Klasse 12]
date: 16.06.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=2.5cm
  - right=2.5cm
---

1. Gegeben ist die wohlbekannte Ebene $E$ mit der Parameterdarstellung
   $$ E: \vec x = \left(\Begin{array}{c}-1\\ 3\\ 6\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ -3\\ 2\End{array}\right) + \mu \left(\Begin{array}{c}2\\ 3\\ -8\End{array}\right)$$
   sowie der Koordinatengleichung $6x+4y+3z = 24$. Außerdem betrachten wir die folgende Gerade:
   $$ g: \vec x = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \kappa \left(\Begin{array}{c}1\\ 1\\ -3\End{array}\right)$$
   a) Verwenden Sie die Parameterdarstellung von $E$, um den Schnittpunkt von $E$ und $g$ zu berechnen.
   b) Ermitteln Sie den Schnittpunkt von $E$ und $g$ nocheinmal, unter Verwendung der Koordinatengleichung von $E$.

2. Berechnen Sie den Schnittpunkt der Ebene $E$ mit der Koordinatengleichung $2x+y+2z=1$ und der Gerade
   $g: \vec x = \left(\Begin{array}{c}3\\ 5\\ 4\End{array}\right) + t \left(\Begin{array}{c}4\\ 2\\ 4\End{array}\right)$.

3. Berechnen Sie den Schnittpunkt von $E: 3x-4y+2z=3$ und
   $g: \vec x = \left(\Begin{array}{c}1\\ 6\\ -1\End{array}\right) + t \left(\Begin{array}{c}1\\ -1\\ 3\End{array}\right)$.

4. Gegeben seien nun
   $E: \vec x = \left(\Begin{array}{c}3\\ 0\\ 2\End{array}\right) + \lambda \left(\Begin{array}{c}0\\ 1\\ 1\End{array}\right) + \mu \left(\Begin{array}{c}-1\\ -2\\ 0\End{array}\right)$
   und 
   $g: \vec x = \left(\Begin{array}{c}1\\ 1\\ 1\End{array}\right) + t \left(\Begin{array}{c}-1\\ -1\\ 1\End{array}\right)$.
   Haben die Ebene $E$ und die Gerade $g$ einen Schnittpunkt?

5. Zeigen Sie, dass $E: 2x-3y+z=-4$ und 
   $g: \vec x = \left(\Begin{array}{c}-6\\ 2\\ 4\End{array}\right) + t \left(\Begin{array}{c}4\\ 3\\ 1\End{array}\right)$
   parallel sind.

6. Bestimmen Sie die Lage der Ebene $E$ und der Gerade $g$ zueinander:
   a) $E: 7x-4y-2z = 4$, 
      $g: \vec x = \left(\Begin{array}{c}2\\ 4\\ 0\End{array}\right) + t \left(\Begin{array}{c}1\\ 0\\ 2\End{array}\right)$
   b) $E: 2y-z = 4$, 
      $g: \vec x = \left(\Begin{array}{c}1\\ 1\\ 1\End{array}\right) + t \left(\Begin{array}{c}2\\ 1\\ 2\End{array}\right)$
   c) $E: 2x+2y+z = 7$, 
      $g: \vec x = \left(\Begin{array}{c}1\\ 2\\ 1\End{array}\right) + t \left(\Begin{array}{c}0\\ 2\\ -4\End{array}\right)$

\pagebreak

## Ergebnisse

1. $S(2|3|0)$
2. $S(-1|3|0)$
3. $S(3|4|5)$

$\text{ }$

6. a) $E$ und $g$ schneiden sich in $S(4|4|4)$
   b) $E$ und $g$ sind parallel
   c) $g$ liegt in $E$

