---
title: "Grundlagenübersicht"
author: [Klasse 12]
date: 12.07.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
geometry:
  - left=2cm
  - right=2cm
  - top=3cm
  - bottom=3cm
---

## 1 Länge/Abstand

$$\left| \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) - \left(\Begin{array}{c}1\\ 1\\ 1\End{array} \right)\right| = \left| \left(\Begin{array}{c}0\\ 1\\ 2\End{array} \right) \right| = \sqrt{0^2+1^2+2^2} = \sqrt{5}$$

Siehe auch havonix Vektorgeometrie S. 69 Aufgabe 1.

## 2 Punktprobe mit Gerade/Liegt $P$ auf $g$?

- $P\left(1|0|0\right)$, $Q\left(-1|0|1\right)$
- $g: \vec x = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 1\\ 1\End{array}\right)$
- $P\in g$?
  $$\left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 1\\ 1\End{array}\right) = \left(\Begin{array}{c}1\\ 0\\ 0\End{array}\right) \qquad \Rightarrow \qquad
  \Begin{array}{rll}
    \text{I:}   &1+\lambda \cdot 1 &= 1 \\
    \text{II:}  &2+\lambda \cdot 1 &= 0 \\
    \text{III:} &3+\lambda \cdot 1 &= 0
  \End{array} \qquad \Rightarrow \qquad
  \Begin{array}{rlr}
    \text{I:}   &\lambda =& 0 \\
    \text{II:}  &\lambda =& -2 \\
    \text{III:} &\lambda =& -3 
  \End{array}$$
  $\Rightarrow$ Widerspruch, also $P\notin g$

- $Q\in g$?
  $$\left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 1\\ 1\End{array}\right) = \left(\Begin{array}{c}-1\\ 0\\ 1\End{array}\right) \qquad \Rightarrow \qquad
  \Begin{array}{rlr}
    \text{I:}   &1+\lambda \cdot 1 =& -1 \\
    \text{II:}  &2+\lambda \cdot 1 =& 0 \\
    \text{III:} &3+\lambda \cdot 1 =& 1
  \End{array} \qquad \Rightarrow \qquad
  \Begin{array}{rlr}
    \text{I:}   &\lambda =& -2 \\
    \text{II:}  &\lambda =& -2 \\
    \text{III:} &\lambda =& -2 
  \End{array}$$
  $\Rightarrow$ $P\in g$

## 3 Punktprobe mit Ebene

### 3.1 Ebene in Parameterform
$E: \vec x = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 0\\ 0\End{array}\right)+ \mu \left(\Begin{array}{c}0\\ 0\\ 1\End{array}\right)$ \newline

Gleichsetzen analog zur Punktprobe mit einer Geraden. Lösen des Gleichungssystems mit $3$ Gleichungen und $2$ Unbekannten. Zwei Gleichungen verwenden, um $\lambda$ und $\mu$ auszurechnen, dann checken, ob $\lambda$ und $\mu$ zur dritten Gleichung "passen".

### 3.2 Ebene in Koordinatenform

- $E: 3x-5y+z = 7$, $P\left(4|2|5\right)$, $Q\left(1|2|3\right)$
- Punkt in Ebenengleichung einsetzen...
- $P\in E$? \newline
  $3\cdot 4- 5\cdot 2 + 5 = 12-10+5 = 7 \Rightarrow P\in E$
- $Q\in E$? \newline
  $3\cdot 1- 5\cdot 2 + 3 = 3-10+3 = -6 \neq 7 \Rightarrow Q\notin E$

## 4 Ebenen-Parameterform in Koordinatenform umrechnen

$E: \vec x = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 0\\ 0\End{array}\right)+ \mu \left(\Begin{array}{c}0\\ 0\\ 1\End{array}\right)$ \newline

In Koordinatenform:
$$E: \left(\left(\Begin{array}{c}x\\ y\\ z\End{array}\right) - \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right)\right) \circ \left(\Begin{array}{c}1\\ 0\\ 0\End{array}\right) \times \left(\Begin{array}{c}0\\ 0\\ 1\End{array}\right) = 0$$

Siehe auch havonix Vektorgeometrie S. 43 Aufgabe 12.

## 5 Gerade aufstellen

$g$ geht durch $P(1|2|3)$ und $Q(1|1|1)$:
$$g: \vec x = \vec p + \lambda (\vec p - \vec q) = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1-1\\ 2-1\\ 3-1\End{array}\right) = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}0\\ 1\\ 2\End{array}\right)$$

Siehe auch havonix Vektorgeometrie S. 40 Aufgabe 7.

## 6 Ebene aufstellen

### 6.1 Parameterform aus Punkten

$E$ geht durch $P(1|2|3)$, $Q(4|5|6)$ und $R(3|2|1)$:
$$E: \vec x = \vec p + \lambda (\vec p - \vec q) + \mu (\vec p - \vec r) =\left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}1 -4\\ 2-5\\ 3-6\End{array}\right) + \mu \left(\Begin{array}{c}1 - 3\\ 2 -2 \\ 3 -1\End{array}\right)=\left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}-3\\ -3\\ -3\End{array}\right) + \mu \left(\Begin{array}{c}- 2\\ 0 \\ 2 \End{array}\right)$$

Siehe auch havonix Vektorgeometrie S. 42 Aufgabe 8.

### 6.2 Koordinatenform aus Spurpunkten

$E$ habe Spurpunkte mit der

- $x$-Achse bei $5$
- $y$-Achse bei $\frac{1}{2}$
- $z$-Achse bei $-3$

"Achsenabschnittsform": $\frac{x}{5} + \frac{y}{\frac{1}{2}} + \frac{z}{-3} = 1$ \newline
Verschönert: $\frac{x}{5} + 2y - \frac{z}{3} = 1$

## 7 Spurpunkte von Geraden mit den Koordinatenebenen

$g: \vec x = \left(\Begin{array}{c}2\\ 0\\ 2\End{array}\right) + \lambda \left(\Begin{array}{c}-2\\ 3\\ 0\End{array}\right)$

- $yz$-Ebene hat die Gleichung $x=0$ \newline
  $g$ in $x=0$: $\quad 2+\lambda\cdot (-2) = 0 \Rightarrow \lambda = 1$ \newline
  Spurpunkt mit der $yz$-Ebene - $\lambda = 1$ in $g$: $S_x= \left(\Begin{array}{c}2\\ 0\\ 2\End{array}\right) + 1 \cdot \left(\Begin{array}{c}-2\\ 3\\ 0\End{array}\right) = \left(\Begin{array}{c}0\\ 3\\ 2\End{array}\right)$

- $xz$-Ebene hat die Gleichung $y=0$ \newline
  $g$ in $y=0$: $\quad 0+\lambda\cdot 3 = 0 \Rightarrow \lambda = 0 \Rightarrow S_y=\left(\Begin{array}{c}2\\ 0\\ 2\End{array}\right) + 0 \cdot \left(\Begin{array}{c}-2\\ 3\\ 0\End{array}\right) = \left(\Begin{array}{c}2\\ 0\\ 2\End{array}\right)$
  
- $xy$-Ebene hat die Gleichung $z=0$ \newline
  $g$ in $z=0$: $\quad 2+\lambda\cdot 0 = 0 \Rightarrow 2 = 0 \Rightarrow$ kein Spurpunkt mit der $xy$-Ebene $\Rightarrow$ $g$ parallel zur $xy$-Ebene

Siehe auch havonix Vektorgeometrie S. 48 Aufgabe 19.

## 8 Spurpunkte von Ebene mit den Koordinatenachsen

- $E: 3x+2y+6z=18$
- Spurpunkt mit der $x$-Achse - auf der $x$-Achse ist $y=0$ und $z=0$: $\quad 3x = 18 \Leftrightarrow x=6 \Rightarrow S_x(3|0|0)$.
- Spurpunkt mit der $y$-Achse; $x=0$ und $z=0$ in $E$: $\quad 3\cdot 0 +2y +6\cdot 0 = 18 \Leftrightarrow y=9 \Rightarrow S_y(0|9|0)$.
- Spurpunkt mit der $z$-Achse: $\quad 3\cdot 0 +2 \cdot 0 +6\cdot z = 18 \Leftrightarrow z=3 \Rightarrow S_z(0|0|3)$.
 
Siehe auch havonix Vektorgeometrie S. 53 Aufgabe 25.

## Lagen von Geraden zueinander

Siehe auch havonix Vektorgeometrie S. 62 Aufgabe 1ff.

## Winkel zwischen Geraden

Siehe auch havonix Vektorgeometrie S. 89 Aufgabe 1.

## Lage von Gerade und Ebene zueinander

Siehe auch havonix Vektorgeometrie S. 65 Aufgabe 9ff.

## Lage von Ebenen zueinander

