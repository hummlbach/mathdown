---
title: "Operatoren"
author: [Klasse 12]
date: 13.06.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
geometry:
  - top=3.5cm
  - bottom=4cm
---

1. Gegeben seien die Vektoren $\vec u$, $\vec v$ und $\vec w$, wie im folgenden zu sehen:

   ![](drei_vektoren_in_wuerfel.png)

   a) Geben Sie die Vektoren in Koordinatenschreibweise an.
   b) "Hängen" Sie die Vektoren zeichnerisch aneinander (d.h. verschieben Sie jeweils einen der Vektoren so, dass er mit seinem Schwanz da anfängt, wo der andere seine Spitze hat.) und geben Sie die Koordinatenschreibweise der (drei) sich ergebenden Vektoren an.
   c) Wie erhält man diese drei Vektoren rechnerisch aus den Vektoren $\vec u$, $\vec v$ und $\vec w$?

   \pagebreak

2. Wir betrachten die vier Vektoren $\vec v_1$, $\vec v_2$, $\vec v_3$ sowie $\vec v_4$.

   ![](zwei_quader_vier_vektoren.png)

   a) Geben Sie die Vektoren in Koordinatenschreibweise an.

   b) Zeichnen Sie jeweils den Vektor ein, der die Spitzen der folgenden Vektoren verbindet:
      \Begin{multicols}{4}
      (i)   $\vec v_2$ und $\vec v_1$
      (ii)  $\vec v_3$ und $\vec v_4$
      (iv)  $\vec v_2$ und $\vec v_4$
      (v)  $\vec v_4$ und $\vec v_1$

      \End{multicols}

      Und geben Sie seine Koordinatenschreibweise an.

   c) Berechnen Sie:
      \Begin{multicols}{4}
      (i)   $\vec v_1 - \vec v_2$
      (ii)  $\vec v_4 - \vec v_3$
      (iv)  $\vec v_4 - \vec v_2$
      (v)  $\vec v_1 - \vec v_4$

      \End{multicols}

   \pagebreak

3. Gegeben seien die Vektoren $\vec u_1 = \left(\Begin{array}{c}0\\ 2\\ 0\End{array}\right)$, $\vec u_2= \left(\Begin{array}{c}4\\ 0\\ -3\End{array}\right)$ und $\vec u_3=\left(\Begin{array}{c}1\\ -2\\ 2\End{array}\right)$.

   a) Zeichnen Sie die Vektoren in ein Koordinatensystem ein.
   b) Berechnen Sie die Länge der Vektoren.
   c) Skalarmultiplizieren Sie
      \Begin{multicols}{4}
      (i) $\frac{1}{2} \cdot \vec u_1$
      (ii) $2 \cdot \vec u_2$
      (iii) $3 \cdot \vec u_3$

      \End{multicols}

      und zeichnen Sie die Ergebnisvektoren ins Koordinatensystem ein.
   d) Berechnen Sie die Längen der Ergebnisvektoren aus c). Wie verhalten sich diese zu den Längen der Vektoren $\vec u_1$, $\vec u_2$ und $\vec u_3$?

4. Bearbeiten Sie die Aufgaben ea) bis ef) von Seite 191 des Buchs havonix Vektorgeometrie. (Skalarprodukt)

5. Bearbeiten Sie die Aufgaben fg), fh) und fi) von Seite 192 des Buchs havonix Vektorgeometrie. (Skalarprodukt)

6. Bearbeiten Sie die Aufgaben ga) bis gf) von Seite 192 des Buchs havonix Vektorgeometrie. (Vektorprodukt)


