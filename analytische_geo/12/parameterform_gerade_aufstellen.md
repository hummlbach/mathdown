---
title: "Aufwärmübung"
author: [Klasse 11]
date: 10.05.2021
keywords: [Parameterform, Punktrichtungsform]
lang: de
header-center: "Analytische Geometrie 3d" 
---

Stellen Sie die Gerade $g$, die durch die beiden Punkte $A(3|1|2)$ sowie $B(5|3|0)$ geht, auf. Gehen Sie dabei wie folgt vor:

a) Zeichnen Sie ein dreidimensionales Koordinatensystem und zeichnen Sie die Punkte $A$ und $B$ ein.
b) Zeichnen Sie $\vec v$ als Ortsvektor vom Ursprung zum Punkt $A$ ein.
c) Berechnen Sie den Vektor $\vec r$, der von $A$ nach $B$ zeigt und zeichnen Sie ihn ins Koordinatensystem ein.
d) Stellen Sie mittels $\vec v$ und $\vec r$ die Parameterform (auch Punkt-Richtungs-Form genannt) der Gerade $g$ auf und zeichnen Sie $g$ ins Koordinatensystem ein.
