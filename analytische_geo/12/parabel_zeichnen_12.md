---
title: "Von der Normal- zur Scheitelform"
author: [Klasse 12]
date: 25.01.2021
keywords: [Parabeln, Scheitelform, Normalform]
lang: de
header-center: Parabeln
---

## I Starters

1. Zeichnen Sie die folgenden Parabeln in ein Koordinatensystem ein:

   \Begin{multicols}{2}

   a) $p_1: y=x^2$
   b) $p_2: y=(x+3)^2$
   c) $p_3: y=x^2-4$
   d) $p_4: y=(x+3)^2+2$


   \End{multicols}

2. Zeichnen Sie die Parabeln $q_i$ in ein **weiteres** Koordinatensystem:

   \Begin{multicols}{2}

   a) $q_1: y=\frac{1}{4}x^2$
   b) $q_2: y=2x^2$
   c) $q_3: y=-x^2+3$
   d) $q_4: y=-\frac{1}{2}(x-4)^2+3$

    
   \End{multicols}

## II Hauptgang

1. Zeichnen Sie die folgenden Parabeln wiederum in ein neues Koordinatensystem ein:

   \Begin{multicols}{2}
   
   a) $f_1: y=x^2+4x+4$
   b) $f_2: y=x^2-6x+9$
   c) $f_3: y=x^2+4x+5$
   d) $f_4: y=x^2-4x-1$
   e) $f_5: y=\frac{1}{4}x^2+2x+2$

    
   \End{multicols}

2. Zeichnen Sie die Parabel $f: y=-2x^2+4x$ in ein Koordinatensystem ein.

3. Zeichnen Sie den Graph der Funktion $g(x)=\frac{1}{2}x^2-x+\frac{1}{2}$ in ein Koordinatensystem.

## III Dessert

1. Berechnen Sie die Schnittpunkte von ...

   \Begin{multicols}{2}

   a) ... $f$ mit der $y$-Achse
   b) ... dem Graph von $g$ mit der $y$-Achse
   c) ... dem Graph von $g$ mit der $x$-Achse
   d) ... $f$ mit der $x$-Achse
   e) ... von $f$ und dem Graph von $g$

   
   \End{multicols}

2. Was fällt Ihnen auf, wenn Sie folgendes tun...?

   a) Berechnen Sie die erste Ableitung $g'$ von $g$.
   b) Setzen Sie die erste Ableitung $g'$ gleich $0$ und lösen Sie nach $x$ auf.

 
