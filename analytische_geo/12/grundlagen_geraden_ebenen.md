---
title: "Grundlagenübung"
author: [Klasse 12]
date: 28.06.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
geometry:
  - top=3cm
  - bottom=3.5cm
---

1. Im folgenden Schaubild sind zwei Ebenen $E_1$ und $E_2$, sowie eine Gerade $g$ zu sehen:

   ![](zwei_ebenen_eine_gerade.png)

   a) Geben Sie $g$ in Parameterform an.
   b) Stellen Sie eine Gleichung für $E_1$ auf.
   c) Geben Sie $E_2$ in Koordinatenform an.

2. Frei nach NSR: Wir betrachten die Ebenen $E_1: 3x+2y+6z = 18$, $E_2: 3x+2y=6$ und die Gerade $g: \vec x = \left(\Begin{array}{c}-1\\ 2\\ 5\End{array}\right) + \lambda \left(\Begin{array}{c}3\\ 1\\ -4\End{array}\right)$.

   a) Geben Sie die Koordinaten der Schnittpunkte $S_x$, $S_y$ und $S_z$ von $E_1$ mit den Koordinatenachsen an.
   b) Zeichnen Sie $S_x$, $S_y$ und $S_z$ und die Spurgeraden von $E_1$ in den Koordiantenebenen rot in ein Koordinatensystem ein.
   c) Formulieren Sie eine Gleichung von $E_1$ in Parameterform.
   d) Zeigen Sie rechnerisch, dass der Punkt $P(3|1,5|1)$ in $E_1$ liegt, nicht aber auf $E_2$.
   e) Zeigen Sie, anhand ihrer Normalenvektoren, dass die Ebenen $E_1$ und $E_2$ nicht parallel sind.
   f) $E_1$ und $E_2$ schneiden sich in der Geraden $s$. Stellen Sie eine Geradengleichung für $s$ auf.
   g) Zeichnen $E_2$ in grün und $s$ in blau ins Koordinatensystem zu $E_1$ ein.
   h) Benennen Sie den Schnittpunkt von $s$ mit der $xz$-Ebene in der Zeichnung mit $Q$ und den Schnittpunkt von $s$ mit der $yz$-Ebene mit $R$. Berechnen Sie den Abstand von $Q$ und $R$.
   i) Untersuchen Sie, wie die Gerade $g$ und die Ebene $E_1$ zueinander liegen.

3. Gegeben sind die drei Geraden: \newline
    $g_1: \vec x = \left(\Begin{array}{c}2\\ 0\\ 4\End{array}\right) + \lambda \left(\Begin{array}{c}1\\ 1\\ 2\End{array}\right)$, \newline
    $g_2: \vec x = \left(\Begin{array}{c}3\\ -2\\ 0\End{array}\right) + \lambda \left(\Begin{array}{c}\sqrt{2}\\ \frac{2}{\sqrt{2}}\\ 2\sqrt{2}\End{array}\right)$ und \newline
    $g_3: \vec x = \left(\Begin{array}{c}5\\ 3\\ 8\End{array}\right) + \lambda \left(\Begin{array}{c}3\\ 3\\ 5\End{array}\right)$.
   a) Erötern Sie die Lage der drei Geraden zueinander und geben Sie die etwaige Schnittpunkte.
   b) Berechnen Sie den Winkel zwischen den Geraden, die sich schneiden.
   c) Mit welcher der gegebenen Geraden $g_i$ ist $h: \vec x = \left(\Begin{array}{c}2\\ 0\\ 3\End{array}\right) + \lambda \left(\Begin{array}{c}\frac{3}{5}\\ 0,6\\ 1\End{array}\right)$ identisch? Begründen sie ihre Wahl (rechnerisch).

   (Siehe auch Havonix Vektorgeometrie Seite 62 Aufgabe 7.)
