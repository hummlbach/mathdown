---
title: "Klausur frei nach NSR"
author: "Name:"
date: 13.07.2022
keywords: [Ebene, Gerade, Abstand, Koordinantenform, Parameterform]
lang: de
header-center: "Vektorgeometrie" 
footer-right: "Bearbeitungszeit 90 Minuten"
---

\Begin{tcolorbox}
### Aufgabe 1
Die Gerade $g$ geht durch die Punkte $A(4|2|-3)$ und $B(-2|6|2)$.

a) Zeichnen Sie $A$ und $B$ und $g$ in ein rechtwinkliges Koordinatensystem.
b) Berechnen Sie den Abstand von $A$ zu $B$.
b) Erstellen Sie eine Vektor-Gleichung für $g$.

\End{tcolorbox}
$\text{ }$
\Begin{tcolorbox}

### Aufgabe 2
Gegeben sind die Geraden $h: \vec x = \left(\Begin{array}{c}2 \\6 \\ 1\End{array}\right) + \alpha \left(\Begin{array}{c}-1 \\1 \\ 0\End{array}\right)$ und $k: \vec x =  \left(\Begin{array}{c}0 \\1 \\ 2\End{array}\right) + \beta \left(\Begin{array}{c}3 \\4 \\ -1\End{array}\right)$

a) Zeigen Sie, dass die Geraden $h$ und $k$ nicht parallel zueinander sind.
b) Belegen Sie anhand einer Rechnung, dass sich $h$ und $k$ schneiden und geben Sie den Schnittpunkt an.
c) Berechnen Sie den Winkel zwischen den Geraden.

\End{tcolorbox}
$\text{ }$
\Begin{tcolorbox}

### Aufgabe 3
Die Ebene $E_1$ enhält die Punkte $C(4|5|1)$, $D(5|7|3)$ und $F(6|7|4)$.

a) Erstellen Sie eine Ebenengleichung von $E_1$ in Parameterform.
b) Berechnen Sie eine Koordinatenform von $E_1$.
c) Geben Sie die Spurpunkte von $E_1$ mit den Koordinatenachsen an.
d) Zeichnen Sie $E_1$ in ein neues Koordinatensystem.

\End{tcolorbox}
$\text{ }$
\Begin{tcolorbox}

### Aufgabe 4
Gegeben sind die Ebene $E_2: 3x-5y+2z = 9$ und die Gerade $q: \vec x =  \left(\Begin{array}{c}5 \\-4 \\ 10\End{array}\right) + \delta \left(\Begin{array}{c}2 \\-3 \\ k\End{array}\right)$

a) Für $k=1$ schneiden sich $E_2$ und $q$ im Punkt $S$. Berechnen Sie den Schnittpunkt $S$.
b) Berechnen Sie einen Wert für $k$, sodass $g$ parallel zu $E_2$ ist.

\End{tcolorbox}
\Begin{tcolorbox}

### Aufgabe 5

Die folgenden Abbildungen zeigen die Graphen $K_f$, $K_g$ und $K_h$ der Funktionen $f$, $g$ und $h$. \newline

![](Nico\ 22\ Abb1.png){height=130px} \ $\quad$ ![](Nico\ 22\ Abb3.png){height=130px} \ $\quad$ ![](Nico\ 22\ Abb2.png){height=130px}

a) Geben Sie an, um welchen Art von Funktionen es sich handelt. Begründen Sie Ihre Aussage.
b) Ermitteln Sie für einen der abgebildeten Graphen einen möglichen Funktionsterm und erklären Sie Ihr Vorgehen.
c) Der Graph der Funktion $g$ schließt im Intervall $[-1;5]$ mit der $x$-Achse eine Fläche ein. Beschreiben Sie ein Verfahren, mit dem man den Inhalt dieser Fläche berechnen kann, und geben Sie einen entsprechenden Rechenausdruck an.
d) Gegeben ist die Funktion $k(x) = -\cos(x)$ im Intervall $[-\pi;\pi]$. Berechen Sie $a$ so, dass gilt $\int_0^a k(x) dx = -1$
e) Bestimmen Sie den Scheitelpunkt $S$ der Parabel $p(x) = x^2-6x+4$. Argumentieren Sie anhand der zweiten Ableitung, dass es sich bei $S$ um ein Minimum von $p$ handelt.

\End{tcolorbox}
