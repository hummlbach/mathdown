---
title: "Lage von Ebenen zueinander"
author: [Klasse 12]
date: 17.06.2022
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Vektorgeometrie" 
---

0. Bestimmen Sie die Spurgerade von der Ebene...
   a) $E_1: x=4$ und der $x-z$-Ebene.
   b) $E_2: y=3$ und der $y-z$-Ebene.
   c) $E_6: y+2z=4$ und der $y-z$-Ebene.
   d) $E_7: 3x-2z=-6$ und der $x-z$-Ebene.
   e) $E_9: -2x+3y+3z=1$ und der $x-y$-Ebene.

1. Berechnen Sie die Schnittgerade der Ebenen $E_1$ und $E_2$.
   a) $E_1: 2x-4y+2z = 0$, $E_2: x+y-z=4$
   b) $E_1: x-4y-2z = 8$, $E_2: 3y+3z=-9$

2. Berechnen Sie die Schnittgerade der Ebenen $E_1$ und $E_2$.

   a) $E_1: \vec x = \left(\Begin{array}{c}-1\\ 2\\ -4\End{array}\right) + r \left(\Begin{array}{c}2\\ 1\\ 3\End{array}\right) + s \left(\Begin{array}{c}2\\ -1\\ 4\End{array}\right)$
, $E_2: -x+2y-2z=-5$
   b) $E_1: \vec x = \left(\Begin{array}{c}-2\\ 2\\ 0\End{array}\right) + r \left(\Begin{array}{c}3\\ -4\\ -2\End{array}\right) + s \left(\Begin{array}{c}1\\ 2\\ 5\End{array}\right)$, $E_2: x-y-2z=-4$
   c) $E_1: \vec x = \left(\Begin{array}{c}0\\ -1\\ 0\End{array}\right) + r \left(\Begin{array}{c}3\\ 2\\ 3\End{array}\right) + s \left(\Begin{array}{c}1\\ 3\\ 3\End{array}\right)$, $E_2: \vec x = \left(\Begin{array}{c}3\\ 5\\ 5\End{array}\right) + t \left(\Begin{array}{c}1\\ 1\\ 2\End{array}\right) + u \left(\Begin{array}{c}2\\ 3\\ 2\End{array}\right)$


3. Bestimmen Sie die Lage der Ebenen $E_1$ und $E_2$ zueinander.
   a) $E_1: 2x-4y+2z = 0$, $E_2: -x+2y-z=12$
   b) $E_1: \vec x = \left(\Begin{array}{c}1\\ 3\\ 2\End{array}\right) + r \left(\Begin{array}{c}1\\ -2\\ 1\End{array}\right) + s \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right)$, $E_2: \vec x = \left(\Begin{array}{c}3\\ 3\\ 6\End{array}\right) + t \left(\Begin{array}{c}4\\ 1\\ 4\End{array}\right) + u \left(\Begin{array}{c}3\\ 1\\ 2\End{array}\right)$
   c) $E_1: \vec x = \left(\Begin{array}{c}1\\ 2\\ 3\End{array}\right) + r \left(\Begin{array}{c}3\\ 4\\ 4\End{array}\right) + s \left(\Begin{array}{c}-1\\ 0\\ 1\End{array}\right)$, $E_2: 4x-7y+4z=2$
   a) $E_1: \vec x = \left(\Begin{array}{c}3\\ 7\\ 9\End{array}\right) + r \left(\Begin{array}{c}2\\ 1\\ 1\End{array}\right) + s \left(\Begin{array}{c}3\\ 2\\ 3\End{array}\right)$
, $E_2: x-3y+z=-4$


\pagebreak

## Ergebnisse

0. a) $g: \vec x = \left(\Begin{array}{c}4\\ 0\\ 0\End{array}\right) + t \left(\Begin{array}{c}0\\ 0\\ 1\End{array}\right)$
   b) $g: \vec x = \left(\Begin{array}{c}0\\ 3\\ 0\End{array}\right) + t \left(\Begin{array}{c}0\\ 0\\ 1\End{array}\right)$
   b) $g: \vec x = \left(\Begin{array}{c}0\\ 4\\ 0\End{array}\right) + t \left(\Begin{array}{c}0\\ -2\\ 1\End{array}\right)$
   b) $g: \vec x = \left(\Begin{array}{c}0\\ 0\\ 3\End{array}\right) + t \left(\Begin{array}{c}-2\\ 0\\ 3\End{array}\right)$
   b) $g: \vec x = \left(\Begin{array}{c}1\\ 1\\ 0\End{array}\right) + t \left(\Begin{array}{c}3\\ 2\\ 0\End{array}\right)$

Die restlichen Aufgaben sind aus dem Paal (V.02.03) geklaut. Die Lösungen sind auf Seite 210 bzw. 211 zu finden.

1. a) ce)
   b) ck)
2. a) cd)
   b) cj)
   c) cg)
3. a) cf) 
   b) ca)
   c) cc)
   a) ci)

