---
title: "Lösung zur Abschlussübung"
author: [Klasse 11]
date: 25.05.2021
keywords: [Geraden, Ebenen, Lage, Punktprobe, Parameterform, Normalenform]
lang: de
header-center: "Analytische Geometrie 3d" 
---

# Lösung zur Abschlussübung
## Geraden, Ebenen und deren Lage zueinander

1. Wir untersuchen die Lage der Geraden $f$, $g$ und $h$ zueinander.
   - Wir untersuchen als erstes wie $f$ und $g$ zueinander liegen. Am schnellsten sieht man, ob die Geraden parallel sind oder nicht. Daher starten wir mit dem Test auf Parallelität. Zwei Geraden sind parallel, wenn sich der Richtungsvektor der einen Gerade als Vielfaches des Richtungsvektors der anderen Gerade ausdrücken lässt. Wir suchen also nach einem $\lambda$, sodass

     $$
     \lambda \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}
     =
     \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix}
     $$

     Daraus ergeben sich drei Gleichungen:

     $$
     \begin{array}{rlr}
     \text{I:}  & 4 \cdot \lambda =& -6 \\
     \text{II:} & 1 \cdot \lambda =& 4 \\
     \text{III:}& 0 \cdot \lambda =& 2
     \end{array}
     $$

     Ergäbe sich aus allen drei Gleichungen der gleiche Wert für $\lambda$, wäre der eine Richtungsvektor ein Vielfaches des anderen und die beiden Geraden wären parallel (oder identisch). Hier allerdings ergibt sich aus der ersten Gleichung $\lambda = -\frac{3}{2}$ und aus der zweiten $\lambda = 4$. (Gleichung III ist sogar unerfüllbar, egal wie wir $\lambda$ wählen.) Die Geraden sind also nicht parallel.

     Als nächstes prüfen wir, ob sich $f$ und $g$ schneiden. Zwei Geraden schneiden sich, wenn wir für die Parameter der beiden Geraden Zahlen so wählen können, dass sich aus den beiden Parameterdarstellungen der gleiche Punkt ergibt. (D.h. Ortsvektor plus $\lambda$ mal Richtungsvektor der einen Gerade soll den gleichen Punkt ergeben, wie die Summe aus Ortsvektors und $\mu$ mal Richtungsvektor der anderen Gerade.)
     Um zu überprüfen, ob $f$ und $g$ einen gemeinsamen Punkt haben (also ob wir wie beschrieben Werte für die Parameter finden), setzen wir die Parameterdarstellungen (mit unterschiedlich benannten Parametern) gleich:

     $$\begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix} =
     \begin{pmatrix}-1 \\ -3 \\ 0\end{pmatrix} + \mu \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}$$

     Daraus ergeben sich drei Gleichungen:

     $$
     \begin{array}{rlr}
      3  &- 6 \lambda =& -1 + 4 \mu \\
      -2 &+ 4 \lambda =& -3 + 1 \mu \\
      0  &+ 2 \lambda =&  0 + 0 \mu
     \end{array}
     $$

     Bringen wir alle Parameter nach links und alle Konstanten (ohne Parameter) nach rechts und fassen zusammen, so ergeben sich die folgenden drei Gleichungen:

     $$
     \begin{array}{rllr}
     \text{I:}  & 6 \lambda + 4\mu &=&  4 \\
     \text{II:} & 4 \lambda - \mu  &=& -1 \\
     \text{III:}& 2 \lambda        &=& 0 
     \end{array}
     $$

     Lässt sich dieses (überbestimmte) Gleichungssystem lösen, so haben wir Parameter gefunden die "den beiden Parameterdarstellungen den gleichen Punkt entlocken" und die Geraden schneiden sich also. Dazu können wir uns zwei der drei Gleichungen aussuchen, das Gleichungssystem aus zwei Gleichungen mit zwei Unbekannten lösen und anschließend prüfen, ob die Lösung zur dritten Gleichung passt. (Es kann passieren, dass sich im ersten Schritt unendlich viele Lösungen ergeben...) In unserem Fall hier, ergibt sich aus der dritten Gleichung glücklicher Weise direkt $\lambda = 0$. Setzen wir dies in Gleichung II ein, so erhalten wir $\mu = 1$. Auch Gleichung I ist mit diesen beiden Werten erfüllt, sodass sich $f$ und $g$ also schneiden. Wir setzen $1$ in die Parameterdarstellung von $g$, oder $0$ in die Parameterdarstellung von $f$ ein, um den Schnittpunkt zu bestimmen und stellen fest, dass der Schnittpunkt gerade der "Aufhängepunkt" $(3|-2|0)$ von $f$ ist.

   - Wir fahren fort mit der Lage von $f$ und $h$ zueinander und starten wieder mit dem Test auf Parallelität:

     $$
     \lambda \begin{pmatrix}1,5 \\ -1 \\ -0,5\end{pmatrix}
     =
     \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix}
     $$

     Daraus ergeben sich drei Gleichungen:

     $$
     \begin{array}{rrr}
     \text{I:}  & 1,5 \lambda =& -6 \\
     \text{II:} & -1 \lambda =& 4 \\
     \text{III:}& -0,5 \lambda =& 2
     \end{array}
     $$

     Aus allen drei Gleichungen ergibt sich konsistent $\lambda = -4$. Der Richtungsvektor von $f$ ist also das $4$-fache des Richtungsvektor von $h$ und die beiden Geraden sind entweder parallel oder gar identisch. Um auszuschließen, dass die beiden identisch sind, testen wir, ob der "Aufhängepunkt" $(0|0|-2)$ von $h$ auf $f$ liegt. Dazu setzen wir die Parameterdarstellung von $f$ mit $(0|0|-2)$ gleich. Finden wir einen "Parameterwert" für $\lambda$, der den Richtungsvektor so skaliert, dass der Ortsvektor von $f$ plus $\lambda$ mal Richtungsvektor von $f$ den Ortsvektor von $h$ ergibt, so müssen die Geraden identisch sein (ansonsten parallel).

     $$\begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix} =
     \begin{pmatrix}0 \\ 0 \\ -2\end{pmatrix}$$

     Daraus ergeben sich drei Gleichungen:

     $$
     \begin{array}{rr}
      3  - 6 \lambda =& 0 \\
      -2 + 4 \lambda =& 0 \\
      0  + 2 \lambda =& -2
     \end{array}
     $$

     Bringen wir alle Konstanten auf eine Seite, erhalten wir die folgenden drei Gleichungen:

     $$
     \begin{array}{rlr}
     \text{I:}  & 6 \lambda =& 3 \\
     \text{II:} & 4 \lambda =& 2 \\
     \text{III:}& 2 \lambda =& -2 
     \end{array}
     $$

     Die ersten beiden Gleichungen "sagen" $\lambda = \frac{1}{2}$ und widersprechen damit Gleichung III, die $\lambda = -1$ "sagt". $(0|0|-2)$ liegt somit nicht auf $f$, die beiden können nicht identisch sein und sind also parallel.

   - Zuletzt untersuchen wir noch wie $g$ und $h$ zueinander liegen. Da $f$ und $h$ parallel sind, $f$ und $g$ aber nicht, können auch $g$ und $h$ nicht parallel sein. Wir überprüfen, ob sich $g$ und $h$ schneiden, indem wir sie gleichsetzen:

     $$\begin{pmatrix}-1 \\ -3 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}
     = \begin{pmatrix}0 \\ 0 \\ -2\end{pmatrix} + \mu \begin{pmatrix}1,5 \\ -1 \\ -0,5\end{pmatrix}$$

     Es ergeben sich wieder drei Gleichungen:
     $$
     \begin{array}{rcr}
     -1 + 4 \lambda &=& 0 + 1,5 \mu \\
     -3 + 1 \lambda &=&  0 -1 \mu \\
     0  + 0 \lambda &=& -2 -0,5 \mu
     \end{array}
     $$

     Wir bringen die Parameter nach links und fassen die Konstanten rechts zusammen:

     $$
     \begin{array}{lrcr}
     \text{I:}& 4 \lambda -1,5 \mu &=& 1 \\
     \text{II:}& \lambda + \mu &=& 3 \\
    \text{III:}& 0,5 \mu &=& -2
     \end{array}
     $$

     Nach der untersten Gleichung ist $\mu = -4$. Damit ergibt sich aus der zweiten Gleichung $\lambda = 7$. Diese Lösung der unteren beiden Gleichungen, löst allerdings Gleichung I nicht: $4\cdot 7-1,5\cdot 4 = 22 \neq 1$. Das Gleichungssystem hat also keine Lösung und $g$ und $h$ sind weder parallel noch schneiden sie sich. Daraus folgt $g$ und $h$ müssen windschief sein.

2. Den Schnittpunkt von $f$ und $g$ haben wir bereits in 1. berechnet. Der Schnittwinkel zwischen den Geraden ist der Schnittwinkel zwischen ihren Richtungsvektoren. Diesen Winkel erhalten wir aus der Gleichung $\cos{\alpha} = \frac{\vec v\circ \vec w}{|\vec v |\cdot |\vec w|}$:
   $$
   \begin{array}{ll}
   \alpha &= \arccos{\left(\frac{\begin{pmatrix}-6\\4\\2\end{pmatrix}\circ\begin{pmatrix}4\\1\\0\end{pmatrix}}{\left |\begin{pmatrix}-6\\4\\2\end{pmatrix}\right | \cdot \left | \begin{pmatrix}4\\1\\0\end{pmatrix} \right |}\right)}
      = \arccos{\frac{-24+4+0}{\sqrt{36+16+4}\cdot\sqrt{16+1+0}}}
          = \arccos{\frac{-20}{\sqrt{56}\cdot\sqrt{17}}} \approx 130,41^\circ
   \end{array}
   $$

3. Um die Lage einer Gerade zu einer Ebene in Normalenform zu erörtern, können wir Ortsvektor plus $\lambda$ mal Richtungsvektor zu einem Vektor zusammenfassen und dessen erste Komponente für $x$, die zweite Komponente für $y$ und die dritte Komponente für $z$ in die Ebenen-Normalform einsetzen. Ist die sich ergebende Gleichung unabhängig von $\lambda$ immer erfüllt, liegt die Gerade in der Ebene. Finden wir genau einen Wert für $\lambda$, der die Gleichung löst, so schneiden sich Gerade und Ebene in einem Punkt. Gibt es keinen Wert für $\lambda$, der die Gleichung löst, so schneiden sich Gerade und Ebene nicht und die Gerade läuft parallel zur Ebene.
   - Wir starten mit $f$, fassen die Parameterform zu einem Vektor zusammen
     $$\begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix} =
       \begin{pmatrix}3 -6 \lambda\\ -2 +4 \lambda \\ 2\lambda \end{pmatrix}$$
     und setzen diesen in die Normalenform von $E$ ein:
     $$7(3-6\lambda)+5(-2+4\lambda)+11\cdot2\lambda=-22$$
     Wir multiplizieren die linke Seite aus und fassen zusammen:
     $$21-42\lambda-10+20\lambda+22\lambda=11\neq -22$$
     $E$ und $f$ sind also parallel.

   - Wir verfahren analog mit $g$, fassen zusammen
     $$\begin{pmatrix}-1 \\ -3 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}
     = \begin{pmatrix}-1 + 4\lambda \\ -3 + \lambda \\ 0 \end{pmatrix}$$
     und setzen in die Normalenform von $E$ ein (Seiten der Normalenform getauscht):
     $$-22 = 7(-1+4\lambda)+5(-3+\lambda)+11\cdot0=-7+4\lambda-15+5\lambda = -22 +9\lambda$$
     Für $\lambda=0$ ist die Gleichung erfüllt, d.h. $E$ und $g$ schneiden sich im Punkt $(-1|-3|0)$.

   - Gleiches Prozedere noch mit $h$:
     $$\begin{pmatrix}0 \\ 0 \\ -2\end{pmatrix} + \lambda \begin{pmatrix}1,5 \\ -1 \\ -0,5\end{pmatrix}
     = \begin{pmatrix}1,5\lambda \\ -\lambda \\ -2-0,5\lambda\end{pmatrix}$$
     Eingesetzt in die Normalenform von $E$:
     $$7\cdot 1,5 \lambda+5\cdot (-\lambda)+11(-2-0,5\lambda)=10,5\lambda-5\lambda-22-5,5\lambda = -22$$
     Diese Gleichung ist (unabhängig von $\lambda$) immer erfüllt. $h$ liegt also in $E$.

4. Die Punktprobe mit einer Ebene in Normalenform führt man durch, indem man den Punkt in die Normalenform einsetzt.
   - $A\in E$? \newline
     $$7\cdot (-4)+5\cdot (-1) + 11 \cdot 1 = -28-5+11=-22$$
     $\Rightarrow \quad A$ liegt also in $E$.
   - $B\in E$? \newline
     $$7\cdot (-3)+5\cdot 2 + 11 \cdot 2 = -21+10+22=11 \neq -22$$
     $\Rightarrow \quad B$ liegt also nicht in $E$.

   Wir überprüfen, ob ein Punkt auf einer Gerade liegt, indem wir die Parameterdarstellung der Gerade mit dem Punkt gleichsetzen. Finden wir einen Parameter, sodass sich aus der Parameterdarstellung der Gerade der Punkt ergibt, so liegt der Punkt auf der Gerade (andernfalls nicht).
   - $A\in f$? \newline
     $$\begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix} =
     \begin{pmatrix}-4 \\ -1 \\ 1\end{pmatrix}$$
     Bringen wir die Konstanten nach rechts, so ergibt sich das folgende Gleichungssystem aus $3$ Gleichungen mit nur einer Unbekannten:
     $$
     \begin{array}{rlr}
     \text{I:}  &-6 \lambda =& -7 \\
     \text{II:} & 4 \lambda =& 1 \\
     \text{III:}& 2 \lambda =& 1 
     \end{array}
     $$
     Die Gleichungen widersprechen sich, was bedeutet, dass wir $\lambda$ nicht so wählen können, dass aus der Parameterform von $f$ der Punkt $A$ "herauspurzelt". $A$ liegt also nicht auf $f$.

   - $B\in f$? \newline
     $$\begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix} =
     \begin{pmatrix}-3 \\ 2 \\ 2\end{pmatrix}$$
     Wir bringen die Konstanten wieder nach rechts und erhalten das Gleichungssystem:
     $$
     \begin{array}{rlr}
     \text{I:}  &-6 \lambda =& -6 \\
     \text{II:} & 4 \lambda =& 4 \\
     \text{III:}& 2 \lambda =& 2 
     \end{array}
     $$
     Aus allen drei Gleichungen ergibt sich $\lambda=1$. Setzen wir also $\lambda=1$ in die Parameterform von $f$ ein, erhalten wir $B$. Der Punkt $B$ liegt also auf $f$.

5. a) Wir prüfen, ob eine Gerade parallel zu einer Ebene in Parameterdarstellung ist, indem wir versuchen den Richtungsvektor der Gerade durch die Richtungsvektoren der Ebene auszudrücken. Im Fall von $E'$ und $f$ sieht das so aus:
      $$\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
      \begin{pmatrix}-6\\ 4\\ 2\end{pmatrix}$$
      Es ergeben sich drei Gleichungen mit zwei Unbekannten:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu-3\lambda &=& -6\\
      \text{II:} &+7\mu +2\lambda &=& 4\\
      \text{III:}&\lambda         &=& 2
      \end{array}
      $$
      Aus den beiden unteren Gleichungen folgt $\lambda=2$ und $\mu=0$. Das passt auch zu Gleichung I, und $E'$ und $f$ müssen parallel sein, oder $f$ muss gar in $E'$ liegen. Um das zu entscheiden, prüfen wir, ob der Ortsvektor von $f$ in $E'$ liegt:
      $$\begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
      \begin{pmatrix}3\\ -2\\ 0\end{pmatrix}$$
      Wir dürfen wieder ein Gleichungssystem lösen:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu-3\lambda  +2 &=& 3\\
      \text{II:} & +7\mu +2\lambda -5 &=& -2\\
      \text{III:}& \lambda         -1 &=& 0
      \end{array}
      $$
      Nach Gleichung III ist also $\lambda=1$, aber Gleichung I und II widersprechen sich was $\mu$ anbelangt, sodass $f$ in der Tat parallel zu $E'$ ist und nicht in $E'$ liegt. \newline
      Wir wissen $h$ ist parallel zu $f$. D.h. $h$ muss auch parallel zu $E'$ sein oder in $E'$ liegen. Wir prüfen also wieder, ob der Ortsvektor von $h$ in $E'$ liegt:
      $$\begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
      \begin{pmatrix}0\\ 0\\ -2\end{pmatrix}$$
      Und lösen wieder ein Gleichungssystem...
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu-3\lambda  +2 &=&  0\\
      \text{II:} & +7\mu +2\lambda -5 &=& 0\\
      \text{III:}&\lambda          -1 &=&  -2
      \end{array}
      $$
      Wir setzen $\lambda = -1$ in I und II ein:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu-3\cdot(-1)  +2 &=& 0\\
      \text{II:} & +7\mu +2\cdot(-1) -5 &=& 0\\
      \end{array}
      $$
      Und bringen die Konstanten auf die rechte Seite:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu &=&  -5\\
      \text{II:} & +7\mu &=& 7\\
      \end{array}
      $$
      Das Gleichungssystem wird also von $\lambda=-1$ und $\mu=1$ gelöst, $(0|0|-2)$ (der Ortsvektor von $h$) liegt in $E'$ und somit liegt ganz $h$ in $E'$ (da der Richtungsvektor von $h$ parallel zu $E'$ ist). \newline
      Schließlich untersuchen wir noch die Lage von $g$ zu $E'$. Wir prüfen wieder zuerst auf Parallelität:
      $$\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
      \begin{pmatrix}4\\ 1\\ 0\end{pmatrix}$$
      Es ergeben sich wieder drei Gleichungen mit zwei Unbekannten:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -5\mu-3\lambda &=& 4\\
      \text{II:} &+7\mu +2\lambda &=& 1\\
      \text{III:}&\lambda         &=& 0
      \end{array}
      $$
      Das Gleichungssystem ist nicht lösbar, d.h. $g$ und $E'$ sind nicht parallel (oder identisch) und müssen sich somit schneiden.
   b) Um den Schnittpunkt von $E'$ und $g$ zu finden, müssen wir ihre Parameterdarstellungen gleichsetzen (wichtig: unterschiedliche Parameter verwenden, nicht zweimal $\lambda$ o.ä.):
      $$\begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
     \begin{pmatrix}-1 \\ -3 \\ 0\end{pmatrix} + \kappa \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}$$
      Wir lösen das Gleichungssystem: 
      $$
      \begin{array}{lrcr}
      \text{I:}  & -4\kappa -5\mu -3\lambda &=& -3\\
      \text{II:} & -\kappa  +7\mu +2\lambda  &=&  2\\
      \text{III:}&                  \lambda &=&  1
      \end{array}
      $$
      Wir setzen $\lambda = 1$ in I und II ein:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -4\kappa -5\mu &=& 0\\
      \text{II:} & -\kappa  +7\mu &=& 0\\
      \end{array}
      $$
      I $-4\cdot$II:
      $$
      \begin{array}{lrcr}
      \text{I:}  & -33\mu &=& 0\\
      \end{array}
      $$
      $\Rightarrow \quad \mu = \kappa = 0$ und der Schnittpunkt von $E'$ und $g$ ist $(-1|-3|0)$.
   c) Die Punktprobe mit einer Ebene in Parameterdarstellung läuft analog zur Punktprobe mit einer Gerade in Parameterdarstellung (Gleichsetzen):
      - $A\in E'$?
        $$\begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
     \begin{pmatrix}-4 \\ -1 \\ 1\end{pmatrix}$$ 
        Gleichungssystem lösen die Drölfhundertzweiunddranzigste:
        $$
        \begin{array}{lrcr}
        \text{I:}  & -5\mu -3\lambda &=& -6\\
        \text{II:} & +7\mu +2\lambda &=&  4\\
        \text{III:}&         \lambda &=&  2
        \end{array}
        $$
        $\Rightarrow \quad \lambda =2$ und $\mu=0$ löst das Gleichungssystem, $A$ liegt also in $E'$. \newline
      - $B\in E'$?
        $$\begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\ 2\\ 1\end{pmatrix}+\mu \begin{pmatrix}-5\\ 7\\ 0\end{pmatrix}=
       \begin{pmatrix}-3 \\ 2 \\ 2\end{pmatrix}$$ 
        $$
        \begin{array}{lrcr}
        \text{I:}  & -5\mu -3\lambda &=& -5\\
        \text{II:} & +7\mu +2\lambda &=&  7\\
        \text{III:}&         \lambda &=&  3
        \end{array}
        $$
        $\Rightarrow \quad$ Das Gleichungssystem ist nicht lösbar und $B$ liegt nicht in $E'$.

6. Um eine Ebene in Parameterform auf Normalenform zu bringen, brauchen wir zunächst einen Vektor, der senkrecht (normal) zur Ebene steht - ein sogenannter Normalenvektor der Ebene. Ein Vektor steht genau dann senkrecht zur Ebene, wenn er senkrecht zu den beiden Richtungsvektoren $\vec v$ und $\vec w$ steht, das Skalarprodukt mit diesen also $0$ ist. D.h. wir suchen einen Vektor $\vec n$ sodass:
    $$v_1\cdot n_1 + v_2 \cdot n_2 + v_3 \cdot n_3 = v \circ n = 0 \qquad \text{ und } \qquad w_1\cdot n_1 + w_2 \cdot n_2 + w_3 \cdot n_3 = w\circ n = 0$$
    Entweder Sie lösen dieses Gleichungssystem (es ergibt sich eine Gerade als Lösung), oder Sie kennen bereits das Vektorprodukt und wissen, dass das Vektorprodukt aus zwei (linear unabhängigen) Vektoren $\vec v$ und $\vec w$ einen Vektor ergibt, der auf den beiden Vektoren $\vec v$ und $\vec w$ senkrecht steht. \newline
    Haben wir den Normalenvektor berechnet, so können wir mit diesem und dem Ortsvektor aus der Parameterdarstellung der Ebene, die Normalenform der Ebene aufstellen. \newline
    Wir berechnen also zunächst das Vektorprodukt der beiden Richtungsvektoren:
    $$\begin{pmatrix}-3\\2\\1\end{pmatrix}\times\begin{pmatrix}-5\\7\\0\end{pmatrix}= \begin{pmatrix}2\cdot 0 - 1\cdot 7 \\ (-3)\cdot 0 - 1 \cdot (-5) \\ (-3) \cdot 7 - 2 \cdot (-5) \end{pmatrix} = \begin{pmatrix}-7 \\ -5 \\ -11\end{pmatrix}$$
    Und stellen mit diesem und dem Ortsvektor aus der Parameterdarstellung von $E'$ die Normalenform auf. Ein Punkt $(x,y,z)$ liegt genau dann auf der Ebene, wenn der Verbindungsvektor vom Ortsvektor zu $(x,y,z)$ senkrecht zum Normalenvektor ist, d.h. genau dann, wenn das Skalarprodukt von Ortsvektor minus $(x,y,z)$ mit dem Normalenvektor $0$ ist.
    $$0 = \left(\begin{pmatrix}2 \\ -5 \\ -1\end{pmatrix} -
    \begin{pmatrix}x \\ y \\ z\end{pmatrix} \right) \circ
    \begin{pmatrix}-7 \\ -5 \\ -11\end{pmatrix} = -14 +7x + 25 + 5y + 11 + 11z = 7x+5y+11z+22$$
    $\Rightarrow E$ und $E'$ sind also identisch.

