---
title: "Abschlussübung"
author: [Klasse 11]
date: 20.05.2021
keywords: [Geraden, Ebenen, Lage, Punktprobe, Parameterform, Normalenform]
lang: de
header-center: "Analytische Geometrie 3d" 
---

1. Bestimmen Sie die Lage der folgenden drei Geraden zueinander:

   $f: \vec X = \begin{pmatrix}3 \\ -2 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}-6 \\ 4 \\ 2\end{pmatrix}$ \newline
   $g: \vec X = \begin{pmatrix}-1 \\ -3 \\ 0\end{pmatrix} + \lambda \begin{pmatrix}4 \\ 1 \\ 0\end{pmatrix}$ \newline
   $h: \vec X = \begin{pmatrix}0 \\ 0 \\ -2\end{pmatrix} + \lambda \begin{pmatrix}1,5 \\ -1 \\ -0,5\end{pmatrix}$

2. Berechnen Sie Schnittpunkt und Schnittwinkel der zwei sich schneidenden Geraden aus 1.

3. Weiter sei die Ebene $E$ in Normalenform gegeben:
   $$E: 7x+5y+11z=-22$$
   Wie liegen die Geraden $f$, $g$ und $h$ zu $E$? Bestimmen Sie den Schnittpunkt mit der Gerade die $E$ schneidet.

4. Liegen die Punkte $A(-4|-1|1)$ und $B(-3|2|2)$ auf $E$ und/oder auf $f$?

5. Erörtern Sie die Lage von
   $$E': \vec X = \begin{pmatrix}2\\-5\\-1\end{pmatrix}+\lambda\begin{pmatrix}-3\\2\\1\end{pmatrix}+\mu\begin{pmatrix}-5\\7\\0\end{pmatrix}$$
   a) Wie liegen $f$, $g$ und $h$ zu $E'$?
   b) Bestimmen Sie den Schnittpunkt mit der Gerade die $E'$ schneidet.
   c) Liegt der Punkt $A$ oder der Punkt $B$ (oder beide) auf $E'$?

6. Bringen Sie die Ebene $E'$ auf Normalenform.

\pagebreak

![](1_ebene_2_punkte_3_geraden.png){}

