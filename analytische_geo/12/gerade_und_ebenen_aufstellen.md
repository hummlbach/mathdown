---
title: "Aufstellen von Geraden und Ebenen"
author: [Klasse 11]
date: 12.07.2021
keywords: [Parameterform, Normalenform, Gerade, Ebene]
lang: de
header-center: "Analytische Geometrie 3d" 
---

1. Auf der Gerade $g$ liegen die Punkte $A(1|1|2)$ und $B(1|1|0)$.
   a) Stellen Sie die Parameterform von $g$ auf.
   b) Zeichnen Sie $g$ in ein Koordinatensystem ein.
2. Die Ebene $E_1$ enthalte die Punkte $A$, $B$ und $C(2|0|0)$.
   a) Stellen Sie die Parameterform von $E_1$ auf.
   b) Zeichnen Sie $E_1$ in ein Koordinatensystem ein.
3. Die Ebene $E_2$ enthalte den Punkt $D(0|2|0)$ und sei senkrecht zu $\vec n = (1|1|0)$.
   a) Stellen Sie die Normalen/Koordinatenform von $E_2$ auf.
   b) Zeichnen Sie $E_2$ ins Koordinatensystem ein.
4. Die Ebene $E_3$ enthalte die Punkte $C$, $P(-1|1|0)$ und sei parallel zu $g$.
   a) Stellen Sie die Parameterform von $E_3$ auf.
   b) Stellen Sie die Normalen- bzw. Koordinatenform von $E_3$ auf.
   c) Zeigen Sie, dass $E_3$ tatsächlich parallel zu $g$ verläuft.
5. Die Ebene $E_4$ sei senkrecht zu $g$ und enthalte den Punkt $A$.
   a) Stellen Sie die Normalen- bzw. Koordinatenform von $E_4$ auf.
   b) Weisen Sie nach, dass $E_4$ tatsächlich senkrecht zu $g$ ist.

