---
title: "Abschlussübungsblatt"
author: [Klasse 11]
date: 26.11.2020
keywords: [Koordinatensystem, Geraden]
lang: de
header-center: "Koordinatensystem, Geraden" 
---

## I Starters

![](koordinatensystem_quadrant1.png){height=500px}

Zeichnen Sie in das Koordinatensystem ein:

a) In Rot die Punkte $(3,0)$, $(4,1)$, $(5,8)$, $(6,10)$, $(7,11)$, $(8,11)$, $(9,11)$, $(10,11)$, $(11,12)$, $(12,19)$, $(13,42)$, $(14,100)$
b) In Blau die Punkte $(3,0)$, $(4,4)$, $(5,29)$, $(6,46)$, $(7,54)$, $(8,56)$, $(8,57)$, $(10,57)$, $(11,59)$, $(12,68)$, $(13,113)$, $(14,168)$

## II Hauptgang

Gegeben seien die Gleichungen G1: $y=2,5x-1,5$ und G2: $y=-x+2$.

a) Wo schneidet die Gerade gegeben durch G1 die y-Achse?
b) Was ist die Steigung der Gerade gegeben durch G2?
c) Zeichnen Sie die Geraden in ein Koordinatensystem ein.
d) In welchem Punkt schneiden sich die beiden Geraden?

e\*) Wo schneidet die Gerade gegeben durch $y=4x-12$ die $x$-Achse?

f\*) Was ist der Winkel zwischen der $x$-Achse und der Gerade gegeben durch G1?

 
## III Dessert

Neben dem kartesischen Koordinatensystem gibt es auch andere Koordinatensysteme. Alternativ zur Angabe von $x$- und $y$-Wert, kann man einen Punkt auch festlegen, durch die Entfernung zum Ursprung und den Winkel zur $x$-Achse entgegen dem Uhrzeigersinn:

![](polarkoordinaten.png){height=300px}

Welche Form bekommt die Kreisscheibe mit Radius $3$, wenn man (in einem neuen Koordiantensystem) an die senkrechte Achse den Winkel zur $x$-Achse (im ursprünglichen Koordinatensystem) entgegen dem Uhrzeigersinn anträgt, und an der waagrechten Achse den Radius?

