---
title: "Übungsblatt 3"
author: [Klasse 11]
date: 01.02.2021
keywords: [Parabel, Quadratische Ergänzung, Scheitelpunktsform]
lang: de
header-center: "Analytische Geometrie" 
---

## Zeichnen Sie ...

A) ... die Parabel $y=\frac{2}{5}x^2-8x+21$. Gehen Sie dabei schrittweise wie folgt vor:

   1. Bringen Sie die Konstante ($+21$) auf die linke Seite.
   2. Klammern Sie den Vorfaktor von $x^2$ (also $\frac{2}{5}$) aus.
   3. Führen Sie die Quadratische Ergänzung durch. D.h. fügen Sie wie folgt eine passende "Närrische Null" ein: halbieren Sie den Vorfaktor von $x$, quadrieren Sie ihn und addieren und subtrahieren Sie ihn. (Der Vorfaktor von $x$ ist nach dem Ausklammern $-20$.)
   4. Wenden Sie die erste oder zweite binomische Formel "rückwärts" an. (Ziel ist also etwas in der Form $(x+a)^2$ bzw. $(x-a)^2$. Dabei finden Sie $a$, indem Sie den Vorfaktor von $x$ durch $2$ teilen.)
   5. Multiplizieren Sie den Vorfaktor ($\frac{2}{5}$) und die (äußere) Klammer aus und fassen Sie die Konstanten auf der rechten oder linken Seite zusammen. (Also z.B. $+21$ auf beiden Seiten.)
   6. Lesen Sie den Scheitelpunkt ab, legen Sie entsprechend ein Koordinatensystem an und zeichnen Sie den Scheitelpunkt ins Koordinatensystem ein...


B) ... die Parabel $y= -\frac{4}{5}x^2 +8x-11$ in ein Koordinatensystem.

\pagebreak

## Lösung zu A

- Zwischenergebnisse:

   1. $y-21 = \frac{2}{5}x^2-8x$
   2. $y-21 = \frac{2}{5}\left(x^2-20x\right)$
   3. $y-21 = \frac{2}{5}\left(x^2-20x+100-100\right)$
   4. $y-21 = \frac{2}{5}\left((x-10)^2-100\right)$
   5. $y-21 = \frac{2}{5}(x-10)^2-40$

- Scheitelpunktsform:
  $$y = \frac{2}{5} (x-10)^2 - 19$$
  bzw.
  $$y+19 = \frac{2}{5} (x-10)^2$$
  der Scheitelpunkt ist also $S(10,-19)$.

