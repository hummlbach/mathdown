---
title: "Koordinatensystem"
author: [Klasse 11]
date: 26.09.2022
keywords: [Koordinatensystem]
lang: de
header-center: "Analytische Geometrie" 
geometry:
  - top=3cm
  - bottom=3.5cm
  - left=2cm
  - right=2cm
---

## I Starters

\Begin{center}
![](fuenfstern.png){height=300px}
\End{center}

a) Geben Sie die Koordinaten der Punkte $A$, $B$, $C$, $D$, und $E$ an.
b) Zeichnen Sie ein kartesisches Koordinatensystem sodass $1$ LE einem Zentimeter (also zwei Kästchen) entspricht und beschriften Sie es entsprechend.
c) Zeichnen Sie die Punkte mit den Koordinaten $(0|1,5)$, $(1|2)$, $(2|1,75)$, $(2,5|1)$, $(2|0)$, $(1|-1)$ und $(0|-2)$ ins Koordinatensystem ein und spiegeln Sie die Punkte dann an der $y$-Achse.

## II Hauptgang

1. Spiegelung an der $x$-Achse
   a) Beschriften Sie die Punkte $A(1|2)$, $B(-3|2)$, $C(-4|-4)$, und $D(0|4,5)$ im Koordinatensystem.
   b) Spiegeln Sie die Punkte $A$, $B$, $C$ und $D$ an der $x$-Achse.

2. Beschreiben Sie wie Sie einen Punkt anhand seiner Koordinaten an der $y$-Achse spiegeln.

3. Zeichnen Sie $3$ Punkte (abseits der Achsen) in ein Koordinatensystem ein und spiegeln Sie die Punkte (graphisch) am Ursprung. Gegeben sei ein beliebiger Punkt $P=(x_P|y_P)$. Sei $Q$ nun der Punkt, den man erhält, wenn man $P$ am Ursprung spiegelt. Geben Sie die Koordinaten von $Q$ an. (Anders gefragt: wie ändern sich die Koordinaten, wenn man einen Punkt am Ursprung spiegelt?)

4. Koordinaten vertauschen
   a) Zeichnen Sie alle Punkt ins Koordinatensystem ein, die sich nicht ändern, wenn man ihre $x$- und $y$-Koordinaten vertauscht.
   b) Benennen Sie die geometrische Operation, die dem Vertauschen von $x$- und $y$-Koordinate entspricht.

## III Dessert

Neben dem kartesischen Koordinatensystem gibt es auch andere Koordinatensysteme. Alternativ zur Angabe von $x$- und $y$-Wert, kann man einen Punkt auch festlegen, durch die Entfernung zum Ursprung und den Winkel zur $x$-Achse entgegen dem Uhrzeigersinn - man spricht dann von Polarkoordinaten.

![](polarkoordinaten.png){height=230px} \ ![](winkel_radius.png){height=230px}

Wir tragen nun den Winkel der Polarkoordinaten in $x$-Richtung und den Radius entlang der $y$-Achse an (wie oben rechts zu sehen).
Der Punkt $(0|2)$ hätte in diesem Koordinatensystem die Koordinaten $(90|2)$ ($90^\circ$ Auslenkung zu $x$-Achse und $2$ vom Ursprung entfernt).

a) Zeichnen Sie die Punkte $(-3|0)$ und $(1|1)$ ins Koordinatensystem (oben rechts) ein.
b) Wie sieht in diesem Koordinatensystem eine Kreislinie mit Radius $3$ aus?
c) Zeichnen Sie einen Halbkreis mit Radius $2$ ins Koordinatensystem ein.
