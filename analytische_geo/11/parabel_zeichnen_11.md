---
title: "Übungsblatt 2"
author: [Klasse 11]
date: 25.01.2021
keywords: [Parabeln, Scheitelform, Normalform]
lang: de
header-center: Analytische Geometrie
---

## I Starters

Zeichnen Sie die folgenden Parabeln in ein Koordinatensystem ein:

   a) $p_1: y=-x^2+3$
   b) $p_2: y=(x+3)^2+2$
   c) $p_3: y=2x^2$
   d) $p_4: y=-\frac{1}{2}(x-4)^2+3$


## II Hauptgang

1. Zeichnen Sie die folgenden Parabeln in ein weiteres Koordinatensystem ein:

   a) $q_1: y=x^2-6x+9$
   b) $q_2: y=x^2+4x+5$
   c) $q_3: y=x^2-4x-1$
   d) $q_4: y=\frac{1}{4}x^2+2x+2$

    
2. Zeichnen Sie die Parabel $f: y=-2x^2+4x$ in ein Koordinatensystem ein.

3. Zeichnen Sie die Parabel $g: y=\frac{1}{2}x^2-x+\frac{1}{2}$ in ein Koordinatensystem.

## III Dessert

Berechnen Sie die Schnittpunkte von ...

   a) ... $f$ mit der $y$-Achse
   b) ... $g$ mit der $y$-Achse
   c) ... $g$ mit der $x$-Achse
   d) ... $f$ mit der $x$-Achse
   e) ... $f$ und $g$

