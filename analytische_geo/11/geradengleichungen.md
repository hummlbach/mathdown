---
title: "Bunter Straus Geraden"
author: [Klasse 11]
date: 10.05.2022
keywords: [Zweipunkteform, Punkt-Steigungs-Form, Parallele, Normale]
lang: de
header-center: Analytische Geometrie
---

1. Zweipunkteform \newline
   Stellen Sie die Gleichung der Gerade auf, die durch die Punkte $P$ und $Q$ geht.
   \Begin{multicols}{3}
   a) $P(2|3), Q(5|9)$
   b) $P(1|4), Q(5|-4)$
   c) $P(-1|1), Q(3|3)$

   \End{multicols}

2. Punktsteigungsform \newline
   Gesucht ist eine Gerade durch den Punkt $P$ mit Steigung $m$. Zeichnen Sie die Gerade und stellen Sie die Geradengleichung auf.
   \Begin{multicols}{3}
   a) $P(4|3), m=0$
   b) $P(-2|-3), m=3$
   c) $P(1|3), m=-2,5$

   \End{multicols}
  
3. Parallelen und Normalen \newline
   Geben Sie eine Gerade an, die durch den angegebenen Punkt $O$ geht und...
   a) parallel zur Gerade aus 1b) ist. $O(5|0)$
   b) senkrecht zur Gerade aus 2a) ist. $O(0|3)$
   c) parallel zur Gerade aus 1c) ist. $O(-2|-4)$
   d) senkrecht zur Gerade aus 2c) ist. $O(1|-3)$

4. Gleichung aus dem Schaubild lesen \newline
   Stellen Sie die Gleichungen zu den Geraden aus dem Schaubild auf.

\Begin{center}
![](geraden.png){height=250px}

\End{center}

5. Schnittpunkte ermitteln
   a) Berechnen Sie die Achsenschnittpunkte der Geraden aus 1a), 2b) und 3c).
   b) Berechen Sie die Schnittpunkte der Geraden aus 4.

$$\text{ }$$
