---
title: "Übungsblatt 1"
author: [Klasse 11]
date: 17.01.2021
keywords: [Koordinatensystem, Geraden]
lang: de
header-center: "Analytische Geometrie" 
---

## I Starters

Zeichnen Sie die folgenden Parablen in ein Koordinatensystem ein:

a) $p_1: y=(x+3)^2$
b) $p_2: y-1=x^2$
c) $p_3: y-1=(x+3)^2$
d) $p_4: y=-\frac{1}{2}x^2$

## II Hauptgang

1. Zeichnen Sie die folgende Parabel in ein Koordinatensystem ein:
$$q_1: y=-\frac{1}{2}x^2+3$$
2. Liegen die folgenden Punkte auf der Parabel?
   a) $(0,3)$
   b) $(1,2)$
   c) $(-2,1)$
3. Zeichnen Sie die folgende Parabel in ein Koordinatensystem ein:
$$q_2: y=2(x-\frac{3}{2})^2-4$$

## III Dessert

1. Berechnen Sie die Schnittpunkte von $q_1$ und $q_2$ mit der $y$-Achse.
2. Berechnen Sie die Schnittpunkte von $q_1$ und $q_2$ mit der $x$-Achse.

