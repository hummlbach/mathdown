---
title: "Trilateration"
author: [Klasse 11]
date: 29.09.2022
keywords: [Lineare Gleichungssysteme, Geraden, Determinante, Trilateration]
lang: de
header-center: "Analytische Geometrie" 
---

 
## Trilateration

Wie funktionieren eigentlich GPS oder das Orten von Handys?

- Indem man die Laufzeit von Funksignalen zwischen Sender und Empfänger misst (beide kennen die Uhrzeit genau; der Sender schickt einen Zeitstempel; der Empfänger erhält dann die Laufzeit aus der Differenz von Ankunftszeit und Zeitstempel), kann man auf deren Entfernung voneinander zurückschließen.
- Funksignale bewegen sich annähernd mit Lichtgeschwindigkeit, also mit fast $300000$ km/s; oder in einer für unseren Zweck besser geeigneten Einheit: $300 m/\mu s$. ($1000000$ Mikrosekunden sind $1$ Sekunde).


- Laufzeit mal Geschwindigkeit ergibt die Entfernung. Braucht das Signal zum Beispiel $4$ Mikrosekunden, so beträgt der Abstand von Sender und Empfänger ungefähr $4 \mu s \cdot 300 m/\mu s = 1200 m$


- Die Entfernung von etwas (in einer Ebene oder auf einer Kugeloberfläche zum Beispiel) zu $3$ gegebenen Punkten, bestimmt die Position des etwas, wie die Zeichnung rechts illustiert. Diese Art der Positionsbestimmung nennt man **Trilateration**.

\Begin{center}

![](Trilateration.png){width=250px}

\End{center}


Der umseitige Kartenausschnitt der Bundesnetzagentur zeigt das Gebiet südlich von Bonlanden und alle Funkmasten in der Umgebung. Die Koordinatenachsen wurden nachträglich und willkürlich aber maßstabsgetreu hinzugefügt. Die Funkmasten $27010339$, $27012252$ und $750119$ haben in diesem Koordinatensystem ungefähr die Koordinaten $A(-8,5|8,5)$, $B(0|0)$ sowie $C(0,5|-11,5)$. 

\Begin{center}

![](bonlanden_funkmasten.png){height=400px}

\End{center}

Die Laufzeiten der Funksignale zwischen einem Handy und den drei Funkmasten $27010339$, $27012252$ und $750119$ seien $7 \mu s$, $3 \mu s$ bzw. $4 \mu s$. Der Standort des Handys werde mit $H(x|y)$ bezeichnet. Ermitteln Sie, den folgenden Schritten folgend, die Koordinaten $x$ und $y$ des Handys.

a) Berechnen Sie aus den Signallaufzeiten die Entfernungen der Funkmasten zum Handy.
b) Formulieren Sie $3$ Gleichungen, die aussagen wie weit $H$ von $A$, $B$ und $C$ entfernt ist.
c) Quadrieren Sie die Gleichungen aus b) und ziehen Sie dann von der ersten und der dritten Gleichung jeweils die zweite ab. Sie erhalten ein lineares Gleichungssystem.
d) Lösen Sie das folgende lineare Gleichungssystem, dass sich aus c) ergeben hat, um die Koordinaten des Handys zu erhalten:
   $$\begin{array}{rrlr}
   \text{I: }&  17x - 17y &=& 15,5 \\
   \text{II: }& x - 23y &=& 104,5
   \end{array}$$

## Tipps

\Begin{turn}{180}
Zu a) $d(A|H) = 7 \mu s \cdot 300m / \mu s = 2100m$
\End{turn}

\Begin{turn}{180}
Zu b) $\sqrt{(x+8,5)^2+(y-8,5)^2} = 2100$
\End{turn}

