---
title: "Schnittpunkte von Parabeln mit anderem Zeug"
author: [Klasse 11]
date: 11.05.2020
keywords: [Schnittpunkte, Parabeln, Geraden]
lang: de
---

# Zusammenfassung zu Schnittpunkten von Parabeln

## Rezept

1. Gleichsetzen (der rechten Seiten der Gleichungen)
2. Alles auf eine Seite bringen
3. Gleichung lösen (z.B. mittels Mitternachtsformel) liefert die $x$-Werte der Schnittpunkte
4. $x$-Werte der Schnittpunkte in eine der Gleichungen einsetzen, um $y$-Werte der Schnittpunkte zu erhalten

## Anzahl der Schnittpunkte

Je nach Lage der beiden Parabeln (bzw. der Parabel und der Gerade), die "geschnitten werden", ergeben sich kein, ein oder zwei Schnittpunkte.

### Lineare Gleichung

Ergibt sich aus dem Gleichsetzen der rechten Seiten eine lineare Gleichung (d.h. das $x^2$ fehlt bzw. fällt weg in Schritt 2), so schneiden sich die Graphen stets in einem Punkt.

**Beispiel:**

Wir berechnen die Schnittpunkte von $p: \, y=-x^2+2$ und $q:\, y=-x^2+4x-2$:

$p\cap q$: $\qquad -x^2+4x-2 = -x^2+2$

Wir addieren $x^2+2$ auf beiden Seiten und erhalten die lineare Gleichung $4x = 4$. $p$ und $q$ haben also einen Schnittpunkt bei $x=1$. Der $y$ Wert des Schnittpunktes liegt bei $y=-x^2+2 = -1^2+2=1$.
\newline

   ![](parabel_schnitt_lineare_gleichung.png){height=220px}

### Quadratische Gleichung

Ergibt sich aus dem Gleichsetzen eine quadratische Gleichung und löst man diese mit der Mitternachtsformel, so sieht man am Wert $D$ unter der Wurzel (genannt Diskriminante), wie oft sich die Graphen schneiden:

- $D > 0$: zwei Schnittpunkte
- $D = 0$: ein Schnittpunkt
- $D < 0$: kein Schnittpunkt

Beipiele...

## Alternativen zur Mitternachtsformel

to be coninued...
