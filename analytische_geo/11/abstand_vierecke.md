---
title: "Abstand und Vierecke"
author: [Klasse 11]
date: 27.09.2022
keywords: [Abstand, Kreis]
lang: de
header-center: "Analytische Geometrie" 
---

## I Starters

1. Abstände zum Ursprung
   a) Zeichnen Sie die Punkte $A(0|4)$, $B(3|0)$ und $C(3|4)$ in ein kartesisches Koordinatensystem ein.
   b) Geben Sie die Entfernung von $B$ zum Ursprung, sowie den Abstand von $B$ und $C$ an.
   c) Verwenden Sie den **Spoiler auf der Rückseite** wenn Sie keine Idee haben, um den Abstand von $C$ zum Ursprung zu berechnen.
   e) Geben Sie einen Punkt $D$ im zweiten Quadranten (abseits der Achsen) an, der den Abstand $5$, $13$, $\sqrt{2}$ oder $1$ vom Ursprung hat.
   d) Zeigen Sie, dass der Punkt $E(-2|-2)$ den Abstand $2\sqrt{2}$ vom Ursprung hat.

\Begin{multicols}{2}
2. Berechnen Sie den Abstand der Punkte...
   a) $A$ und $B$
   b) $A$ und $C$
   c) $C$ und $B$
   d) $D$ und $E$

   $\text{ }$ \newline
   ![](abstaende.png)
   
\End{multicols}

## II Hauptgang

\Begin{multicols}{2}
1. Ordnen Sie die Begriffe den geometrischen Figuren zu und begründen Sie ihre Wahl (mit Rechnungen).
   a) Eine **Raute** ist ein Viereck dessen Seiten alle gleichlang sind.
   b) Ein **Quadrat** ist ein Viereck dessen Winkel alle gleichgroß und dessen Seiten alle gleichlang sind.
   c) Ein **Drachen** ist ein Viereck das zwei Paare gleichlanger, benachbarter Seiten besitzt.

   ![](qudrat_raute_drachen.png)

\End{multicols}

\Begin{multicols}{2}
2. Ordnen Sie außerdem unbegründet die folgenden Begriffe den geometrischen Figuren zu.
   a) Ein **Rechteck** ist ein Viereck dessen Innenwinkel alle $90^\circ$ messen.
   b) Ein **Parallelogram** ist ein Viereck bei dem die gegenüberliegenden Seiten parallel sind.
   c) Ein (gleichschenkliges) **Trapez** ist ein Viereck bei dem zwei Seiten parallel zueinander liegen und die beiden anderen gleichlang sind.

   ![](rechteck_trapez_paralellogram.png)

\End{multicols}

3. Gegeben sei das (gleichschenklige) Dreieck mit den Ecken $A(0|7)$, $B(8|1)$ und $C(7|8)$
   a) Berechnen Sie die Seitenlängen des Dreiecks.
   b) Zeichnen Sie das Dreieck in ein Koordinatensystem und ermitteln Sie den Mittelpunkt $M$ der Strecke $\overline{AB}$. Sehen Sie einen Zusammenhang zwischen den Koordinaten von $A$ und $B$ und denen von $M$?
   c) Berechnen Sie den Flächeninhalt des Dreiecks $ABC$

4. a) Zeichnen Sie alle Punkte in ein kartesisches Koordinatensystem ein, die den Abstand $2$ zum Ursprung haben.
   b) Zeigen Sie rechnerisch, dass die Punkte $A(2|0)$ und $B(\sqrt{2}|\sqrt{2})$ auf dem Kreis mit Radius $2$ um den Ursprung liegen.
   c) Geben Sie eine Gleichung an, die den Kreis mit Radius $2$ um den Ursprung beschreibt.
   d) Geben Sie eine Gleichung an, die den Kreis mit Radius $1$ um den Punkt $(2|3)$ beschreibt.

## III Spoiler

1. c) Satz des Pythagoras
   d) Abstand       Tipp
      -------       -----
      $5$           Analog zu c)
      $13$          $x^2+y^2 = 13^2$
      $\sqrt{2}$    $x^2+y^2 = \sqrt{2}^2 = 2$
      $1$           $\sin{\alpha} = \frac{y}{1}$, $\cos{\alpha} = \frac{x}{1}$
