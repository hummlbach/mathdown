---
title: "Übungsblatt 4"
author: [Klasse 11]
date: 04.02.2020
keywords: [Schnittpunkte, Parabeln, Geraden]
lang: de
header-center: "Schnittpunkte von Parabeln" 
---

## I Starters

1. Lesen Sie aus dem Schaubild die Schnittpunkte heraus, zwischen

   \Begin{multicols}{2}

   a) der Gerade $g_1$ und der $y$-Achse.
   b) der Gerade $g_2$ und der $x$-Achse.
   c) der Gerade $g_1$ und der Gerade $g_2$.
   d) der Parabel $p_1$ und der $y$-Achse.
   e) der Parabel $p_1$ und der $x$-Achse.
   f) der Parabel $p_2$ und der $x$-Achse.
   g) der Parabel $p_1$ und der Gerade $g_1$.
   h) der Parabel $p_2$ und der Gerade $g_1$.
   i) der Parabel $p_1$ und der Gerade $g_2$.
   j) der Parabel $p_1$ und der Parabel $p_2$.

   \End{multicols}

   ![](schnitte_geraden_parabeln.png){height=310px}


2. Gegeben sei die Gerade $g: y = -\frac{1}{2}x+1$. Zeichnen Sie $g$ und berechnen Sie die Schnittpunkte von $g$ mit der

   a) $y$-Achse.
   b) $x$-Achse.
   c) Gerade $h: y=2$
   d) Gerade $f: y=x-5$

3. Berechnen Sie die Schnittpunkte der Parabel $q: y=x^2-2x-3$ mit den Koordinatenachsen und zeichnen Sie $q$.

\pagebreak

## II Hauptgang

1. Zeichnen Sie die Parabel $p: y=\frac{1}{4}x^2+x-3$ in ein Koordinatensystem und berechnen Sie die Schnittpunkte (falls vorhanden) von $p$ mit 

   a) der $x$-Achse
   b) der Gerade $h_1: x=-4$
   c) der Gerade $h_2: y=5$
   d) der Gerade $h_3: y=-x-7$
   e) der Gerade $h_4: y=2x-5$
   f) der Gerade $h_5: y=\frac{1}{2}x-1$

2. Gegeben seien die Parabeln

   - $q_1: y=-2x^2+7$
   - $q_2: y=(x+3)^2+1$
   - $q_3: y=-\frac{3}{4}x^2+6x-10$ und
   - $q_4: y=\frac{1}{4}x^2-2$.

   Zeichnen Sie die Parabeln in ein Koordinatensystem und berechnen Sie die Schnittpunkte von

   a) $q_1$ und $q_2$.
   b) $q_1$ und $q_4$.
   c) $q_3$ und $q_4$.
   d) $q_2$ und $q_3$.
   e) $q_2$ und $q_4$.


