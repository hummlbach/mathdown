---
title: [14 Punkte + 4 Extrapunkte]
author: "Name:"
date: 12.05.2021
keywords: [Parabeln]
lang: de
header-center: Klassenarbeit zu Parabeln
---

1. Das Schaubild zeigt die Ausschnitte von vier Parabeln.

   \Begin{multicols}{2}
   a) \Begin{multicols}{2}
      Welcher Graph gehört zur folgenden Wertetabelle? (1P)

      Begründen Sie Ihre Entscheidung.

      $x$  $y$
      --- ----
      $0$ $1$
      $1$ $0$
      $2$ $-3$

      \End{multicols}
   b) Stellen Sie die Gleichungen der verschobenen Normalparabeln $p_1$ und $p_2$ auf. (2P)
   c) Berechnen Sie die Koordinaten des Schnittpunktes $Q$ von $p_1$ und $p_2$. (2P)
   d) Wie heißt die Gleichung der Parabel $p_4$? Entnehmen Sie die dazu erforderlichen Werte dem Schaubild. (1P)

      ![](Waldorf-RAP-2015-P4.png){height=300px}


   \End{multicols}

2. Zu einer verschobenen, nach oben geöffneten Normalparabel $p$ gehört die unvollständig ausgefüllte Wertetabelle:

   $x$    $0$   $1$   $2$   $3$   $4$   $5$
   ----  ----  ----  ----  ----  ----  ----
   $y$   $11$   $6$               $3$

   Weiter sei $g$ eine Gerade durch den Punkt $P(-2,5|6)$ mit Steigung $m=-1$.

   a) Geben Sie die Gleichung der Parabel $p$ an. (2P, Zwischenergebnis: $p: y=x^2-6x+11$)
   b) Vervollständigen Sie die Wertetabelle und begründen oder zeigen Sie, dass $S(3|2)$ der Scheitelpunkt von $p$ ist. (3P)
   c) Stellen Sie die Geradengleichung von $g$ auf. (1P, Zwischenergebnis: $g: y=-x+3,5$)
   d) Weisen Sie rechnerisch nach, dass $p$ und $g$ keine gemeinsamen Punkte haben. (2P)
   e) Eine Gerade $h$ läuft parallel zur Gerade $g$ und geht durch den Scheitelpunkt von $p$. Wie lautet die Geradengleichung von $h$? (2 Extrapunkte)
   f) Berechnen Sie die Koordinaten des Schnittpunktes $R$ der Geraden $h$ mit der $x$-Achse. (2 Extrapunkte)
   
