---
title: "Kreise"
author: [Klasse 11]
date: 28.09.2022
keywords: [Kreis]
lang: de
header-center: "Analytische Geometrie" 
geometry:
  - top=3.5cm
  - bottom=3cm
---

## I Starters

\Begin{multicols}{2}

1. Zeichnen Sie die Kreise zu den folgenden Gleichungen:
   a) $\sqrt{(x-1)^2+(y-2)^2}=3$
   b) $\sqrt{(x-4,5)^2+(y+6)^2}=1,5$

2. Stellen Sie die Kreisgleichungen für die Kreise im Schaubild rechts auf.

   ![](drei_kreise.png){height=180px}

\End{multicols}

## II Hauptgang

1. Prüfen Sie, ob die Punkte $P_1$ und $P_2$ auf dem angegebenen Kreis $K$ liegt:
   a) $P_1(0|0)$, $P_2(-1|-7)$, $c: \sqrt{(x-3)^2+(y+4)^2} = 5$
   b) $P_1(\sqrt{2}|-0,5)$, $P_2(-1|-1,5)$, $c: \sqrt{x^2+(y-1,5)^2}=3$

\Begin{multicols}{2}

2. Wir betrachten den Kreis im Schaubild rechts.
   a) Geben Sie $2$ Punkte an, die auf dem Kreis liegen und belegen Sie das anhand einer Rechnung.
   b) Geben Sie $2$ Punkte an, die nicht auf dem Kreis liegen und belegen Sie das anhand einer Rechnung.

   ![](ein_kreis.png){height=180px}

\End{multicols}

3. Berechnen Sie die Schnittpunkte der Kreise $c_1$ und $c_2$.
   a) Der Kreis $c_1$ hat seinen Mittelpunkt bei den Koordinaten $(0|-1)$ und einen Radius von $2$. Der Kreis $c_2$ hat seinen Mittelpunkt bei $(1|1)$ und den Radius $1$.
   b) Der Kreis $c_1$ hat seinen Mittelpunkt bei den Koordinaten $(-1|-2)$ und einen Radius von $3$. Der Kreis $c_2$ hat seinen Mittelpunkt bei $(1|1)$ und den Radius $2$.

4. Geben Sie die Anzahl der Schnittpunkte der folgenden Kreispaare an und begründen Sie ihre Antwort.
   a) $c_1: \sqrt{(x-3)^2+(y-1)^2} = 2$, $c_2: \sqrt{(x-3)^2+(y-5)^2}=3$
   b) $c_1: \sqrt{x^2+y^2} = 2$, $c_2: \sqrt{(x+3)^2+(y+4)^2}=3$
   c) $c_1: \sqrt{(x+4)^2+(y-2)^2} = 4$, $c_2: \sqrt{(x-2)^2+(y+6)^2}=5$
   d) $c_1: \sqrt{(x-3)^2+y^2} = 6$, $c_2: \sqrt{(x-3)^2+(y-1,5)^2}=3$

## III Dessert

1. Geben Sie prosaisch die Definition eines Kreises wieder.

2. Die Form der Kreise hängt also entscheidend davon ab, wie Abstände gemessen werden. Es gibt auch andere durchaus sinnvolle Abstandsbegriffe (Normen/Metriken). Zeichnen Sie die Einheitskreise (Kreis mit Radius $1$ und dem Ursprung als Mittelpunkt) in ein Koordinatensystem für die folgenden Normen:
   a) $||(x,y)|| = \max{(x,y)}$
   b) $||(x,y)|| = |x|+|y|$
   c) $||(x,y)|| = \sqrt[4]{x^4+y^4}$

Die zentralen Forderungen für eine Abstandsmessung sind übrigens:

1) Positiv Definitheit: \newline $d(A,B) \ge 0$ und $d(A,B) = 0 \Rightarrow A = B$ \newline (Abstände sind positiv, nur der Punkt selber hat zu sich Abstand $0$)
2) Symmetrie: \newline $d(A,B) = d(B,A)$ \newline  (Von $A$ nach $B$ ists genau so weit wie von $B$ nach $A$)
3) Dreiecksungleichung: \newline $d(A,B) + d(B,C) \ge d(A,C)$ \newline  (Von $A$ nach $C$ über $B$ ists weiter als direkt von $A$ nach $C$)
