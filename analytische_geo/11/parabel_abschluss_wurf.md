---
title: "Abschlussübung"
author: [Klasse 11]
date: 29.04.2021
keywords: [Parabeln]
lang: de
header-center: Analytische Geometrie
---

Eine Gerade $g$ durch den Ursprung stelle eine Rampe dar. Vom Punkt $A(3,5|1,75)$ auf der Rampe wird ein Ball geschossen. Die Flugbahn des Balls beschreibt einen Teil eine Normalparabel $p$, und der Ball landet bei $x=2$ wieder auf der Rampe. Der Punkt $S$ bezeichne den höchsten Punkt in der Flugbahn des Balls.

1. Wie lauten die Koordinaten von $S$? (9P)
   a) Stellen Sie die Geradengleichung von $g$ auf und verwenden Sie sie, um die $y$-Koordinate des Aufschlagpunktes $B$ zu bestimmen. (Zwischenergebnis: $B(2|1)$)
   b) Wie lautet die Normalform von $p$? (Zwischenergebnis: $p: y=-x^2+6x-7$)
   c) Bringen Sie $p$ auf Scheitelpunktsform, um die Koordinaten von $S$ abzulesen. (Zwischenergebnis: $S(3|2)$)

2. Vom Punkt $B$ prallt der Ball der Parabel $q$ folgend wieder ab. Wo trifft er wieder auf die Rampe? (6P)
   a) Stellen Sie anhand des Schaubildes die Gleichung von $q$ auf.
   b) Berechnen Sie die Schnittpunkte von $g$ und $q$.

   ![](krude_flugbahnen.png){height=300px}

3. Prüfen Sie, ob die Punkte $C(2,5|1,75)$ und $D(2,5|1,5)$ auf der bis hier her beschriebenen Flugbahn des Balles liegen? (3P)

4. Wie lautet die Parabelgleichung für den ersten Teil der Flugbahn, wenn der Ball in gleicher Weise vom Punkt $D(4|2)$ aus geschossen wird? (2P)

5. Zeichnen Sie $g$ und $p$ in das Schaubild zu $q$ ein. (4P)

6. Um wieviel Grad ist die Rampe geneigt? (2 Extrapunkte)

Disclaimer: Ballistische Flugbahnen bilden zwar tatsächlich Parabeln (wenn man den Luftwiderstand vernachlässigt), der hier beschriebene Abprallvorgang ist aber wahrscheinlich physikalisch grob falsch.
