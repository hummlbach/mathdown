---
title: "Geraden aufstellen"
author: [Klasse 11]
date: 30.09.2022
keywords: [Kreis]
lang: de
header-center: "Analytische Geometrie" 
---

## I Starters

1. Ermitteln Sie den Zusammen von $x$- und $y$-Koordinaten der im Schaubild gezeigten Geraden (zum Beispiel indem Sie von jeder Gerade einige Punkte aufschreiben und den $x$-Wert mit dem $y$-Wert vergleichen).

   \Begin{multicols}{2}

   \Begin{center}

   ![](geraden_intro_1.png){height=180px}

   ![](geraden_intro_2.png){height=180px}

   \End{center}

   \End{multicols}

2. Zeichnen Sie die folgenden Geraden in ein kartesisches Gleichungssystem.
   \Begin{multicols}{2}
   a) $y = -3$
   b) $y = 3x$
   c) $y = -\frac{1}{2}x$
   d) $x = 0$
   e) $y = x + 2$
   f) $y = 2x - 1$

   \End{multicols}

3. Beschreiben Sie möglichst effizient (mit so wenig Information wie möglich) eine Gerade. \newline
   Leitfragen:
     - Durch welche (zwei) Informationen ist eine Gerade eindeutig festgelegt?
     - Was bedeuten die Zahlen in einer Geradengleichung?

## II Hauptgang

1. Geben Sie die Steigung, den $y$-Achsenabschnitt und die Gleichung der Geraden im Schaubild an.

2. Zeichnen Sie die Gerade zu den folgenden Gleichungen in ein kartesisches Koordinatensystem ein:
   a)
   b)
   c)
   d)

3. Überprüfen Sie, ob die Punkte $P_1$ und $P_2$ auf der Gerade $g$ liegen:
   a)
   b)
   c)

4. Eine Gerade mit Steigung $2$ gehe durch den Punkt $(3|4)$. Ermitteln Sie die Geradengleichung.

5. Im 2020 stieß Deutschland $8$ Millionen Tonnen $\text{CO}_2$ aus.
   a) Berechnen Sie das Jahr in dem Deutschland Klimaneutral wird, wenn der $\text{CO}_2$-Ausstoß (seitdem) jedes Jahr um $0,75$ Millionen Tonnen reduziert wird.
   b) Veranschaulichen Sie das durch eine Gerade in einem kartesischen Koordinatensystem.
