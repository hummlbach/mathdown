---
title: "Steigung von Geraden"
author: [Klasse 11]
date: 05.10.2022
keywords: [Kreis]
lang: de
header-center: "Analytische Geometrie" 
---

## I Starters

1. Steigungen herauslesen again.

2. Stellen Sie Geradengleichungen von zwei parallelen Geraden auf.

3. Gegeben seien folgende Geraden...
   a) Geben die Geraden an, die parallel zueinander sind und begründen Sie Ihre Entscheidung.
   b) Zeichnen Sie die Geraden in ein Koordinatensystem ein.

4. Gegeben seien die Geradenpaare aus den folgenden Schaubildern Steigungen und orthogonalen Geraden vergleichen.

   Entweder
   - Geraden mit reziprok-negativer Steigung zeichnen lassen oder
   - senkrechte Geraden zeichnen und Steigung herauslesen lassen.

## II Hauptgang

1. Parallele Gerade aus Punkt aufstellen.

2. Senkrechte Gerade aus Punkt aufstellen.

3. Winkel zwischen Gerade und $x$-Achse anhand des Tangens?

