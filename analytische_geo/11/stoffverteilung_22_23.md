---
title: "Stoffverteilung 22/23"
author: [Klasse 11]
date: 19.08.2021
keywords: [Analytische Ggeometrie]
lang: de
geometry:
  - top=3cm
  - bottom=3cm
---

# Epochen 11. Klasse 2022/23

## Trigonometrie Wiederholung

Von         Bis         Themen
-----       -----       ---------
12.09.      12.09.      Wiederholung Definition Sinus/Cosinus/Tangens und Übungen
13.09.      13.09.      Wiederholung Sinus und Cosinussatz
14.09.      14.09.      Umrechnung von Grad in Gon und vice versa und Vermessungsbeispiel
15.09.      15.09.      Koordinatensystem und Fehlerrechnung

## Analytische Geometrie

Von         Bis         Themen
-----       -----       ---------
27.09.      27.09.      Wiederholung Koordinatensystem
28.09.      28.09.      Abstand und Vierecke (Andere Normen als Dessert)
29.09.      29.09.      Kreise (Ellipsen als Dessert)
30.09.      30.09.      Mittelpunkte und Schnitte von Kreisen?
03.10.      07.10.      Geraden
23.01.      27.01.      Parabeln
27.02.      03.03.      Parabeln

## Analysis

Von         Bis         Themen
-----       -----       ---------
15.05.      17.05.      Was ist eine Funktion? Beispiele. Monotonie? Symmetrie?

## Letzte Epoche

Von         Bis         Themen
-----       -----       ---------
26.06.      05.07.      Projektive Geometrie? Ableitung? Vektorrechnung in 2D? Stochastik?

# Weitere Themen in den Fachstunden

- LGS mit $3$ Unbekannten und $3$ Gleichungen
- Weitere Trigonometrie-Übungen für die 11R
- Wiederholung der Zinsrechnung für doe 11R

