---
title: "Abschlussübung zu Parabeln"
author: Klasse 11
date: 11.05.2021
keywords: [Parabeln]
lang: de
header-center: Analytische Geometrie
---

  ![](parabeln_zum_abschluss.png){height=420px}

1. Lesen Sie aus dem Schaubild die Gleichung der Parabel $p$ heraus.
2. Die nach oben geöffnete Normalparabel $q$ gehe durch den Punkt $P(-1|-2)$ sowie durch den Scheitelpunkt von $p$. Stellen Sie die Gleichung zu $q$ auf.
3. Berechnen Sie den zweiten Schnittpunkt $Q$ von $p$ und $q$.
4. Füllen Sie die folgende Wertetabelle für die Parabel $q$:

   $x$   $-2$  $-1$   $0$   $1$   $2$   $3$   $4$  $5$
   ----  ----  ----  ----  ----  ----  ----  ---- ---- 
   $y$         $-2$                     $6$

5. Wo liegt der Scheitelpunkt $S$ von $q$?
6. Zeichnen Sie $q$ ins Schaubild zu $p$ ein.

