#!/bin/bash

if [ $# -ne 2 ]; then
    echo "usage: $0 <probabilities.data> <histogram.png>"
    exit 1
fi

OUT_FILE=$(realpath $2)
IN_FILE=$(basename $1 .data)
cp $1 /tmp/$IN_FILE

cd /tmp

gnuplot << EOF
set boxwidth 3.0
set style data histograms
set style histogram cluster
set style fill solid 1.0 border lt -1
set xrange [-0.5: ]
set terminal pngcairo
set output "$OUT_FILE"
plot "$IN_FILE"
EOF

rm $IN_FILE
