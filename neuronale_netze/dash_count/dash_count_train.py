import tensorflow as tf


def train(model, epochs, x_train, y_train, x_test, y_test):
    test_loss = tf.keras.metrics.Mean(name='test_loss')
    test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')

    for ep in range(epochs):
        test_loss.reset_states()
        test_accuracy.reset_states()

        train_ds = tf.data.Dataset.from_tensor_slices(
                    (x_train, y_train)).shuffle(10000).batch(32)
        test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)
        
        for images, labels in train_ds:
            model.train_step(images, labels)

        for test_images, test_labels in test_ds:
            predictions = model.predict(test_images)
            loss = model.loss_object(test_labels, predictions)
            test_loss(loss)
            test_accuracy(test_labels, predictions)

        print(f'Epoch {epoch + 1}, '
#              f'Loss: {train_loss.result()}, '
#              f'Accuracy: {train_accuracy.result() * 100}, '
              f'Test Loss: {test_loss.result()}, '
              f'Test Accuracy: {test_accuracy.result() * 100}'
              )

