from layerplot import LayerPlot
import tensorflow as tf
import numpy as np


def map_activation(activation):
    if activation == 'hardlimit':
        return hardlimit
    if activation == 'identity':
        return identity
    if activation == 'iszero':
        return iszero
    return activation

def hardlimit(x):
    cond = tf.less(x, tf.zeros(tf.shape(x)))
    out = tf.where(cond, tf.zeros(tf.shape(x)), tf.ones(tf.shape(x)))
    return out

def identity(x):
    return x

def iszero(x):
    cond = tf.equal(x, tf.zeros(tf.shape(x)))
    out = tf.where(cond, tf.ones(tf.shape(x)), tf.zeros(tf.shape(x)))
    return out

class DashCountModel(tf.keras.Sequential):
    def __init__(
            self,
            input_rows,
            input_cols,
            hidden_layer_counts,
            hidden_layer_activations,
            output_neuron_count,
            output_activation):
        my_layers=[]
        my_layers.append(tf.keras.layers.Flatten(input_shape=(input_rows,input_cols)))
        for (neuron_count, activation) in zip(hidden_layer_counts, hidden_layer_activations):
            my_layers.append(
                    tf.keras.layers.Dense(neuron_count,activation=map_activation(activation))
            )
        my_layers.append(tf.keras.layers.Dense(
                output_neuron_count, activation=map_activation(output_activation)
        ))
        super(DashCountModel, self).__init__(my_layers)
        self.optimizer = tf.keras.optimizers.Adam()
        self.loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

#    def call(self, x):
#        for layer in self.layers:
#            x = layer(x)
#        return x

    def predict(self, images):
        predictions = self(images, training=False)
        #print("Es waren "+ str(predictions[0]) + " Striche im Bild.")
        return predictions

#    def train_step(self, images, labels, loss_object, optimizer):

    @tf.function
    def my_train_step(self, images, labels):
        with tf.GradientTape() as tape:
            predictions = self(images, training=True)
            loss = self.loss_object(labels, predictions)
        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))
        return predictions, loss

    def set_weights(self, layer, neuron, weights, bias):
        current_weights = self.layers[layer].get_weights()
        current_weights[0][:,neuron] = weights
        current_weights[1][neuron] = bias
        self.layers[layer].set_weights(current_weights)

    def unflatten_weights(self, layer, neuron, row_count, col_count):
        current_weights = self.layers[layer].get_weights()
        weights = current_weights[0][:,neuron]
        weights_unflattened = np.zeros((row_count+1, col_count),np.int8)
        for i in range(row_count):
            weights_unflattened[i] = weights[i*col_count:(i+1)*col_count]
    #    weights_unflattened[row_count] = col_count*[0]
        weights_unflattened[row_count] = col_count*[current_weights[1][neuron]]
        return weights_unflattened

    def plot_layer(self, layer, neuron_count, row_count, col_count):
        plot = LayerPlot(neuron_count)
        for i in range(neuron_count):
            plot.add_neuron(i, self.unflatten_weights(layer, i, row_count, col_count))
        plot.show()
     

