import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

class LayerPlot:
    def __init__(self, neuron_count):
        self.figure = plt.figure()
        self.gridspec = gs.GridSpec(ncols=1, nrows=neuron_count)

    def add_neuron(self, neuron, weights):
        subplot = self.figure.add_subplot(self.gridspec[neuron,0])
        image = subplot.imshow(weights)
        plt.colorbar(image)
        subplot.grid(False)
        plt.yticks([])
        plt.xticks([])

    def show(self):
        #plt.colorbar()
        plt.show()


