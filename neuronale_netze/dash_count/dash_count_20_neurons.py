from dash_count_model import DashCountModel
  

def create():
    model = DashCountModel(3, 5, [20], ['hardlimit'], 5, 'iszero')
    model.set_weights( 1, 0, [3, 3,-2,-1, 0,
                             -3,-3,-2,-1, 0,
                              0, 0, 0, 0, 0], -4)

    model.set_weights( 1, 1, [-1, 1, 1,-1, 0,
                              -1,-1,-1,-1, 0,
                               0, 0, 0, 0, 0],-2)

    model.set_weights( 1, 2, [ 0,-1, 1, 1,-1,
                               0,-1,-1,-1,-1,
                               0, 0, 0, 0, 0],-2)

    model.set_weights( 1, 3, [-1, 1, 1, 1,-1,
                              -1,-1,-1,-1,-1,
                               0, 0, 0, 0, 0],-3)

    model.set_weights( 1, 4, [0,-1,-2, 3, 3,
                              0,-1,-2,-3,-3,
                              0, 0, 0, 0, 0], -4)

    model.set_weights( 1, 5, [0, 0, 0, 0, 0,
                             -3,-3,-2,-1, 0,
                              3, 3,-2,-1, 0], -4)

    model.set_weights( 1, 6, [ 0, 0, 0, 0, 0,
                              -1,-1,-1,-1, 0,
                              -1, 1, 1,-1, 0],-2)

    model.set_weights( 1, 7, [0, 0, 0, 0, 0,
                              0,-1,-1,-1,-1,
                              0,-1, 1, 1,-1], -2)

    model.set_weights( 1, 8, [ 0, 0, 0, 0, 0,
                              -1,-1,-1,-1,-1,
                              -1, 1, 1, 1,-1],-3)

    model.set_weights( 1, 9, [0, 0, 0, 0, 0,
                              0,-1,-2,-3,-3,
                              0,-1,-2, 3, 3], -4)

    model.set_weights( 1, 10,[-4,-4,-4,-1, 0,
                               4, 4,-3,-1, 0,
                              -4,-4,-4,-1, 0],-5)

    model.set_weights( 1, 11,[-1,-1,-1,-1, 0,
                              -1, 1, 1,-1, 0,
                              -1,-1,-1,-1, 0],-2)

    model.set_weights( 1, 12,[ 0,-1,-4,-4,-4,
                               0,-1,-3, 4, 4,
                               0,-1,-4,-4,-4],-5)

    model.set_weights( 1, 13,[ 0,-1,-1,-1,-1,
                               0,-1, 1, 1,-1,
                               0,-1,-1,-1,-1],-2)

    model.set_weights( 1, 14,[-1,-1,-1,-1,-1,
                              -1, 1, 1, 1,-1,
                              -1,-1,-1,-1,-1],-3)

    model.set_weights( 1, 15,[1,-2, 0, 0, 0,
                              2,-2, 0, 0, 0,
                              1,-2, 0, 0, 0], -3)

    model.set_weights( 1, 16,[-2,1,-2, 0, 0,
                              -2,2,-2, 0, 0,
                              -2,1,-2, 0, 0], -3)

    model.set_weights( 1, 17,[0,-2,1,-2, 0,
                              0,-2,2,-2, 0,
                              0,-2,1,-2, 0], -3)

    model.set_weights( 1, 18,[0, 0,-2,1,-2,
                              0, 0,-2,2,-2,
                              0, 0,-2,1,-2], -3)

    model.set_weights( 1, 19,[0, 0, 0,-2,1,
                              0, 0, 0,-2,2,
                              0, 0, 0,-2,1], -3)

    model.set_weights( 2, 0 ,[1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1], 0)

    model.set_weights( 2, 1 ,[1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1], -1)

    model.set_weights( 2, 2 ,[1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1], -2)

    model.set_weights( 2, 3 ,[1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1], -3)

    model.set_weights( 2, 4 ,[1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1,
                              1, 1, 1, 1, 1], -4)

    return model


### notes

#from dash_data import DashData
#import numpy as np
#features = DashData(3,5).pics
#model = DashCountModel(3, 5, [20], ['hardlimit'], 1, 'identity')

#test_data = np.array([[[0,0,0,0,1],
#                       [1,1,1,0,1],
#                       [0,0,0,0,1]]])
#
#
#labels = model.predict([test_data])

#model.plot_layer(1, 20, 3, 5)

#model.compile(optimizer='adam',
#              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
#              metrics=['accuracy'])
#def train(model, features, labels):
#    model.fit(features, labels, epochs=1)


# - learn
# - show learning

