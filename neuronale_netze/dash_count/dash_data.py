import numpy as np
import matplotlib.pyplot as plt


def plot(pic):
    plt.figure()
    plt.imshow(pic)
    plt.colorbar()
    plt.grid(False)
    plt.show()

class DashData:
    def __init__(self, row_count, col_count):
        self.pics = []
        pixel_count = row_count*col_count
        all_pics_count = pow(2, pixel_count)
        for i in range(all_pics_count):
            pic = np.zeros((row_count, col_count),np.int8)
            for j in range(pixel_count):
                if i % 2 == 1:
                    pic[j//col_count][j%col_count]=1
                i=i>>1
            self.pics = self.pics + [pic]
        self.pics = np.array(self.pics)
        
    def __str__(self):
        return str(self.pics)


