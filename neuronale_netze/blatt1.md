---
title: "Übungsblatt 1"
author: [Klasse 9]
date: 11.02.2021
lang: de
header-center: "Neuronale Netze" 
---

## I Exklusives Oder

Rechnen Sie nach, dass das folgende neuronale Netz ein *exklusives Oder* darstellt:

\Begin{multicols}{2}

![](perceptron-xor.png){height=200px}

- An den Verbindungslinien stehen wie gehabt die Gewichte mit denen die Eingänge in den Neuronen gewichtet summiert werden.
- In den Kreisen (abgesehen von den Input-Neuronen) stehen hier die Schwellwerte. Ist die gewichtete Summe der Eingänge kleiner als der Schwellwert, so gibt das Neuron $0$ aus. Wird der Schwellwert überschritten, wird eine $1$ ausgegeben.)

\End{multicols}

Zur Erinnerung: ein exklusives Oder bekommt als "Eingabe" zwei Werte, die jeweils $0$ oder $1$ sein können. Sind die beiden Eingabewerte gleich (d.h. beide $0$ oder beide $1$), so ist das exklusive Oder $0$. Ist ein Eingabewert $0$ und der andere $1$ so ist deren exklusives Oder $1$.

(Sie müssen also einmal vorne eine $0$ und eine $1$ reinstecken und überprüfen, ob hinten eine $1$ rauskommt, einmal zwei $1$er vorne einspeisen und nachrechnen, dass hinten eine $0$ herauskommt usw.)

## II Striche zählen

Es seien Bilder mit (der Einfachheit halber) nur $5$ mal $3$ Bildpunkten (sogennante *Pixel*) gegeben. Jedes Pixel ist entweder weiß (beim Rechnen dargestellt durch eine $0$) oder schwarz (entspräche einer $1$).

In diesen Bildern sollen Striche gezählt werden. Die Striche sollen aus $2$ oder $3$ senkrecht oder waagerecht nebeneinanderliegen Pixeln bestehen. Ein paar Beispiele:

![](striche_in_5x3.png){}

Das neuronale Netz bekommt also $15$ Input-Neuronen. (Für jedes Pixel ein Neuron. Die ersten $5$ Neuronen von oben im Input-Layer, könnten die erste Reihe Pixel darstellen bzw. "wahrnehmen" usw.) Jedes Eingabe-Neuron bekommt entweder eine $1$ oder eine $0$ , je nachdem, ob der jeweilige Bildpunkt schwarz oder weiß ist.

Weiter sollte das Netz $5$ Neuronen in der Ausgabe-Schicht haben. Optimalerweise gibt

- das oberste Neuron eine $1$ und die restlichen eine $0$ aus, wenn im Eingabebild kein Strich zu sehen ist;
- das zweite Neuron eine $1$ und die restlichen eine $0$ aus, wenn im Eingabebild $1$ Strich zu sehen ist;
- das dritte Neuron eine $1$ und die restlichen eine $0$ aus, wenn im Eingabebild $2$ Striche zu sehen sind, usw;

Wieviele hidden Layer mit wievielen Neuronen Sie einbauen ist Ihnen überlassen (ein oder zwei "verborgene Schichten" sollten aber reichen).

Tipp: Sie könnten sich die Gewichte auch als $5$ mal $3$ Block vorstellen und mit Reihen und Spalten von $1$ern und $-1$ spielen. Als Inspiration könnte noch <https://youtu.be/aircAruvnKk?t=519> dienen.

