---
title: "Tensorflow zählt Striche"
author: [Klasse 9]
date: 12.04.2021
keywords: [Volumen, Pyramide, Kegel]
lang: de
header-center: "Neuronale Netze" 
---


# How to count dashes

## Vorbereitungen um lokal zu arbeiten

### Ordentliche Arbeitsumgebung

Wenn Sie das ausprobieren wollen, aktivieren Sie am besten Windows-Feature WSL (Windows Subsystem for Linux) und installieren die Ubuntu App aus dem Microsoft App-Store. (Noch einfacher wäre es, Sie hätten einen Linux-Rechner...)

### Tensorflow installieren

Richten Sie sich ein "virtual environment" mit Tensorflow ein. Sie finden eine Anleitung dazu hier: [www.tensorflow.org/install/pip](https://www.tensorflow.org/install/pip)

## Eine Online-IDE verwenden

Es kann einfacher eine Online-IDE zu verwenden, als python und alle benötigten Abhängigkeiten lokal zu installieren. Es bietet sich zum Beispiel Replit an (hat alles was wir brauchen). Um Replit zu verwenden, legen Sie sich auf [replit.com](https://replit.com) einen Account an und laden Sie die beiliegenden python-Dateien hoch.

## Lets do something

(Bevor wir starten müssen Sie das *virtuelle environment* wie in der vorherigen Anleitung beschrieben.)

### Unsere Daten: 3x5 Schwarz-Weiß Bildchen

Die Datei `dash_data.py` enthält Code, um alle möglichen Schwarz-Weiß Bilder beliebiger Größe zu generieren.
Um diesen Code (und damit generierten Daten) zu verwenden, müssen wir python bescheid geben, dass wir diese Datei *importieren* wollen:

```
>>> import dash_data as dd
```

Nun können wir auf den Code in der Datei zugreifen indem wir `dd` verwenden. Wir erzeugen als erstes alle möglichen 3x5 Bildchen:

```
>>> features = dd.DashData(3,5).pics
```

Den Code aus `dash_data.py` haben unter dem Namen `dd` importiert. Auf den Inhalt der Datei können wir jetzt zugreifen, indem wir an `dd` einen Punkt anhängen und danach den Namen von irgendwas aus der Datei `dash_data.py` schreiben.
In der Datei `dash_data.py` gibt es also ein Ding (eine sogenannte *Klasse*) namens `DashData`, das weiß wie man alle Bilder, der in runden Klammern angegebenen Größe, erzeugt. In der Klasse `DashData` gibt es dann wiederum ein sogenanntes *Attribut* namens `pics`, das dann die eigentlichen Daten enthält. Auf die Dinge in einer Klasse greifen wir wiederum zu, indem wir einen Punkt, gefolgt von dem Namen des Dings in der Klasse, anhängen.
Um nicht immer den ganzen Kladeradatsch mit den ganzen Punkten schreiben zu müssen (u.a. deswegen) speichern wir die Bilder in einer sogenannten *Variable* namens `features`.

Die Variable `features` enthält jetzt also (sozusagen) $32768$ Bilder. Man wählt eines dieser Bilder aus indem man eine Zahl zwischen $0$ und $32767$ in eckigen Klammern anhängt:

```
>>> features[30212]
array([[0, 0, 1, 0, 0],
       [0, 0, 0, 0, 1],
       [1, 0, 1, 1, 1]], dtype=int8)
```

Neben der Klasse `DashData`, die die Bild-Daten erzeugt, gibt es in der Datei `dash_data.py` auch noch eine sogenannte *Funktion* namens `plot`, die die Bild-Daten malen kann. `plot` bekommt in runden Klammern das Bild übergeben, das angezeigt werden soll:

```
>>> dd.plot(features[12345])
```

![](pic3x5_12345.png){height=300px}


### Manuelles Netz zählt Striche

In `dash_count_20_neurons.py` wird manuell ein neuronales Netz definiert von dem wir wissen, dass es die Striche richtig zählt. Wir importieren die Datei und erzeugen eine *Instanz* von dem neuronalen Netz, idem wir die `create` Funktion aufrufen:

```
>>> import dash_count_20_neurons as dc20
>>> model_manual = dc20.create()
```

Das Netz hat auch eine *Methode* (= eine Funktion in einer Klasse) `plot_layer`, die eine Schicht von Neuronen malen kann:

```
>>> model_manual.plot_layer(1, 20, 3, 5)
```

![](manual_model_20_neurons_layer1.png){height=550px}

Wir können dieses Netz, von dem wir schon wissen, dass es wie gewollt zählt, verwenden, um zu jedem Bild die Anzahl der Striche darin zu speichern, indem wir dem Netz die Bilder in runden Klammern übergeben:

```
>>> labels = model_manual(features)
```

In `labels` sind nun für alle Bilder die Werte der Output-Neuronen-Schicht (bestehend aus $5$ Neuronen) gespeichert:

```
>>> labels[12345]
<tf.Tensor: shape=(5,), dtype=float32, numpy=array([0., 0., 0., 1., 0.], dtype=float32)>
```

Zur Erinnerung: gibt das erste Neuron eine $1$ aus, hat das Netz keinen Strich entdeckt, gibt das zweite Neuron eine $1$ aus, meint das Netz einen Strich gezählt zu haben usw.
In diesem Fall zeigt das vierte Output-Neuron eine $1$, d.h. das Netz hat korrekterweise $3$ Striche gezählt.
Mit den Bilddaten und der Anzahl der jeweils vorhandenen Striche in `labels`, können wir jetzt ein Netz trainieren und sehen, ob es unserem manuell erzeugten Netz ähnelt.


### Ein Netz trainieren

Die eigentliche Neuronale-Netze-Funktionalität bringt eine Bibliothek (ein Haufen Dateien mit viel Code) namens *tensorflow* mit. In der Datei `dash_count_model.py` haben wir noch eine Schicht um tensorflow herumprogrammiert um, das Anlegen eines Netzes für unseren Zweck des Strichezählens noch etwas einfacher zu machen und um die Gewichte der Layer visualisieren zu können. Wir laden tensorflow und die `DashCountModel` und legen ein Netz an:

```
>>> from dash_count_model import DashCountModel
>>> import tensorflow as tf
>>> model = DashCountModel( \
               3, 5, [20,10], ['sigmoid', 'sigmoid'], 5, 'softmax')
```

Die ersten zwei Argumente geben Zeilen und Spalten der Bilder an. Danach folgt in den eckigen Klammern für jeden hidden Layer die Anzahl seiner Neuronen. Dann die Aktivierungsfunktionen für die hidden Layer und schließlich die Anzahl der Neuronen und die Aktivierungsfunktion des Output-Layers.

Um das Netz zu trainieren braucht es noch eine Zeile Magie, und dann kanns losgehen...

```
>>> model.compile( \
       optimizer='adam', \
       loss=tf.keras.losses.CategoricalCrossentropy(from_logits=True), \
       metrics=['accuracy'])
>>> model.fit(features, labels.numpy(), epochs=20)
```

Nach ein bisschen Training können wir uns jetzt ansehen, wie die Gewichte des ersten Layers aussehen:

```
>>> model.plot_layer(1, 20, 3, 5)
```

