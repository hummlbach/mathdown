---
title: "Übungsblatt 2"
author: [Klasse 9]
date: 05.03.2021
lang: de
header-center: "Neuronale Netze" 
---

## Nochmal Strichelchen

Gegeben seien die folgenden $3$x$5$-Bildchen:

![](5_striche_in_5x3.png){}

Rechnen Sie nach, welche der folgenden Neuronen durch welches Bild aktiviert wird. (Die Aktivierungsfunktion sei dabei die Hardlimit-Funktion.)

1. Das kennen Sie schon:

   ![](neuron_horizontaler_strich.png){}

2. Ein weiteres Neuron:

   ![](neuron_langer_strich.png){}

3. Noch ein Neuron:

   ![](neuron_kurzer_strich.png){}

4. Und ein letztes:

   ![](neuron_strich.png){}


Zur Erinnerung, die Hardlimit-Funktion ist wie folgt definiert ($x$ ist dabei die gewichtete Summe der Eingänge):

$$
hlim(x) = \left\{
\begin{array}{lcr}
0 & \text{ falls } & x < 0 \\
1 & \text{ falls } & x \ge 0
\end{array}
\right.
$$

Das bedeutet, solange die gewichtete Summe den Schwellwert (Zahl im Neuron mit umgedrehtem Vorzeichen) nicht erreicht, "feuert" das Neuron nicht.
